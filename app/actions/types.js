export const LOGIN_SERVICE_LOADING = 'login_service_loading';

export const INTRO_FINISHED = 'intro_finished';
export const LANG_SWITCH = 'language_switched';

export const LOGIN_SERVICE_ERROR = 'login_service_error';
export const LOGIN_SUCCESS = 'login_success';
export const LOGOUT_SUCCESS = 'logout_success';

export const OTP_VERIFIED = 'otp_verified';
export const OTP_VERIFY_SERVICE_ERROR = 'verify_otp_error';

export const SERVICE_LOADING = 'service_loading';
export const SERVICE_ERROR = 'service_error';
export const GET_ORDER_SUCCESS = 'get_order_success';
export const RESET_ERROR = 'reset_error';

export const SIGNUP_SERVICE_ERROR = 'signup_service_error';
export const SIGNUP_SUCCESS = 'signup_success';
export const SIGNOUT_SUCCESS = 'signout_success';

export const HOMEPAGE_SUCCESS = 'homepage_success';
export const HOMEPAGE_SERVICE_ERROR = 'homepage_service_error';
export const HOMEPAGE_SERVICE_LOADING = 'homepage_service_loading';
export const ADD_TO_CART = 'add_to_cart';

export const PRODUCT_LIST_SUCCESS = 'product_list_success';
export const PRODUCT_SERVICE_ERROR_RESET = 'product_error_reset';
export const PRODUCT_SERVICE_ERROR = 'product_service_error';
export const PRODUCT_SERVICE_LOADING = 'product_service_loading';

export const DETAIL_LIST_SUCCESS = 'detail_success';
export const DETAIL_SERVICE_ERROR_RESET = 'detail_error_reset';
export const DETAIL_SERVICE_ERROR = 'detail_service_error';
export const DETAIL_SERVICE_LOADING = 'detail_service_loading';

export const WL_SERVICE_SUCCESS = 'wl_success';
export const WL_SERVICE_ERROR = 'wl_error';
export const WL_SERVICE_LOADING = 'wl_loading';

export const MAP_SUCCESS = 'map_success';
export const MAP_ERROR = 'map_error';
export const MAP_LOADING = 'map_loading';
export const MAP_DATA = 'map_data';

export const FAV_SERVICE_LOADING = 'fav_service_loading';
export const FAV_SERVICE_ERROR = 'fav_service_error';
export const FAV_SERVICE_ERROR_RESET = 'fav_service_error_reset';
export const FAV_SERVICE_SUCCESS = 'fav_service_success';

export const FILTER_CAT_SUCCESS = 'filter_cat_success';
export const FILTER_BRAND_SUCCESS = 'filter_brand_success';

export const TERMS_SUCCESS = 'terms_success';
export const TERMS_SUCCESS_ERROR = 'terms_success_error';
export const TERMS_SUCCESS_LOADING = 'terms_success_error_loading';


export const USER_DETAILS_SUCCESS = 'user_details_success';
export const USER_DETAILS_ERROR = 'user_details_error';
export const USER_DETAILS_LOADING = 'user_details_loading';

export const REGION_LIST_SUCCESS = 'region_list_success';

export const UPDATE_USER_SUCCESS = 'update_user_success';
export const UPDATE_USER_ERROR = 'update_user_error';
export const UPDATE_USER_LOADING = 'update_user_loading';


export const ADDRESS_DETAILS_LOADING = 'address_details_loading';
export const ADDRESS_DETAILS_ERROR = 'address_details_error';
export const ADDRESS_DETAILS_SUCCESS = 'address_details_success';

export const SAVE_ADDRESS_DETAILS_SUCCESS = 'save_address_details_success';
export const SAVE_ADDRESS_DETAILS_ERROR = 'save_address_details_error';
export const SAVE_ADDRESS_DETAILS_LOADING = 'save_address_details_loading';

export const EDIT_ADDRESS_DETAILS_SUCCESS = 'edit_address_details_success';
export const EDIT_ADDRESS_DETAILS_ERROR = 'edit_address_details_error';
export const EDIT_ADDRESS_DETAILS_LOADING = 'edit_address_details_loading';

export const DELETE_ADDRESS_DETAILS_SUCCESS = 'delete_address_details_success';
export const DELETE_ADDRESS_DETAILS_ERROR = 'delete_address_details_error';
export const DELETE_ADDRESS_DETAILS_LOADING = 'delete_address_details_loading';

export const SET_DEFAULT_ADDRESS_SUCCESS = 'set_default_address_success';
export const SET_DEFAULT_ADDRESS_ERROR = 'set_default_address_error';
export const SET_DEFAULT_ADDRESS_LOADING = 'set_default_address_loading';





