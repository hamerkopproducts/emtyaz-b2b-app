import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import { defaultLoginState } from "../reducers/loginReducer";
import axios from 'react-native-axios';
import globals from "../lib/globals"

export const apiServiceActionLoading = () => ({
	type: ActionTypes.LOGIN_SERVICE_LOADING
});

export const apiServiceActionError = (error) => ({
	type: ActionTypes.LOGIN_SERVICE_ERROR,
	error: error
});

export function loginServiceActionSuccess(responseData, phone) {
	return {
		type: ActionTypes.LOGIN_SUCCESS,
		responseData: responseData,
		phone: phone
	};
}

export function resetError() {
	return {
		type: ActionTypes.RESET_ERROR
	};
}

export function otpVerifyServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.OTP_VERIFIED,
		responseData: responseData
	};
}

export const verifyOtpApiServiceActionError = (error) => ({
	type: ActionTypes.OTP_VERIFY_SERVICE_ERROR,
	error: error
});

export const introFinished = () => ({
	type: ActionTypes.INTRO_FINISHED
});

export const saveSelectedLanguage = (languageSelected) => ({
	type: ActionTypes.LANG_SWITCH,
	languageSelected: languageSelected
});

// Map data save
export const map_loading = () => ({
	type: ActionTypes.MAP_LOADING
});

export const map_data = (data) => ({
	type: ActionTypes.MAP_DATA,
	data: data
});

export const map_error = (error) => ({
	type: ActionTypes.MAP_ERROR,
	error: error
});

export function map_success(responseData, phone) {
	return {
		type: ActionTypes.MAP_SUCCESS,
		responseData: responseData
	};
}
//Map end

// Map save api
export function saveMapLocation(params, token) {
	return async dispatch => {
		dispatch(map_loading());
		await axios.post(apiHelper.getMapAPI(), params, apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(map_data(params));
				dispatch(map_success(response.data));
			}).catch(error => {
				dispatch(map_error(error.response.data.error));
			});
	}
}

export function doLogin(params) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.getLoginAPI(), params, apiHelper.getLoginAPIHeader())
			.then(response => {
				dispatch(loginServiceActionSuccess(response.data, params.phone));
			}).catch(error => {
				console.log(' error response::' + JSON.stringify(error))
				dispatch(apiServiceActionError(error.response.data.error));
			});
	}
}

export function doVerifyOtp(params) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.getOtpVerifyAPI(), params)
			.then(response => {
				dispatch(otpVerifyServiceActionSuccess(response.data));
			}).catch(error => {
				dispatch(verifyOtpApiServiceActionError(error.response.data.error));
			});
	}
}

export function resetLoginErrorMeesage() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.RESET_LOGIN_ERROR_MESSAGE
		});
	};
}

export function doLogout(resetNavigation: Function) {
	return dispatch => {
		dispatch({
			type: ActionTypes.LOGOUT_SUCCESS,
			...defaultLoginState,
			resetNavigation
		});
	};
}

export function enableLogin() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.ENABLE_LOGIN_SUCCESS
		});
	};
}
