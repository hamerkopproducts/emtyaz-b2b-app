import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import { defaultSignUpState } from "../reducers/signUpReducer";
import axios from 'react-native-axios';
import globals from "../lib/globals"

export const apiServiceActionLoading = () => ({
	type: ActionTypes.LOGIN_SERVICE_LOADING
});

export const apiServiceActionError = (error) => ({
	type: ActionTypes.SIGNUP_SERVICE_ERROR,
	error: error
});

export function signUpServiceActionSuccess(responseData,phone_no) {
	return {
		type: ActionTypes.SIGNUP_SUCCESS,
		responseData: responseData,
		phone: phone_no
	};
}

export function resetError() {
	return {
		type: ActionTypes.RESET_ERROR
	};
}


export function doSignUp(params) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.getSignUpAPI(), params, apiHelper.getLoginAPIHeader())
			.then(response => {
				dispatch(signUpServiceActionSuccess(response.data,params.phone_no));
			}).catch(error => {
				console.log(' error response::' + JSON.stringify(error))
				dispatch(apiServiceActionError(error.response.data.error));
			});
	}
}

export function doVerifyOtp(params) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.getOtpVerifyAPI(), params)
			.then(response => {
				dispatch(otpVerifyServiceActionSuccess(response.data));
			}).catch(error => {
				dispatch(verifyOtpApiServiceActionError(error.response.data.error));
			});
	}
}
export function doLogoutFromSignup(resetNavigation: Function) {
	return dispatch => {
		dispatch({
			type: ActionTypes.SIGNOUT_SUCCESS,
			...defaultSignUpState,
			resetNavigation
		});
	};
}
export function resetLoginErrorMeesage() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.RESET_LOGIN_ERROR_MESSAGE
		});
	};
}

export function enableLogin() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.ENABLE_LOGIN_SUCCESS
		});
	};
}
