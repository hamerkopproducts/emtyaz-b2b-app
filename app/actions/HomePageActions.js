import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper";
import axios from 'react-native-axios';

// Home page.
export const apiServiceActionLoading = () => ({
	type: ActionTypes.HOMEPAGE_SERVICE_LOADING
});
export const apiServiceActionError = (error) => ({
	type: ActionTypes.HOMEPAGE_SERVICE_ERROR,
	error: error
});
export function homePageServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.HOMEPAGE_SUCCESS,
		responseData: responseData,
	};
}

// Products.
export const product_apiServiceActionLoading = () => ({
	type: ActionTypes.PRODUCT_SERVICE_LOADING
});
export const product_apiServiceActionError = (error) => ({
	type: ActionTypes.PRODUCT_SERVICE_ERROR,
	error: error
});
export const product_apiServiceActionErrorReset = () => ({
	type: ActionTypes.PRODUCT_SERVICE_ERROR_RESET
});
export function productsListServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.PRODUCT_LIST_SUCCESS,
		responseData: responseData,
	};
}

// fav.
export const fav_service_loading = () => ({
	type: ActionTypes.FAV_SERVICE_LOADING
});
export const fav_service_error = (error) => ({
	type: ActionTypes.FAV_SERVICE_ERROR,
	error: error
});
export const fav_service_error_reset = () => ({
	type: ActionTypes.FAV_SERVICE_ERROR_RESET
});
export function fav_service_success(responseData) {
	return {
		type: ActionTypes.FAV_SERVICE_SUCCESS,
		responseData: responseData,
	};
}
export function fav_service_reset_success() {
	return {
		type: ActionTypes.FAV_SERVICE_SUCCESS,
		responseData: {},
	};
}

// details
export const detail_apiServiceActionLoading = () => ({
	type: ActionTypes.DETAIL_SERVICE_LOADING
});
export const detail_apiServiceActionError = (error) => ({
	type: ActionTypes.DETAIL_SERVICE_ERROR,
	error: error
});
export const detail_apiServiceActionErrorReset = () => ({
	type: ActionTypes.DETAIL_SERVICE_ERROR_RESET
});
export function detail_listServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.DETAIL_LIST_SUCCESS,
		responseData: responseData,
	};
}

// Filter cat
export function filter_service_success(responseData) {
	return {
		type: ActionTypes.FILTER_CAT_SUCCESS,
		responseData: responseData,
	};
}

// Filter brands
export function filter_brand_success(responseData) {
	return {
		type: ActionTypes.FILTER_BRAND_SUCCESS,
		responseData: responseData,
	};
}

export function addItemsToCart(data) {
	return {
		type: ActionTypes.ADD_TO_CART,
		data: data,
	};
}

export function resetError() {
	return {
		type: ActionTypes.RESET_ERROR
	};
}

export const introFinished = () => ({
	type: ActionTypes.INTRO_FINISHED
});


// WISHLIST DATA
export const wl_service_loading = () => ({
	type: ActionTypes.WL_SERVICE_LOADING
});
export const wl_service_error = (error) => ({
	type: ActionTypes.WL_SERVICE_ERROR,
	error: error
});
export function wl_service_success(responseData) {
	return {
		type: ActionTypes.WL_SERVICE_SUCCESS,
		responseData: responseData,
	};
}

export function wishListData(token) {
	return async dispatch => {
		dispatch(wl_service_loading());
		await axios.get(apiHelper.getMyWishListApi(), apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(wl_service_success(response.data));
			}).catch(error => {
				dispatch(wl_service_error(error.response.data.error));
			});
	}
}

export function homePage(params,token) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.get(apiHelper.getHomePageAPI(params), apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(homePageServiceActionSuccess(response.data));
			}).catch(error => {
				dispatch(apiServiceActionError(error.response.data.error));
			});
	}
}

export function productsListAll(params, token, page) {
	return async dispatch => {
		dispatch(product_apiServiceActionLoading());
		await axios.post(apiHelper.getProductsAPI() + `?page=${page}`, params, apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(productsListServiceActionSuccess(response.data));
			}).catch(error => {
				console.log(' error response::' + JSON.stringify(error))
				dispatch(product_apiServiceActionError(error.response.data.error));
			});
	}
}

export function productDetails(slug, token) {
	return async dispatch => {
		dispatch(detail_apiServiceActionLoading());
		await axios.get(apiHelper.getProductDetailApi() + `${slug}`, apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(detail_listServiceActionSuccess(response.data));
			}).catch(error => {
				dispatch(detail_apiServiceActionError(error.response.data.error));
			});
	}
}

export function addRemoveFav(params, token) {
	return async dispatch => {
		dispatch(fav_service_loading());
		await axios.post(apiHelper.getAddFavAPI(), params, apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(fav_service_success(response.data));
			}).catch(error => {
				dispatch(fav_service_error(error.response.data.error));
			});
	}
}

export function filter_cat_data(token) {
	return async dispatch => {
		await axios.get(apiHelper.getFilterCatAPI(), apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(filter_service_success(response.data));
			}).catch(error => {
				console.log(error.response.data.error);
			});
	}
}

export function filter_brand_data(token) {
	return async dispatch => {
		await axios.get(apiHelper.getFilterBrandAPI(), apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(filter_brand_success(response.data));
			}).catch(error => {
				console.log(error.response.data.error);
			});
	}
}