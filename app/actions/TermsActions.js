import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import { defaultLoginState } from "../reducers/termsReducer";
import axios from 'react-native-axios';
import globals from "../lib/globals"

export const apiServiceActionLoading = () => ({
	type: ActionTypes.TERMS_SUCCESS_LOADING
});

export const apiServiceActionError = (error) => ({
	type: ActionTypes.TERMS_SUCCESS_ERROR,
	error: error
});

export function termsServiceActionSuccess(responseData, phone) {
	return {
		type: ActionTypes.TERMS_SUCCESS,
		responseData: responseData,
	};
}

export function resetError() {
	return {
		type: ActionTypes.RESET_ERROR
	};
}

export const introFinished = () => ({
	type: ActionTypes.INTRO_FINISHED
});

export const saveSelectedLanguage = (languageSelected) => ({
	type: ActionTypes.LANG_SWITCH,
	languageSelected: languageSelected
});

export function termsAndConditions() {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.get(apiHelper.getTermsAndConditionsAPI())
			.then(response => {
				dispatch(termsServiceActionSuccess(response.data));
			}).catch(error => {
				console.log(' error response::' + JSON.stringify(error))
				dispatch(apiServiceActionError(error.response.data.error));
			});
	}
}

export function enableLogin() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.ENABLE_LOGIN_SUCCESS
		});
	};
}
