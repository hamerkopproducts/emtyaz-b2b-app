import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import { defaultLoginState } from "../reducers/loginReducer";
import axios from 'react-native-axios';
import globals from "../lib/globals"

export const apiServiceActionLoading = () => ({
	type: ActionTypes.ADDRESS_DETAILS_LOADING
});
export const editaddressapiServiceActionLoading = () => ({
	type: ActionTypes.EDIT_ADDRESS_DETAILS_LOADING
});

export const apiServiceActionError = (error) => ({
	type: ActionTypes.ADDRESS_DETAILS_ERROR,
	error: error
});
export const apiSaveAddressServiceActionError = (error) => ({
	type: ActionTypes.SAVE_ADDRESS_DETAILS_ERROR,
	error: error
});

export function GetAddressDetailsSuccess(responseData) {
	return {
		type: ActionTypes.ADDRESS_DETAILS_SUCCESS,
		responseData: responseData
	};
}
export function GetEditAddressDetailsSuccess(responseData) {
	return {
		type: ActionTypes.EDIT_ADDRESS_DETAILS_SUCCESS,
		responseData: responseData
	};
}
export function SetEditAddressDetailsSuccess(responseData) {
	return {
		type: ActionTypes.SAVE_ADDRESS_DETAILS_SUCCESS,
		responseData: responseData
	};
}
export function SetAddressDetailsSuccess(responseData) {
	return {
		type: ActionTypes.SAVE_ADDRESS_DETAILS_SUCCESS,
		responseData: responseData
	};
}
export const SetDeleteAddressDetailsSuccess = (responseData) => ({
	type: ActionTypes.DELETE_ADDRESS_DETAILS_SUCCESS,
	responseData: responseData
});
export const SetDefaultAddressSuccess = (responseData) => ({
	type: ActionTypes.SET_DEFAULT_ADDRESS_SUCCESS,
	responseData: responseData
});
export const apiDefaultAddressActionError = (responseData) => ({
	type: ActionTypes.SET_DEFAULT_ADDRESS_ERROR,
	responseData: responseData
});
export function resetError() {
	return {
		type: ActionTypes.RESET_ERROR
	};
}

export const saveSelectedLanguage = (languageSelected) => ({
	type: ActionTypes.LANG_SWITCH,
	languageSelected: languageSelected
});


export function doAddressDetailsFetch(token) {
	return async dispatch => {
        dispatch(apiServiceActionLoading());
		await axios.get(apiHelper.getAddressDetailsApi(),apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(GetAddressDetailsSuccess(response.data));
			}).catch(error => {
				console.log(' error response::' + JSON.stringify(error))
				dispatch(apiServiceActionError(error.response.data.error));
			});
	}
}
export function doAddressDetailSave(params,token) {
	return async dispatch => {
        dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.setAddressDetailsApi(),params,apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(SetAddressDetailsSuccess(response.data));
			}).catch(error => {
				console.log(' error response::' + JSON.stringify(error))
				dispatch(apiSaveAddressServiceActionError(error.response.data.error));
			});
	}
}
export function doEditAddressDetailsFetch(id,token) {
	return async dispatch => {
        dispatch(editaddressapiServiceActionLoading());
		await axios.get(apiHelper.getEditAddressDetailsApi(id),apiHelper.getAPIHeader(token))
			.then(response => {
				console.log(' Edit user data response::',response.data.data[0].region.lang[0].name )
				dispatch(GetEditAddressDetailsSuccess(response.data.data));
			}).catch(error => {
				console.log('EDIT ADDRESS FETCH error response::' + JSON.stringify(error))
				dispatch(apiFetchEditAddressServiceActionError(error.response.data.error));
			});
	}
}

export function doEditedAddressDetailSave(params,token) {
	return async dispatch => {
        dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.setEditedAddressDetailsApi(),params,apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(SetEditAddressDetailsSuccess(response.data));
			}).catch(error => {
				console.log('EDIT ADDRESS SAVE error response::' + JSON.stringify(error))
				dispatch(apiSaveAddressServiceActionError(error.response.data.error));
			});
	}
}
export function doDeleteAddress(id,token) {
	
	return async dispatch => {
		console.debug('data from edit user action',token
		 );
        dispatch(apiServiceActionLoading());
		await axios.delete(apiHelper.deleteAddressDetailsApi(id),apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(SetDeleteAddressDetailsSuccess(response.data));
			}).catch(error => {
				console.log('delete ADDRESS error response::' + JSON.stringify(error))
				dispatch(apiSaveAddressServiceActionError(error.response.data.error));
			});
	}
}
export function setAddressAsDefault(params,token) {
	
	return async dispatch => {
		console.debug('setAddressAsDefault from action',params
		 );
        dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.setAddressAsDefaultApi(),params,apiHelper.getAPIHeader(token))
			.then(response => {
				console.debug('setAddressAsDefault from action',response.data);
				dispatch(SetDefaultAddressSuccess(response.data));
			}).catch(error => {
				console.log('delete ADDRESS error response::' + JSON.stringify(error))
				dispatch(apiDefaultAddressActionError(error.response.data.error));
			});
	}
}
