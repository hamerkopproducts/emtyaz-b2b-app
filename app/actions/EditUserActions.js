import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import { defaultLoginState } from "../reducers/loginReducer";
import axios from 'react-native-axios';
import globals from "../lib/globals"

export const apiServiceActionLoading = () => ({
	type: ActionTypes.USER_DETAILS_LOADING
});

export const apiServiceActionError = (error) => ({
	type: ActionTypes.USER_DETAILS_ERROR,
	error: error
});

export function GetUserDetailsSuccess(responseData) {
	return {
		type: ActionTypes.USER_DETAILS_SUCCESS,
		responseData: responseData
	};
}
export function GetRegionListSuccess(responseData) {
	return {
		type: ActionTypes.REGION_LIST_SUCCESS,
		responseData: responseData
	};
}
export function UpdateUserDetailsSuccess(responseData) {
	return {
		type: ActionTypes.UPDATE_USER_SUCCESS,
		responseData: responseData
	};
}

export function resetError() {
	return {
		type: ActionTypes.RESET_ERROR
	};
}

export const saveSelectedLanguage = (languageSelected) => ({
	type: ActionTypes.LANG_SWITCH,
	languageSelected: languageSelected
});

export function doUserDetailsEdit(token) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.get(apiHelper.getUserEditDetailsAPI(),apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(GetUserDetailsSuccess(response.data));
			}).catch(error => {
				console.log(' error response::' + JSON.stringify(error))
				dispatch(apiServiceActionError(error.response.data.error));
			});
	}
}
export function doRegionDropdown(lang,token) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.get(apiHelper.getRegionDropDownListAPI(lang),apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(GetRegionListSuccess(response.data));
			}).catch(error => {
				console.log(' error response::' + JSON.stringify(error))
				dispatch(apiServiceActionError(error.response.data.error));
			});
	}
}
export function doUpdateUserDetails(params,token) {
	return async dispatch => {
		console.debug('USER DETAILS LIST', params);
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.setupdateUserDetailsApi(),params,apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(UpdateUserDetailsSuccess(response.data));
			}).catch(error => {
				console.log(' error response::' + JSON.stringify(error))
				dispatch(apiServiceActionError(error.response.data.error));
			});
	}
}


