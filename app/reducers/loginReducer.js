import { act } from 'react-test-renderer';
import * as ActionTypes from '../actions/types'

const defaultLoginState = {
    userData: {},
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    success: undefined,
    isIntroFinished: false,
    languageSelected: null,
    otpAPIReponse: {},
    phone: '',
    isLoggedOut: false,
    map_data: {},
    map_local_data: {}
};

export default function loginReducer(state = defaultLoginState, action) {
    switch (action.type) {
        case ActionTypes.LOGIN_SERVICE_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.INTRO_FINISHED:
            return Object.assign({}, state, {
                isIntroFinished: true,
            });
        case ActionTypes.LANG_SWITCH:
            return Object.assign({}, state, {
                languageSelected: action.languageSelected,
            });

        case ActionTypes.LOGIN_SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.LOGIN_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                phone: action.phone,
                isLoggedOut: false,
                otpAPIReponse: action.responseData
            });
        case ActionTypes.RESET_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error:null,
            });

        // Map data
        case ActionTypes.MAP_ERROR:
            return Object.assign({}, state, {
                map_isLoading: false,
                map_error: action.error
            });
        case ActionTypes.MAP_LOADING:
            return Object.assign({}, state, {
                map_isLoading: true
            });
        case ActionTypes.MAP_SUCCESS:
            return Object.assign({}, state, {
                map_isLoading: false,
                map_data: action.responseData,
            });
        case ActionTypes.MAP_DATA:
            return Object.assign({}, state, {
                map_local_data: action.data,
            });
        // Map data end
            
        case ActionTypes.OTP_VERIFY_SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.OTP_VERIFIED:
            return Object.assign({}, state, {
                isLoading: false,
                userData: action.responseData,
                isLogged: true
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                userData: {},
                isLogged: false,
                isLoading: false,
                error: undefined,
                success: undefined,
                resetNavigation: action.resetNavigation,
                isSessionExpired: true,
                isLoggedOut: true
            };
        default:
            return state;
    }
}
