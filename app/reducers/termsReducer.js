import { act } from 'react-test-renderer';
import * as ActionTypes from '../actions/types'

const defaultTermsState = {
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    success: undefined,
    isIntroFinished: false,
    languageSelected: null,
    termsAPIReponse: {},
    isLoggedOut: false
};

export default function termsReducer(state = defaultTermsState, action) {
    switch (action.type) {
        case ActionTypes.TERMS_SUCCESS_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.LANG_SWITCH:
            return Object.assign({}, state, {
                languageSelected: action.languageSelected,
            });

        case ActionTypes.TERMS_SUCCESS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.TERMS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                phone: action.phone,
                isLoggedOut: false,
                termsAPIReponse: action.responseData
            });
        case ActionTypes.RESET_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error:null,
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                isLogged: false,
                resetNavigation: undefined,
                isLoading: false,
                error: undefined,
                success: undefined,
                isIntroFinished: false,
                languageSelected: null,
                termsAPIReponse: {},
                isLoggedOut: false
            };
        default:
            return state;
    }
}
