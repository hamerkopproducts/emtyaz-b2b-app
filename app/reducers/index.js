import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import orderReducer from './orderReducer';
import signUpReducer from './signUpReducer'
import homepageReducer from './homepageReducer'
import termsReducer from './termsReducer'
import editUserDetailsReducer from './editUserDetailsReducer'
import addressDetailsReducer from './addressDetailsReducer'



const rootReducer = combineReducers({
    loginReducer,
    orderReducer,
    signUpReducer,
    homepageReducer,
    termsReducer,
    editUserDetailsReducer,
    addressDetailsReducer
});

export default rootReducer;
