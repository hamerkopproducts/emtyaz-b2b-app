import { act } from 'react-test-renderer';
import * as ActionTypes from '../actions/types'
import { dropDownRegionMapper} from '../lib/HelpperMethods'
import _ from 'lodash'


const defaultaddressDetailsPageState = {
    userData: {},
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    success: undefined,
    isIntroFinished: false,
    languageSelected: null,
    userDetails: {},
    regionList: {},
    phone: '',
    isLoggedOut: false,
    addressDetails: {},
    addressDetailsSaved: {},
    editAddressDetails: {},
    deleteAddressDetails: {},
    addressspreadLoader: false
};

export default function addressDetailsReducer(state = defaultaddressDetailsPageState, action) {
    switch (action.type) {
        case ActionTypes.ADDRESS_DETAILS_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
                error: ''
            });
        case ActionTypes.EDIT_ADDRESS_DETAILS_LOADING:
            return Object.assign({}, state, {
                addressspreadLoader: true,
                error: ''
            });
        case ActionTypes.SAVE_ADDRESS_DETAILS_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
                error: ''
            });
        case ActionTypes.LANG_SWITCH:
            return Object.assign({}, state, {
                languageSelected: action.languageSelected,
            });
        case ActionTypes.RESET_ERROR:
           
            return Object.assign({}, state, {
                isLoading: false,
                error:null,
            });
        case ActionTypes.ADDRESS_DETAILS_SUCCESS:
            return Object.assign({}, state, {
                
                isLoading: false,
                isLoggedOut: false,
                addressDetailsSaved: '',
                deleteAddressDetails: '',
                defaultAddessDetails: '',
                addressDetails: action.responseData.data,
                error: ''
            });
        case ActionTypes.SAVE_ADDRESS_DETAILS_SUCCESS:
            
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                addressDetailsSaved: action.responseData,
                error: '',
                editAddressDetails: ''
            });
        case ActionTypes.SAVE_ADDRESS_DETAILS_ERROR:
            
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                error: action.error
            });
        case ActionTypes.EDIT_ADDRESS_DETAILS_SUCCESS:
            
            return Object.assign({}, state, {
                addressspreadLoader: false,
                isLoggedOut: false,
                editAddressDetails: action.responseData,
                error: ''
            });
        case ActionTypes.DELETE_ADDRESS_DETAILS_SUCCESS:
            
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                deleteAddressDetails: action.responseData,
                error: ''
            });
        case ActionTypes.SET_DEFAULT_ADDRESS_SUCCESS:
            
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                defaultAddessDetails: action.responseData,
                error: ''
            });
        case ActionTypes.SET_DEFAULT_ADDRESS_ERROR:
            
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                error: action.error,
                defaultAddessDetails: ''
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                userData: {},
                isLogged: false,
                isLoading: false,
                error: undefined,
                success: undefined,
                resetNavigation: action.resetNavigation,
                isSessionExpired: true,
                isLoggedOut: true
            };
        default:
            return state;
    }
}
