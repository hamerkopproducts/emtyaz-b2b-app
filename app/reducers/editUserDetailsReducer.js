import { act } from 'react-test-renderer';
import * as ActionTypes from '../actions/types'
import { dropDownRegionMapper} from '../lib/HelpperMethods'
import _ from 'lodash'


const defaultEditUserPageState = {
    userData: {},
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    success: undefined,
    isIntroFinished: false,
    languageSelected: null,
    homepageAPIReponse: {},
    userDetails: {},
    regionList: {},
    phone: '',
    updateduserdetails: {},
    isLoggedOut: false
};

export default function editUserDetailsReducer(state = defaultEditUserPageState, action) {
    switch (action.type) {
        case ActionTypes.USER_DETAILS_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.LANG_SWITCH:
            return Object.assign({}, state, {
                languageSelected: action.languageSelected,
            });
            
        case ActionTypes.USER_DETAILS_ERROR:
                return Object.assign({}, state, {
                    isLoading: false,
                    error: action.error
                });
        case ActionTypes.RESET_ERROR:
           
            return Object.assign({}, state, {
                isLoading: false,
                error:null,
            });
        case ActionTypes.USER_DETAILS_SUCCESS:
            
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                userDetails: action.responseData,
            });
        case ActionTypes.REGION_LIST_SUCCESS:
            console.log('>>>>Items',_.get(state,'action',{}))
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                updateduserdetails: '',
                regionList: _.get(action, 'responseData.data', [])
            });
        case ActionTypes.UPDATE_USER_SUCCESS:
            
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                updateduserdetails: action.responseData,
            });
        case ActionTypes.UPDATE_USER_ERROR:
            
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                error: action.error
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                userData: {},
                isLogged: false,
                isLoading: false,
                error: undefined,
                success: undefined,
                resetNavigation: action.resetNavigation,
                isSessionExpired: true,
                isLoggedOut: true
            };
        default:
            return state;
    }
}
