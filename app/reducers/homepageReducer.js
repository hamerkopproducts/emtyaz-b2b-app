import { act } from 'react-test-renderer';
import * as ActionTypes from '../actions/types'

const defaultHomePageState = {
    userData: {},
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    success: undefined,
    isIntroFinished: false,
    languageSelected: null,
    homepageAPIReponse: {},
    phone: '',
    isLoggedOut: false,
    cartItems: [],
    productsList: {},
    product_details: {},
    f_isLoading: false,
    f_error: {},
    fav_data: {},
    filter_cat_data: [],
    filter_brand_data: [],
    wl_data: []
};

export default function homepageReducer(state = defaultHomePageState, action) {
    switch (action.type) {
        case ActionTypes.HOMEPAGE_SERVICE_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.INTRO_FINISHED:
            return Object.assign({}, state, {
                isIntroFinished: true,
            });
        case ActionTypes.LANG_SWITCH:
            return Object.assign({}, state, {
                languageSelected: action.languageSelected,
            });
            
        case ActionTypes.HOMEPAGE_SERVICE_ERROR:
                return Object.assign({}, state, {
                    isLoading: false,
                    error: action.error
                });
        case ActionTypes.RESET_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error:null,
            });
        case ActionTypes.HOMEPAGE_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                homepageAPIReponse: action.responseData,
            });

        // Product list
        case ActionTypes.PRODUCT_SERVICE_LOADING:
            return Object.assign({}, state, {
                p_isLoading: true,
            });
        case ActionTypes.PRODUCT_SERVICE_ERROR:
            return Object.assign({}, state, {
                p_isLoading: false,
                p_error: action.error
            });
        case ActionTypes.PRODUCT_SERVICE_ERROR_RESET:
            return Object.assign({}, state, {
                p_isLoading: false,
                p_error:null,
            });
        case ActionTypes.PRODUCT_LIST_SUCCESS:
            return Object.assign({}, state, {
                p_isLoading: false,
                productsList: action.responseData,
            });
        //Product list end

        // Details
        case ActionTypes.DETAIL_SERVICE_LOADING:
            return Object.assign({}, state, {
                d_isLoading: true,
            });
        case ActionTypes.DETAIL_SERVICE_ERROR:
            return Object.assign({}, state, {
                d_isLoading: false,
                d_error: action.error
            });
        case ActionTypes.DETAIL_SERVICE_ERROR_RESET:
            return Object.assign({}, state, {
                d_isLoading: false,
                d_error:null,
            });
        case ActionTypes.DETAIL_LIST_SUCCESS:
            return Object.assign({}, state, {
                d_isLoading: false,
                product_details: action.responseData,
            });
        // Details list end

        // WL
        case ActionTypes.WL_SERVICE_LOADING:
            return Object.assign({}, state, {
                wl_isLoading: true,
            });
        case ActionTypes.WL_SERVICE_ERROR:
            return Object.assign({}, state, {
                wl_isLoading: false,
                wl_error: action.error
            });
        case ActionTypes.WL_SERVICE_SUCCESS:
            return Object.assign({}, state, {
                wl_isLoading: false,
                wl_data: action.responseData,
            });
        // WL end

        // fav
        case ActionTypes.FAV_SERVICE_LOADING:
            return Object.assign({}, state, {
                f_isLoading: true,
            });
        case ActionTypes.FAV_SERVICE_ERROR:
            return Object.assign({}, state, {
                f_isLoading: false,
                f_error: action.error
            });
        case ActionTypes.FAV_SERVICE_ERROR_RESET:
            return Object.assign({}, state, {
                f_isLoading: false,
                f_error:null,
            });
        case ActionTypes.FAV_SERVICE_SUCCESS:
            return Object.assign({}, state, {
                f_isLoading: false,
                fav_data: action.responseData,
            });
        // fav end

        //filter cat
        case ActionTypes.FILTER_CAT_SUCCESS:
            return Object.assign({}, state, {
                filter_cat_data: action.responseData,
            });
        // Fdilter brand
        case ActionTypes.FILTER_BRAND_SUCCESS:
            return Object.assign({}, state, {
                filter_brand_data: action.responseData,
            });

        case ActionTypes.ADD_TO_CART:
                return Object.assign({}, state, {
                    cartItems: action.data,
                });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                userData: {},
                isLogged: false,
                isLoading: false,
                error: undefined,
                success: undefined,
                resetNavigation: action.resetNavigation,
                isSessionExpired: true,
                isLoggedOut: true
            };
        default:
            return state;
    }
}
