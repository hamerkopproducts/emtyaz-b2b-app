import { act } from 'react-test-renderer';
import * as ActionTypes from '../actions/types'

const defaultSignUpState = {
    userData: {},
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    success: undefined,
    isIntroFinished: false,
    languageSelected: null,
    otpAPIReponse: {},
    phone: '',
    isLoggedOut: false
};

export default function signUpReducer(state = defaultSignUpState, action) {
    switch (action.type) {
        case ActionTypes.LOGIN_SERVICE_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.INTRO_FINISHED:
            return Object.assign({}, state, {
                isIntroFinished: true,
            });
        case ActionTypes.LANG_SWITCH:
            return Object.assign({}, state, {
                languageSelected: action.languageSelected,
            });

        case ActionTypes.SIGNUP_SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.SIGNUP_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                otpAPIReponse: action.responseData,
                phone: action.phone
            });
        case ActionTypes.RESET_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error:null,
            });
            case ActionTypes.SIGNOUT_SUCCESS:
            return {
                userData: {},
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    success: undefined,
    isIntroFinished: false,
    languageSelected: null,
    otpAPIReponse: {},
    phone: '',
    isLoggedOut: true
            };
        default:
            return state;
    }
}
