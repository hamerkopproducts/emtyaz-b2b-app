import * as ActionTypes from '../actions/types'

const defaultState = {
    orderData: [],
    isLoading: false,
    error: undefined,
    success: undefined
};

export default function orderReducer(state = defaultState, action) {
    switch (action.type) {
        case ActionTypes.SERVICE_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.GET_ORDER_SUCCESS:
            console.log(action.responseData)
            return Object.assign({}, state, {
                isLoading: false,
                orderData: action.responseData
            });
        default:
            return state;
    }
}
