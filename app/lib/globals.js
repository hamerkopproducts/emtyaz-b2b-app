import { Platform, Dimensions } from 'react-native';
let { height, width } = Dimensions.get('window');
import { getStatusBarHeight, getBottomSpace } from 'react-native-iphone-x-helper'
let headerHeight= 50;
let footerTabBarHeight = 80;
let screenMargin = 20;
let screenPaddingFromFooter = 5;

module.exports = {
  DEVICE_TYPE: Platform.OS === "ios" ? 1 : 2,
  DEVICE_TYPE_STRING: Platform.OS,
  SPLASH_DELAY: 3000,
  SCREEN_SIZE: {
    width: width,
    height: height
  },
  COLOR: {
    headerColor: '#2BB354',
    transparent: 'transparent',
    screenBackground: '#FFFFFF',
    textColor: '#4E4E55',
    Red:'#db3236',
    Green:'#4E4E55',
    Blue:'#3b5998',
    white:'#FFFFFF',
    drakBlack:'#1c1c1c',
    lightBlack:'#242424',
    Yellow:'#4E4E55',
    Orange:"#4E4E55",
    drakGray:'#919191',
    lightGray:'#cfcfcf',
    themeGreen:'#3fb851',
    themeOrange:'#e83a0c',
    fbButton:'#3B5998',
    googleButton:'#DB3236',
    addressLabelColor:'#1C1C1C',
    greenTexeColor:'#3FB851',
    yellowTextColor:'#FAA53B'

    
  },
  INTEGER: {
    headerHeight: headerHeight,
    footerTabBarHeight: footerTabBarHeight,
    screenPaddingFromFooter: screenPaddingFromFooter,
    screenWidthWithMargin: width - (2 * screenMargin),
    leftRightMargin: screenMargin,
    statusBarHeight: getStatusBarHeight('safe'),
    bottomSpace: getBottomSpace()
  },
  FONTS: {
    //poppinsBold: 'Poppins-Bold',
    poppinsLight: 'Poppins-Light',
    poppinsMedium: 'Poppins-Medium',
    poppinsRegular: 'Poppins-Regular',
    poppinssemiBold: 'Poppins-SemiBold',
    notokufiArabic:'NotoKufiArabic',
    notokufiarabicBold:'NotoKufiArabic-Bold',
  },
  FONTSIZE: {
    fontSizeSeven: 7,
    fontSizeEight: 8,
    fontSizeNine: 9,
    fontSizeTen: 10,
    fontSizeEleven: 11,
    fontSizeTwelve: 12,
    fontSizeThirteen: 13,
    fontSizeFourteen: 14,
    fontSizeFifteen: 15,
    fontSizeSixteen: 16,
    fontSizeSeventeen: 17,
    fontSizeEighteen: 18,
    fontSizeTwenty: 20,
    fontSizeTwentyTwo: 22,
    fontSizeTwentyFour: 24
  },
  MARGIN: {
    marginOne: 1,
    marginTwo: 2,
    marginThree: 3,
    marginFour: 4,
    marginFive: 5,
    marginSix: 6,
    marginSeven: 7,
    marginEight: 8,
    marginNine: 9,
    marginTen: 10,
    marginTwelve: 12,
    marginThirteen: 13,
    marginFifteen: 15,
    marginEighteen: 18,
    marginTwenty: 20
  }
};
