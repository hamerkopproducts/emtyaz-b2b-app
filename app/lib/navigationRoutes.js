import React, { Component } from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import globals from "./globals"
import appTexts from "../lib/appTexts"

import SplashScreen from '../containers/SplashScreen'
//Login Screens
import LoginScreen from '../containers/LoginScreen'

//ChooseLanguage Screen
import ChooseLanguageScreen from '../containers/ChooseLanguageScreen'
import SearchAreaView from '../containers/SearchArea'

//Choosecategory Screen
import ChoosecategoryScreen from '../containers/ChoosecategoryScreen'

//Chatting Screen
import ChatScreen from '../containers/ChatScreen'

//Tankuorder  Screen
import ThankyouorderScreen from '../containers/ThankyouorderScreen'

//Quotation  Screen
import QuotationScreen from '../containers/MyquotationScreen'

//TankuQuotation  Screen
import ThankyouquotationScreen from '../containers/ThankyouquotationScreen'

//SignUp Screen
import OTPScreen from '../containers/OTPScreen'

//Walkthrough Screen
import WalkthroughScreen from '../containers/WalkthroughScreen'

//Signin Screen
import SigninScreen from '../containers/SigninScreen'

//Signup Screen
import SignupScreen from '../containers/SignupScreen'

//Privacy Screen
import PrivacyScreen from '../containers/PrivacypolicyScreen'

//Terms Screen
import TermsScreen from '../containers/TermsconditionsScreen'

//Faq Screen
import FaqScreen from '../containers/FaqScreen'

//Address Screen
import AddressdetailsScreen from '../containers/AddressdetailsScreen'

//Deliverybillingaddres sScreen
import DeliverybillingaddressScreen from '../containers/DeliverybillingaddressScreen'

//Notification Screen
import NotificationScreen from '../containers/NotificationScreen'

// QuotationsummeryScreen
import QuotationsummeryScreen from '../containers/QuotationsummeryScreen'

//Home Tab Screens
import HomeScreen from '../containers/HomeScreen'

// ProductsScreen Tab Screen
import ProductsScreen from '../containers/ProductsScreen'
import MapScreen from '../containers/MapScreen'
// Search Modal Screen
import SearchModal from '../components/SearchModal'

//Details Screens
import DetailsScreen from '../containers/DetailsScreen'

// Cart Screen
import CartScreen from '../containers/CartScreen'

//FavouriteScreen Tab Screen
import FavouritesScreen from '../containers/FavouritesScreen'

//MyOrdersScreen Tab Screen
import MyOrdersScreen from '../containers/MyOrdersScreen'

//Profile Tab Screen
import ProfileScreen from '../containers/ProfileScreen' 

//Footer Tab Item Style
import FooterTabItem from '../components/FooterTabItem'

//OrderStatus Details screen
import OrderStatusDetails from '../containers/OrderStatusDeatilsScreen'
// HomeTab stacks for screen navigation
const HomeStack = createStackNavigator();
const HomeTab = () => {
	return (
		<HomeStack.Navigator screenOptions={{ headerShown: false }}>
			<HomeStack.Screen name="HomeScreen" component={HomeScreen} />
		</HomeStack.Navigator>
	)
}

// ProductsTab stacks for screen navigation
const ProductsScreenStack = createStackNavigator();
const ProductsTab = () => {
	return (
		<ProductsScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<ProductsScreenStack.Screen name="ProductsScreen" component={ProductsScreen} />
		</ProductsScreenStack.Navigator>
	)
}

// FavouriteTab stacks for screen navigation
// const FavouritesScreenStack = createStackNavigator();
// const FavouriteTab = () => {
// 	return (
// 		<FavouritesScreenStack.Navigator screenOptions={{ headerShown: false }}>
// 			<FavouritesScreenStack.Screen name="Search" component={SearchAreaView} />
// 		</FavouritesScreenStack.Navigator>
// 	)
// }
const SearchScreenStack = createStackNavigator();
const SearchTab = () => {
	return (
		<SearchScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<SearchScreenStack.Screen name="Search" component={SearchAreaView} />
		</SearchScreenStack.Navigator>
	)
}

// MyOrdersTab stacks for screen navigation
const MyOrdersScreenStack = createStackNavigator();
const MyOrdersTab = () => {
	return (
		<MyOrdersScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<MyOrdersScreenStack.Screen name="MyOrdersScreen" component={MyOrdersScreen} />
			
		</MyOrdersScreenStack.Navigator>
	)
}

// ProfileTab stacks for screen navigation
const ProfileScreenStack = createStackNavigator();
const ProfileTab = () => {
	return (
		<ProfileScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<ProfileScreenStack.Screen name="ProfileScreen" component={ProfileScreen} />
		</ProfileScreenStack.Navigator>
	)
}

//Language stacks for screen navigation
const LanguageStack = createStackNavigator();
const LanguageStackNavigator = () => {
	return (
		<LanguageStack.Navigator
			screenOptions={{
				headerShown: false
			}}
		>
			<LanguageStack.Screen name="ChooseLanguageScreen" component={ChooseLanguageScreen} />
		</LanguageStack.Navigator>
	)
}


//Initialize bottom tab navigator
const DashboardTabRoutes = createBottomTabNavigator();

const TabNavigator = () => {
	return (
		<DashboardTabRoutes.Navigator tabBarOptions={{
			showLabel: false,
			style: {
				height: globals.INTEGER.footerTabBarHeight,
				borderWidth: 0,
				borderTopLeftRadius: 20,
				borderTopRightRadius: 20,
				borderTopWidth: 0.5,
				borderRightWidth: 0.5,
				borderLeftWidth: 0.5,
				backgroundColor: globals.COLOR.white,
				borderColor: "#ccc",
				shadowColor: "#000",
				shadowOffset: { width: 0, height: 0 },
				shadowOpacity: 0.2,
				shadowRadius: 1,
				elevation: 1,
			}
		}}>
			<DashboardTabRoutes.Screen
				name="Home"
				component={HomeTab}
				options={{
					tabBarLabel: 'Home',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={0} tabBarLabel={appTexts.STRING.home} isFocused={focused} />
					),
				}}
			/>
			<DashboardTabRoutes.Screen
				name="Products"
				component={ProductsTab}
				options={{
					tabBarLabel: 'Products',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={1} tabBarLabel={appTexts.STRING.products} isFocused={focused} />
					),
				}}
			/>
			<DashboardTabRoutes.Screen
				name="Search"
				component={SearchTab}
				options={{
					tabBarLabel: 'Search',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={2} tabBarLabel={appTexts.STRING.search} isFocused={focused} />
					),
				}}
			/>
			{/* <DashboardTabRoutes.Screen
				name="Favourites"
				component={FavouriteTab}
				options={{
					tabBarLabel: 'Favourites',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={2} tabBarLabel={appTexts.STRING.favourites} isFocused={focused} />
					),
				}}
			/> */}
			<DashboardTabRoutes.Screen
				name="MyOrders"
				component={MyOrdersTab}
				options={{
					tabBarLabel: 'My Orders',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={3} tabBarLabel={appTexts.STRING.myOrders} isFocused={focused} />
					),
				}}
			/>
			<DashboardTabRoutes.Screen
				name="MyProfile"
				component={ProfileTab}
				options={{
					tabBarLabel: 'My Profile',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={4} tabBarLabel={appTexts.STRING.myProfile} isFocused={focused} />
					),
				}}
			/>
		</DashboardTabRoutes.Navigator>

	);
};


//Auth stacks for screen navigation
const AuthScreenStack = createStackNavigator();
const AuthStackNavigator = () => {
	return (
		<AuthScreenStack.Navigator
			screenOptions={{
				headerShown: false
			}}
		>
			<AuthScreenStack.Screen name="SplashScreen" component={SplashScreen} />
			<AuthScreenStack.Screen name="WalkthroughScreen" component={WalkthroughScreen} />
			<AuthScreenStack.Screen name="SignupScreen" component={SignupScreen} />
			<AuthScreenStack.Screen name="OTPScreen" component={OTPScreen} />
			<AuthScreenStack.Screen name="ChooseLanguageScreen" component={ChooseLanguageScreen} />
			<AuthScreenStack.Screen name="SigninScreen" component={SigninScreen} />
			<AuthScreenStack.Screen name="MapScreen" component={MapScreen} />
			
			
		</AuthScreenStack.Navigator>
	)
}

//Main stacks for screen navigation
const MainScreenStack = createStackNavigator();
const MainScreenStackNavigator = () => {
	return (
		<MainScreenStack.Navigator
			screenOptions={{
				headerShown: false
			}}
		>
			<MainScreenStack.Screen name="AuthStackNavigator" component={AuthStackNavigator} />
			 <MainScreenStack.Screen name="TabNavigator" component={TabNavigator} />
			<MainScreenStack.Screen name="CartScreen" component={CartScreen} />
			<MainScreenStack.Screen name="OrderStatusDetails" component={OrderStatusDetails} />
			<MainScreenStack.Screen name="DetailsScreen" component={DetailsScreen} />
			<MainScreenStack.Screen name="QuotationScreen" component={QuotationScreen} />
			<MainScreenStack.Screen name="ChoosecategoryScreen" component={ChoosecategoryScreen} />
			<MainScreenStack.Screen name="FaqScreen" component={FaqScreen} />
			<MainScreenStack.Screen name="PrivacyScreen" component={PrivacyScreen} />
			<MainScreenStack.Screen name="TermsScreen" component={TermsScreen} />
			<MainScreenStack.Screen name="QuotationsummeryScreen" component={QuotationsummeryScreen} />
			<MainScreenStack.Screen name="AddressdetailsScreen" component={AddressdetailsScreen} />
			<MainScreenStack.Screen name="DeliverybillingaddressScreen" component={DeliverybillingaddressScreen} />
			<MainScreenStack.Screen name="FavouritesScreen" component={FavouritesScreen} />	
			<MainScreenStack.Screen name="MapScreen" component={MapScreen} />	

		</MainScreenStack.Navigator>
	)
}
const MyTheme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		background: '#FFFFFF'
	},
};

export const LanguageContainer = () => {
	return (
		<NavigationContainer>
			<LanguageStackNavigator />
		</NavigationContainer>
	)
}


export const LoginContainer = () => {
	return (
		<NavigationContainer>
			<AuthStackNavigator />
		</NavigationContainer>
	)
}

export const HomeContainer = () => {
	return (
		<NavigationContainer theme={MyTheme}>
			<MainScreenStackNavigator />
		</NavigationContainer>
	)
}