import globals from "./globals"

let productionURL = '';
let developmentURL = 'https://emdev.ourdemo.online/api/';
let baseURL = developmentURL;

let loginAPI = 'b2b/b2b-login';
let mapAPI = 'b2b/location';
let otpVerifyAPI = 'b2b/otp-verify';
let signUpAPI = 'b2b/user-signup';
let homePageAPI = 'b2b/home';
let productsAPI = 'b2b/list-products';
let product_details_API = 'b2b/product-details/';
let addFavAPI = 'b2b/product/favourite';
let filterCatAPI = 'b2b/list-filter/category';
let filterBrandAPI = 'b2b/list-filter/brand';
let myWishListAPI = 'b2b/my-wishlist';

let termsAndConditionsPageAPI = 'b2b/terms-condition';
let userDetailsEditPageAPI = 'b2b/b2b-user/edit';
let regionListDropDownAPI = 'b2b/region-list';
let updateUserDetailsApi = 'b2b/b2b-user/update';
let addressListDetailsApi = 'b2b/list-address';
let SaveAddressDetailsApi = 'b2b/add-address';
let EditAddressDetailsApi = 'b2b/edit-address/';
let SaveEditAddressDetailsApi = 'b2b/update-address';
let DeleteAddressApi = 'b2b/address/';
let setAsDefaultADrsApi = 'b2b/change/default';

const apiHelper = {
  getAPIHeaderForDelete: (params,token) => {
  	const AuthStr = 'Bearer '.concat(token);
    let headerConfig = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthStr
      },
      data: params
    };
    return headerConfig;
  },
  getAPIHeader: (token) => {
  	const AuthStr = 'Bearer '.concat(token);
    let headerConfig = { 
      headers: { 
        'Content-Type': 'application/json',
         Authorization: AuthStr, 
         Accept: 'application/json' } };
    return headerConfig;
  },
  getLoginAPIHeader: () => {
    let headerConfig = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' } };
    return headerConfig;
  },
  getLoginAPI: () => {
    return baseURL + loginAPI;
  },
  getMapAPI: () => {
    return baseURL + mapAPI;
  },
  getSignUpAPI: () => {
    return baseURL + signUpAPI;
  },
  getOtpVerifyAPI: () => {
    return baseURL + otpVerifyAPI;
  },
  getHomePageAPI: () => {
    return baseURL + homePageAPI
  },
  getProductsAPI: () => {
    return baseURL + productsAPI
  },
  getAddFavAPI: () => {
    return baseURL + addFavAPI
  },
  getFilterCatAPI: () => {
    return baseURL + filterCatAPI
  },
  getFilterBrandAPI: () => {
    return baseURL + filterBrandAPI
  },
  getProductDetailApi: () => {
    return baseURL + product_details_API
  },
  getMyWishListApi: () => {
    return baseURL + myWishListAPI
  },
  getTermsAndConditionsAPI: () => {
    return baseURL + termsAndConditionsPageAPI
  },
  getUserEditDetailsAPI: () => {
    return baseURL + userDetailsEditPageAPI
  },
  getRegionDropDownListAPI: () => {
    return baseURL + regionListDropDownAPI
  },
  setupdateUserDetailsApi: () => {
    return baseURL + updateUserDetailsApi
  },
  getAddressDetailsApi: () => {
    return baseURL + addressListDetailsApi
  },
  setAddressDetailsApi: () => {
    return baseURL + SaveAddressDetailsApi
  },
  getEditAddressDetailsApi: (id) => {
    return baseURL + EditAddressDetailsApi + id
  },
  setEditedAddressDetailsApi: () => {
    return baseURL + SaveEditAddressDetailsApi
  },
  deleteAddressDetailsApi: (id) => {
    return baseURL + DeleteAddressApi + id
  },
  setAddressAsDefaultApi: () => {
    return baseURL + setAsDefaultADrsApi
  },
}

export default apiHelper;
