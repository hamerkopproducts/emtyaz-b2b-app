import _ from 'lodash'
export const dropDownRegionMapper = (data) => {
    return _.map(data, item => {
        return {
          value: _.get(item, ['lang', 0, 'name'], ''),
          id: _.get(item, ['lang', 0, 'region_id'], '')
        }
      })} 