import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
const styles = StyleSheet.create({
    mainView:{
        
    },
    contentView:{
        flexDirection:'row',
        justifyContent:'space-between',
        paddingBottom:'4%',
        paddingTop:'4%',
    },

    bordersStyle:{
        borderWidth:0.5,
        borderColor:globals.COLOR.lightGray,
        
      },
    categoryText:{
        fontFamily:globals.FONTS.poppinsRegular,
        fontSize:13,
    },
  });
  
  export { styles };