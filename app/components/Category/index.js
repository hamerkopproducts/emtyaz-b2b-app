import React, { useState } from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { styles } from "./styles";
import Icon from "react-native-vector-icons/Entypo";

const Category = (props) => {
  const { itemText, id, selectCatItem, selected } = props;

  return (
    <View style={styles.mainView}>
      <TouchableOpacity onPress={() => selectCatItem(id)}>
        <View style={styles.contentView}>
          <Text style={styles.categoryText}>{itemText}</Text>

          <Image
            source={
                selected
                ? require("../../assets/images/modal/checked.png")
                : require("../../assets/images/modal/uncheck.png")
            }
          />
        </View>
      </TouchableOpacity>
      <View style={styles.bordersStyle}></View>
    </View>
  );
};
Category.propTypes = {
  onItemClick: PropTypes.func,
  itemText: PropTypes.string,
};

export default Category;
