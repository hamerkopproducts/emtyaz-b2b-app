import React, { useState } from "react";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Modal from "react-native-modal";
import CustomRadioButton from "../CustomRadioButton";

const PackSizeModal = (props) => {
  const { 
    togglePackSizeModal, 
    isPackSizeModalVisible, 
    packSizes,
    setSelectedQuantity,
    selectedQuantity,
    selectedQuantityCount,
    setSelectedQuantityCount
  } = props;

  const [qty, setQty] = useState(selectedQuantity);
  const [qtyCount, setQtyCount] = useState(selectedQuantityCount);

  const renderEachQty = (item) => {
    let discount_percentage = '';
    if(item.discount_price) {
      try {
        discount_percentage = 100 - Math.floor( ( item.discount_price/item.price ) * 100);
      } catch(err) {
        discount_percentage = '';
      }
    }

    return (
      <TouchableOpacity style={styles.boxWrap}>
        <View style={styles.boxInput}>
          <View style={styles.textInputWrap}>
            <View style={styles.textOne}>
              <Text style={styles.boxTextInput}>
                {item.quantity}
              </Text>
            </View>
            <View style={styles.textTwo}>
              <Text style={styles.boxTextTwoInput}>SAR {item.price}</Text>
            </View>
            {item.discount_price &&
              <View style={styles.textThree}>
                <Text style={styles.boxTextThreeInput}>SAR {item.discount_price}</Text>
              </View>
            }
            {discount_percentage &&
              <View style={styles.textFour}>
                <Text style={styles.boxTextFourInput}>{discount_percentage}% off</Text>
              </View>
            }
          </View>
        </View>
        <CustomRadioButton
          id={item.id}
          name=''
          selected_id={qty}
          onPress={(id, name) => {
            setQty(id);
            setQtyCount(item.quantity);
          }}
        />
      </TouchableOpacity>
    )
  }

  return (
    <Modal
      animationIn="slideInUp"
      animationOut="slideOutRight"
      isVisible={isPackSizeModalVisible}
      style={styles.packSizeModalView}
      onBackdropPress={() => togglePackSizeModal() }
    >
      <View style={styles.packSizeModalContent}>
        <View style={styles.xButton}>
          <TouchableOpacity
            onPress={() => {
              togglePackSizeModal();
            }}
          >
            <Text style={styles.xText}>X</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.packSizeModalTopView}>
          <View style={styles.headingPackSelect}>
            <Text style={styles.headingStyle}>{appTexts.PACKSIZE.Heading}</Text>
          </View>

          <View style={styles.butWrapper}>
            <TouchableOpacity onPress={() => {
              setSelectedQuantity(qty);
              setSelectedQuantityCount(qtyCount);
              togglePackSizeModal();
            }}>
              <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>
                  {appTexts.PACKSIZE.ButtonTitleOne}
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setQty(selectedQuantity) }>
              <View style={styles.buttonTwoStyle}>
                <Text style={styles.buttonText}>
                  {appTexts.PACKSIZE.ButtonTitleTwo}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.touchableopacityWrap}>
          <FlatList
            data={packSizes}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item) => packSizes.indexOf(item).toString()}
            showsVerticalScrollIndicator={true}
            renderItem={({ item, index }) => (
              renderEachQty(item)
            )}
          />
        </View>
      </View>
    </Modal>
  );
};
PackSizeModal.propTypes = {};

export default PackSizeModal;
