import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";

const styles = StyleSheet.create({
packSizeModalView:{
    margin:0,
},
packSizeModalContent:{
//height: '50%',
marginTop: 'auto',
backgroundColor:'white',
paddingLeft:'5%',
paddingRight:'5%',
//backgroundColor:'red',
},
xText:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    fontSize: 17,
    color:'grey',
},
  packSizeModalTopView:{
    paddingTop:'2%',
    //paddingLeft:'3%',
    paddingRight:'3%',
    flexDirection:'row',
    justifyContent:'space-between',
    //backgroundColor:'yellow',
         
  },
  headingLine:{
    flexDirection:'row',
    justifyContent:'space-between',
    
    
    
  },
  headingStyle:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    fontSize: 17, 
  },
  buttonWrap:{
    justifyContent:'flex-end',
    flexDirection:'row',
    
  },
  saveButton:{
    width:85,
    height:70,
         //padding:'2%',
  },
  saveButtonStyle:{
    backgroundColor:'green',
    //height:'30%',
    justifyContent:'center',
    alignItems:'center',
    //alignSelf:'center',
  },
  saveButtonText:{
    fontSize:14,
    color:'white',
    textAlign:'center',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  resetButton:{
    width:75,
    height:70,
    marginLeft:'2%',
  
    
  },
  resetButtonStyle:{
    backgroundColor:'orange',
    //height:'30%',
    justifyContent:'center',
    alignItems:'center',
    //alignSelf:'center',
  },
  resetButtonText:{
    fontSize:14,
    color:'white',
    textAlign:'center',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  xButton:{
    justifyContent:'flex-end',
    alignItems:'flex-end',
    paddingTop:'4%',
  },
  boxOne:{
    borderWidth:1,
   // borderColor:globals.COLOR.lightGray,
    width:'60%',
    padding:'5%',
    borderColor:'red',
  },

  touchableopacityWrap:{
 
      paddingTop:'5%',
   },
   boxWrap:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    paddingBottom:'8%',
    
    
  },
  boxInput:{
    borderWidth:0.5,
    width:'90%',
    padding:'2.5%',
   
    borderColor:globals.COLOR.lightGray,
      },
      boxTextInput:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
        fontSize: 13,   
        paddingLeft:'1%',
      },
textInputWrap:{
flexDirection:'row',
},
textOne:{
flexBasis:'20%',
paddingLeft:'1%',
//backgroundColor:'red',

},
textTwo:{
flexBasis:'24%',
//backgroundColor:'yellow',
justifyContent:'center',
alignItems:'center',

},
boxTextTwoInput:{
fontSize:10,
color:'grey',
//alignSelf:'center',
//textAlign:'center',
},
textThree:{
flexBasis:'26%',
//backgroundColor:'green',
justifyContent:'center',
alignItems:'center',

},
boxTextThreeInput:{
fontSize:12,
fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
},
textFour:{
flexBasis:'18%',
backgroundColor:'green',
justifyContent:'center',
alignItems:'center',
marginLeft:'5%',
},
boxTextFourInput:{
fontSize:10,
color:'white',

},
butWrapper:{
flexDirection:'row',
alignSelf:'flex-end',
// paddingTop:'3%',
paddingBottom:'3%',
},
buttonStyle: {
backgroundColor: 'green',
width: 52,
height: 28,
justifyContent: "center",
alignItems: "center",
},
buttonText: {
color: globals.COLOR.white,
fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
fontSize: 12,
},
buttonTwoStyle:{
backgroundColor: 'orange',
width: 52,
height: 28,
justifyContent: "center",
alignItems: "center",
marginLeft:'6%',
},
});

export { styles };