import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity} from 'react-native';
import { styles } from "./styles";

const CategoryItem = (props) => {
  const {
    itemImage,
    badgeLabel,
    onPress,
    selected
  } = props;

  return (
    <View style={styles.categoryItemContainer}>
      
      <View style={[
        styles.imageContainer,
        selected && {borderColor: 'green', borderWidth: 2}
        ]}>
        <TouchableOpacity onPress={() => onPress() }>
          {itemImage.uri !='' &&
            <Image style={[
              styles.itemImage
            ]} 
              resizeMode="contain" source={itemImage}/>
          }
          {itemImage.uri =='' &&
          <View style={ styles.itemImage } >

          </View>
          }
        </TouchableOpacity>
      </View>
      {badgeLabel && badgeLabel !==''? <View style={styles.badgeContainer}>
        <Text style={styles.badgeLabel}>{badgeLabel}</Text>
      </View>: null}
    </View>
  );
};
CategoryItem.propTypes = {
  badgeLabel:PropTypes.string
};

export default CategoryItem;