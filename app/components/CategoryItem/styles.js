import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  categoryItemContainer: {
    width: (globals.INTEGER.screenWidthWithMargin / 4),
    height: (globals.INTEGER.screenWidthWithMargin / 4),
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 5
  },
  imageContainer:{
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    borderWidth: 1,
    padding: 3,
    borderColor: '#dadada',
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 100
  },
  badgeLabel:{
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.poppinsMedium,
    fontSize: 10,
  },
  badgeContainer:{
    position:'absolute',
    top:0,
    height: 20,
    width: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:20,
    backgroundColor:'red'
  }
});

export { styles };
