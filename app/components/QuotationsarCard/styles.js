import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  fullWidthRowContainer:{
    width: '100%',
    height: 120,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor:'red'
  },
  firstContainer:{
    width: '100%',
    height: 120,
    paddingTop:10,
    paddingBottom:10,
    flexDirection:'row'
  },
  secondContainer: {
    width: '100%',
    height: 10,
    flexDirection: 'row'
  },
  image: {
    height: 90,
    width: 90,
  },
  imageContainer:{
    height: 100,
    width:100,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth:1
  },
  labelContainer:{
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 100,
  },
  labelView:{
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 60,
    flexDirection:'row'
  },
  sizeSelectContainer: {
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 40
  },
  sizeSelectView:{
    marginLeft:10,
    width: 100,
    height: 30,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  itemNameText:{
    color: globals.COLOR.lightBlack,
    //color: globals.COLOR.addressLabelColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 12
  },
  sizeText:{
    marginRight:23,
    color: globals.COLOR.lightBlack,
    //color: globals.COLOR.addressLabelColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 12
  },
  dropDownArrow: {
    
  },
  textContainer:{
    marginLeft: 20,
    width: globals.INTEGER.screenWidthWithMargin - 160,
    height: 60,
  },
  favIconContainer: {
    alignItems: 'flex-end',
    width:40,
    height: 60
  },
  nameLabel:{
    top:0,
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 11,
    height: 20
  },
  addButtonContainer:{
    position:'absolute',
    right:0,
    bottom:20
  }
});

export { styles };
