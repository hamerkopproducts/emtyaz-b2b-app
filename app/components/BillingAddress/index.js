import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity} from 'react-native';
import { styles } from "./styles";
import Modal from 'react-native-modal';
import CustomRadioButton from '../../components/CustomRadioButton';
import Icon from "react-native-vector-icons/FontAwesome";
import appTexts from '../../lib/appTexts';
import Loader from '../../components/Loader';
const DeliveryAddress = (props) => {
  const {
        item,
        spredDataBillingAdrs,
        validateAddressType,
        validateNewOrUpdate,
        editAddress,
        fetcheditAddress,
        setAsDefaultAddress,
        deleteAddress,
        clearFileds,
        toggleModal,
        isLoading,
        adrsId
        
      } = props;
  
  return (
    
    <View style={styles.billinguserDetails}>

    <View style={styles.billingnameandIcon}>
      <Text style={styles.nameText}>{item.name}</Text>
      <View style={styles.iconList}>
        <View style={{ paddingHorizontal: "3%" }}>
          <TouchableOpacity
            onPress={() => {
              
              editAddress(item.id)
              validateAddressType(true)
              validateNewOrUpdate(false)
              toggleModal();
              
            }}
          >
             <Image
                  source={require("../../assets/images/profileItem/edit.png")}
                  resizeMode="contain"
                  style={styles.arrowicon}
                />
            {/* <Icon
              style={{ fontSize: 16, color: "#ACACAC" }}
              name="pencil"
              // source={require("../../assets/images/profileItem/edit.png")}
            /> */}
          </TouchableOpacity>
        </View>
        <TouchableOpacity 
        onPress={() => {
          deleteAddress(item.id)
          
        }}
        >
        <Icon
          style={{ fontSize: 16, color: "#ACACAC" }}
          name="trash-o"
        />
        </TouchableOpacity>
      </View>
    </View>
    <View style={styles.nameandIcon}>
      <Text style={styles.placeText}>{item.apartment_name}</Text>
      <TouchableOpacity  onPress={() => {
          setAsDefaultAddress(item.id)  
        }}>
        <View style={styles.iconList}>
          <Icon
            style={{ fontSize: 16, color: item.is_default === 'Y' ? "#f7aa1a" : "lightgray" }}
            name="heart"
          />
        </View>
        </TouchableOpacity>
    </View>
    <View style={styles.userDetails}>
      <View style={styles.textContent}>
        <View style={{ paddingLeft: "0%" }}>
          <View style={styles.textContent}>
            <Text style={styles.placeText}>{item.street_name}</Text>
            <Text style={styles.placeText}>PO Box: {item.postal_code}</Text>
            <Text style={styles.placeText}>{item.landmark}</Text>
            <Text style={styles.placeText}>+966 {item.phone}</Text>
          </View>

        </View>
      </View>
    </View>
  </View>
  );
};
DeliveryAddress.propTypes = {
  item:PropTypes.object,
  spredDataBillingAdrs:PropTypes.func,
  validateAddressType:PropTypes.func,
  validateNewOrUpdate:PropTypes.func,
  setAsDefaultAddress:PropTypes.func,
  toggleModal:PropTypes.func,
  editAddress:PropTypes.func,
  fetcheditAddress:PropTypes.func,
  deleteAddress:PropTypes.func,
  clearFileds:PropTypes.func,
  adrsId:PropTypes.number,
  isLoading:PropTypes.bool,


  
};

export default DeliveryAddress;