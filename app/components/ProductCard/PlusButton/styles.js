import { StyleSheet } from "react-native";
import globals from "../../../lib/globals"

const styles = StyleSheet.create({
  buttonView: {
    width: 22.5,
    height: 22.5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'red'
  },
  plusText:{
    alignSelf: 'center',
    color:'white',
    fontSize:24
  },
  itemImage:{
    width:10,
    height:10
  }
});

export { styles };
