import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  itemContainer: {
     width: (globals.INTEGER.screenWidthWithMargin - globals.INTEGER.leftRightMargin) / 2,
     marginLeft: globals.INTEGER.leftRightMargin,
     height: 272,
    //width: 165,
    //marginLeft: 10,
    //height: 275,
    marginBottom: globals.INTEGER.leftRightMargin,
    alignItems: 'center',
    borderColor:'#ddd',
    borderWidth: 0.5,
    borderRadius: 1,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.2,
    shadowRadius:0.1,
    elevation: 1,
    
  },
  labelView: {
    width: '90%',
    height: 38,
    flexDirection: 'row',
    
  },
  discountTextContainer: {
    width: '90%',
    height: 40,
    justifyContent: 'center'
  },
  textContainer:{
    marginTop: '5%',
    marginLeft:'5%',
    marginRight: '5%',
    width: '85%',
    flexDirection: 'row',
    //backgroundColor:'red'
  },
  favIconContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: '10%',
    height: 40
  },
  image: {
    height: 118,
    width: 118,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  discountText:{
    color: globals.COLOR.greenTexeColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsLight,
    fontSize: 11
  },
  nameText:{
    paddingLeft:12,
    paddingRight:12,
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 10.2,
    textAlign: 'left'
  },
  secondContainer: {
    width: '90%',
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  greenContainer:{
    height: 14,
    width: 14,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: globals.COLOR.greenTexeColor,
    borderWidth:1
  },
  greenDot: {
    height:5,
    width: 5,
    backgroundColor: globals.COLOR.greenTexeColor,
    borderRadius: 5
  },
  sizeSelectView: {
    marginRight: 10,
    width:55,
    height: 25,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth: 1
  },
  sizeText: {
    marginRight: 10,
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 9.5
  },
  dropDownArrow:{
    width:10,
    height:10,
    marginRight:1
  }
});

export { styles };
