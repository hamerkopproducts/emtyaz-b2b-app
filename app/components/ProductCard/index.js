import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { View, Image, Text, TouchableOpacity, FlatList } from "react-native";
import { styles } from "./styles";
import DotedContainerItem from "./DotedContainerItem";
import PlusButton from "./PlusButton";

import appTexts from "../../lib/appTexts";
import BannerModal from "../../components/BannerModal";
import QuantityModal from "../../components/QuantityModal";
import PackModal from "../../components/PackModal";
import PackSizeModal from "../../components/PackSizeModal";

const ProductCard = (props) => {
  const { item, itemClick, addRemoveFav, wishListItems } = props;

  let initialVariant = "";
  let initialVariantName = "";
  try {
    initialVariant = item.product.b2bpricevariant[0].attribute_variant_id;
    initialVariantName = item.product.b2bpricevariant[0].variant.name;
  } catch (err) {
    initialVariant = "";
    initialVariantName = "";
  }

  const [isPackModalVisible, setIsPackModalVisible] = useState(false);
  const [isPackSizeModalVisible, setIsPackSizeModalVisible] = useState(false);
  const [isBannerModalVisible, setIsBannerModalVisible] = useState(false);
  const [isQuantityModalVisible, setIsQuantityModalVisible] = useState(false);

  const [selectedVariant, setSelectedVariant] = useState(initialVariant);
  const [selectedVariantName, setSelectedVariantName] = useState(
    initialVariantName
  );
  const [selectedQuantity, setSelectedQuantity] = useState("");
  const [selectedQuantityCount, setSelectedQuantityCount] = useState("");
  const [
    quantitiesOfSelectedvariant,
    setQuantitiesOfSelectedvariant,
  ] = useState([]);

  if (selectedVariant == "" && initialVariant != selectedVariant) {
    setSelectedVariant(initialVariant);
    setSelectedVariantName(initialVariantName);
  }

  if (typeof item.product == "undefined") {
    item.product = Object.assign({}, item);
  }
  const type = item.product.product_type;
  const slug = item.product.slug;
  const product_id = item.product.id;
  const b2bpricevariant = item.product.b2bpricevariant;
  const b2bprice = item.product.b2bprice;

  useEffect(() => {
    let quantities = [];
    for (let incr = 0; incr < b2bprice.length; incr++) {
      if (b2bprice[incr].variant_id == selectedVariant) {
        quantities.push(b2bprice[incr]);
      }
    }
    if (type == "simple") {
      quantities = b2bprice;
    }
    setQuantitiesOfSelectedvariant(quantities);

    let initialQty = "";
    let initialQtyCount = "";
    try {
      initialQty = quantities[0].id;
      initialQtyCount = quantities[0].quantity;
    } catch (err) {
      initialQty = "";
      initialQtyCount = "";
    }
    setSelectedQuantity(initialQty);
    setSelectedQuantityCount(initialQtyCount);
  }, [selectedVariant]);

  const c_image = item.product.cover_image;
  let name = "";
  try {
    name = item.product.lang[0].name;
  } catch (err) {
    name = "";
  }
  const qtySlicedFirstThree = quantitiesOfSelectedvariant.slice(0, 1);

  const togglePackSizeModal = () => {
    setIsPackSizeModalVisible(false);
  };

  const openPackSizeModal = () => {
    setIsPackSizeModalVisible(true);
  };

  const togglePackModal = () => {
    setIsPackModalVisible(false);
  };

  const openPackModal = () => {
    setIsPackModalVisible(true);
  };

  const toggleQuantityModal = () => {
    setIsQuantityModalVisible(false);
  };

  const openQuantityModal = () => {
    setIsQuantityModalVisible(true);
  };

  const closeBannerModal = () => {
    setIsBannerModalVisible(false);
  };

  const openBannerModal = () => {
    props.addToCart({
      variant_id: selectedVariant,
      variant_name: selectedVariantName,
      quantityId: selectedQuantity,
      product_id: product_id,
      name: name,
      quantity: 1,
      type: type,
      image: c_image,
      quantity_count: selectedQuantityCount,
    });
    setIsBannerModalVisible(true);
  };

  const isInWishList = wishListItems.indexOf(product_id) !== -1;

  return (
    <View style={styles.itemContainer}>
      <View style={styles.labelView}>
        <View style={styles.discountTextContainer}></View>
        <View style={styles.favIconContainer}>
        <TouchableOpacity onPress={() => {
                addRemoveFav(product_id)
              }}>
          <Image
            source={
              isInWishList
                ? require("../../assets/images/products/heart-active.png")
                : require("../../assets/images/products/favourite.png")
            }
          />
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.imageContainer}>
        <TouchableOpacity
          onPress={() => {
            itemClick(slug);
          }}
        >
          {c_image &&
          <Image
            resizeMode="contain"
            source={{
              uri: c_image,
            }}
            style={styles.image}
          />
          }
        </TouchableOpacity>
      </View>

      <View style={styles.textContainer}>
        <View style={styles.greenContainer}>
          <View style={styles.greenDot}></View>
        </View>
        <Text numberOfLines={3} style={[styles.nameText, {height: 60}]}>{name}</Text>
      </View>
      <View style={styles.secondContainer}>
      {type == 'complex' &&
        <TouchableOpacity onPress={() => { openQuantityModal() }}>
          <View style={styles.sizeSelectView}>
            <Text style={styles.sizeText}>{selectedVariantName}</Text>
            <Image
              style={styles.dropDownArrow}
              source={require("../../assets/images/home/down-arrown.png")}
            />
          </View>
        </TouchableOpacity>
      }

        <FlatList
          data={qtySlicedFirstThree}
          horizontal
          keyExtractor={(item) => qtySlicedFirstThree.indexOf(item).toString()}
          showsVerticalScrollIndicator={false}
          renderItem={({ item, index }) => (
            <>
              <DotedContainerItem
                color={selectedQuantity == item.id ? "green" : ""}
                qty={selectedQuantityCount}
                id={item.id}
                onEventClick={(id) => {
                  // setSelectedQuantity(id);
                  // setSelectedQuantityCount(item.quantity);
                  openPackSizeModal();
                }}
              />
              {quantitiesOfSelectedvariant.length > 3 && index == 2 && (
                <MoreItem onItemClick={openPackModal} />
              )}
            </>
          )}
        />

        <PlusButton onButtonClick={openBannerModal}/>

        <QuantityModal
          setSelectedVariant={setSelectedVariant}
          setSelectedVariantName={setSelectedVariantName}
          selectedVariantId={selectedVariant}
          b2bQuantity={b2bpricevariant}
          isQuantityModalVisible={isQuantityModalVisible}
          toggleQuantityModal={toggleQuantityModal} 
        />

        <PackModal 
          isPackModalVisible={isPackModalVisible} 
          togglePackModal={togglePackModal}
          packSizes={quantitiesOfSelectedvariant}
          selectedQuantity={selectedQuantity}
          setSelectedQuantity={(id, quantity) => {
            setSelectedQuantity(id);
            setSelectedQuantityCount(quantity);
            togglePackModal();
            setTimeout(() => {
              openPackSizeModal();
            }, 400);
          }}
        />
        {isPackSizeModalVisible &&
          <PackSizeModal
            packSizes={quantitiesOfSelectedvariant}
            selectedQuantity={selectedQuantity}
            selectedQuantityCount={selectedQuantityCount}
            setSelectedQuantity={setSelectedQuantity}
            setSelectedQuantityCount={setSelectedQuantityCount}
            isPackSizeModalVisible={isPackSizeModalVisible} 
            togglePackSizeModal={togglePackSizeModal} 
          />
        }
        <BannerModal 
          isBannerModalVisible={isBannerModalVisible} 
          closeModal={closeBannerModal} 
          bannerText={appTexts.BANNER.CartAdded}
          navigate={()=> {
            props.navigation.navigate('CartScreen');
            closeBannerModal();
          }}
        />

      </View>
    </View>
  );
};
ProductCard.propTypes = {
  item: PropTypes.object,
};

export default ProductCard;
