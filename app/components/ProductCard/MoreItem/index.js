import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text} from 'react-native';
import { styles } from "./styles";

const MoreItem = (props) => {
  const {
        itemImage,
        nameLabel
      } = props;

  return (
  <View style={styles.sizeSelectView}>
    <Text style={styles.symbolText}>{'More'}</Text>
      <Image source={require('../../../assets/images/login/icn-pass.png')} />
  </View>
  );
};
MoreItem.propTypes = {
  itemImage:PropTypes.number,
  nameLabel:PropTypes.string
};

export default MoreItem;