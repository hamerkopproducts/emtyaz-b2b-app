import { StyleSheet } from "react-native";
import globals from "../../../lib/globals"

const styles = StyleSheet.create({
  sizeSelectView: {
    width: 60,
    height: 30,
    marginRight:15,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  symbolText:{
    marginLeft: 5,
    color: globals.COLOR.greenTexeColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize:11,
    marginRight:5
  },
  countText: {
    color: 'black',
    fontSize: 14
  }
});

export { styles };
