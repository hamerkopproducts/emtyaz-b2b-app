import React from "react";
import { StyleSheet } from "react-native";
import Stars from "react-native-stars";
import Icon from "react-native-vector-icons/MaterialIcons";
// import colors from '../../../configs/colors';
const ratingCompleted = (rating) => {
  console.log("Rating is: " + rating);
};
const RatingUserComponent = ({ segment_id, rating, onUpdate }) => {
  return (
    <>
      <Stars
        default={rating}
        count={5}
        half={false}
        fullStar={
          <Icon name={"star"} solid={true} style={[styles.myStarStyle]} />
        }
        emptyStar={
          <Icon
            name={"star-outline"}
            style={[styles.myStarStyle, styles.myEmptyStarStyle]}
          />
        }
        halfStar={<Icon name={"star-half"} style={[styles.myStarStyle]} />}
        update={(val) => onUpdate(segment_id, val)}
      />
    </>
  );
};
const styles = StyleSheet.create({
  myStarStyle: {
    color: "#f7aa1a",
    backgroundColor: "transparent",
    fontSize: 16,
    marginRight: 3,
    paddingTop:'1.5%'
  },
  myEmptyStarStyle: {
    color: "#f7aa1a",
  },
});

export default RatingUserComponent;
