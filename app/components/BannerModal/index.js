import React from "react";
import { Modal, View, Text, TouchableOpacity } from "react-native";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";

const BannerModal = (props) => {
  const { closeModal, bannerText, isBannerModalVisible, navigate } = props;

  return (
    <Modal
      animationType="fade"
      transparent={true}
      onRequestClose={() => null}
      visible={isBannerModalVisible}
    >
      <View style={styles.popupContainer}>
        <TouchableOpacity
          style={styles.popupBackground}
          onPress={() => {
            closeModal();
          }}
        />
        <View style={styles.popupContentContainer}>
          <View style={styles.bannerView} onPress={() => {}}>
            <Text style={styles.bannerText}>{bannerText}</Text>
            <View style={styles.closeView}>
              <TouchableOpacity
                style={styles.closeContainer}
                onPress={() => {
                  closeModal();
                }}
              >
                <Text style={styles.closeText}>{"X"}</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.viewTextContainer}
                onPress={() => {
                  navigate();
                }}
              >
                <Text style={styles.viewText}>{appTexts.BANNER.View}</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};

BannerModal.propTypes = {};

export default BannerModal;
