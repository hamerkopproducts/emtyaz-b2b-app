import { StyleSheet,I18nManager,Platform } from "react-native";
import globals from "../../lib/globals"

const images = {
  searchIcon: require("../../assets/images/header/home-search.png")
};

const styles = StyleSheet.create({
	popupContainer:{
      width: '100%',
    	height: '100%',
      flexDirection: 'column',
      backgroundColor: globals.COLOR.transparent
    },
    popupBackground:{
      width: '100%',
      height: '100%',
      backgroundColor: 'red',
      opacity:0.01,
    	zIndex: 1
    },
    popupContentContainer:{
    	top:0,
      position:'absolute',
      backgroundColor: globals.COLOR.white,
      marginTop: Platform.OS === 'ios' ? globals.INTEGER.statusBarHeight + globals.INTEGER.headerHeight: (globals.INTEGER.statusBarHeight + globals.INTEGER.headerHeight - 20),
      height: globals.SCREEN_SIZE.height - (globals.INTEGER.bottomSpace + globals.INTEGER.statusBarHeight + globals.INTEGER.headerHeight),
    	width: '100%',
    	flexDirection: 'column',
      alignItems: 'center',
      justifyContent: "flex-start",
      zIndex: 3
    },
  headingText:{
    marginTop:10,
    marginBottom:10,
    textAlign: "left",
    color: globals.COLOR.black,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14,
    height:30,
    width:'90%'
  },
  listContainer: {
    height: globals.SCREEN_SIZE.height - (globals.INTEGER.bottomSpace + globals.INTEGER.statusBarHeight + globals.INTEGER.headerHeight + 50),
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: globals.COLOR.white
  },
  flatListStyle: {
    height:'100%',
    width: '100%'
  },
  noDataContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  noDataText: {
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontSize: 12
  },
  listRow:{
    marginTop:10,
    marginLeft:'5%',
    width: '90%',
    height:40
  },
  categoriesView:{
    marginTop: 0,
    height: 70,
    marginLeft: '5%',
    width: '90%'
  },
  categoriesText:{
    textAlign: "left",
    color: globals.COLOR.greenTexeColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14,
    marginBottom:10
  },
  selectedView: {
    flexDirection:'row'
  },
  selectedItemText:{
    textAlign: "center",
    padding:10,
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14
  },
  itemView:{
    backgroundColor:globals.COLOR.greenTexeColor,
    marginRight:15,
  }
  
});

export { images, styles  };
