import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text,I18nManager, TouchableOpacity,ScrollView} from 'react-native';
import appTexts from "../../lib/appTexts";
import { styles } from "./styles";

//import DotedContainerItem from "./DotedContainerItem";
//import MoreItem from "./MoreItem";

const OrderItemCard = (props) => {
    const {
      itemImage,
      itemClick
          
        } = props;

    return(    
        
        <View style={styles.fullWidthRowContainer}>
            <View style={styles.rowContainer}>
                <View style={styles.firstContainer}>
                
                <View style={styles.imageContainer}>
                    <Image resizeMode="contain" source={itemImage} style={styles.image}/>
                </View>
                
                <View style={styles.labelContainer}>
                    <View style={styles.labelView}>
                    <View style={styles.textContainer}>
                        <Text style={styles.itemNameText}>{I18nManager.isRTL ? 'تحديد سبب الإلغاءتحديد سبب الإلغاء' :'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder'}</Text>
                        <View style={styles.textStyleLineTwo}>
                        <Text style={styles.basicText}>{appTexts.CART.Noss}</Text>
                        </View>
                        <View style={styles.textStyleLineThree}>
                        <Text style={styles.basicText}>SAR 50 X100</Text>   
                        <Text style={[styles.basicText], { fontWeight: "bold", marginRight: 20 }}>SAR 9800</Text>
                        </View>
                    </View>
                    </View>
                </View>
            </View>
        </View>
        </View>
        
     
      );
      };
      OrderItemCard.propTypes = {
        itemImage:PropTypes.object,
  
};

export default OrderItemCard;
