import { StyleSheet,I18nManager  } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  fullWidthRowContainer:{
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center'
  },
  firstContainer:{
    marginBottom: 40,
    flexDirection:'row',
  },
  image: {
    height: 90,
    width: 90,
  },
  imageContainer:{
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth:1,
    marginRight:'2%',
  },
  textStyleLineTwo:{
      flexDirection: 'row',
      paddingTop:'6%',
  },
  textStyleLineThree:{
      paddingTop:'2%',
      flexDirection:'row',
      justifyContent:'space-between',
  },
  basicText:{
    fontSize: 12,
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
      flexDirection:'row',
  },
  totalText:{
    fontSize: 12,
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
      flexDirection:'row',
      fontWeight: '600'
  },
  labelContainer:{
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 100
  },
  labelView:{
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 60,
    flexDirection:'row'
  },
  itemNameText:{
    color: globals.COLOR.lightBlack,
    //color: globals.COLOR.addressLabelColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 12,
    marginRight: 50
  },
});

export { styles };