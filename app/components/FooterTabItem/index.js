import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text} from 'react-native';
import { images, styles } from "./styles";
import { connect } from "react-redux";

const FooterTabItem = (props) => {
  const {
        tabBarIndex,
        isFocused,
    tabBarLabel,
        isNotificationTab
      } = props;

 const notificationsCount = 0;

 let tabBarImage;
  let tabTextColor = '#3FB851';
 if(parseInt(tabBarIndex) === 0){
    if(isFocused){
      tabTextColor = '#3FB851';
      tabBarImage = images.homeIconSelected;
    }
    else{
      tabTextColor = '#cfcfcf';
      tabBarImage = images.homeIconUnSelected;
    }
 }else if(parseInt(tabBarIndex) === 1){
  if(isFocused){
    tabTextColor = '#3FB851';
    tabBarImage = images.productsIconSelected;
  }
  else{
    tabTextColor = '#cfcfcf';
    tabBarImage = images.productsIconUnSelected;
  }
 }else if(parseInt(tabBarIndex) === 2){
  if(isFocused){
    tabTextColor = '#3FB851';
    tabBarImage = images.favIconSelected;
  }
  else{
    tabTextColor = '#cfcfcf';
    tabBarImage = images.favIconUnSelected;
  }
 }else if(parseInt(tabBarIndex) === 3){
  if(isFocused){
    tabTextColor = '#3FB851';
    tabBarImage = images.ordersIconSelected;
  }
  else{
    tabTextColor = '#cfcfcf';
    tabBarImage = images.ordersIconUnSelected;
  }
 }else if(parseInt(tabBarIndex) === 4){
  if(isFocused){
    tabTextColor = '#3FB851';
    tabBarImage = images.profileIconSelected;
  }
  else{
    tabTextColor = '#cfcfcf';
    tabBarImage = images.profileIconUnSelected;
  }
 }
  return (
    <View style={styles.tabBarItem}>
      <View style={styles.tabBarIconContainer}>
        <Image resizeMode="contain" source={tabBarImage} style={styles.tabIconStyle}/>
        {isNotificationTab && props.notificationsCount > 0 ? 
        <View style={styles.badgeCountContainer}>
            <Text style={styles.badgeCount}>{props.notificationsCount > 9 ? '9+' : props.notificationsCount}</Text>
          </View> : null}
      </View>
      <Text style={[styles.tabLabel, { color: tabTextColor}]}>{tabBarLabel}</Text>
    </View>
  );
};
FooterTabItem.propTypes = {
  tabBarIndex: PropTypes.number,
  isFocused: PropTypes.bool,
  tabBarLabel: PropTypes.string,
  isNotificationTab: PropTypes.bool
};

export default connect(
  (state) => ({ 
    notificationsCount: 0
  })
)(FooterTabItem);