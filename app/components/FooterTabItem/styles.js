import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  homeIconSelected : require("../../assets/images/footerTabIcons/home_activeC.png"),
  homeIconUnSelected : require("../../assets/images/footerTabIcons/homeC.png"),
  productsIconSelected: require("../../assets/images/footerTabIcons/products_activeC.png"),
  productsIconUnSelected: require("../../assets/images/footerTabIcons/productsC.png"),
  favIconSelected: require("../../assets/images/footerTabIcons/search_active.png"),
  favIconUnSelected: require("../../assets/images/footerTabIcons/searchIcon.png"),
  ordersIconSelected: require("../../assets/images/footerTabIcons/myorders_activeC.png"),
  ordersIconUnSelected: require("../../assets/images/footerTabIcons/myordersC.png"),
  profileIconSelected: require("../../assets/images/footerTabIcons/myprofile_activeC.png"),
  profileIconUnSelected: require("../../assets/images/footerTabIcons/myprofileC.png")
};

const styles = StyleSheet.create({
  tabBarItem: {
    width: (globals.SCREEN_SIZE.width / 4),
    height: globals.INTEGER.footerTabBarHeight,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabBarIconContainer:{
    width: (globals.SCREEN_SIZE.width / 4),
    height: globals.INTEGER.footerTabBarHeight-50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  badgeCountContainer:{
    position: 'absolute',
    top: (globals.INTEGER.footerTabBarHeight/2)-15,
    left: ((globals.SCREEN_SIZE.width / 4)/2),
    width: 18,
    height: 18,
    backgroundColor: 'red',
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2
  },
  badgeCount:{
    color:globals.COLOR.white,
    fontFamily: globals.FONTS.helveticaNeueLight,
    textAlign: 'center',
    fontSize: globals.SCREEN_SIZE.width * 0.030
  },
  tabLabel:{
    top:0,
    color:'black',
    fontSize:10,
     fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  tabIconStyle:{
    width:19,
    height:19,
  },
});

export { images, styles  };
