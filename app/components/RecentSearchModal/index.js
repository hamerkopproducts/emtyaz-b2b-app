import PropTypes from 'prop-types';
import React from 'react';
import { Modal, View, StatusBar, Text, TouchableOpacity,Image,FlatList,TextInput } from 'react-native';
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";

const RecentSearchModal = (props) => {

  const {
      isSearchModalVisible,
    closeModal,
    serachText,
    showSelectedSearch,
    setSerachText
      } = props;
  let listData = [{ 'id': 1, name: 'Fresh Food cupboard' }, { 'id': 2, name: 'Fresh Food packets' }, 
  { 'id': 3, name: 'Fresh Food' }, { 'id': 4, name: 'Fresh Sea Food' },
    { 'id': 5, name: 'Dibba Al - Fujairah' },
     { 'id': 6, name: 'Dry Fruits' }, 
     { 'id': 7, name: 'Ready to cook' }, 
     { 'id': 8, name: 'Frozen Food' }]
  const renderItem = ({ item, index }) => <TouchableOpacity style={styles.listRow} onPress={()=>{showSelectedSearch()}}>
    <Text style={styles.itemText}>{item.name}</Text>
    <View style={styles.arrowView}>
      <Image source={images.searchIcon} style={styles.searchIcon} /></View></TouchableOpacity>;
  return (
    <Modal
            animationType="fade"
            transparent={true}
            onRequestClose={() => null}
            visible={isSearchModalVisible}>
            <View style={styles.popupContainer}>
        <TouchableOpacity style={styles.popupBackground} onPress={() => { closeModal()}}/>
              <View style={styles.popupContentContainer}>
          <View style={styles.headerContainer}>
            <StatusBar
              backgroundColor={globals.COLOR.headerColor}
              barStyle="dark-content"
            />
            <View style={styles.headingView}>
            <Text style={styles.headerTitleText}>{'Search Products'}</Text>
            </View>
          </View>
          <View style={styles.searchContainer}>
            <View style={styles.searchView}>
              <View style={styles.searchIocnView}>
              <Image source={images.searchIcon} style={styles.searchIcon} />
              </View>
              <TextInput style={styles.searchInput} value={serachText}
              placeholder={'Search Products'}
                clearButtonMode={'while-editing'}
                onChangeText={val => {
                  setSerachText(val)
                }} autoCapitalize="none"></TextInput>
            </View>
            <Text style={styles.recentSerachText}>{'Recent Searches'}</Text>
            <View style={styles.listContainer}>
            {listData.length === 0 ? <View style={styles.noDataContainer}><Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text></View> :

              <FlatList
                style={styles.flatListStyle}
                data={listData}
                extraData={listData}
                keyExtractor={(item, index) => index.toString()}
                /*onEndReachedThreshold={0.5}
                onEndReached={({ distanceFromEnd }) => {
                  if (listData.length >= (currentPage * pageLimit)) {
                    loadMoreItems();
                  }
                }}*/
                //onRefresh={() => { onRefresh() }}
                //refreshing={isRefreshing}
                showsVerticalScrollIndicator={false}
                renderItem={renderItem} />}
            </View>
          </View>
              </View>
            </View>
  </Modal>
  );
};

RecentSearchModal.propTypes = {
 
};

export default RecentSearchModal;
