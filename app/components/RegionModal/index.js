import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text,TouchableOpacity} from 'react-native';
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Modal from 'react-native-modal';
import {dropDownRegionMapper} from '../../lib/HelpperMethods'
import CustomRadioButton from '../../components/CustomRadioButton';

const RegionModal = (props) => {
  const {
    toggleRegionModal,
    isRegionModalVisible,
    regionList
  } = props;

  return (
    <Modal animationIn="slideInUp" 
      animationOut="slideOutRight" 
      swipeDirection={["left", "right", "up", "down"]}
      isVisible={isRegionModalVisible} 
      style={styles.regionModalView} >
      <View style={styles.regionModalContent}>
      {/* height: 50,
  width: 50,
  backgroundColor: 'pink', */}
  <TouchableOpacity onPress={() => {toggleRegionModal();}}>
        <View style={styles.closeButton}>
            
              <Text style={styles.closeText}>X</Text>
            
        </View>
        </TouchableOpacity>
        <View style={styles.regionHeading}>
          <Text style={styles.regionHeadingStyle}>Select your city</Text>
        </View>

        <FlatList
          data={regionList}
          keyExtractor={(item) => regionList.indexOf(item).toString()}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) => (
            <View style={styles.lineOne}>
              <Text style={styles.smallText}>{item.lang[0].name}</Text>
                <CustomRadioButton
                  id={item.lang[0].region_id}
                  name={item.lang[0].name}
                  onPress={(id, name) =>  {
                    props.setSelectedRegion(id);
                    props.setSelectedRegionName(name);
                  }}
                  selected_id={props.selectedRegionId}
                />
            </View>
          )}
        />
      </View>
    </Modal>
  );
};
RegionModal.propTypes = {
};

export default RegionModal;