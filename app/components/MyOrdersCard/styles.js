import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  fullWidthRowContainer:{
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: 110,
    marginTop:5,
    marginBottom:15,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth:1,
    padding:20
  },
  firstContainer:{
    width: '100%',
    height: 30,
    flexDirection:'row',
    alignItems:'flex-start'
  },
  secondContainer: {
    width: '100%',
    height: 20,
    flexDirection: 'row',
    //backgroundColor: 'green'
  },
  thirdContainer: {
    width: '100%',
    height: 20,
    flexDirection: 'row',
    //backgroundColor: 'yellow'
  },
  orderIdText:{
    width:'60%',
    textAlign: 'left',
    color: globals.COLOR.lightTextColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    fontSize: 12
  },
  deliveryScheduleText:{
    width: '50%',
    textAlign: 'left',
    color:'#919191',
    fontSize: 11,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  deliveryTimeText: {
    width: '50%',
    textAlign: 'left',
    color: globals.COLOR.lightTextColor,
    fontSize: 12,
    fontFamily:I18nManager.isRTL ? globals.FONTS.poppinssemiBold : globals.FONTS.poppinssemiBold,
  },
  scheduleText:{
   
    textAlign: 'right',
    color:'#919191',
    fontSize: 11,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  amountText:{
    textAlign: 'left',
    color: '#242424',
    fontSize: 12,
    fontFamily:I18nManager.isRTL ? globals.FONTS.poppinssemiBold : globals.FONTS.poppinssemiBold,
  },
  statusContainer:{
    width: '40%',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'flex-end'
  },
  amountContainer:{
    width: '50%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  pendingdotView:{
  width:6,
  height:6,
  borderRadius:3,
  marginRight:5,
  backgroundColor:'#DB3236'
  },
  statusText: {
    textAlign: 'right',
    color: globals.COLOR.lightTextColor,
    fontSize: 12,
     fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsLight,
  },
  delivereddotView:{
    width:6,
    height:6,
    borderRadius:3,
    marginRight:5,
    backgroundColor:'#97B337'
  },
  accepteddotView:{
    width:6,
    height:6,
    borderRadius:3,
    marginRight:5,
    backgroundColor:'#1F9245'
  },
  deliveryscheduledotView:{
    width:6,
    height:6,
    borderRadius:3,
    marginRight:5,
    backgroundColor:'#FBA233'
  }
  
});

export { styles };