import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Image, Text, TouchableOpacity,I18nManager } from "react-native";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
const MyOrdersCard = (props) => {
  const { item, itemClick, tabIndex,  } = props;
  

  let statusTextColor =
    item.status === "Pending" 
      ? "#DB3236"
      : item.status === "Delivered"
      ? "#97B337"
      : item.status === "Accepted"
      ? "#1F9245"
      : item.status === "Delivery Scheduled"
      ? "#FBA233"
      : item.status === "Requested"
      ? "#DB3236"
      : item.status === "Approved"
      ? "#1F9245"
      : "none";

  return (
    <TouchableOpacity
      style={styles.fullWidthRowContainer}
      onPress={() => {
        itemClick({});
      }}
    >
      <View style={styles.rowContainer}>
        <View style={styles.firstContainer}>
          <Text style={styles.orderIdText}>{item.orderId}</Text>
        
          <View style={styles.statusContainer}>
            {item.status === "Pending" && (
              <View style={styles.pendingdotView}></View>
            )}
            {item.status === "Delivered" && (
              <View style={styles.delivereddotView}></View>
            )}
            {item.status === "Accepted" && (
              <View style={styles.accepteddotView}></View>
            )}
             {item.status === "Delivery Scheduled" && (
              <View style={styles.deliveryscheduledotView}></View>
            )}
            {item.status === "Requested" && (
              <View style={styles.pendingdotView}></View>
            )}
            {item.status === "Approved" && (
              <View style={styles.accepteddotView}></View>
            )}
            <Text style={[styles.statusText, { color: statusTextColor }]}>
              {item.status === 'Pending' && (I18nManager.isRTL ? 'تحت المراجع' :'Pending')}
              {item.status === 'Delivered' && (I18nManager.isRTL ? 'تم التوصيل' :'Delivered')}
              {item.status === 'Accepted' && (I18nManager.isRTL ? 'تم القبول' :'Accepted')}
              {item.status === 'Delivery Scheduled' && (I18nManager.isRTL ? 'جدول التوصيل' :'Delivery Scheduled')}
              {item.status === 'Requested' && (I18nManager.isRTL ? 'تم رفع طلبك' :'Requested')}
              {item.status === 'Approved' && (I18nManager.isRTL ? 'تم الموافقة' :'Approved')}

            </Text>
          </View>

        </View>
        <View style={styles.secondContainer}>
          {tabIndex === 0 &&(<Text style={styles.deliveryScheduleText}>{appTexts.MY_ORDERS.DeliverySchedule}</Text>)}
          {tabIndex === 1 &&(<Text style={styles.deliveryScheduleText}>{appTexts.MY_ORDERS.QuotationDate}</Text>)}
          <View style={styles.amountContainer}>
          {tabIndex === 0 &&(<Text style={styles.scheduleText}>{appTexts.MY_ORDERS.OrderAmount}</Text>)}
          {tabIndex === 1 &&(<Text style={styles.scheduleText}>{appTexts.MY_ORDERS.QuotationAmount}</Text>)}
          </View>
        </View>
        <View style={styles.thirdContainer}>
          <Text style={styles.deliveryTimeText}>{item.deliveryTime}</Text>
          <View style={styles.amountContainer}>
            <Text style={styles.amountText}>{item.orderAmount}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
MyOrdersCard.propTypes = {
  item: PropTypes.object,
  tabIndex: PropTypes.number,
};

export default MyOrdersCard;