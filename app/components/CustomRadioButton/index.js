import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { styles } from "./styles";


const Customradiobutton = props => {
  let id = '';
  try {
    id = props.id;
  } catch(err) {
    id = '';
  }
  let name = '';
  try {
    name = props.name;
  } catch(err) {
    name = '';
  }

  return (
    <View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={() => { props.onPress(id, name) } }>
          <View style={styles.circle} >
            { props.selected_id == id ? (<View style={styles.checkedCircle} />) : (<View />)}
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Customradiobutton;

