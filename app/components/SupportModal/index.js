
import React from "react";
import { View, Text, TouchableOpacity, I18nManager ,Image,TextInput} from "react-native";
import appTexts from "../../lib/appTexts";
import globals from "../../lib/globals";
import { styles } from "./styles";
import Modal from "react-native-modal";
import DropDownPicker from "react-native-dropdown-picker";

  const SupportModal = ({ isSupportModalVisible, onColse }) => {
  return (
    <Modal
            isVisible={isSupportModalVisible}
            style={styles.modalMaincontentHelp}
          >
            <View style={{backgroundColor: 'white', padding: 20}}>
              <TouchableOpacity
                style={styles.buttonwrapper}
                onPress={() => {
                  onColse(true);
                }}
              >
                <Text style={styles.closeButton}>X</Text>
                <View style={{flexDirection: 'row'}}>
                <Text style={styles.helpText}>{appTexts.SUPPORT.Message}</Text>
                </View>
                <View style={styles.contacts}>
                  <Text style={styles.cont}>
                    {appTexts.SUPPORT.contact}
                  </Text>
                  <View>
                  <View style= {styles.contactTitle}>
                    <Image source={require("../../assets/images/myOrders/call_icon.png")} />
                    
                    <Text style={styles.contactdetails}>
                        00002345676567
                    </Text>
                  </View>
                  <View style={styles.contactTitle}>
                    <Image source={require("../../assets/images/myOrders/mail_icon.png")} />
                    
                    <Text style={styles.contactdetails}>
                    support.emtyaz.com
                    </Text>
                  </View>
                  </View>
                </View>
              </TouchableOpacity>
              <View style={{ paddingBottom: "5%", paddingTop: "2%" }}>
                <DropDownPicker
                  items={[{ label: "Damaged", value: "Damaged" }, { label: "Differnet Product", value: "Differnet" }]}
                  dropDownStyle={{placeholderTextColor: "red"}}
                  placeholder={appTexts.SUPPORT.orderissue}
                  labelStyle={{
                    fontSize: 13,
                    fontFamily: I18nManager.isRTL
                      ? globals.FONTS.notokufiArabic
                      : globals.FONTS.poppinsRegular,
                    textAlign: "left",
                    color: '#242424',
                  }}
                  containerStyle={{ height: 30 }}
                  style={styles.dropDownStyle}
                  itemStyle={{
                    justifyContent: "flex-start",
                  }}
                  dropDownStyle={{ backgroundColor: "white" }}
                />
                <TextInput
                  style={styles.inputmessage}
                  underlineColorAndroid="transparent"
                  placeholder={appTexts.SUPPORT.Type}
                  placeholderTextColor="#C9C9C9"
                  autoCapitalize="none"
                />
              </View>
              <View style={styles.buttonWrappers}>
                <TouchableOpacity onPress={() => console.log("click")}>
                  <View style={styles.submitButton}>
                    <Text style={styles.sendbuttonText}>
                      {appTexts.SUPPORT.Send}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
  );
};


export default SupportModal;
