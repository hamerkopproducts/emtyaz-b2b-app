import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  searchIcon: require("../../assets/images/header/home-search.png")
};

const styles = StyleSheet.create({
	popupContainer:{
      justifyContent: 'flex-end',
      flex: 1,
      backgroundColor: 'rgba(0,0,0,0.5)',
      marginBottom: 20
    },
    popupBackground:{
      flex:1,
      backgroundColor: globals.COLOR.transparent,
    },
  popupContentContainer: {
    flexDirection: 'column',
    justifyContent: "flex-end",
    alignItems: 'center',
  },
  popupContent: {
    
    backgroundColor: globals.COLOR.white,
    alignItems: 'flex-end',
  },
  closeContainer:{

    minHeight: 50,
    alignItems: 'flex-end',
  },
  popupClose: {
    minHeight: 50,
    width: 40,
    marginTop: 10,
  },
  popupCloseText: {
    flex: 1,
    textAlign: 'right',
    color: 'grey',
    
    fontSize: 14,
  },
  sortByText: {
    textAlign: 'left',
    color: globals.COLOR.lightTextColor,
    fontSize: 24,
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS. poppinsMedium,
  },

  textContainer: {
    marginLeft: '5%',
    marginRight: '5%',
    width: '90%',
    flexDirection: 'row'
  },

  text: {
    color: 'white',
    fontSize: 20,
  },
  container: {
    alignItems: 'center',
    flexDirection: 'row'
  },
  leftHeadingContainer: {
    flex: 1,
    alignSelf: 'flex-end'
    
  },
  filterLabel: {
    
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    marginLeft: 5,
    marginRight: 5,
    color: globals.COLOR.white,
   //fontFamily: globals.FONTS.poppinsMedium,
    fontSize: 13
  },
  applyContainer: {
    padding: 10,
    marginRight: '1%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  resetContainer: {
    backgroundColor: globals.COLOR.themeOrange,
    minHeight: 40,
    width: '20%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  filterContainer: {
    justifyContent: "flex-end"
  },
  filterItems: {
    paddingVertical:5,
    flexDirection: 'row',
    alignItems: 'center'
  },
  filterBullet: {
    height: 15,
    width: 15,
  },
  filterItemsLabel: {
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
   marginLeft: 10,
  },
  radioButton: {
    color: 'grey',
    flex: 1,
    flexDirection: 'row-reverse'
  },
  
  uncheckedRadio: {
    
    justifyContent: 'center', 
    alignItems: 'center', 
    borderRadius: 10, 
    height: 20, 
    width: 20, 
    borderWidth: 1, 
    borderColor: 'grey'
  },
  
});

export { images, styles  };
