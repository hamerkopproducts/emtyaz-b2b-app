import PropTypes from "prop-types";
import React, { useCallback, useState } from "react";
import { Modal, View, Text, TouchableOpacity, I18nManager } from "react-native";
import appTexts from "../../lib/appTexts";
import globals from "../../lib/globals";
import { styles } from "./styles";

const filterItems = [
  {
    id: "Pending",
    text: I18nManager.isRTL ? "تحت المراجع" : "Pending",
    onPress: () => {},
    color: "red",
  },
  {
    id: "Accepted",
    text: I18nManager.isRTL ? "تم القبول" : "Accepted",
    onPress: () => {},
    color: "#30A739",
  },
  {
    id: "Delivered",
    text: I18nManager.isRTL ? "تم التوصيل" : "Delivered",
    onPress: () => {},
    color: "#86A82A",
  },
  {
    id: "Delivery Scheduled",
    text: I18nManager.isRTL ? "جدول التوصيل" : "Delivery Scheduled",
    onPress: () => {},
    color: "#F39A17",
  },
  {
    id: "Requested",
    text: I18nManager.isRTL ? "تم رفع طلبك" : "Requested",
    onPress: () => {},
    color: "red",
    isQuation: true,
  },
  {
    id: "Approved",
    text: I18nManager.isRTL ? "تم الموافقة" : "Approved",
    onPress: () => {},
    color: "#30A739",
    isQuation: true,
  },
];

const RenderBarButton = ({ text,bgcolor, onPress = () => {} }) => (
  <TouchableOpacity
    style={[styles.applyContainer,{ backgroundColor: bgcolor}]}
    onPress={onPress}
  >
    <Text style={styles.filterLabel}>{text}</Text>
  </TouchableOpacity>
);

const RenderItem = ({ id, text, active, onPress = () => {}, color, show }) => {
  if (show) return null;
  return (
    <TouchableOpacity style={styles.filterItems} onPress={() => onPress()}>
      <View style={styles.filterItems}>
        <View style={[styles.filterBullet, { backgroundColor: color }]} />
        <Text style={styles.filterItemsLabel}>{text}</Text>
      </View>
      <View style={styles.radioButton}>
        <View style={styles.uncheckedRadio}>
          <View
            style={[
              { height: 10, width: 10, borderRadius: 10 },
              { backgroundColor: active ? "grey" : "white" },
            ]}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const OrderFilterModal = ({
  isMyOrderFilterModalVisible,
  closeFiltermodal,
  myQuation = true,
  navigation
}) => {
  const [selectedRadio, onPressRadio] = useState("");
  return (
    <Modal
      animationType="fade"
      transparent={true}
      onRequestClose={() => null}
      visible={isMyOrderFilterModalVisible}
    >
      <View style={styles.popupContainer}>
          <TouchableOpacity
            style={styles.popupBackground}
            onPress={() => {
              closeFiltermodal();
            }}
          />
        <View style={{ backgroundColor: "white", paddingHorizontal: 20, paddingVertical: 10 }}>
          <View style={styles.container}>
            
            <View style={styles.leftHeadingContainer}>
              <Text style={styles.sortByText}>{appTexts.MY_ORDERS.Filter}</Text>
            </View>
            <RenderBarButton
              text={appTexts.CATEGORY_CONTENT.Button1}
              onPress={() => {}}
              bgcolor={globals.COLOR.themeGreen}
            />
            <RenderBarButton 
            text={appTexts.CATEGORY_CONTENT.Button2}
            onPress={() => {}} 
            bgcolor={globals.COLOR.themeOrange}/>
             <TouchableOpacity
              style={styles.popupClose}
              onPress={() => {
                closeFiltermodal();
              }}
            >
              <Text style={styles.popupCloseText}>{'X'}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.filterContainer}>
            {filterItems.map(({ id, text, color, isQuation }) => (
              <RenderItem
                key={id}
                id={id}
                text={text}
                active={selectedRadio === id}
                color={color}
                onPress={() => {
                  onPressRadio()
                  closeFiltermodal()
                  navigation.navigate('OrderStatusDetails')
                }
                }
                isQuation={isQuation}
                show={(myQuation && isQuation) || (!myQuation && !isQuation)}
              />
            ))}
          </View>
        </View>
      </View>
    </Modal>
  );
};

OrderFilterModal.propTypes = {};

export default OrderFilterModal;
