import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text,TouchableOpacity} from 'react-native';
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Modal from 'react-native-modal';
import CustomRadioButton from '../../components/CustomRadioButton';

const QuantityModal = (props) => {
  const {
    toggleQuantityModal,
    isQuantityModalVisible,
    b2bQuantity
  } = props;

  return (
    <Modal animationIn="slideInUp" 
      animationOut="slideOutRight" 
      swipeDirection={["left", "right", "up", "down"]}
      isVisible={isQuantityModalVisible} 
      style={styles.quantityModalView}
      onBackdropPress={() => toggleQuantityModal() }
    >
      <View style={styles.quantityModalContent}>
        <View style={styles.closeButton}>
            <TouchableOpacity onPress={() => {toggleQuantityModal();}}>
              <Text style={styles.closeText}>X</Text>
            </TouchableOpacity>
        </View>
        <View style={styles.quantityHeading}>
          <Text style={styles.quantityHeadingStyle}>{appTexts.DETAILSSCREEN.HeadingText}</Text>
        </View>

        <FlatList
          data={b2bQuantity}
          keyExtractor={(item) => b2bQuantity.indexOf(item).toString()}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) => (
            <View style={styles.lineOne}>
              <Text style={styles.smallText}>{item.variant.name}</Text>
                <CustomRadioButton
                  id={item.attribute_variant_id}
                  name={item.variant.name}
                  onPress={(id, name) =>  {
                    props.setSelectedVariant(id);
                    props.setSelectedVariantName(name);
                  }}
                  selected_id={props.selectedVariantId}
                />
            </View>
          )}
        />
      </View>
    </Modal>
  );
};
QuantityModal.propTypes = {
};

export default QuantityModal;