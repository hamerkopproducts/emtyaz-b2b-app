import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";

const styles = StyleSheet.create({


  quantityModalView:{
    margin:0,
},
quantityModalContent:{
  //height: '40%',
  marginTop: 'auto',
  backgroundColor:'white',
  paddingLeft:'5%',
  paddingRight:'5%',
  paddingBottom:'3%',
},
closeButton:{
  //justifyContent:'flex-end',
  //alignItems:'flex-end',
  alignSelf:'flex-end',
  paddingRight:'3%',
   paddingTop:'4%',
  // paddingRight:'3%',
    paddingLeft:'3%',
  //   //paddingTop:'2%',
    paddingBottom:'2%',
},
closeText:{
  fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    fontSize: 15,
    color:'grey',
},
quantityHeading:{
  flexDirection: 'row',
  paddingTop:'1%',
  paddingBottom:'2%',
},
quantityHeadingStyle:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
fontSize: 17,
},
lineOne:{
  flexDirection:'row',
  justifyContent:'space-between',
  paddingTop:'3%',
  paddingBottom:'3%',
},
smallText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
  fontSize: 14,
},

});

export { styles };