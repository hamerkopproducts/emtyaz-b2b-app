import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text,TouchableOpacity} from 'react-native';
import { styles } from "./styles";
import appTexts from "../../../lib/appTexts";


const MoreItem = (props) => {
  const {
        itemImage,
        nameLabel,
        onItemClick,
      } = props;

  return (
    <TouchableOpacity onPress={() => {onItemClick();}}>
  <View style={styles.sizeSelectView}>
    <Text style={styles.symbolText}>{appTexts.MOREITEM.more}</Text>
      <Image source={require('../../../assets/images/cartScreenIcons/refresh/refresh.png')} />
  </View>
  </TouchableOpacity>
  );
};
MoreItem.propTypes = {
  itemImage:PropTypes.number,
  nameLabel:PropTypes.string,
  onItemClick:PropTypes.func,
};

export default MoreItem;