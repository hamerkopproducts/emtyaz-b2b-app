import React, { Component } from "react";
import PropTypes from "prop-types";
import { TouchableOpacity, Image } from "react-native";
import { styles } from "./styles";

const PlusButton = (props) => {
  const { onButtonClick } = props;

  return (
    <TouchableOpacity
      style={styles.buttonView}
      onPress={() => {
        onButtonClick();
      }}
    >
      <Image
        style={styles.itemImage}
        source={require("../../../assets/images/products/+.png")}
      />
    </TouchableOpacity>
  );
};
PlusButton.propTypes = {};

export default PlusButton;
