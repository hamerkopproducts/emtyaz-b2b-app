import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Image, Text ,I18nManager} from "react-native";
import { styles } from "./styles";
import { TouchableOpacity } from "react-native-gesture-handler";

const DotedContainerItem = (props) => {
  const { qty, onEventClick, id, color } = props;
  
  return (
    <TouchableOpacity
      onPress={() => {
        onEventClick(id);
      }}
    >
      <View style={[
        styles.sizeSelectView, 
        (props.color && props.color != '') && {borderColor: 'green'} 
        ]}>
        <Text style={styles.symbolText}>{"X"}</Text>
        <Text style={styles.countText}>{qty}</Text>
      </View>
    </TouchableOpacity>
  );
};
DotedContainerItem.propTypes = {
  itemImage: PropTypes.number,
  nameLabel: PropTypes.string,
};

export default DotedContainerItem;
