import { StyleSheet,I18nManager  } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  fullWidthRowContainer:{
    width: '100%',
    height: 160,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: 160,
    justifyContent: 'center',
    alignItems: 'center'
  },
  firstContainer:{
    width: '100%',
    height: 120,
    paddingTop:10,
    paddingBottom:10,
    flexDirection:'row'
  },
  secondContainer: {
    width: '100%',
    height: 40,
    flexDirection: 'row'
  },
  image: {
    height: 90,
    width: 90,
  },
  imageContainer:{
    height: 100,
    width:100,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth:1
  },
  labelContainer:{
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 100,
  },
  labelView:{
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 60,
    flexDirection:'row'
  },
  sizeSelectContainer: {
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 40
  },
  sizeSelectView:{
    marginLeft:20,
    width: 80,
    height: 30,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth: 1
  },
  itemNameText:{
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 12
  },
  sizeText:{
    marginRight:20,
    color: globals.COLOR.addressLabelColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 11
  },
  dropDownArrow: {
   width:12,
   height:10, 
  },
  textContainer:{
    marginLeft: 20,
    width: globals.INTEGER.screenWidthWithMargin - 160,
    height: 60,
  },
  favIconContainer: {
    alignItems: 'flex-end',
    width:40,
    height: 60
  },
  nameLabel:{
    top:0,
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 11,
    height: 20
  },
  addButtonContainer:{
    position:'absolute',
    right:0,
    bottom:20
  },
  quantityPackView:{
    margin:0,
  },
  closeButton:{
    //justifyContent:'flex-end',
    //alignItems:'flex-end',
    alignSelf:'flex-end',
    paddingRight:'4%',
    paddingTop:'1%',
  },
  closeText:{
    fontSize:18,
  },
  packHeadingText:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    fontSize: 16, 
  },
  packModalHeading:{

  },
  packModalContent:{
    height: '50%',
    marginTop: 'auto',
    backgroundColor:'white',
    
  },
  packModalContentView:{
    paddingLeft:'5%',
    paddingRight:'8%',
  },
  touchableopacityWrapper:{
   
   paddingTop:'6%',
  
  
  },
  boxWrapper:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    
    
  },
  boxText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.poppinsRegular,
    fontSize: 14,   
    paddingLeft:'2%',
  },
  inputBox:{
    borderWidth:1,
    width:'90%',
    padding:'1%',
   
    borderColor:globals.COLOR.lightGray,
      },
      
    closeButton:{
      //justifyContent:'flex-end',
      //alignItems:'flex-end',
      alignSelf:'flex-end',
      paddingRight:'4%',
      paddingTop:'2%',
    },
    xText:{
      fontSize:18,
    },
    packSizeModalView:{
      margin:0,
 },
 packSizeModalContent:{
  height: '50%',
  marginTop: 'auto',
  backgroundColor:'white',
  paddingLeft:'5%',
  paddingRight:'5%',
  //backgroundColor:'red',
},
    packSizeModalTopView:{
      paddingTop:'2%',
      //paddingLeft:'3%',
      paddingRight:'3%',
      flexDirection:'row',
      justifyContent:'space-between',
      //backgroundColor:'yellow',
           
    },
    headingLine:{
      flexDirection:'row',
      justifyContent:'space-between',
      
      
      
    },
    headingStyle:{
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
      fontSize: 16, 
    },
    buttonWrap:{
      justifyContent:'flex-end',
      flexDirection:'row',
      
    },
    saveButton:{
      width:60,
      height:55,
           //padding:'2%',
    },
    saveButtonStyle:{
      backgroundColor:'green',
      //height:'30%',
      justifyContent:'center',
      alignItems:'center',
      //alignSelf:'center',
    },
    saveButtonText:{
      fontSize:12,
      color:'white',
      textAlign:'center',
    },
    resetButton:{
      width:60,
      height:55,
      marginLeft:'2%',
    
      
    },
    resetButtonStyle:{
      backgroundColor:'orange',
      //height:'30%',
      justifyContent:'center',
      alignItems:'center',
      //alignSelf:'center',
    },
    resetButtonText:{
      fontSize:12,
      color:'white',
      textAlign:'center',
    },
    xButton:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      paddingTop:'3%',
    },
    boxOne:{
      borderWidth:1,
     // borderColor:globals.COLOR.lightGray,
      width:'60%',
      padding:'5%',
      borderColor:'red',
    },

    touchableopacityWrap:{
   
        paddingTop:'5%',
     },
     boxWrap:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      paddingBottom:'5%',
      
      
    },
    boxInput:{
      borderWidth:0.5,
      width:'90%',
      padding:'1.5%',
     
      borderColor:globals.COLOR.lightGray,
        },
        boxTextInput:{
          fontFamily: globals.FONTS.poppinsRegular,
          fontSize: 13,   
          paddingLeft:'1%',
        },
textInputWrap:{
  flexDirection:'row',
},
textOne:{
  flexBasis:'20%',
  paddingLeft:'1%',
  //backgroundColor:'red',
  
},
textTwo:{
  flexBasis:'20%',
  //backgroundColor:'yellow',
  justifyContent:'center',
  alignItems:'center',
  
},
boxTextTwoInput:{
  fontSize:10,
  color:'grey',
  //alignSelf:'center',
  //textAlign:'center',
},
textThree:{
  flexBasis:'24%',
  //backgroundColor:'green',
  justifyContent:'center',
  alignItems:'center',
  
},
boxTextThreeInput:{
  fontSize:12,
},
textFour:{
  flexBasis:'15%',
  backgroundColor:'green',
  justifyContent:'center',
  alignItems:'center',
  marginLeft:'5%',
},
boxTextFourInput:{
  fontSize:10,
  color:'white',

},
butWrapper:{
  flexDirection:'row',
  alignSelf:'flex-end',
 // paddingTop:'3%',
  paddingBottom:'3%',
},
buttonStyle: {
  backgroundColor: 'green',
  width: 48,
  height: 25,
  justifyContent: "center",
  alignItems: "center",
},
buttonText: {
  color: globals.COLOR.white,
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  fontSize: 10,
},
buttonTwoStyle:{
  backgroundColor: 'orange',
  width: 48,
  height: 25,
  justifyContent: "center",
  alignItems: "center",
  marginLeft:'6%',
},
  
});

export { styles };
