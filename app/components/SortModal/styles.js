import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  searchIcon: require("../../assets/images/header/home-search.png")
};

const styles = StyleSheet.create({
	popupContainer:{
      width: '100%',
    	height: '100%',
      flexDirection: 'column',
      backgroundColor: globals.COLOR.transparent
    },
    popupBackground:{
      width: '100%',
      height: '100%',
      backgroundColor: globals.COLOR.transparent,
    	zIndex: 1
    },
    popupContentContainer:{
    	top:0,
    	position:'absolute',
      height: '100%',
    	width: '100%',
    	flexDirection: 'column',
      alignItems: 'center',
      justifyContent: "flex-end",
      zIndex: 3
    },
  popupContent:{
        height: '40%',
        width: '100%',
    backgroundColor: globals.COLOR.white,
    alignItems: 'flex-end',
      },
  popupClose:{
    height: 30,
    width: 40,
    marginTop: 10,
  },
  popupCloseText:{
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontSize: 24
  },
  sortByText:{
    marginLeft:'5%',
    width:'95%',
    textAlign: 'left',
    color: globals.COLOR.lightTextColor,
    fontSize: 24
  },
  flatListStyle:{
    marginTop:20,
    marginLeft: '5%',
    width: '95%',
  },
  listItem:{
    width: '95%',
    height:40
  },
  labelView: {
    width: '100%',
    height: 40,
    flexDirection: 'row'
  },
  discountTextContainer: {
    width: '90%',
    height: 40,
    justifyContent: 'center'
  },
  textContainer: {
    marginLeft: '5%',
    marginRight: '5%',
    width: '90%',
    flexDirection: 'row'
  },
  favIconContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: '10%',
    height: 40
  }
});

export { images, styles  };
