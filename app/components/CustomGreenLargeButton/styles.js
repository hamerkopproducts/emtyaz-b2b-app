import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";

const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: globals.COLOR.themeGreen,
    width: 315,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: globals.COLOR.white,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15,
  },
});

export { styles };
