import PropTypes from "prop-types";
import React, { Component } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { styles } from "./styles";

const CustomGreenLargeButton = (props) => {
  const { buttonText } = props;

  return (
     <View style={styles.buttonStyle}>
     <Text style={styles.buttonText}>{buttonText}</Text>
    </View>
    );
};

CustomGreenLargeButton.propTypes = {
  buttonText: PropTypes.string,
};

export default CustomGreenLargeButton;
