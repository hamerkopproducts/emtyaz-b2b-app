import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  backButtonImage:require("../../assets/images/header/icn-back.png"),
  languageFlagImage: require("../../assets/images/header/lang.png"),
  logo: require("../../assets/images/header/Emtyaz_logo_white.png"),
  rightIcon: require("../../assets/images/header/cart.png"),
  searchIcon: require("../../assets/images/header/search_white.png")
};

const styles = StyleSheet.create({
  headerContainer: {
    flex: 1,
    height: globals.INTEGER.headerHeight,
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: globals.DEVICE_TYPE === 1 ? globals.INTEGER.headerHeight : 0,
    backgroundColor: globals.COLOR.headerColor
  },
  leftIconContainer:{
    marginTop:0,
    paddingLeft: globals.SCREEN_SIZE.width <=320 ? globals.MARGIN.marginEight : globals.MARGIN.marginFifteen,
    paddingRight:globals.SCREEN_SIZE.width <=320 ? globals.MARGIN.marginEight : globals.MARGIN.marginFifteen,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    height : "100%",
    left:0
  },
    headerTitleContainer: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    height : "100%",
    alignSelf:'center'
  },
  headerTitleText: {
    textAlign: "center",
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    // fontSize: globals.SCREEN_SIZE.width * 0.04,
    fontSize:15
  },
  rightIconContainer:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    height : "100%",
    right: 0
  },
  rightIcon: {
    marginRight: globals.MARGIN.marginFifteen
  },
  bellIcon:{
    marginRight: globals.MARGIN.marginFifteen,
  
    height: 20,
    width: 17,
    resizeMode:'cover'
  },
  badgeContainer: {
    position: 'absolute',
    top: 5,
    right: 5,
    height: 20,
    width: 20,
    borderRadius: 20,
    backgroundColor: 'red',
    justifyContent: "center",
    alignItems: "center"
  },
  notificationCountText: {
    textAlign: "center",
    color: globals.COLOR.white,
    fontSize: 12
  },
  backArrow:{
    height: 19,
    width: 18,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
    //resizeMode:'cover'
  }
});

export { images, styles  };
