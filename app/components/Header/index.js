import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import { StatusBar, View, Text, Image, TouchableOpacity, I18nManager } from 'react-native';
import globals from "../../lib/globals"
import RNRestart from 'react-native-restart';

import { images, styles } from "./styles";
import { connect } from 'react-redux';

const Header = (props) => {
  const {
    isBackButtonRequired,
    isLanguageButtonRequired,
    isRightButtonRequired,
    isLogoRequired,
    headerTitle,
    isSearchButtonRequired,
    onCartButtonPress,
    onSearchPress,
    customHeaderStyle,
    onBackButtonPress
  } = props;

  let cart_count = '';
  try {
    cart_count = props.cartItems.length;
  } catch(err) {
    cart_count = '';
  }
  const [count, setCount] = useState(cart_count);
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    cart_count = props.cartItems.length;
    setCount(cart_count);
  };

  let headerStyle = styles.headerContainer;
  if (customHeaderStyle)
    headerStyle = customHeaderStyle;
    const switchlang = async() => {
      if (I18nManager.isRTL) {
        I18nManager.forceRTL(false);
      } else {
        I18nManager.forceRTL(true);
      }
      setTimeout(() => {
        RNRestart.Restart();
      }, 500);
    }
  return (
    <View style={[headerStyle]}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={globals.COLOR.themeGreen}
       
      />

      {isBackButtonRequired ? (
        <TouchableOpacity style={styles.leftIconContainer}
          onPress={()=>{onBackButtonPress()}}>
          <Image resizeMode="contain" source={images.backButtonImage}
            style={styles.backArrow} />
        </TouchableOpacity>
      ) : null}
      {isLanguageButtonRequired ? (
        <TouchableOpacity style={styles.leftIconContainer}
          onPress={() => { switchlang()}}>
          <Text style={styles.headerTitleText}>{I18nManager.isRTL ? 'EN': 'AR'}</Text>
        </TouchableOpacity>
      ) : null}
      <View style={styles.headerTitleContainer}>
        {isLogoRequired ? (<Image source={images.logo} resizeMode="contain" style={{ alignItems: "stretch" }} />)
          :
          (<Text style={styles.headerTitleText}>{headerTitle}</Text>)
        }
      </View>

      <View style={styles.rightIconContainer}>
        {isRightButtonRequired ? (
          <TouchableOpacity style={[styles.rightIconContainer, isSearchButtonRequired && { right: 40}]} onPress={()=>{onCartButtonPress()}}>
            <Image source={images.rightIcon} style={styles.rightIcon} />
            <View style={styles.badgeContainer}>
              <Text style={styles.notificationCountText}>{ count }</Text>
            </View>
          </TouchableOpacity>
        ) :null}
        {isSearchButtonRequired ? (
        <TouchableOpacity style={styles.rightIconContainer} onPress={() => { onSearchPress()}}>
            <Image source={images.searchIcon} style={styles.rightIcon} />
          </TouchableOpacity>) :null}

      </View>

    </View>
  );
};

Header.propTypes = {
  isBackButtonRequired: PropTypes.bool,
  isLanguageButtonRequired: PropTypes.bool,
  isRightButtonRequired: PropTypes.bool,
  isLogoRequired: PropTypes.bool,
  //headerTitle: PropTypes.string,
  headerTitle: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  isSearchButtonRequired: PropTypes.bool,
  onCartButtonPress: PropTypes.func,
  onSearchPress: PropTypes.func,
  customHeaderStyle: PropTypes.object,
  onBackButtonPress: PropTypes.func
};

const mapStateToProps = (state, props) => {
  return {
    cartItems: state.homepageReducer.cartItems
  }
};

const HeaderWithRedux = connect(
  mapStateToProps
)(Header);

export default HeaderWithRedux;
