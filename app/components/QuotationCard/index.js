import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity,I18nManager} from 'react-native';
import { styles } from "./styles";

const quotationCard = (props) => {
  const {
        itemImage,
        itemClick
      } = props;

  return (
    <TouchableOpacity style={styles.fullWidthRowContainer}>
    <View style={styles.rowContainer}>
      <View style={styles.firstContainer}>
        <View style={styles.imageContainer}>
          <Image resizeMode="contain" source={itemImage} style={styles.image}/>
        </View>
        <View style={styles.labelContainer}>
          <View style={styles.labelView}>
            <View style={styles.textContainer}>
                <Text style={styles.itemNameText}>{I18nManager.isRTL ? 'تحديد سبب الإلغاءتحديد سبب الإلغاء' :'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder'}</Text>
            </View>
                      </View>
          <View style={styles.sizeSelectContainer}>
            <View style={styles.sizeSelectView}>
              <Text style={styles.sizeText}>{I18nManager.isRTL ? '2 X 100حبه' :'100 pc X 2'}</Text>
               
            </View>
            
          </View>
          
        </View>
      </View>
     
      
      </View>
      </TouchableOpacity>
  );
};
quotationCard.propTypes = {
  itemImage:PropTypes.number
};

export default quotationCard;