import PropTypes from "prop-types";
import React, { useCallback, useState } from "react";
import {
  Modal,
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  TextInput,
  I18nManager,
} from "react-native";

import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import Slider from "rn-range-slider";
import Thumb from "./Slider/Thumb";
import Rail from "./Slider/Rail";

import RailSelected from "./Slider/RailSelected";
import Notch from "./Slider/Notch";
import Label from "./Slider/Label";
import DividerLine from "../DividerLine";
import DotedContainerItem from "./DotedContainerItem";

const FilterModal = (props) => {
  const { isFilterModalVisible, closeModal, brands, brandFilters, applyBrandFilter } = props;

  const [rangeDisabled, setRangeDisabled] = useState(false);
  const [low, setLow] = useState(0);
  const [high, setHigh] = useState(1000);
  const [min, setMin] = useState(0);
  const [max, setMax] = useState(1000);
  const [floatingLabel, setFloatingLabel] = useState(false);

  const [selectedFilters, setSelectedFilters] = useState(brandFilters);

  const applyFilter = () => {
    applyBrandFilter(selectedFilters);
  }

  const selectFilterCat = (id) => {
    let tempFilters = [...selectedFilters];
    const index = tempFilters.indexOf(id);
    if(index == -1) {
      tempFilters.push(id);
    } else {
      tempFilters.splice(index, 1);
    }
    setSelectedFilters(tempFilters);
  }

  const renderThumb = useCallback(() => <Thumb />, []);
  const renderRail = useCallback(() => <Rail />, []);
  const renderRailSelected = useCallback(() => <RailSelected />, []);
  const renderLabel = useCallback((value) => <Label text={value} />, []);
  const renderNotch = useCallback(() => <Notch />, []);
  const handleValueChange = useCallback((low, high) => {
    setLow(low);
    setHigh(high);
  }, []);

  let listData = [];
  try{
    listData = brands.data.brand;
  } catch(err) {
    listData = [];
  }

  const renderItem = ({ item, index }) => (
    <View style={styles.listItem}>
      <View style={styles.labelView}>
        <View style={styles.discountTextContainer}>
          <Text style={styles.discountText}>{item.lang[0].name}</Text>
        </View>
        <TouchableOpacity style={styles.favIconContainer} onPress={() => {
          selectFilterCat(item.id)
        }}>
          <Image
            source={
              selectedFilters.indexOf(item.id) !== -1
                ? require("../../assets/images/modal/checked.png")
                : require("../../assets/images/modal/uncheck.png")
            }
          />
        </TouchableOpacity>
      </View>
      <DividerLine />
    </View>
  );

  return (
    <Modal
      animationType="fade"
      transparent={true}
      onRequestClose={() => null}
      visible={isFilterModalVisible}
    >
      <View style={styles.popupContainer}>
        <TouchableOpacity
          style={styles.popupBackground}
          onPress={() => {
            closeModal();
          }}
        />
        <View style={styles.popupContentContainer}>
          <View style={styles.popupContent}>
            <View style={styles.closeContainer}>
              <TouchableOpacity
                style={styles.popupClose}
                onPress={() => {
                  closeModal();
                }}
              >
                <Text style={styles.popupCloseText}>{"X"}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.container}>
              <View style={styles.leftHeadingContainer}>
                <Text style={styles.sortByText}>
                  {appTexts.MY_ORDERS.Filter}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.applyContainer}
                onPress={() => {
                  closeModal();
                  applyFilter();
                }}
              >
                <Text style={styles.filterLabel}>
                  {appTexts.CATEGORY_CONTENT.Button1}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.resetContainer}
                onPress={() => {
                  props.resetBrandFilter(brandFilters);
                  closeModal();
                }}
              >
                <Text style={styles.filterLabel}>
                  {appTexts.CATEGORY_CONTENT.Button2}
                </Text>
              </TouchableOpacity>
            </View>
            <Text style={styles.priceText}>{""}</Text>
            <Slider
              style={styles.slider}
              min={min}
              max={max}
              step={1}
              disableRange={rangeDisabled}
              floatingLabel={floatingLabel}
              renderThumb={renderThumb}
              renderRail={renderRail}
              renderRailSelected={renderRailSelected}
              //renderLabel={renderLabel}
              renderNotch={renderNotch}
              onValueChanged={handleValueChange}
            />
            <View style={styles.horizontalContainer}>
              <Text style={styles.valueText}>{"AR " + low}</Text>
              <Text style={styles.valueText}>{"AR " + high}</Text>
            </View>
            <DividerLine />
            <Text style={styles.quantityText}>
              {appTexts.PRODUCT_FLITER.Quantity}
            </Text>
            <View style={styles.dotedItemContainer}>
              <DotedContainerItem text={"1"} unit={"Kg"} />
              <DotedContainerItem text={"2"} unit={"Kg"} />
              <DotedContainerItem text={"3"} unit={"Kg"} />
              <DotedContainerItem text={"5"} unit={"Kg"} />
            </View>
            <Text style={styles.brandText}>
              {appTexts.PRODUCT_FLITER.Brands}
            </Text>
            <FlatList
              style={styles.flatListStyle}
              data={listData}
              extraData={listData}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={true}
              renderItem={renderItem}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

FilterModal.propTypes = {};

export default FilterModal;
