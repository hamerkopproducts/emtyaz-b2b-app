import React, { memo } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Label = ({ text }) => {
  
  return (
    <View style={styles.root}>
      <Text style={styles.text}>{'AR '+text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    alignItems: 'center',
    padding: 1,
    backgroundColor: '#fff',
    borderRadius: 4,
  },
  text: {
    fontSize: 16,
    color: '#cfcfcf',
  },
});

export default memo(Label);
