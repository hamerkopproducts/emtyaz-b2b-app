import { StyleSheet } from "react-native";
import globals from "../../../lib/globals"

const styles = StyleSheet.create({
  sizeSelectView: {
    width: 50,
    height: 30,
    marginRight:15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth: 1,
    borderRadius: 1,
    borderStyle: 'dashed'

  },
  symbolText:{
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 11,
    marginRight:5
  },
  countText: {
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 11
  }
});

export { styles };
