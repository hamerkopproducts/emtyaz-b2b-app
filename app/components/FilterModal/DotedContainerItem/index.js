import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text} from 'react-native';
import { styles } from "./styles";

const DotedContainerItem = (props) => {
  const {
        text,
        unit
      } = props;

  return (
  <View style={styles.sizeSelectView}>
      <Text style={styles.symbolText}>{text}</Text>
      <Text style={styles.countText}>{unit}</Text>
  </View>
  );
};
DotedContainerItem.propTypes = {
 
};

export default DotedContainerItem;