import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";

let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;

const images = {
  binIcon: require("../../assets/images/addressDetails/bin.png"),
  editIcon: require("../../assets/images/addressDetails/edit.png"),
  heartIcon: require("../../assets/images/addressDetails/heart-active.png"),


};

const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: globals.COLOR.screenBackground,
  },
  screenContainer: {
    flex: 1,
    marginTop: globals.INTEGER.heightTen,
    flexDirection: "column",
    backgroundColor: globals.COLOR.white,
    // alignItems: 'center',
    // justifyContent:'center'
  },
  headerButtonContianer: {
    flexDirection: "row",
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: "center",
  },
  headerButton: {
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty,
  },
  headerButtonMargin: {
    marginLeft: globals.MARGIN.marginTen,
  },
  profileScreen: {
    //backgroundColor:'red',
    paddingLeft: "5%",
    paddingRight: "5%",
  },
  editArrow: {
    alignSelf: "flex-end",
    paddingTop: "4%",
  },
  profileEdit: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: "1%",
    paddingBottom: "5%",
    alignItems: "center",
    //backgroundColor:'red'
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
  },
  profileContent: {
    flexDirection: "row",
  },
  profileInformation: {
    flexDirection: "column",
    flexWrap: "wrap",
    justifyContent: "center",
    paddingLeft: "6%",
  },
  name: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.lightBlack,
    fontSize: 16,
  },
  address: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakGray,
    fontSize: 12,
  },
  place: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakGray,
    fontSize: 12,
  },
  profileItem: {
    paddingTop: "2%",
    // paddingLeft:'5%',
    // paddingRight:'5%'
  },
  modalMainContent: {
    //marginTop: 150,
    // margin: 0,
  },
  modalmainView: {
    backgroundColor: "white",
    // width: ("90%"),
    //height:'100%',
    padding: "5%",
    //borderRadius: 10,
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  closeButton:{
    alignSelf:'flex-end',
    paddingRight:'3%',
    paddingLeft:'3%',
    // paddingTop:'2%',
    // paddingBottom:'1%',
    color:'#707070',
    fontSize:16
  },
  restText:{
   alignItems:'flex-start' ,
   fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
      fontSize:16,
   paddingRight:'3%',
    paddingLeft:'3%',
    //paddingTop:'1%',
    paddingBottom:'1.3%'
  },resetPassword:{
    paddingRight:'3%',
    paddingLeft:'3%',
    paddingTop:'6%',
    paddingBottom:'3.5%',
    //backgroundColor:'red'
  },
  passwordText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    fontSize:13,
    color:'#8D8D8D'
    //paddingTop:'2%'
  },
  buttonWrapper:{
    flexDirection:'row',
    justifyContent:'space-between',
     paddingLeft:'3%',
     paddingRight:'3%',
     // backgroundColor:'red',
      //paddingTop:'5%',
      //paddingBottom:'10%'
    //  marginBottom:10
     paddingBottom:'14%',
     paddingTop:'1%'
  },
  cPassword:{
marginTop:-12,
flexDirection:'row',
borderBottomColor: "#EEEEEE",
borderBottomWidth: 1,
justifyContent: 'center',
    alignItems: 'center',

  },
  numberEnter:{
    fontSize: 14,
    color: "#242424",
    // marginLeft:-4,
   // lineHeight: 9,
    //alignItems: "center",
    textAlign: I18nManager.isRTL ? "right" : "left",
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
      flex:1
   
  },imageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  pwdOne:{
    paddingTop:'2%'
  },
  shadowContainerStyle: {   //<--- Style with elevation
    borderWidth: 0,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 1: 3,
    backgroundColor:  '#ffffff',
    paddingLeft:'5%',
    paddingRight:'5%'
  },
  delivreyAddress:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingTop:'4%',
    alignItems:'center'
  },
  billingAddress:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingTop:'5%',
    alignItems:'center'
  },
  deliveryText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsMedium,
    fontSize:I18nManager.isRTL?14:14,
  },addnewButton: {
    backgroundColor: globals.COLOR.themeGreen,
    width: 80,
    height: 32,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: globals.COLOR.white,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 11,
  },
  delivreyaddressDetail:{
    paddingTop:'5%'

  },nameandIcon:{
    flexDirection:'row',
    justifyContent:'space-between',alignItems:'center'
  },
  billingnameandIcon:{
    flexDirection:'row',
    justifyContent:'space-between',alignItems:'center',paddingTop:'3%'
  },iconList:{
    flexDirection:'row',
    justifyContent:'space-between'
  },
  nameText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color:'#383838',
  }, 
  placeText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 12,
    color:'#686868',
    textAlign: 'left'
  },
  userDetails:{
paddingTop:'1%',
borderBottomColor:'#CFCFCF',
borderBottomWidth:1,
paddingBottom:'6%'
  },
  billinguserDetails:{
    paddingTop:'3%',
    paddingBottom:'6%',
  },
  nameandiconSecond:{
    flexDirection:'row',
    paddingTop:'6%',
    justifyContent:'space-between',alignItems:'center'
  },
  arrowicon:{
    width:15,
    height:15
  },cancelButton: {
    backgroundColor: '#FBFBFB',
    borderColor:'#CFCFCF',
borderWidth:1,
    width: 134,
    height: 45,
    justifyContent: "center",
    alignItems: "center",},submitButton: {
      backgroundColor: globals.COLOR.themeGreen,
      width: 134,
      height: 45,
      justifyContent: "center",
      alignItems: "center",},
      saveText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
        fontSize: 13,
        color:'white'
      },cancelText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
        fontSize: 13,
        color:'#242424'
      },
      resendWrapper: {
        flexDirection: "row",
        justifyContent: "center",
        paddingLeft: "3%",
        paddingRight: "3%",
        paddingTop:'4%',
        paddingBottom:'4%'
        //  paddingTop: hp("3%"),
        //  paddingBottom: hp("2%"),
        //padding:'10%'
      },
      flagsectionWrapper: {
        //flexBasis: "30%",
        width:'30%',
        //backgroundColor: "pink",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        borderBottomColor: "#EEEEEE",
        borderBottomWidth: 1,
      },
      numbersectionWrapper: {
        //flexBasis: "70%",
        width:'75%',
        //backgroundColor: "pink",
        marginLeft: "2%",
      },
      numberEnters: {

    width: "100%",
    fontSize: 14,
    lineHeight: 24,
    alignItems: "center",
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    color: '#242424',
    textAlign: I18nManager.isRTL ? "right" : "left",
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
    // lineHeight: 9,
    color: "#242424",
        // fontSize: 14,
        // color: "#242424",
        // lineHeight: 9,
        // alignItems: "center",
        // fontFamily: I18nManager.isRTL
        //   ? globals.FONTS.notokufiArabic
        //   : globals.FONTS.poppinsRegular,
        // borderBottomColor: "#EEEEEE",
        // borderBottomWidth: 1,
        // textAlign: I18nManager.isRTL ? "right" : "left",
      },
      acceptSection:{
        flexDirection:'row',
        alignItems:'center',
        // paddingLeft:'2%',
        // paddingTop:hp('1.3%')
      
      },
      setasText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsRegular,
        fontSize:12,
        color:'#242424',
        paddingLeft:'2%',
        paddingRight:'2%'
      },
      textContent:{
        flexDirection:'column',
        width:"70%",
      }

});

export { images, styles };
