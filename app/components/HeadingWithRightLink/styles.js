import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 60,
    flexDirection:'row'
  },
  leftHeadingContainer:{
    left:0,
    width:'80%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  leftHeadingView:{
    flexDirection: 'row',
  },
  leftHeadingLabelView:{
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  rightLinkContainer: {
    right:0,
    width: '20%',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  nameLabel:{
    top:0,
    color: globals.COLOR.addressLabelColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.poppinssemiBold,
    fontSize:14
  },
  locationnameLabel:{
    top:0,
    color: globals.COLOR.addressLabelColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.poppinsRegular,
    fontSize:14
  },
  linkLabel:{
    top: 0,
    color: globals.COLOR.greenTexeColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.poppinsMedium,
    fontSize: 13
  },
  linkLabelYellow: {
    top: 0,
    color: globals.COLOR.yellowTextColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.poppinsLight,
    fontSize: 13
  },
  leftIcon:{
    width:20,
    height:20
  }
});

export { styles };
