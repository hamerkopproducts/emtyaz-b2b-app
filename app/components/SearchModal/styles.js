import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  searchIcon: require("../../assets/images/header/home-search.png")
};

const styles = StyleSheet.create({
	popupContainer:{
      width: '100%',
    	height: '100%',
      flexDirection: 'column',
      backgroundColor: globals.COLOR.transparent
    },
    popupBackground:{
      width: '100%',
      height: '100%',
      backgroundColor: globals.COLOR.white,
    	opacity:0.7,
    	zIndex: 1
    },
    popupContentContainer:{
    	top:0,
    	position:'absolute',
    	flex: 1,
    	width: '100%',
    	flexDirection: 'column',
      alignItems: 'center',
      justifyContent: "flex-start",
      zIndex: 3
    },
  headerContainer: {
    width: '100%',
    height: globals.INTEGER.statusBarHeight+160,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 0,
    backgroundColor: globals.COLOR.headerColor
  },
  headingView:{
    position: 'absolute',
    top: globals.INTEGER.statusBarHeight,
    height: globals.INTEGER.headerHeight,
    alignItems: "center",
    justifyContent: "center"
  },
  headerTitleText: {
    textAlign: "center",
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15
  },
  searchContainer:{
    top: globals.INTEGER.statusBarHeight + globals.INTEGER.headerHeight,
    position: 'absolute',
    height:360,
    width:'90%',
    borderWidth: 0.5,
    borderRadius: 2,
    borderColor: '#ddd',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    backgroundColor: globals.COLOR.white
  },
  searchView:{
    width:'100%',
    height:60,
    alignItems: 'center',
    flexDirection:'row',
    backgroundColor: globals.COLOR.lightGray,
  },
  searchIocnView:{
    marginLeft: '5%',
    width: '5%',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  searchIcon:{
   
  },
  searchInput:{
    width: '80%',
    padding:10,
    textAlign: "left",
    color: globals.COLOR.drakGray,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14
  },
  listContainer: {
    width: '100%',
    height: 200,
    alignItems: 'center',
    justifyContent: 'center'
  },
  flatListStyle: {
    width: '100%'
  },
  noDataContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  noDataText: {
    textAlign: 'center',
    color: globals.COLOR.lightTextColor,
    fontSize: 12
  },
  listRow:{
    marginTop:10,
    marginLeft:'5%',
    width: '90%',
    height:40
  },
  categoriesView:{
    marginTop: 0,
    height: 70,
    marginLeft: '5%',
    width: '90%'
  },
  categoriesText:{
    textAlign: "left",
    color: globals.COLOR.textColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14,
    marginBottom:10
  },
  selectedView: {
    flexDirection:'row'
  },
  selectedItemText:{
    textAlign: "center",
    padding:10,
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14
  },
  itemText: {
    width: '90%',
    textAlign: "left",
    color: globals.COLOR.textColor,
    fontSize: 12,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular
  },
  itemView:{
    backgroundColor:globals.COLOR.greenTexeColor,
    marginRight:15,
  }
  
});

export { images, styles  };
