import PropTypes from 'prop-types';
import React from 'react';
import { Modal, View, StatusBar, Text, TouchableOpacity,Image,FlatList,TextInput } from 'react-native';
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import SearchContent from './SearchContent'

const SearchModal = (props) => {

  const {
    isSearchModalVisible,
    closeModal,
    serachText,
    setSerachText
    } = props; 

  return (
    <Modal
      animationType="fade"
      transparent={true}
      onRequestClose={() => null}
      visible={isSearchModalVisible}>
        <SearchContent closeModal={closeModal} serachText={serachText} setSerachText={setSerachText}/>
  </Modal>
  );
};

SearchModal.propTypes = {
 
};

export default SearchModal;
