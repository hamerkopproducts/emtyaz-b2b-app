import PropTypes from 'prop-types';
import React from 'react';
import { View, StatusBar, Text, TouchableOpacity, Image, FlatList, TextInput } from 'react-native';
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";

const SearchContent = (props) => {

    const {
        closeModal,
        serachText,
        setSerachText
    } = props;
    let listData = [
        { 'id': 1, name: 'Fresh Food cupboard' }, 
        { 'id': 2, name: 'Fresh Food packets' }, 
        { 'id': 3, name: 'Fresh Food' }, 
        { 'id': 4, name: 'Fresh Sea Food' }
    ];
    const renderItem = ({ item, index }) => <View style={styles.listRow}><Text style={styles.itemText}>{item.name}</Text></View>;

    return (
        <View style={styles.popupContainer}>

            <TouchableOpacity style={styles.popupBackground} onPress={() => { closeModal() }} />
            <View style={styles.popupContentContainer}>
                <View style={styles.headerContainer}>
                    <StatusBar
                        backgroundColor={globals.COLOR.headerColor}
                        barStyle="dark-content"
                    />
                    <View style={styles.headingView}>
                        <Text style={styles.headerTitleText}>{appTexts.STRING.SearchProducts}</Text>
                    </View>
                </View>
                <View style={styles.searchContainer}>
                    <View style={styles.searchView}>
                        <View style={styles.searchIocnView}>
                            <Image source={images.searchIcon} style={styles.searchIcon} />
                        </View>
                        <TextInput style={styles.searchInput} value={serachText}
                            placeholder={appTexts.STRING.SearchProducts}
                            clearButtonMode={'while-editing'}
                            onChangeText={val => {
                                setSerachText(val)
                            }} autoCapitalize="none"></TextInput>
                    </View>
                    <View style={styles.listContainer}>
                        {listData.length === 0 ? <View style={styles.noDataContainer}><Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text></View> :

                            <FlatList
                                style={styles.flatListStyle}
                                data={listData}
                                extraData={listData}
                                keyExtractor={(item, index) => index.toString()}
                                /*onEndReachedThreshold={0.5}
                                onEndReached={({ distanceFromEnd }) => {
                                  if (listData.length >= (currentPage * pageLimit)) {
                                    loadMoreItems();
                                  }
                                }}*/
                                //onRefresh={() => { onRefresh() }}
                                //refreshing={isRefreshing}
                                showsVerticalScrollIndicator={false}
                                renderItem={renderItem} />}
                    </View>
                    <View style={styles.categoriesView}>
                        <Text style={styles.categoriesText}>{'Categories'}</Text>
                        <View style={styles.selectedView}>
                            <View style={styles.itemView}>
                                <Text style={styles.selectedItemText}>{'Fresh Food'}</Text>
                            </View>
                            <View style={styles.itemView}>
                                <Text style={styles.selectedItemText}>{'Fresh Food Packets'}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
};

export default SearchContent;
