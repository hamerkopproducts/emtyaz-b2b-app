import React, { useCallback, useState } from "react";
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
  I18nManager,
} from "react-native";
import appTexts from "../../lib/appTexts";
import globals from "../../lib/globals";
import { styles } from "./styles";
import OrderCard from "../OrederItemCard";
import Rating from "../StarRating";
import RatingEditModal from "../../components/RatingEditModal";
import SupportModal from "../../components/SupportModal";

const summery = {
  title1: appTexts.THANKUORDER_VIEW.Subtotal,
  title1Valu: "SAR 160.00",
  title2: appTexts.QUOTATIONS_SUMMERY.Discount,
  title2Value: "SAR 20.00",
  title3: appTexts.THANKUORDER_VIEW.DeliveryCharge,
  title3Value: "SAR 10.00",
  titleTotal: appTexts.THANKUORDER_VIEW.TotalInclusivefVAT,
  totalValue: "SAR 162.00",
  methodTitle: appTexts.THANKUORDER_VIEW.Paymenttime,
  methodValue: "Credit/Debit/MADA",
  addressName: "John Mathew",
  addressHouse: "Al Basaam 2",
  addressStreet: "Al naemia street",
  addressPo: "PO Box: 2211",
  addresState: "Al nameem,jaddah",
  adderessPhone: "+966 7489328930",
  itemQuantity: appTexts.CART.Nos,
  itemPrice: "SAR 50 X200",
  itemPriceTotal: "SAR 9800",
  itemPrice2: "SAR 50 X100",
  itemPriceTotal2: "SAR 4800",
  orderID: "QUOT123456789",
  scheduledDate: "12 July 2020, 2pm-5pm",
  statusText: appTexts.ORDER_STATUS.Pending,
  deliveryInfo: appTexts.ADDRESSDETAILS.Deliverinformation,
  orderSummary: appTexts.THANKUORDER_VIEW.OrderSummary,
  scheduledOn: appTexts.QUOTATIONS_SUMMERY.ScheduledOn,
  orderNote: appTexts.THANKUORDER_VIEW.Ordernote,
  reorder: appTexts.ORDER_STATUS.Reorder,
  cancelOrder: appTexts.CART.CancelOrder,
  contactSupport: appTexts.CART.ContactSupport,
};
const RenderSummeryItem = ({ item, value }) => (
  <View style={styles.item}>
    <Text style={styles.summeryItem}>{item}</Text>
    <Text style={[styles.summeryItem, { alignSelf: "flex-end" }]}>{value}</Text>
  </View>
);
const RenderSummery = () => (
  <View style={styles.summeryMain}>
    <View style={{flexDirection: 'row'}}>
    <Text style={styles.summeryTitle}>{summery.orderSummary}</Text>
    </View>
    <RenderSummeryItem item={summery.title1} value={summery.title1Valu} />
    <RenderSummeryItem item={summery.title2} value={summery.title2Value} />
    <RenderSummeryItem item={summery.title3} value={summery.title3Value} />

    <View style={styles.line} />
    <View style={styles.item}>
      <Text style={styles.summeryItem}>{summery.titleTotal}</Text>
      <Text style={([styles.summeryItem], { fontWeight: "bold" })}>
        {summery.totalValue}
      </Text>
    </View>

    {/* <RenderSummeryItem item={summery.titleTotal} value={summery.totalValue} /> */}

    <View style={styles.line} />
    <RenderSummeryItem item={summery.methodTitle} value={summery.methodValue} />
  </View>
);
const RenderAddress = ({ type }) => (
  <View>
    <View style={{flexDirection: 'row'}}>
    <Text style={styles.summeryTitle}>{type}</Text>
    </View>
    <View style={styles.alignArabic}>
      <Text style={styles.summeryItem}>
        {summery.addressName}
        {summery.addressHouse}
        {"\n"}
        {summery.addressStreet}
        {"\n"}
        {summery.addressPo}
        {"\n"}
        {summery.addresState}
        {"\n"}
        {summery.adderessPhone}
      </Text>
    </View>
  </View>
);
const RenderOrderNote = ({ title }) => (
  <View>
    <View style={{flexDirection: 'row'}}>
    <Text style={styles.summeryTitle}>{title}</Text>
    </View>
    <Text style={styles.summeryItem}>
      Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
      ligula eget dolor. Aeneanmas. Cum sociis natoque penatibus et magnis dis
      parturie montes, nascetur ridiculus mus.
    </Text>
  </View>
);

const RenderButton = ({ text, bgcolor, onPressSupportModal = () => {} }) => (
  <TouchableOpacity
    style={[styles.applyContainer, { backgroundColor: bgcolor }]}
    onPress={onPressSupportModal}
  >
    <Text style={styles.buttonLabel}>{text}</Text>
  </TouchableOpacity>
);
const RenderTopDetails = () => (
  <View style={styles.topDetails}>
    <View style={styles.quotationID}>
      <Text style={styles.quotationID}>{summery.orderID}</Text>
    </View>
    <View style={styles.statusReorder}>
      <Text style={[styles.status,{marginBottom: 5, color: globals.COLOR.themeGreen} ]}>
         •{summery.statusText}
      </Text>
      <TouchableOpacity style={styles.reorderButton} onPress={() => {}}>
        <Text style={styles.reorderButtonText}>{summery.reorder}</Text>
      </TouchableOpacity>
    </View>
  </View>
);
const RenderDeliveryInfo = () => (
  <View style={styles.summeryMain}>
    <View style={{flexDirection: 'row'}}> 
    <Text style={styles.summeryTitle}>{summery.deliveryInfo}</Text>
    </View>

    <View style={styles.delPersonDetails}>
      <Text style={styles.deliveryName}>Antony Wilson{"\t"}</Text>
      <Image source={require("../../assets/images/myOrders/call_icon.png")} />
      <Text style={styles.deliveryName}>{summery.adderessPhone}</Text>
    </View>
  </View>
);
const RenderDeliverySchedule = () => (
  <View>
    <View style={{ flexDirection: "row" }}>
    <Text style={styles.scheduleTitle}>{summery.scheduledOn}</Text>
    </View>
    <View style={{ flexDirection: "row" }}>
      <Text style={styles.scheduleDate}>{summery.scheduledDate}</Text>
    </View>
  </View>
);
const RenderReview = ({ title, onPress }) => {
  return (
    <View>
      <View style={styles.reviewTitle}>
        <Text style={styles.summeryTitle}>{title}</Text>
        <View style={styles.editIcon}>
          <TouchableOpacity
            style={styles.filterContainer}
            onPress={onPress}
          >
            <Image
              source={require("../../assets/images/profileItem/edit.png")}
              style={styles.editIcon}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.ratingSpot}>
        <View>
          <Text style={styles.RatingText}>{appTexts.RATING.Quality}</Text>
          <Text style={styles.RatingText}>
            {appTexts.RATING.Supportfromteam}
          </Text>
          <Text style={styles.RatingText}>{appTexts.RATING.DeliveryTime}</Text>
        </View>
        <View>
          <Rating />
          <Rating />
          <Rating />
        </View>
      </View>

      <Text style={styles.summeryItem}>
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo
        ligula eget dolor. Aeneanmas. Cum sociis natoque penatibus et magnis dis
        parturie montes, nascetur ridiculus mus.
      </Text>
    </View>
  );
};
const StatusDetailsCard = ({ editReveiwPress }) => {
  const [isRatingEditModalVisible, setIsRatingEditModalVisible] = useState(
    false
  );
  const [isSupportModalVisible, setIsSupportModalVisible] = useState(
    false
  );
  return (
    <ScrollView>
      <View style={styles.container}>
        <RenderTopDetails />
        <RenderDeliveryInfo />
        <View style={{flexDirection: 'row'}}>
        <Text style={styles.items2}>2 {appTexts.MYQUATION.Items}</Text>
        </View>
        <OrderCard
          itemImage={require("../../assets/images/home/product_02.png")}
        />
        <OrderCard
          itemImage={require("../../assets/images/home/product_01.png")}
        />
        <RenderSummery />
        <View style={[styles.summeryMain, { marginTop: 10 }]}>
          <RenderAddress type={appTexts.QUOTATIONS_SUMMERY.Deliveryaddress} />
          <RenderDeliverySchedule />
          <View style={styles.line} />
          <View style={([styles.line], { marginBottom: 10 })} />
          <RenderAddress type={appTexts.QUOTATIONS_SUMMERY.Billing} />
        </View>
        <View style={[styles.summeryMain, { marginTop: 10 }]}>
          <RenderOrderNote title={summery.orderNote} />
        </View>
        <View style={[styles.summeryMain, { marginTop: 10 }]}>
          <RenderReview
            title={appTexts.RATING.Yourreview}
            onPress={() => setIsRatingEditModalVisible(true)}
          />
        </View>
        <View style={styles.buttonArea}>
          <RenderButton
            text={summery.contactSupport}
            
            onPressSupportModal={() => setIsSupportModalVisible(true)}
            bgcolor={globals.COLOR.themeGreen}
          />
          <RenderButton
            text={summery.cancelOrder}
            
            onPress={() => {}}
            bgcolor={globals.COLOR.themeOrange}
          />
        </View>
      </View>
      <SupportModal
      onColse={()=> setIsSupportModalVisible(false)}
      isSupportModalVisible={isSupportModalVisible}
      />
      <RatingEditModal
        onColse={()=> setIsRatingEditModalVisible(false)}
        isRatingModalVisible={isRatingEditModalVisible}
      />
    </ScrollView>
  );
};
StatusDetailsCard.propTypes = {};

export default StatusDetailsCard;
