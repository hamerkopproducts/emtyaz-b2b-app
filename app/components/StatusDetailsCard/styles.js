import { StyleSheet, I18nManager,Platform } from "react-native";
import globals from "../../lib/globals";

const styles = StyleSheet.create({
  nameText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color: "#383838",
  },
  summeryItem: {
    marginLeft: 10,
    fontSize: 13,
    color: "#383838",
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
  },
  container: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
    flex: 1,
    padding: 20,
  },
  topDetails: {
      marginBottom: 10,
    justifyContent: 'space-between',
    flexDirection: "row",
  },
  reorderButton: {
    padding: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  reorderButtonText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    padding: 7,
    fontSize: 12,
    color: 'white',
    backgroundColor: globals.COLOR.themeGreen
  },
  statusReorder: {},
  itemCard: {
      marginTop: 10,
    flexDirection: "row",
    marginBottom: 10,
  },
  itemDetails: {
    flex: 1,
    paddingHorizontal: 10,
    paddingBottom: 10,
    justifyContent: "space-between",
  },
  itemName: {},
  itemquantity: {
      fontSize: 12,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: 'flex-end'
  },
  tinyLogo: {
    borderWidth: 0.5,
    borderColor: "#383838",
    height: 100,
    width: 100,
  },
  summeryMain: {
    paddingVertical: 10,
      marginBottom:20,
      borderWidth: 0,
    shadowOffset: {
      width: 1,
      height: 1,
      
    },
    shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 1: 3,
    backgroundColor:  '#ffffff'
  },
  summeryTitle: {
    marginLeft: 10,
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsMedium,
    color: globals.COLOR.drakBlack,
    fontSize: 14,
  },
  RatingText:
  {fontFamily: I18nManager.isRTL
  ? globals.FONTS.notokufiArabic
  : globals.FONTS.poppinsRegular,
  fontSize:13, 
  color:'#1C1C1C',
  paddingTop:'1.5%'},
  paymentMethod: {
    flexDirection: "row",
  },
  ratingSpot: {
    flexDirection: "row", 
    justifyContent: "space-between",
    paddingBottom:'4%' ,
    alignItems:'center',
    paddingHorizontal: 10
    
  },
  line: {
    borderBottomColor: "grey",
    borderBottomWidth: 0.3,
  },
  address: {
    marginTop: 20,
  },

  dAddress: {},
  item: {
    padding: 10,
    justifyContent: "space-between",
    flexDirection: "row",
  },
  orderNote: {
    borderWidth: 1,
    marginTop: 20,
  },
  alignArabic: {
    flexDirection: 'row'
  },
  editIcon: {
    alignSelf: "flex-end",
    paddingTop: "4%",
    resizeMode: 'contain'
  },
  quotationID: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    justifyContent: "space-between" 
  },
  status: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
   
  },
  items2: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
      marginBottom: 20
  },
  statusTexts: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    marginBottom: 5, 
    color: globals.COLOR.themeGreen 
  },
  scheduleDate: {
    marginLeft: 10, 
    color: globals.COLOR.themeGreen,
    marginBottom: 10,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  reviewTitle: {
    flexDirection: "row",
        justifyContent: "space-between",
        marginRight: 10,
  },
  scheduleTitle: {
    marginLeft: 10,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
    color: globals.COLOR.drakBlack,
    fontSize: 14,
    marginTop: 5, 
    marginLeft: 10 
  },
  delPersonDetails: {
    flexDirection: "row",
     marginTop:5, 
     marginBottom: 10,
     alignItems: 'center'

  },
  deliveryName: {
    fontSize: 13,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    marginLeft: 10,
    marginRight: 10

  },
  buttonArea: {
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center",
    marginTop: 15,
  },
  buttonLabel: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    marginLeft: 5,
    marginRight: 5,
    fontSize: 13,
    color: "white",
  },
  applyContainer: {
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
    justifyContent: 'space-between',
    padding: 12,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  quotationText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
    color: globals.COLOR.drakBlack,
    fontSize: 14,
  },
});

export { styles };
