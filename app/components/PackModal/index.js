import React, { useState } from "react";
import { View, FlatList, Text, TouchableOpacity } from "react-native";

import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Modal from "react-native-modal";
import CustomRadioButton from "../CustomRadioButton";

const PackModal = (props) => {
  const { togglePackModal, isPackModalVisible, packSizes, selectedQuantity, setSelectedQuantity } = props;

  const renderEachQty = (item) => {
    return (
      <View style={styles.touchableopacityWrapper}>
        <TouchableOpacity style={styles.boxWrapper}>
          <View style={styles.inputBox}>
            <Text style={styles.boxText}>
              { item.quantity }
            </Text>
          </View>
          <CustomRadioButton
            id={item.id}
            name={''}
            selected_id={selectedQuantity}
            onPress={(id, name) => setSelectedQuantity(id, item.quantity) }
          />
        </TouchableOpacity>
      </View>
    )
  }

  return (
    <Modal
      animationIn="slideInUp"
      animationOut="slideOutRight"
      isVisible={isPackModalVisible}
      style={styles.packModalView}
      onBackdropPress={() => togglePackModal() }
    >
      <View style={styles.packModalContent}>
        <View style={styles.closeButton}>
          <TouchableOpacity
            onPress={() => {
              togglePackModal();
            }}
          >
            <Text style={styles.closeText}>X</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.packModalContentView}>
          <View style={styles.packModalheading}>
            <Text style={styles.packHeadingText}>
              {appTexts.QUANTITY.Heading}
            </Text>
          </View>
          <FlatList
            data={packSizes}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item) => packSizes.indexOf(item).toString()}
            showsVerticalScrollIndicator={true}
            renderItem={({ item, index }) => renderEachQty(item)}
          />
        </View>
      </View>
    </Modal>
  );
};
PackModal.propTypes = {};

export default PackModal;
