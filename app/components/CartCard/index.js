import React, { useState } from "react";
import PropTypes from "prop-types";
import { View, Image, Text, TouchableOpacity } from "react-native";
import { styles } from "./styles";
import DividerLine from "../DividerLine";
import Modal from "react-native-modal";
import appTexts from "../../lib/appTexts";

const CartCard = (props) => {

  const { itemImage, item, index, removeItem, updateItemQty, wishList, addRemoveFav } = props;
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);

  let WishListItems = [];
  if(wishList.data) {
    WishListItems = wishList.data.map(item => {
      return item.product_id;
    });
  }

  return (
    <View style={styles.fullWidthRowContainer}>
      <View style={styles.rowContainer}>
        <View style={styles.firstContainer}>
          <View style={styles.imageContainer}>
            <Image
              resizeMode="contain"
              source={{ uri: itemImage }}
              style={styles.image}
            />
          </View>
          <View style={styles.labelContainer}>
            <View style={styles.labelView}>
              <View style={styles.textContainer}>
                <Text style={styles.itemNameText}>
                  {item.name} {item.variant_name}
                </Text>
              </View>
              <TouchableOpacity onPress={() => setIsDeleteModalVisible(true) }>
              <View style={styles.deleteIconContainer}>
                <Image
                  source={require("../../assets/images/cartScreenIcons/delete.png")}
                  style={styles.binStyle}
                />
              </View>
              </TouchableOpacity>
              <View style={styles.favIconContainer}>
                <TouchableOpacity onPress={() => addRemoveFav(item.product_id) }>
                  <Image 
                    source={WishListItems.indexOf(item.product_id) != -1 ? 
                      require('../../assets/images/products/heart-active.png') :
                      require('../../assets/images/products/favourite.png')}
                    style={styles.favIconStyle} 
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.sizeSelectContainer}>
              <View style={styles.labelView}>
                <View style={styles.sizeSelectView}>
                  <Text style={styles.totalCountText}>
                    {" "}
                    {item.quantity_count} x {item.quantity}
                  </Text>
                </View>
                <View style={styles.countContainer}>
                  <View style={styles.countSelectView}>
                    <TouchableOpacity onPress={() => updateItemQty(index, -1)}>
                      <Text style={styles.symbolText}>{"-"}</Text>
                    </TouchableOpacity>
                    <View style={styles.verticalDivider} />
                    <Text style={styles.countText}>{item.quantity}</Text>
                    <View style={styles.verticalDivider} />
                    <TouchableOpacity  onPress={() => updateItemQty(index, 1)}>
                      <Text style={styles.symbolText}>{"+"}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
      <DividerLine />

      <Modal
            isVisible={isDeleteModalVisible}
            style={styles.modalMaincontentHelp}
          >
            <View style={styles.modalmainView}>
              <TouchableOpacity
                style={styles.buttonwrapper}
                onPress={() => {
                  setIsDeleteModalVisible(false);
                }}
              >
                <Text style={styles.closeButton}>X</Text>
                <Text style={styles.logoutText}>{appTexts.CART.Delete}</Text>
              </TouchableOpacity>
              <View style={styles.logoutWrappers}>
                <TouchableOpacity onPress={() => {
                  removeItem(index);
                  setIsDeleteModalVisible(false);
                }}>  
                  <View style={styles.yesButton}>
                    <Text style={styles.sendbuttonText}>
                      {appTexts.LOGOUT.Yes}
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setIsDeleteModalVisible(false) }>
                  <View style={styles.noButton}>
                    <Text style={styles.sendbuttonText}>
                      {appTexts.LOGOUT.No}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>

    </View>
  );
};

CartCard.propTypes = {
  itemImage: PropTypes.number,
  nameLabel: PropTypes.string,
};

export default CartCard;
