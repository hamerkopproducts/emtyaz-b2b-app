import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  fullWidthRowContainer:{
    width: '100%',
    height: 160,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: 160,
    justifyContent: 'center',
    alignItems: 'center'
  },
  firstContainer:{
    width: '100%',
    height: 120,
    paddingTop:10,
    paddingBottom:10,
    flexDirection:'row'
  },
  secondContainer: {
    width: '100%',
    height: 40,
    flexDirection: 'row'
  },
  image: {
    height: 90,
    width: 90,
  },
  imageContainer:{
    height: 100,
    width:100,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth:1
  },
  labelContainer:{
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 100,
  },
  labelView:{
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 60,
    flexDirection:'row'
  },
  sizeSelectContainer: {
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 40,
    flexDirection:'row'
  },
  sizeSelectView:{
    marginLeft:20,
    width: 80,
    height: 30,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  countContainer:{
    flex:1,
    height: 30,
    alignItems: 'flex-end'
  },
  countSelectView: {
    marginLeft: 20,
    width: 92,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: globals.COLOR.themeGreen
  },
  itemNameText:{
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 11
  },
  totalCountText:{
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 14
  },
 countText: {
    width: 30,
    textAlign:'center',
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 16
  },
  symbolText: {
    width: 30,
    textAlign: 'center',
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 20
  },
  dropDownArrow: {
    
  },
  textContainer:{
    marginLeft: 20,
    width: globals.INTEGER.screenWidthWithMargin - 200,
    height: 60
  },
  favIconContainer: {
    alignItems: 'flex-end',
    width:40,
    height: 50
  },
  favIconStyle:{
    width:20,
    height:19,
  },
  deleteIconContainer:{
    alignItems: 'flex-end',
    width: 40,
    height: 50
  },
  binStyle:{
    width:15,
    height:19,
  },
  nameLabel:{
    top:0,
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 11,
    height: 20
  },
  addButtonContainer:{
    position:'absolute',
    right:0,
    bottom:20
  },
  verticalDivider:{
    width:0.5,
    height:'80%',
    backgroundColor: globals.COLOR.white,
  },

  modalMainContent: {
    justifyContent: "flex-end",
    margin: 0,
  },
  modalMaincontentHelp:{
      justifyContent: "center",
  },
  modalmainView: {
    backgroundColor: "white",
    padding: "4%",
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  buttonWrapper:{
    paddingLeft:'3%',
    paddingRight:'3%',
    paddingBottom:'6%',
    paddingTop:'3%',
  },
  logoutWrappers:{
    paddingLeft:'9%',
    paddingRight:'9%',
   paddingBottom:'15%',
   paddingTop:'12%',
   flexDirection:'row',
   justifyContent:'space-around'
 },
  closeButton:{
    alignSelf:'flex-end',
    paddingRight:'3%',
    paddingLeft:'3%',
    paddingTop:'2%',
    paddingBottom:'2%',
    color:'#707070',
    fontSize:16
  },
  logoutText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    fontSize:15, 
    alignSelf:'center',
    paddingTop:'6%'
  },
  yesButton:{
    backgroundColor:  globals.COLOR.themeGreen,
    width: 110,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
  },
  noButton:{
    backgroundColor:  globals.COLOR.themeOrange,
    width: 110,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
  },
  sendbuttonText:{
    color: globals.COLOR.white,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15, 
  },
});

export { styles };
