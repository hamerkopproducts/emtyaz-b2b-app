import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text} from 'react-native';
import { styles } from "./styles";

const DotedContainerItem = (props) => {
  const {
        itemImage,
        nameLabel
      } = props;

  return (
  <View style={styles.sizeSelectView}>
    <Text style={styles.symbolText}>{'X'}</Text>
    <Text style={styles.countText}>{'200'}</Text>
  </View>
  );
};
DotedContainerItem.propTypes = {
  itemImage:PropTypes.number,
  nameLabel:PropTypes.string
};

export default DotedContainerItem;