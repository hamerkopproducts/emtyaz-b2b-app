import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Image} from 'react-native';
import { styles } from "./styles";

const PlusButton = (props) => {
  const {
        
      } = props;

  return (
    <TouchableOpacity style={styles.buttonView}>
      <Image style={styles.itemImage} source={require('../../../assets/images/products/plus.png')} />
    </TouchableOpacity>
  );
};
PlusButton.propTypes = {
  
};

export default PlusButton;