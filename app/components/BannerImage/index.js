import React, { Component, createRef } from 'react';
import { View, Image, FlatList } from 'react-native';
import { styles } from "./styles";
import globals from "../../lib/globals";

class BannerImage extends Component {

  constructor(props) {
    super(props);
  }

  flatList = createRef();
  CurrentSlide = 0;
  IntervalTime = 4000;

  _goToNextPage = () => {
    if(this.props.bannerImage.length > 1) {
      if (this.CurrentSlide >= this.props.bannerImage.length - 1) {
        this.CurrentSlide = 0;
      }
      this.flatList.current.scrollToIndex({
        index: ++this.CurrentSlide,
        animated: true,
      });
    }
  };

  _startAutoPlay = () => {
    this._timerId = setInterval(this._goToNextPage, this.IntervalTime);
  };

  _stopAutoPlay = () => {
    if (this._timerId) {
      clearInterval(this._timerId);
      this._timerId = null;
    }
  };

  componentDidMount() {
    if(typeof this.props.bannerImage == 'object') {
      this._stopAutoPlay();
      this._startAutoPlay();
    }
  }

  componentWillUnmount() {
    this._stopAutoPlay();
  }

  render() {

    const { bannerImage, type } = this.props;

    if(type == 'middle') {
      return (
          <Image 
            resizeMode='cover'
            style={{ width: '100%', height: 100 }}
            source={bannerImage}
          />
      )
    } else {
      return (
        <FlatList
          ref={this.flatList}
          flatListRef={React.createRef()}
          data={bannerImage}
          showsHorizontalScrollIndicator={false}
          horizontal
          keyExtractor={(item) => bannerImage.indexOf(item).toString()}
          showsVerticalScrollIndicator={true}
          renderItem={({ item, index }) => (
            <View>
              <View style={styles.imageContainer}>
                <Image resizeMode='cover'
                  style={{ width: globals.SCREEN_SIZE.width, height: 100 }}
                  source={{
                    uri: item
                  }}
                />
              </View>
            </View>
          )}
        />
      )
    }
  }
}

export default BannerImage;