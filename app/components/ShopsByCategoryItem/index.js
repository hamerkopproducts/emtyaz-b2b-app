import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Image, Text, I18nManager } from "react-native";
import { styles } from "./styles";
import { Item } from "native-base";
import { TouchableOpacity } from "react-native-gesture-handler";

const ShopsByCategoryItem = (props) => {
  const { itemData, onPress } = props;
  const lang = I18nManager.isRTL ? 'ar' : 'en';

  let image = "";
  try {
    image = itemData.lang[lang].image_path;
  } catch (err) {
    image = "";
  }
  let name = "";
  try {
    name = itemData.lang[lang].name;
  } catch (err) {
    name = "";
  }

  return (
    <TouchableOpacity onPress={() => onPress(itemData.id) }>
      <View style={styles.categoryItemContainer}>
        <View style={styles.imageContainer}>
          {image &&
            <Image
              style={styles.itemImage}
              resizeMode="contain"
              source={{
                uri: image,
              }}
            />
          }
        </View>
        <Text style={styles.nameLabel}>{ name }</Text>
      </View>
    </TouchableOpacity>
  );
};
ShopsByCategoryItem.propTypes = {
  itemData: PropTypes.object,
  itemImage: PropTypes.number,
  nameLabel: PropTypes.string,
  details: PropTypes.string,
};

export default ShopsByCategoryItem;
