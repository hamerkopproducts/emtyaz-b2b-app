import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  categoryItemContainer: {
    width: (globals.INTEGER.screenWidthWithMargin / 3),
    height: (globals.INTEGER.screenWidthWithMargin / 3),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  imageContainer:{
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 100,
    padding: 5,
    borderColor: '#dadada'
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 100
  },
  nameLabel:{
    top:5,
    color: globals.COLOR.addressLabelColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic: globals.FONTS.poppinssemiBold,
    fontSize: 11,
    height: 20
  },
});

export { styles };
