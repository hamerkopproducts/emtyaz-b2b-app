import React, { useState, useRef } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  FlatList,
  Image,
  ScrollView,
  TouchableHighlight,
  Button,
  I18nManager,
} from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";
import PropTypes from "prop-types";
import PromotionalProductCard from "../../components/PromotionalProductCard";
import ProductCard from "../../components/ProductCard";
import SearchModal from "../../components/SearchModal";
import RecentSearchModal from "../../components/RecentSearchModal";
import FilterModal from "../../components/FilterModal";
import SortModal from "../../components/SortModal";

import Header from "../../components/Header";
import appTexts from "../../lib/appTexts";
import CategoryItem from "../../components/CategoryItem";
import Modal from "react-native-modal";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CustomGreenLargeButton from "../../components/CustomGreenLargeButton";
import Category from "../../components/Category";
import Loader from "../../components/Loader";

const ProductsView = (props) => {
  const {
    columnCount,
    changeListingStyle,
    onCartButtonPress,
    isSearchModalVisible,
    onSearchPress,
    serachText,
    itemClick,
    setSerachText,
    isRecentSearchModalVisible,
    onRecentSearchClose,
    closeSearchModal,
    showSelectedSearch,
    isFilterModalVisible,
    onFilterPress,
    closeFilterModal,
    onCategoryModalClick,
    isCategoryModalVisible,
    onSortPress,
    isSortModalVisible,
    closeSortModal,
    data,
    isLoading,
    filter_cat_data,
    filter_brand_data,
    applySelectedFilters,
    category,
    wishList,
    addRemoveFav,
    update_products_list
  } = props;

  let WishListItems = [];
  if(wishList.data) {
    WishListItems = wishList.data.map(item => {
      return item.product_id;
    });
  }

  let listData = [];
  
  try {
    if(data.data) {
      listData = data.data;
    }
  } catch(err) {
    listData = [];
  }

  let categories = [];
  try {
    categories = filter_cat_data.data.categories;
  } catch(err) {
    categories = [];
  }

  const total = data.total;
  const [scrollViewWidth, setScrollViewWidth] = useState(0);
  const [currentXOffset, setCurrentXOffset] = useState(0);

  const [brandFilters, setBrandFilters] = useState([]);
  const [appliedFilters, setAppliedFilters] = useState([]);
  const [tempFilter, setTempFilters] = useState([]);

  if(category && JSON.stringify(tempFilter) != JSON.stringify([category])) {
    setTempFilters([category]);
    setAppliedFilters([category]);
  }

  const applyFilter = () => {
    setAppliedFilters(tempFilter);
    onCategoryModalClick();
    setTimeout(() => {
      applySelectedFilters(tempFilter, brandFilters);
    }, 400);
  }

  const applyBrandFilter = (brandFiltersApplied) => {
    setBrandFilters(brandFiltersApplied)
    setTimeout(() => {
      applySelectedFilters(appliedFilters, brandFiltersApplied);
    }, 400);
  }

  const resetBrandFilter = () => {
    setBrandFilters([])
    setTimeout(() => {
      applySelectedFilters(appliedFilters, []);
    }, 400);
  }

  const selectFilterCat = (id) => {
    let tempFilters = [...tempFilter];
    const index = tempFilters.indexOf(id);
    if(index == -1) {
      tempFilters.push(id);
    } else {
      tempFilters.splice(index, 1);
    }
    setTempFilters(tempFilters);
  }

  const inputEl = useRef(null);
  const renderItem = ({ item, index }) => (
    <ProductCard 
      wishListItems={WishListItems}
      navigation={props.navigation}
      addToCart={(item) => {
        let existing = false;
        let all = props.cartItems ? [...props.cartItems] : [];
        if (props.cartItems) {
          for (let inc = 0; inc < all.length; inc++) {
            const _itm = all[inc];
            if (
              _itm.type == item.type &&
              _itm.variant_id == item.variant_id &&
              _itm.quantityId == item.quantityId &&
              _itm.product_id == item.product_id
            ) {
              existing = true;
              all[inc].quantity += 1;
              props.addItemsToCart(all);
            }
          }
        }
        if (existing == false) {
          all.push(item);
          props.addItemsToCart(all);
        }
      }}
      item={item}
      itemClick={itemClick}
      addRemoveFav={addRemoveFav} 
    />
  );

  const renderItem1 = ({ item, index }) => (
    <PromotionalProductCard
      wishListItems={WishListItems}
      navigation={props.navigation}
      addToCart={(item) => {
        let existing = false;
        let all = props.cartItems ? [...props.cartItems] : [];
        if (props.cartItems) {
          for (let inc = 0; inc < all.length; inc++) {
            const _itm = all[inc];
            if (
              _itm.type == item.type &&
              _itm.variant_id == item.variant_id &&
              _itm.quantityId == item.quantityId &&
              _itm.product_id == item.product_id
            ) {
              existing = true;
              all[inc].quantity += 1;
              props.addItemsToCart(all);
            }
          }
        }
        if (existing == false) {
          all.push(item);
          props.addItemsToCart(all);
        }
      }}
      item={item}
      itemClick={itemClick}
      addRemoveFav={addRemoveFav}
    />
  );

  const handleScroll = (event) => {
    console.log("currentXOffset =", event.nativeEvent.contentOffset.x);
    let newXOffset = event.nativeEvent.contentOffset.x;
    setCurrentXOffset(newXOffset);
  };

  const leftArrow = () => {
    let eachItemOffset = scrollViewWidth / 6; // Divide by 10 because I have 10 <View> items
    let _currentXOffset = currentXOffset - eachItemOffset;
    inputEl.current.scrollTo({ x: _currentXOffset, y: 0, animated: true });
  };

  const rightArrow = () => {
    let eachItemOffset = scrollViewWidth / 6; // Divide by 10 because I have 10 <View> items
    let _currentXOffset = currentXOffset + eachItemOffset;
    inputEl.current.scrollTo({ x: _currentXOffset, y: 0, animated: true });
  };

  return (
    <View style={styles.screenMain}>
      {isLoading && <Loader />}
      <Header
        navigation={props.navigation}
        // isLogoRequired={true}
        // isLanguageButtonRequired={true}
        headerTitle={appTexts.STRING.products}
        isRightButtonRequired={true}
        isSearchButtonRequired={false}
        onCartButtonPress={onCartButtonPress}
        onSearchPress={onSearchPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <View style={styles.screenContainer}>
        <View style={styles.container}>
          <View style={styles.leftHeadingContainer}>
            <TouchableOpacity
              onPress={() => {
                onCategoryModalClick();
              }}
            >
              <Text style={styles.nameLabel}>
                {total}{I18nManager.isRTL ? "طلبات " : " Items"}
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.filterContainer}
            onPress={() => {
              onFilterPress();
            }}
          >
            <Image
              style={styles.itemImage}
              source={require("../../assets/images/myOrders/filter.png")}
            />
            <Text style={styles.filterLabel}>{appTexts.MY_ORDERS.Filter}</Text>
          </TouchableOpacity>
          <Modal
            isVisible={isCategoryModalVisible}
            style={styles.modalMainContent}
          >
            <View style={styles.modalmainView}>
              <KeyboardAwareScrollView
                scrollEnabled={true}
              >
                <View>
                  <TouchableOpacity
                    style={styles.buttonwrapper}
                    onPress={() => {
                      onCategoryModalClick();
                    }}
                  >
                    <Text style={styles.closeButton}>X</Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.categoryHeading}>
                  <View style={styles.textWrapper}>
                    <Text style={styles.categoryTextHeading}>
                      {appTexts.CATEGORY_CONTENT.Heading}
                    </Text>
                    <Text style={styles.lowerText}>
                      {appTexts.CATEGORY_CONTENT.SubHeading}
                    </Text>
                  </View>
                  <View style={styles.buttonCover}>
                    <View style={styles.applyButton}>
                      <TouchableOpacity style={styles.applyButton} onPress={() => applyFilter() }>
                        <Text style={styles.applyButtonText}>
                          {appTexts.CATEGORY_CONTENT.Button1}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={styles.resetButton}>
                      <TouchableOpacity style={styles.resetButton}  onPress={() => {
                        setTempFilters([]);
                        setAppliedFilters([]);
                      }}>
                        <Text style={styles.resetButtonText}>
                          {appTexts.CATEGORY_CONTENT.Button2}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <View style={styles.bordersStyle}></View>
                <View style={styles.childComponent}>

                <FlatList
                  data={categories}
                  keyExtractor={(item, index) => index.toString()}
                  showsVerticalScrollIndicator={false}
                  renderItem={({ item, index }) => 
                    <Category 
                      selected={tempFilter.indexOf(item.id) !== -1} 
                      selectCatItem={selectFilterCat} 
                      id={item.id} 
                      itemText={item.lang[0].name} 
                    />
                  }
                />
                </View>
              </KeyboardAwareScrollView>
            </View>
          </Modal>
          {columnCount === 2 ? (
            <TouchableOpacity
              style={styles.viewChangeContainer}
              onPress={() => {
                changeListingStyle();
              }}
            >
              <View style={styles.greenLine}></View>
              <View style={styles.greenLine}></View>
              <View style={styles.greenLine}></View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={styles.viewChangeContainer}
              onPress={() => {
                changeListingStyle();
              }}
            >
              <Image
                style={styles.itemImage}
                source={require("../../assets/images/products/gridC.png")}
              />
            </TouchableOpacity>
          )}
        </View>
        <View style={styles.scrollableView}>
          <TouchableHighlight
            style={styles.leftArrowStyle}
            onPress={() => {
              leftArrow();
            }}
          >
            <Image
              style={styles.itemImage}
              source={require("../../assets/images/products/arrow-dropleft.png")}
            />
          </TouchableHighlight>

          <ScrollView
            style={styles.scrollViewStyle}
            contentContainerStyle={{ alignItems: "center" }}
            horizontal={true}
            pagingEnabled={true}
            ref={inputEl}
            showsHorizontalScrollIndicator={false}
            onContentSizeChange={(w, h) => setScrollViewWidth(w)}
            scrollEventThrottle={16}
            //scrollEnabled={false} // remove if you want user to swipe
            onScroll={(event) => {
              handleScroll(event);
            }}
          >
            {categories.map(item => 
              <CategoryItem
                itemImage={{
                  uri: item.lang[0].image_path
                }}
                badgeLabel={""}
                onPress={onCategoryModalClick}
                selected={tempFilter.indexOf(item.id) !==-1 }
              />
            )
          }
          </ScrollView>
          <TouchableHighlight
            style={styles.rightArrowStyle}
            onPress={() => {
              rightArrow();
            }}
          >
            <Image
              style={styles.itemImage}
              source={require("../../assets/images/products/arrow-dropright.png")}
            />
          </TouchableHighlight>
        </View>
        <View style={styles.listContainer}>
          {listData.length === 0 ? (
            <View style={styles.noDataContainer}>
              <Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text>
            </View>
          ) : (
            <FlatList
              style={styles.flatListStyle}
              data={listData}
              extraData={listData}
              keyExtractor={(item, index) => index.toString()}
              onEndReachedThreshold={0.5}
							onEndReached={() => {
								update_products_list()
							}}
              //onRefresh={() => { onRefresh() }}
              //refreshing={isRefreshing}
              numColumns={columnCount}
              key={columnCount}
              showsVerticalScrollIndicator={false}
              renderItem={columnCount === 1 ? renderItem1 : renderItem}
            />
          )}
        </View>
      </View>
      <SearchModal
        isSearchModalVisible={isSearchModalVisible}
        closeModal={closeSearchModal}
        serachText={serachText}
        setSerachText={setSerachText}
      />
      <RecentSearchModal
        isSearchModalVisible={isRecentSearchModalVisible}
        closeModal={onRecentSearchClose}
        serachText={serachText}
        setSerachText={setSerachText}
        showSelectedSearch={showSelectedSearch}
      />
      <FilterModal
        isFilterModalVisible={isFilterModalVisible}
        closeModal={closeFilterModal}
        brands={filter_brand_data}
        brandFilters={brandFilters}
        applyBrandFilter={applyBrandFilter}
        resetBrandFilter={resetBrandFilter}
      />
      <SortModal
        isSortModalVisible={isSortModalVisible}
        closeModal={closeSortModal}
      />
    </View>
  );
};

ProductsView.propTypes = {
  onFilterPress: PropTypes.func,
  isFilterModalVisible: PropTypes.bool,
  closeFilterModal: PropTypes.func,
  onCategoryModalClick: PropTypes.func,
  isCategoryModalVisible: PropTypes.bool,
};

export default ProductsView;
