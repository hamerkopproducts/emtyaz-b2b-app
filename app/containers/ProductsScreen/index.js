import React, { useEffect, useState } from "react";

import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import ProductsView from "./ProductsView";
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions";
import * as HomePageActions from "../../actions/HomePageActions";
import { I18nManager } from "react-native";

const ProductsScreen = (props) => {
  const [columnCount, setColumnCount] = useState(1);
  const [isSearchModalVisible, setIsSearchModalVisible] = useState(false);
  const [isRecentSearchModalVisible, setIsRecentSearchModalVisible] = useState(
    false
  );
  const [isFilterModalVisible, setIsFilterModalVisible] = useState(false);
  const [isSortModalVisible, setIsSortModalVisible] = useState(false);
  const [serachText, setSerachText] = useState("");
  const [isCategoryModalVisible, setIsCategoryModalVisible] = useState(false);
  const [pageIndex, setPageIndex] = useState(1);
  const [loadedIndexes, setLoadedIndexes] = useState([]);
  const [allPageData, setAllPageData] = useState([]);
  const [lastPage, setLastPage] = useState(1);

  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        props.wishListData(props.userData.data.access_token)
        props.filter_cat_data(props.userData.data.access_token);
        props.filter_brand_data(props.userData.data.access_token);
        props.productsListAll({language: I18nManager.isRTL ? 'ar' : 'en'}, props.userData.data.access_token, 1);
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  };

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if (props.error && props.error.msg) {
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, props.error.msg);
      props.resetError();
    }
    if(props.productsList && props.productsList.success == true) {
      if(pageIndex == 1 && loadedIndexes.indexOf(1) == -1) {
        setLoadedIndexes([1]);
        setAllPageData(Object.assign({}, props.productsList.data));
        const l_page = props.productsList.data.last_page;
        setLastPage(l_page);
        props.productsListServiceActionSuccess({});
      } else if(pageIndex !== 1 && props.productsList.data.current_page == pageIndex) {
        if(loadedIndexes.indexOf(pageIndex) == -1) {
          let data = Object.assign({}, props.productsList.data);
          data.data = [...allPageData.data, ...data.data];
          setAllPageData( data );
          let pageIndexes = loadedIndexes;
          pageIndexes.push(pageIndex);
          setLoadedIndexes(pageIndexes);
          props.productsListServiceActionSuccess({});
        }
      }
    }
  };
  const onSearchPress = () => {
    //alert('hi')
    setIsRecentSearchModalVisible(true);
  };
  const onRecentSearchClose = () => {
    setIsRecentSearchModalVisible(false);
  };

  const changeListingStyle = () => {
    setColumnCount(columnCount === 1 ? 2 : 1);
  };
  const onCartButtonPress = () => {
    props.navigation.navigate("CartScreen");
  };
  const showDetails = (slug) => {
    props.navigation.navigate("DetailsScreen", {slug: slug});
  };
  const showSelectedSearch = () => {
    setIsSearchModalVisible(true);
    setIsRecentSearchModalVisible(false);
  };
  const closeSearchModal = () => {
    setIsSearchModalVisible(false);
  };
  const closeFilterModal = () => {
    setIsFilterModalVisible(false);
  };
  const onFilterPress = () => {
    setIsFilterModalVisible(true);
  };
  const onCategoryModalClick = () => {
    setIsCategoryModalVisible(!isCategoryModalVisible);
  };
  const onSortPress = () => {
    setIsSortModalVisible(true);
  };
  const closeSortModal = () => {
    setIsSortModalVisible(false);
  };
  const applySelectedFilters = (cats, brands) => {
    NetInfo.fetch().then((state) => {
      let params = {
        language: I18nManager.isRTL ? 'ar' : 'en'
      }
      if(cats.length > 0) {
        params.category = cats;
      }
      if(brands.length > 0) {
        params.brand = brands;
      }
      if (state.isConnected) {
        setPageIndex(1);
        setLoadedIndexes([]);
        setAllPageData([]);
        props.productsListAll(params, props.userData.data.access_token, 1);
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  const addRemoveFavProduct = (id) => {
    const apiParam = {
      product_id: id
    };
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        props.addRemoveFav(apiParam, props.userData.data.access_token);
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  const update_products_list = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        const page = pageIndex + 1;
        if(lastPage >= page) {
          setPageIndex(page);
          props.productsListAll({language: I18nManager.isRTL ? 'ar' : 'en'}, props.userData.data.access_token, page);
        }
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }
  
  const category = props.route.params;

  return (
    <ProductsView
      columnCount={columnCount}
      isSearchModalVisible={isSearchModalVisible}
      changeListingStyle={changeListingStyle}
      onCartButtonPress={onCartButtonPress}
      onSearchPress={onSearchPress}
      serachText={serachText}
      setSerachText={setSerachText}
      itemClick={showDetails}
      isRecentSearchModalVisible={isRecentSearchModalVisible}
      onRecentSearchClose={onRecentSearchClose}
      showSelectedSearch={showSelectedSearch}
      closeSearchModal={closeSearchModal}
      isFilterModalVisible={isFilterModalVisible}
      closeFilterModal={closeFilterModal}
      isSortModalVisible={isSortModalVisible}
      closeSortModal={closeSortModal}
      onFilterPress={onFilterPress}
      onCategoryModalClick={onCategoryModalClick}
      isCategoryModalVisible={isCategoryModalVisible}
      onSortPress={onSortPress}
      data={allPageData}
      cartItems={props.cartItems}
      addItemsToCart={props.addItemsToCart}
      navigation={props.navigation}
      isLoading={props.isLoading||props.f_isLoading}
      filter_cat_data={props.f_cat_data}
      filter_brand_data={props.f_brand_data}
      applySelectedFilters={applySelectedFilters}
      category={category}
      wishList={props.wishList}
      addRemoveFav={addRemoveFavProduct}
      update_products_list={update_products_list}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    productsList: state.homepageReducer.productsList,
    userData: state.loginReducer.userData,
    cartItems: state.homepageReducer.cartItems,
    isLoading: state.homepageReducer.p_isLoading,
    error: state.homepageReducer.p_error,
    f_cat_data: state.homepageReducer.filter_cat_data,
    f_brand_data: state.homepageReducer.filter_brand_data,
    wishList: state.homepageReducer.wl_data,
    f_isLoading: state.homepageReducer.f_isLoading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      productsListAll: HomePageActions.productsListAll,
      addItemsToCart: HomePageActions.addItemsToCart,
      resetError: HomePageActions.product_apiServiceActionErrorReset,
      filter_cat_data: HomePageActions.filter_cat_data,
      filter_brand_data: HomePageActions.filter_brand_data,
      addRemoveFav: HomePageActions.addRemoveFav,
      wishListData: HomePageActions.wishListData,
      productsListServiceActionSuccess: HomePageActions.productsListServiceActionSuccess
    },
    dispatch
  );
};

const productsScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsScreen);

productsScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default productsScreenWithRedux;
