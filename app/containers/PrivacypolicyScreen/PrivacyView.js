import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";

const PrivacyView = (props) => {
	const {
		logoutButtonPress,onBackButtonPress
	} = props;

	return (
	
			<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isLogoRequired={false}
				headerTitle={appTexts.PROFILEITEM.PrivacyPolicy}
				isBackButtonRequired={true}
				onBackButtonPress={onBackButtonPress}
				// isRightButtonRequired={true}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
			<View style={styles.contentWrapper}>
			<Text style={styles.privacyContent}>{appTexts.PRIVACY.Heading}</Text>
			</View>

				</View>
			

	);
};

PrivacyView.propTypes = {
	logoutButtonPress: PropTypes.func,
};

export default PrivacyView;
