import React from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity, Image,ScrollView } from "react-native";
import globals from "../../lib/globals";
import { styles, images } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";

const QuotationView = (props) => {
  const { logoutButtonPress } = props;

  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        //headerTitle={'Privacy Policy'}
        isBackButtonRequired={false}
        // isRightButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      /><ScrollView>
      <View style={styles.contentWrapper}>
        <View style={styles.tickImage}>
          <Image source={images.tickImage} style={styles.tickImages} />
          <Text style={styles.thankText}>
            {appTexts.THANKUORDER_VIEW.Thanku}
          </Text>
          <Text style={styles.desText}>
            {appTexts.THANKUORDER_VIEW.Quotationdecritpion}
          </Text>
          <Text style={styles.desText2}>
            {appTexts.THANKUORDER_VIEW.QuotationdecritpionEmail}
          </Text>
        </View>
        <View style={styles.orderDeatils}>
          <View style={styles.orderStatus}>
            <Text style={styles.orderText}>
              {appTexts.THANKUORDER_VIEW.Quotationstatus}
            </Text>
            <Text style={styles.orderText}>Pending</Text>
          </View>
          <View style={styles.orderStatus}>
            <Text style={styles.orderText}>
              {appTexts.THANKUORDER_VIEW.Quotationid}
            </Text>
            <Text style={styles.orderText}>ORD12345</Text>
          </View>
          <View style={styles.qStatus}>
            <Text style={styles.orderText}>
              {appTexts.THANKUORDER_VIEW.QuotationDelivery}
            </Text>
            <Text style={styles.qText}> Jedda, Al Nameem</Text>
          </View>
         
        </View>
       
       

				
				<View style={styles.logoutWrappers}>
            <TouchableOpacity onPress={() => console.log("click")}>
            <View style={styles.yesButton}>
            <Text style={styles.sendbuttonText}>{appTexts.THANKUORDER_VIEW.VIEWQUOATATION}</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => console.log("click")}>
            <View style={styles.noButton}>
            <Text style={styles.sendbuttonText}>{appTexts.THANKUORDER_VIEW.CONTINUESHOPPING}</Text>
          </View>
          </TouchableOpacity>
            </View>
			
				

      </View>
	  </ScrollView>
    </View>
  );
};

QuotationView.propTypes = {
  logoutButtonPress: PropTypes.func,
};

export default QuotationView;
