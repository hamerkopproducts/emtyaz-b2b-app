import React, { useState, useEffect } from 'react';
import { BackHandler,I18nManager } from 'react-native';
import { connect } from 'react-redux';
import ChooseLanguageView from './ChooseLanguageView';
import { bindActionCreators } from "redux";
import RNRestart from 'react-native-restart';
import * as LoginActions from "../../actions/LoginActions";

const ChooseLanguageScreen = (props) => {
  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {};

  const selectLanguage = async(selectedLanguage) => {
      
    if (selectedLanguage === 'EN')
    {
      I18nManager.forceRTL(false);
      props.saveSelectedLanguage(selectedLanguage);
      setTimeout(() => {
        RNRestart.Restart();
      }, 500);
    } 
    else if (selectedLanguage === 'AR')
    {
      I18nManager.forceRTL(true);
      props.saveSelectedLanguage(selectedLanguage);
      setTimeout(() => {
        RNRestart.Restart();
      }, 500);
    } 
    else
    {
      props.navigation.navigate('WalkthroughScreen');
    }
};

  return (
    <ChooseLanguageView selectLanguage={selectLanguage}/>
  );

};


const mapStateToProps = (state, props) => {
  return {
    isLoading: state.loginReducer.isLoading,
    isIntroFinished: state.loginReducer.isIntroFinished
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    saveSelectedLanguage: LoginActions.saveSelectedLanguage
  }, dispatch)
};

const chooseLanguageWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ChooseLanguageScreen);

chooseLanguageWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default chooseLanguageWithRedux;
