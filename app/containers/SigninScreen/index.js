import React, { useState, useEffect } from "react";
import { BackHandler, Keyboard } from "react-native";
import { connect } from "react-redux";
import SigninView from "./SigninView";
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";
import NetInfo from "@react-native-community/netinfo";
import Toast from 'react-native-toast-message';
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions";
import loginReducer from "../../reducers/loginReducer";

const SigninScreen = (props) => {
  //Initialising states
  
  const [number, setNumber] = useState('');

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener(
      "hardwareBackPress",
      (backPressed = () => {
        BackHandler.exitApp();
        return true;
      })
    );
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
    if (props.otpAPIReponse && props.otpAPIReponse.success) {
      props.loginServiceActionSuccess(null, props.phone);
      props.navigation.navigate('OTPScreen', { phone: props.phone})
    }
    else if (props.error && props.error.msg){
      functions.displayToast('error', 'top', props.error.msg);
      props.resetError();
    }
  };
  
  const loginButtonPress = () => {
    if (number.trim() === '') {
      functions.displayToast('error', 'top',  appTexts.VALIDATION_MESSAGE.Type,appTexts.VALIDATION_MESSAGE.Mobile_error_msg);
    }
    else {
      Keyboard.dismiss()
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          let apiParam = {
            "phone": number.trim(),
            "language" : 'en'
          }
          props.doLogin(apiParam);
        } else {
          functions.displayToast('error', 'top', appTexts.VALIDATION_MESSAGE.Type,appTexts.VALIDATION_MESSAGE.Mobile_error_msg);
        }
      });
      // props.navigation.navigate('OTPScreen');
    }
  };
  const signupNewUser = () => {
    props.navigation.navigate('SignupScreen')
    
  };
  const backButtonpress = () => {
    props.navigation.navigate('ChooseLanguageScreen')
  };
 

  return (
    <SigninView
    loginButtonPress={loginButtonPress} 
    backButtonpress={backButtonpress}
    signupNewUser={signupNewUser}
    setNumber={setNumber} 
    number={number}
    isLoading={props.isLoading}
     />
  );
};

const mapStateToProps = (state, props) => {
  return {
    isLoading: state.loginReducer.isLoading,
    error: state.loginReducer.error,
    otpAPIReponse: state.loginReducer.otpAPIReponse,
    homeData: state.loginReducer.userData,
    phone: state.loginReducer.phone
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      doLogin: LoginActions.doLogin,
      resetError: LoginActions.resetError,
      loginServiceActionSuccess: LoginActions.loginServiceActionSuccess,
    },
    dispatch
  );
};

const SignInWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(SigninScreen);

SignInWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default SignInWithRedux;
