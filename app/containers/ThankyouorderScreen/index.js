import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import OrderView from './OrderView';
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";
import globals from "../../lib/globals";
import functions from "../../lib/functions"


const OrderScreen = (props) => {

  

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  
  const logoutButtonPress = () => {
    props.doLogout();
  };

  return (
    <OrderView logoutButtonPress={logoutButtonPress}/>
  );

};


const mapStateToProps = (state, props) => {
  return {
    
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    doLogout: LoginActions.doLogout
  }, dispatch)
};

const profileScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderScreen);

profileScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default profileScreenWithRedux;
