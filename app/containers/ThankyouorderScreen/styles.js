import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;

const images = {
  tickImage: require("../../assets/images/orderItem/tick.png"),
};

const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    //flexDirection:'column',
    backgroundColor: globals.COLOR.screenBackground,
  },
  screenContainer: {
    flex: 1,
    marginTop: globals.INTEGER.heightTen,
    flexDirection: "column",
    backgroundColor: globals.COLOR.white,
    alignItems: "center",
    justifyContent: "center",
  },
  headerButtonContianer: {
    flexDirection: "row",
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: "center",
  },
  headerButton: {
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty,
  },
  headerButtonMargin: {
    marginLeft: globals.MARGIN.marginTen,
  },
  contentWrapper: {
    //backgroundColor:'red',
    flex: 1,
  },
  privacyContent: {
    paddingLeft: "5%",
    paddingRight: "5%",
    paddingTop: hp("2%"),
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakBlack,
    fontSize: 13,
  },
  tickImage: {
    alignItems: "center",
    paddingTop: "10%",
  },
  tickImages: {
    width: 70,
    height: 70,
  },
  thankText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
    color: globals.COLOR.drakBlack,
    fontSize: 18,
    paddingTop: "3%",
  },
  desText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakGray,
    fontSize: 14,
    paddingLeft: "10%",
    paddingRight: "10%",
    //justifyContent:'center',
    textAlign: "center",
  },
  orderStatus: {
    flexDirection: "row",
    justifyContent: "center",
  },
  orderText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakBlack,
    fontSize: 14,
  },

  orderDeatils: {
    paddingTop: "4%",
  },
  billDeatils: {
    paddingTop: "4%",
  },
  billText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinssemiBold,
    color: globals.COLOR.drakBlack,
    fontSize: 15,
    paddingLeft: "5%",
    paddingRight:'5%',
    paddingTop: "2%",
    paddingBottom: "3%",
  },
  sarText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: "#242424",
    fontSize: 13,
    paddingTop: "1.5%",
    //paddingLeft:'5%'
  },
  sarsText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: "#242424",
    fontSize: 13,
    paddingTop: "1.5%",
  },

  firstRow: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  billwrapper:{
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: "5%",
    paddingRight: "5%",
    alignItems: "center",
  },
  vatSection:{
    flexDirection: "row",
    justifyContent: "space-between", paddingLeft: "5%",
    paddingRight: "5%",paddingTop:'3%'
  },
  bottomBorder:{
    borderBottomWidth: 1,
            borderBottomColor: "lightgray",
            marginLeft: "5%",
			marginRight: "5%",
			paddingTop:'3%',
  }, logoutWrappers:{
    paddingLeft:'5%',
    paddingRight:'5%',
   paddingBottom:'15%',
   paddingTop:'12%',
   //alignSelf:'center',
   flexDirection:'row',
   justifyContent:'space-around'
 },

yesButton:{
  backgroundColor:  globals.COLOR.themeGreen,
  width: 156,
  height: 48,
  justifyContent: "center",
  alignItems: "center",
},
noButton:{
  backgroundColor:  globals.COLOR.themeOrange,
  width: 156,
  height: 48,
  justifyContent: "center",
  alignItems: "center",
},
sendbuttonText:{
  color: globals.COLOR.white,
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  fontSize: 12, 
}
});

export { images, styles };
