import React, { useState, useRef } from "react";
import {
  View,
  TouchableOpacity,
  Text,
  FlatList,
  Image,
  ScrollView,
  TextInput,
} from "react-native";

import globals from "../../lib/globals";
import { styles } from "./styles";
import CartCard from "../../components/CartCard";
import DividerLine from "../../components/DividerLine";

import Header from "../../components/Header";
import appTexts from "../../lib/appTexts";
import ListModal from "../../components/ListModal";
import BannerModal from "../../components/BannerModal";
import Loader from "../../components/Loader";

const CartView = (props) => {
  const {
    notesText,
    setNotesText,
    listModalIndex,
    closeListModal,
    onBackButtonPress,
    openSavedListModal,
    openWhishListModal,
    isBannerModalVisible,
    closeBannerModal,
    openBannerModal,
    cartItems,
    removeItem,
    updateItemQty,
    addRemoveFav,
    wishList,
    isLoading,
    map_local_data
  } = props;

  let listData = cartItems;

  let count = 0;
  if(listData) {
    count = listData.length;
  }

  let location_name = 'Riyadh, SA';
	if(typeof map_local_data != 'undefined' && map_local_data.location && map_local_data.location.name) {
		location_name = map_local_data.location.name;
	}

  const renderItem = ({ item, index }) => (
    <CartCard
      index={index}
      item={item} 
      itemImage={item.image}
      removeItem={removeItem}
      updateItemQty={updateItemQty}
      addRemoveFav={addRemoveFav}
      wishList={wishList}
    />
  );

  return (
    <View style={styles.screenMain}>

      {isLoading && <Loader />}

      <Header
        navigation={props.navigation}
        isBackButtonRequired={true}
        headerTitle={appTexts.CART.HeadingCart}
        onBackButtonPress={onBackButtonPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <ScrollView
        style={styles.screenContainerScrollView}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.screenContainer}>
          <View style={styles.container}>
            <View style={styles.categoriesView}>
              <Text style={styles.itemsCount}>{appTexts.CART.Items} ({count})</Text>
              <View style={styles.selectedView}>
                <TouchableOpacity
                  style={styles.itemView}
                  onPress={() => {
                    openSavedListModal();
                  }}
                >
                  <Text style={styles.symbolText}>{"+"}</Text>
                  <Text style={styles.selectedItemText}>
                    {appTexts.CART.AddFav}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <DividerLine />
          <View style={styles.listContainer}>
            {listData.length === 0 ? (
              <View style={styles.noDataContainer}>
                <Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text>
              </View>
            ) : (
              <FlatList
                style={styles.flatListStyle}
                data={listData}
                extraData={listData}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                renderItem={renderItem}
              />
            )}
          </View>
          <View style={styles.locationLabelView}>
            <Text style={styles.locationLabel}>
              {appTexts.CART.Location}
              <Text style={styles.locationText}>{location_name}</Text>
            </Text>
          </View>
          <View style={styles.notesView}>
            <TextInput
              style={styles.notesInput}
              value={notesText}
              placeholder={appTexts.CART.Notes}
              clearButtonMode={"while-editing"}
              onChangeText={(val) => {
                setNotesText(val);
              }}
              autoCapitalize="none"
            ></TextInput>
          </View>
          <View style={styles.dateView}>
            <Text style={styles.dateLabel}>{appTexts.CART.ExpectedDate}</Text>
            <View style={styles.dateSelectView}>
              <Image
                style={styles.calendarImage}
                source={require("../../assets/images/cartScreenIcons/calendar.png")}
              />
              <Text style={styles.dateText}>{"10-06-2020"}</Text>
            </View>
          </View>
          <TouchableOpacity
            style={styles.requestQuoteView}
            onPress={() => {
              openBannerModal();
            }}
          >
            <Text style={styles.buttonText}>{appTexts.CART.QuoteRequest}</Text>
          </TouchableOpacity>
        </View>
        <ListModal
          closeModal={closeListModal}
          listModalIndex={listModalIndex}
        />
        <BannerModal
          isBannerModalVisible={isBannerModalVisible}
          closeModal={closeBannerModal}
          bannerText={"Moved To Save For Later"}
        />
      </ScrollView>
    </View>
  );
};

CartView.propTypes = {};

export default CartView;
