import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import CartView from './CartView';
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import functions from "../../lib/functions"
import * as HomePageActions from "../../actions/HomePageActions";

const CartScreen = (props) => {

  const [notesText, setNotesText] = useState('');
  const [listModalIndex, setListModalIndex] = useState(-1);
  const [isBannerModalVisible, setIsBannerModalVisible] = useState(false);
  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  const changeListingStyle = () => {
    setColumnCount(columnCount === 1 ? 2 :1);
  };
  const onBackButtonPress = () => {
   props.navigation.goBack()
  };
  const closeListModal = () => {
    setListModalIndex(-1);
  };
  
  const openWhishListModal = () => {
    setListModalIndex(1);
  };
  const openSavedListModal = () => {
    setListModalIndex(0);
  };
  const closeBannerModal = () => {
    setIsBannerModalVisible(false)
  };
  const openBannerModal = () => {
    setIsBannerModalVisible(true)
  };
  const addRemoveFavProduct = (id) => {
    const apiParam = {
      product_id: id
    };
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        props.addRemoveFav(apiParam, props.userData.data.access_token);
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  return (
    <CartView 
      onBackButtonPress={onBackButtonPress} 
      notesText={notesText} 
      setNotesText={setNotesText} 
      closeListModal={closeListModal} 
      listModalIndex={listModalIndex}
      openWhishListModal={openWhishListModal}
      openSavedListModal={openSavedListModal}
      isBannerModalVisible={isBannerModalVisible} closeBannerModal={closeBannerModal} openBannerModal={openBannerModal}
      cartItems={props.cartItems}
      removeItem={(index)=> {
        let items = [...props.cartItems];
        items.splice(index, 1);
        props.addItemsToCart(items);
      }}
      updateItemQty={(itemIndex, qty) => {
        let items = [...props.cartItems];
        if(items[itemIndex].quantity + qty < 1) {
          return false;
        }
        items[itemIndex].quantity = items[itemIndex].quantity + qty;
        props.addItemsToCart(items);
      }}
      wishList={props.wishList}
      addRemoveFav={addRemoveFavProduct}
      isLoading={props.f_isLoading}
      map_local_data={props.map_local_data}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    cartItems: state.homepageReducer.cartItems,
    wishList: state.homepageReducer.wl_data,
    f_isLoading: state.homepageReducer.f_isLoading,
    userData: state.loginReducer.userData,
    map_local_data: state.loginReducer.map_local_data
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    addItemsToCart: HomePageActions.addItemsToCart,
    addRemoveFav: HomePageActions.addRemoveFav,
    wishListData: HomePageActions.wishListData,
  }, dispatch)
};

const cartScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(CartScreen);

cartScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default cartScreenWithRedux;
