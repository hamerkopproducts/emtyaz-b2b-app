import React, { useState } from 'react';
import { View, ScrollView, Text, FlatList, StatusBar } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";
import PropTypes from 'prop-types';
import Header from "../../components/Header";
import ShopsByCategoryItem from "../../components/ShopsByCategoryItem";
import BannerImage from "../../components/BannerImage";

import HeadingWithRightLink from "../../components/HeadingWithRightLink";
import PromotionalProductCard from "../../components/PromotionalProductCard";
import DividerLine from "../../components/DividerLine";
import { SliderBox } from "react-native-image-slider-box";
import appTexts from "../../lib/appTexts";
import { TouchableOpacity } from 'react-native-gesture-handler';
import Loader from "../../components/Loader";

const HomeView = (props) => {
	const {
		onCartButtonPress,
		itemClick,
		categorySellAllPressed,
		mapClick,
		topbannerImageDetails,
		bottombannerImageDetails,
		shopByCategorydetail,
		promotionalProduct,
		addRemoveFav,
		f_isLoading,
		listPage,
		wishList,
		map_local_data
	} = props;

	let WishListItems = [];
	if(wishList.data) {
		WishListItems = wishList.data.map(item => {
			return item.product_id;
		});
	}

	let images = [
		topbannerImageDetails

	];
	let bottombanner = [
		bottombannerImageDetails
	]

	const [promoProduct, setPromoProducts] = useState(promotionalProduct.slice(0,2));
	const [seeAllPromoProducts, setSeeAllPromoProducts] = useState(false);
	const [shopByCatSliced, setShopByCatSliced] = useState(shopByCategorydetail.slice(0, 3));
	const [showCatSeeAllBtn, setShowCatSeeAllBtn] = useState(true);

	let location_name = 'Riyadh, SA';
	if(typeof map_local_data != 'undefined' && map_local_data.location && map_local_data.location.name) {
		location_name = map_local_data.location.name;
	}

	return (

		<View style={styles.screenMain}>

			<StatusBar
				barStyle="light-content"
				hidden={false}
				backgroundColor={globals.COLOR.screenBackground}
			/>

			{f_isLoading && <Loader />}

			<Header
				navigation={props.navigation}
				isLogoRequired={true}
				isLanguageButtonRequired={true}
				isRightButtonRequired={true}
				onCartButtonPress={onCartButtonPress}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>

			<ScrollView style={styles.screenContainerScrollView} showsVerticalScrollIndicator={false}>
				<View style={styles.screenDesignContainer}>
					<View style={styles.screenContainerWithMargin}>
						<HeadingWithRightLink 
							locationnameLabel={location_name} 
							leftIcon={require('../../assets/images/home/maps.png')}
							rightLinkText={appTexts.HOMETEXT.change} 
							color={'green'} 
							onItemClick={mapClick} 
						/>
					</View>

					<SliderBox images={images} sliderBoxHeight={100} ImageComponentStyle={{ borderRadius: 2, width: globals.SCREEN_SIZE.width - 40 }} />
					{/*<BannerImage bannerImage={require('../../assets/images/temp/banner.png')} />*/}
					<View style={styles.screenContainerWithMargin}>
						{/* <HeadingWithRightLink 
						nameLabel={appTexts.HOMETEXT.shopcategory} 
						rightLinkText={appTexts.HOMETEXT.seeall} 
						color={'yellow'}
						/> */}
						<View style={styles.categoryContainer}>
							<Text style={styles.categoryTitle}>
								{appTexts.HOMETEXT.shopcategory}
							</Text>
							{showCatSeeAllBtn &&
								<TouchableOpacity onPress={() => {
									setShowCatSeeAllBtn(false);
									setShopByCatSliced(shopByCategorydetail) 
								}}>
									<Text style={styles.categorySellall}>
										{appTexts.HOMETEXT.seeall}
									</Text>
								</TouchableOpacity>
							}
						</View>
						<View style={styles.categoryItemRow}>
							<FlatList
								data={shopByCatSliced}
								numColumns={3}
								keyExtractor={(item) => shopByCatSliced.indexOf(item).toString()}
								showsVerticalScrollIndicator={true}
								renderItem={({ item, index, separators }) => (
									<ShopsByCategoryItem onPress={listPage} itemData={item} />
								)}
							/></View>
						<BannerImage type="middle" bannerImage={require('../../assets/images/temp/banner-sec.png')} />

						<HeadingWithRightLink
							nameLabel={appTexts.HOMETEXT.promotionalitems}
							rightLinkText={appTexts.HOMETEXT.seeall}
							color={'yellow'}
							onItemClick={() =>{
								setSeeAllPromoProducts(true);
								setPromoProducts(promotionalProduct);
							}}
							seeAllPromoProducts={seeAllPromoProducts==false}
						/>


					</View>
					<DividerLine />
					<View style={styles.promotionalContainer}>
						<FlatList
							data={promoProduct}
							keyExtractor={(item) => promoProduct.indexOf(item).toString()}
							showsVerticalScrollIndicator={true}
							renderItem={({ item, index, separators }) => (
								<PromotionalProductCard
									wishListItems={WishListItems}
									navigation={props.navigation}
									addToCart={(item)=> {
										let existing = false;
										let all = props.cartItems ? [...props.cartItems] : [];
										if(props.cartItems) {
											for(let inc=0; inc<all.length; inc++) {
												const _itm = all[inc];
												if(_itm.type ==  item.type &&
													_itm.variant_id ==  item.variant_id &&
													_itm.quantityId ==  item.quantityId &&
													_itm.product_id ==  item.product_id
													) {
														existing = true;
														all[inc].quantity += 1;
														props.addItemsToCart(all);
													}
											}
										}
										if(existing == false) {
											all.push(item);
											props.addItemsToCart(all);
										}
									}}
									item={item} 
									itemClick={itemClick}
									addRemoveFav={addRemoveFav}
								/>
							)}
						/>
					</View>

					<View style={styles.screenContainerWithMargin}>
						<BannerImage type="bottom" bannerImage={bottombanner} />
					</View>

				</View>
			</ScrollView>
		</View >


	);


};

HomeView.propTypes = {
	onCartButtonPress: PropTypes.func,

};

export default HomeView;
