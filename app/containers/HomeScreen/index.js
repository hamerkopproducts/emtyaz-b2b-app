import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import HomeView from './HomeView';
import { bindActionCreators } from "redux";

import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions";
import * as HomePageActions from "../../actions/HomePageActions";


const HomeScreen = (props) => {
  
  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {

        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.wishListData(props.userData.data.access_token)
        let apiParam = {
          "language": props.languageSelected
        }
        props.homePage(apiParam, props.userData.data.access_token);
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {

  };

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if (props.f_error && props.f_error.msg) {
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, props.error.msg);
      props.resetError();
    }
    if (props.fav_data && props.fav_data.success == true) {
      props.wishListData(props.userData.data.access_token);
      functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, props.fav_data.msg);
      props.resetFavSuccess();
    }
  };
  const onCartButtonPress = () => {
    props.navigation.navigate('CartScreen')
  };
  const showDetails = (slug) => {
    props.navigation.navigate('DetailsScreen', {slug: slug})
  };
  const listPage = (cat) => {
    props.navigation.navigate('Products', {categoryId: cat});
  }

  const categorySellAllPressed = () => {
    props.navigation.navigate('ChoosecategoryScreen')
  };
  const onBackButtonPress = () => {
    props.navigation.goBack()
  };

  const mapClick = () => {
    props.navigation.navigate('MapScreen')
  };

  const addRemoveFavProduct = (id) => {
    const apiParam = {
      product_id: id
    };
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        props.addRemoveFav(apiParam, props.userData.data.access_token);
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  let topbannerImage = ''
  try {
    topbannerImage = props.homepageAPIReponse.data.topBanner.app_image
  } catch (error) {
    topbannerImage = ''
  }
  let bottomBannerImage

  try {
    bottomBannerImage = props.homepageAPIReponse.data.bottomBanner.app_image
  } catch (error) {
    bottomBannerImage = ''

  }
  let catifo = []

  try {
    catifo = props.homepageAPIReponse.data.shopByCategory
  } catch (error) {
    catifo = []

  }
  let proinfo = ''
  try {
    proinfo = props.homepageAPIReponse.data.promotionalProduct
  } catch (error) {
    proinfo = '';
  }
  
  const addItemsToCart = props.addItemsToCart;
  const cartItems = props.cartItems;

  return (
    <HomeView
      onCartButtonPress={onCartButtonPress}
      itemClick={showDetails}
      categorySellAllPressed={categorySellAllPressed}
      onBackButtonPress={onBackButtonPress}
      mapClick={mapClick}
      topbannerImageDetails={
        topbannerImage
      }
      bottombannerImageDetails={
        bottomBannerImage
      }
      shopByCategorydetail={
        catifo
      }
      promotionalProduct={proinfo}
      cartItems={cartItems}
      addItemsToCart={addItemsToCart}
      navigation={props.navigation}
      addRemoveFav={addRemoveFavProduct}
      f_isLoading={props.f_isLoading}
      f_error={props.f_error}
      fav_data={props.fav_data}
      listPage={listPage}
      wishList={props.wishList}
      map_local_data={props.map_local_data}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    phone: state.loginReducer.phone,
    userData: state.loginReducer.userData,
    error: state.loginReducer.error,
    languageSelected: state.loginReducer.languageSelected,
    homepageAPIReponse: state.homepageReducer.homepageAPIReponse,
    errorHomePage: state.homepageReducer.error,
    cartItems: state.homepageReducer.cartItems,
    f_isLoading: state.homepageReducer.f_isLoading,
    f_error: state.homepageReducer.f_error,
    fav_data: state.homepageReducer.fav_data,
    wishList: state.homepageReducer.wl_data,
    map_local_data: state.loginReducer.map_local_data
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    homePage: HomePageActions.homePage,
    addItemsToCart: HomePageActions.addItemsToCart,
    addRemoveFav: HomePageActions.addRemoveFav,
    resetError: HomePageActions.fav_service_error_reset,
    resetFavSuccess: HomePageActions.fav_service_reset_success,
    wishListData: HomePageActions.wishListData
  }, dispatch)
};

const homeScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);

homeScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default homeScreenWithRedux;
