import React, { Component } from "react";
import { View, Image, StatusBar } from "react-native";
import { styles } from "./styles";
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";

class Splash extends Component {

	constructor(props) {
		super(props);
	}

	navigateToScreen = async () => {
		try {
			if (this.props.languageSelected) {
				if (this.props.isLogged === true) {
					this.props.navigation.navigate('TabNavigator', { screen: this.props.currentScreen });
				} else {
					if(this.props.isIntroFinished) {
						this.props.navigation.navigate('SigninScreen');
					} else {
						this.props.navigation.navigate('WalkthroughScreen');
					}
				}
			} else {
				this.props.navigation.navigate('ChooseLanguageScreen');
			}
		} catch (e) {
			if (this.props.isLogged === true) {
				this.props.navigation.navigate('TabNavigator', { screen: this.props.currentScreen });
			} else {
				this.props.navigation.navigate('ChooseLanguageScreen');
			}
		}
	}

	componentDidMount() {
		setTimeout(() => {
			this.navigateToScreen();
		}, 2000);
	}

  render() {
    return (
      <View style={styles.screenMain}>
		  <StatusBar backgroundColor={'#37b34a'} hidden={true} barStyle={'light-content'} />
			<View style={styles.screenMainContainer}>
			 <Image source={require('./images/splash_new.png')}style={styles.logo}/>
			</View>
		</View>
    );
  }
}
/**
 * Assigning the state to props
 * @param {*} state
 */
const mapStateToProps = (state) => ({
	isLogged: state.loginReducer.isLogged,
	languageSelected: state.loginReducer.languageSelected,
	currentScreen: state.loginReducer.currentScreen,
	isIntroFinished: state.loginReducer.isIntroFinished,
});

/**
 * Dispatch action
 * @param {*} dispatch
 */
const mapDispatchToProps = dispatch => ({
});

/**
 * Connecting the LoginScreen component to a Redux store
 */
export default connect(mapStateToProps, mapDispatchToProps)(Splash)