import { StyleSheet } from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {};
const styles = StyleSheet.create({
  screenMain:{
    flex:1,
    backgroundColor: globals.COLOR.screenBackground
},
bgImage:{
  position: 'absolute',
  top: 0,
  width: globals.SCREEN_SIZE.width,
  height: globals.SCREEN_SIZE.height
},
screenMainContainer:{
  position: 'absolute',
  top: 0,
  width: globals.SCREEN_SIZE.width,
  height: globals.SCREEN_SIZE.height,
  alignItems: 'center',
  justifyContent: 'center'
},
screenContainer:{
  flex:1,
  backgroundColor: globals.COLOR.transparent,
  marginBottom: globals.INTEGER.screenBottom
},
logo:{
  width: '100%',
  height: '100%',
  // resizeMode: 'cover'
}
});

export { images, styles };
