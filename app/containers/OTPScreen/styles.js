import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const images = {
  backIcon: require("../../assets/images/chooseLanguage/backarrow.png"),
  logoImage: require("../../assets/images/header/Emtyaz_logo_color.png"),
};
const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    backgroundColor: globals.COLOR.screenBackground,
  },
  arrowWrapper: {
    paddingLeft: "5%",
    paddingTop: hp("2%"),
  },
  arrowicon: {
    width: 20,
    height: 20,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
  logoImage: {
    alignSelf: "center",
  },
  logoWrapper: {
    paddingTop: hp("2%"),
  },
  contentWrapper: {
    justifyContent: "center",
    paddingTop: hp("15%"),
  },
  contentHeader: {
    textAlign: "center",
    paddingTop: hp("1%"),
    paddingBottom: hp("1%"),
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinssemiBold,
    color: globals.COLOR.drakBlack,
    fontSize: 17,
  },
  contentDescription: {
    textAlign: "center",
    paddingLeft: "10%",
    paddingRight: "10%",
    lineHeight: 24,
    fontSize: 13,
    color: globals.COLOR.drakGray,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
  },
  buttonWrapper: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: hp("5%"),
    //backgroundColor:'red'
  },
  otpWrapper: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignSelf: "center",
    width: "90%",
    //alignItems:'center',
    paddingTop: hp("1%"),
    paddingLeft: "12%",
    paddingRight: "12%",
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
  textInput: {
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
    paddingLeft: 0,
    height: 50,
    //fontSize: 13,
    fontSize: hp("3.3%"),
    width: "17%",
    borderColor: "#c1c1c1",
    borderBottomWidth: 1,
    textAlign: "center",
    fontFamily: globals.FONTS.avenirHeavy,
    //fontFamily: globals.FONTS.avenirLight,
  },
  buttonbottomWrapper: {
    flexDirection: "row",
    justifyContent: "center",
    paddingTop: hp("3%"),
    alignItems: "center",
  },
  DdntreceiveText: {
    fontSize: 13,
    color: globals.COLOR.lightBlack,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
  },
  resendText: {
    fontSize: 14,
    paddingLeft: "2%",
    textDecorationLine: "underline",
    color: globals.COLOR.lightBlack,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
  },
  modalMainContent: {
    //justifyContent: "center",
    justifyContent: "flex-end",
    margin: 0,
  },
  modalmainView: {
    backgroundColor: "white",
    padding: "4%",
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  resendWrapper: {
    flexDirection: "row",
    justifyContent: "center",
    paddingLeft: "3%",
    paddingRight: "3%",
    paddingTop: hp("5%"),
    paddingBottom: hp("5%"),
  },
  flagsectionWrapper: {
    flexBasis: "23%",
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "center",
    borderBottomColor: "#c1c1c1",
    borderBottomWidth: 1,
  },
  numbersectionWrapper: {
    flexBasis: "48%",
    marginTop: 30,
    marginLeft: "2%",
    marginRight:5
  },
  resendMainView: { backgroundColor: 'white', padding: 20, justifyContent: 'space-between' },
  resendOtp: { flexDirection: 'row', marginBottom: 50,justifyContent: 'space-between' },
  closeButton:{
    alignSelf:'flex-end',
    paddingTop:'2%',
    paddingBottom:'2%',
    color:'#707070',
    fontSize:16
  },
  buttonsectionWrapper: {
    marginTop: 35,
    backgroundColor: globals.COLOR.themeOrange,
    padding: 10
  },
  resendButton: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: globals.COLOR.themeOrange,
    padding: 10
  },
  numberEnter: {
    width: '45%',
    marginLeft: 10,
    fontSize: 14,
    color: "#7D7B7D",
    alignItems: "center",
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    borderBottomColor: "#c1c1c1",
    borderBottomWidth: 1,
    textAlign: I18nManager.isRTL ? "right" : "left",
    
  },
  countryCode: {
    fontSize: 14,
                alignItems: "center",
                color: "#7D7B7D",
                // borderBottomColor: "#c1c1c1",
                // borderBottomWidth: 1,

  },
  orangeWrapper: {
    backgroundColor: "pink",
  },
  resendbText: {
    color: "white",
    fontSize: 13,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
  },
});

export { images, styles };
