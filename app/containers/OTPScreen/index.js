import React, { useState, useEffect } from "react";
import { BackHandler, Keyboard } from "react-native";
import { connect } from "react-redux";
import OTPView from "./OTPView";
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";
import * as HomePageActions from "../../actions/HomePageActions";
import NetInfo from "@react-native-community/netinfo";
import functions from "../../lib/functions";
import appTexts from "../../lib/appTexts";
import Toast from "react-native-toast-message";
import { is } from "@babel/types";


const OTPScreen = (props) => {
  //Initialising states
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [otp1, setOtp1] = useState('');
  const [otp2, setOtp2] = useState('');
  const [otp3, setOtp3] = useState('');
  const [otp4, setOtp4] = useState('');


  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener(
      "hardwareBackPress",
      (backPressed = () => {
        BackHandler.exitApp();
        return true;
      })
    );
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
    if (props.userData && props.userData.success) {
      try {
        if(props.userData.data.user.location_details && 
          props.userData.data.user.location_details != '' && 
          props.userData.data.user.location_details != null) 
          {
          const parsedLocation = JSON.parse(props.userData.data.user.location_details);
          props.saveLocation(parsedLocation);
        } else {
          props.saveLocation({});
        }
      } catch(err) {
        props.saveLocation({});
      }
      props.navigation.navigate('TabNavigator');
    }
   

    else if (props.error && props.error.msg) {
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.specifyOtp);
      props.resetError();
    }

  };

  const loginButtonPress = () => {
    props.doLogin({});
  };
  const toggleModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  const otpVerificationPress = () => {
    if (otp1.trim() === '' || otp2.trim() === '' || otp3.trim() === '' || otp4.trim() === '') {
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.specifyOtp);
    }
    else {
      Keyboard.dismiss()
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          let apiParam = {
            "phone": props.phone,
            "otp": otp1 + otp2 + otp3 + otp4,
            "fcm_token": "fdgrghajdhaskfldhgkagfagwifhwe"
          }
          props.doVerifyOtp(apiParam);
        } else {
          functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.ALERT_MESSAGES.specifyOtp);
        }
      });
    }
    // props.navigation.navigate('MapView');

  };
  const resendOtp = () => {
    functions.displayToast('success', 'bottom', 'Otp send to your registred number');

  };
  const backButtonPress = () => {
    props.navigation.goBack()

  };
  return (
    <OTPView
      otpVerificationPress={otpVerificationPress}
      resendOtp={resendOtp}
      backButtonPress={backButtonPress}
      otp1={otp1}
      otp2={otp2}
      otp3={otp3}
      otp4={otp4}
      setOtp1={setOtp1}
      setOtp2={setOtp2}
      setOtp3={setOtp3}
      setOtp4={setOtp4}
      isModalVisible={isModalVisible}
      toggleModal={toggleModal}
      isLoading={props.isLoading}
      
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    isLoading: state.loginReducer.isLoading,
    phone: state.loginReducer.phone,
    userData: state.loginReducer.userData,
    error: state.loginReducer.error,
    languageSelected: state.loginReducer.languageSelected,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      doLogin: LoginActions.doLogin,
      doVerifyOtp: LoginActions.doVerifyOtp,
      resetError: LoginActions.resetError,
      saveLocation: LoginActions.saveMapLocation
    },
    dispatch
  );
};

const OtpVerifyWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OTPScreen);

OtpVerifyWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default OtpVerifyWithRedux;
