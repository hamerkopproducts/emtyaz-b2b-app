import React from "react";
import { View, StatusBar, 
  Text, TouchableOpacity, 
  Image, SafeAreaView
} from "react-native";

import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { images, styles } from "./styles";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import AppIntroSlider from "react-native-app-intro-slider";

const data = [
  {
    id:'1',
    heading: appTexts.WALKTHROUGH_SLIDER.Heading,
    description: appTexts.WALKTHROUGH_SLIDER.Descritpion,
    image: require("../../assets/images/walkThrough/slide.png"),
  },
  { id:'2',
    heading: appTexts.WALKTHROUGH_SLIDER.Heading,
    description: appTexts.WALKTHROUGH_SLIDER.Descritpion,
    image: require("../../assets/images/walkThrough/slide.png"),
  },
  {  id:'3',
    heading: appTexts.WALKTHROUGH_SLIDER.Heading,
    description: appTexts.WALKTHROUGH_SLIDER.Descritpion,
    image: require("../../assets/images/walkThrough/slide.png"),
  },
];


const WalkthroughView = (props) => {
  _renderItem = ({ item }) => {
    return (
      <View
        style={[
          styles.slide,
          {
            backgroundColor: item.bg,
          },
        ]}
      >
        <StatusBar
          barStyle="light-content"
          hidden={false}
          backgroundColor={globals.COLOR.screenBackground}
        />
        <SafeAreaView>
          <Image source={item.image} style={styles.image} />
          <View style={styles.WalkthroughContents}>
            <Text style={styles.contentHeading}>{item.heading}</Text>
          </View>

        </SafeAreaView>
      </View>
    );
  };

  _keyExtractor = (item, index) => index.toString();

  const {
    onBackButtonPress, 
    skipButtonPressed,
    nextButtonPressed,
    onFinish,
    currentIntroSlider,
    updateCurrentIntroSlider
  } = props;

  return (
    <View style={styles.screenMain}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={globals.COLOR.screenBackground}
      />
      <View style={styles.arrowWrapper}>
        <TouchableOpacity onPress={() => { onBackButtonPress() }}>
          <Image
            source={images.backIcon}
            resizeMode="contain"
            style={styles.arrowicon}
          />
        </TouchableOpacity>
      </View>
      <AppIntroSlider
        ref={component => intro_slider = component}
        keyExtractor={this._keyExtractor}
        renderItem={this._renderItem}
        showDoneButton={false}
        showNextButton={false}
        dotStyle={{ backgroundColor: "green", marginBottom: hp('45%'), width: 26, height: 3 }}
        activeDotStyle={{ backgroundColor: "#8FBC8F", marginBottom: hp('45%'), width: 26, height: 3 }}
        data={data}
        onSlideChange={(a, b) => {
          updateCurrentIntroSlider(a);
        }}
      />
      <View style={styles.buttonWrapper}>
      <TouchableOpacity onPress={() => { skipButtonPressed() }}>
        <View style={styles.leftButton}>
          <Text style={styles.skipText}>{appTexts.WALKTHROUGH_SLIDER.Skip}</Text>
          
        </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={() => {
          const _nextIndex = currentIntroSlider + 1;
          updateCurrentIntroSlider(_nextIndex);
          if(data.length == _nextIndex) {
            onFinish();
          } else {
            intro_slider.goToSlide(_nextIndex);
          }
         }}>
        <View style={styles.leftButton}>
          <Text style={styles.skipText}>{appTexts.WALKTHROUGH_SLIDER.Next}</Text>
    
        </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default WalkthroughView;
