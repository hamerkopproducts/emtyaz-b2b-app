import React, { useState, useEffect } from 'react';
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';
import WalkthroughView from './WalkthroughView';
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";

const WalkthroughScreen = (props) => {
  //mounted
  useEffect(() => handleComponentMounted(), []);
  const [currentIntroSlider, updateCurrentIntroSlider] = useState(0);

  const handleComponentMounted = () => {
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
  };

  const onBackButtonPress = () => {
    props.navigation.goBack()
  };

  const skipButtonPressed = () => {
    props.navigation.navigate('SigninScreen')
  };

  const nextButtonPressed =() => {
   console.log('next')
  }

  const introFinished = () => {
    props.introFinished();
    props.navigation.navigate('SigninScreen');
  }

  return (
    <WalkthroughView
      onBackButtonPress={onBackButtonPress}
      skipButtonPressed={skipButtonPressed}
      nextButtonPressed={nextButtonPressed}
      onFinish={introFinished}
      currentIntroSlider={currentIntroSlider}
      updateCurrentIntroSlider={(index) => updateCurrentIntroSlider(index)}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    languageSelected: state.loginReducer.languageSelected
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    introFinished: LoginActions.introFinished
  }, dispatch)
};

const WalkthroughWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(WalkthroughScreen);

WalkthroughWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default WalkthroughWithRedux;
