import React from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity,I18nManager } from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";
import { Accordion } from "native-base";
import Icon from "react-native-vector-icons/AntDesign";
//import Icon from 'react-native-vector-icons/FontAwesome'; 
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const dataArray = [
  {
    title: appTexts.CATEGORY_CONTENT.Title,
    content: appTexts.CATEGORY_CONTENT.Content,
  },
  {
	title: appTexts.CATEGORY_CONTENT.Title,
    content: appTexts.CATEGORY_CONTENT.Content,
  },
  {
	title: appTexts.CATEGORY_CONTENT.Title,
    content: appTexts.CATEGORY_CONTENT.Content,
  },
  {
	title: appTexts.CATEGORY_CONTENT.Title,
    content: appTexts.CATEGORY_CONTENT.Content,
  },
];

const CategoryView = (props) => {
  const {
    onBackButtonPress,
    shopByCategorydetail
    
	} = props;

  const _renderHeader = (item, expanded) => {
    return (
      <View style={styles.renderHeadings}>
		   <Icon style={{ fontSize: 20, color: "#ACACAC" }} name="infocirlceo" />
        <Text style={styles.rendercontentText}> {item.lang.en.name}</Text>
        {expanded ? (
          <Icon style={{ fontSize: 15, color: "#ACACAC" }} name="minus" />
        ) : (
          <Icon style={{ fontSize: 15, color: "#ACACAC" }} name="plus" />
        )}
      </View>
    );
  };
  const _renderContent = (item,expanded) => {
	return (
	
		<View style={styles.listWrapper}>
			<View style={{flexDirection:'row',alignItems:'center'}}>
			<Icon style={{ fontSize: 20, color: "#ACACAC" }} name="linechart" />
		    <Text style={styles.listItem}> {item.lang.en.name}</Text>
			</View>
			<Icon style={{ fontSize: 12, color: "#ACACAC" , transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]}} name="right" />
		</View>
	  );
  };

  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        headerTitle={appTexts.PROFILEITEM.Choosecat}
        isBackButtonRequired={true}
        onBackButtonPress={onBackButtonPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <View style={styles.formWrapper}>

        <Accordion
          dataArray={shopByCategorydetail}
          animation={true}
          expanded={true}
          renderHeader={_renderHeader}
          renderContent={_renderContent}
          style={{ borderWidth: 0 }}
        />
      </View>
    </View>
  );
};

CategoryView.propTypes = {
  logoutButtonPress: PropTypes.func,
};

export default CategoryView;
