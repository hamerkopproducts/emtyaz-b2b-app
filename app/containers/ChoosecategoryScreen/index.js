import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import CategoryView from './CategoryView';
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";
import globals from "../../lib/globals";
import functions from "../../lib/functions"


const CategoryScreen = (props) => {

  const {
		
		
	} = props;


  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if(props.homepageAPIReponse){
    }
  
  };
  
  const logoutButtonPress = () => {
    props.doLogout();
  };
  const onBackButtonPress = () => {
    props.navigation.goBack()
   };
  return (
    <CategoryView 
    logoutButtonPress={logoutButtonPress}
    onBackButtonPress={onBackButtonPress}
    shopByCategorydetail={props.homepageAPIReponse.data.shopByCategory}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    homepageAPIReponse: state.homepageReducer.homepageAPIReponse
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    doLogout: LoginActions.doLogout
  }, dispatch)
};

const categoryScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoryScreen);

categoryScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default categoryScreenWithRedux;
