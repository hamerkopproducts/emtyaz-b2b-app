import PropTypes from 'prop-types';
import React, { useCallback, useState} from 'react';
import StatusView from './OrderStatusDetailsView'
const OrderStatusDetails = (props) => {

  const {
      } = props; 
      const onBackButtonPress = () => {
        props.navigation.goBack()
       };
  return (
      <StatusView 
      onBackButtonPress={onBackButtonPress}/>
  );
};

OrderStatusDetails.propTypes = {
 
};

export default OrderStatusDetails;