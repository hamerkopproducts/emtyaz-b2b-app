import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";

let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;

const images = {
  binIcon: require("../../assets/images/addressDetails/bin.png"),
  editIcon: require("../../assets/images/addressDetails/edit.png"),
  heartIcon: require("../../assets/images/addressDetails/heart-active.png"),
};

const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    flexDirection: "column",
    //backgroundColor: globals.COLOR.screenBackground,
  },
  screenContainer: {
    flex: 1,
    marginTop: globals.INTEGER.heightTen,
    flexDirection: "column",
    //backgroundColor: globals.COLOR.white,
    // alignItems: 'center',
    // justifyContent:'center'
  },
  firstWrapper:{
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: "5%",
    paddingRight: "2%",
    paddingTop: "5%",
  },
  headerButtonContianer: {
    flexDirection: "row",
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: "center",
  },
  headerButton: {
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty,
  },
  headerButtonMargin: {
    marginLeft: globals.MARGIN.marginTen,
  },
  profileScreen: {
    //backgroundColor:'red',
    paddingLeft: "5%",
    paddingRight: "5%",
  },
  editArrow: {
    alignSelf: "flex-end",
    paddingTop: "4%",
  },
  profileEdit: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: "1%",
    paddingBottom: "5%",
    alignItems: "center",
    //backgroundColor:'red'
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
  },
  profileContent: {
    flexDirection: "row",
  },
  profileInformation: {
    flexDirection: "column",
    flexWrap: "wrap",
    justifyContent: "center",
    paddingLeft: "6%",
  },
  name: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.lightBlack,
    fontSize: 16,
  },
  address: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakGray,
    fontSize: 12,
  },
  place: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakGray,
    fontSize: 12,
  },
  profileItem: {
    paddingTop: "2%",
    // paddingLeft:'5%',
    // paddingRight:'5%'
  },
  modalMainContent: {
    //justifyContent: "center",
    //justifyContent: "flex-end",
    //margin: 0,
  },
  modalmainView: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius: 0,
    borderTopLeftRadius: 0,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  closeButton: {
    alignSelf: "flex-end",
    paddingRight: "3%",
    paddingLeft: "3%",
    paddingTop: "2%",
    paddingBottom: "2%",
    color: "#707070",
    fontSize: 16,
  },
  restText: {
    alignItems: "flex-start",
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
    fontSize: 16,
    paddingRight: "3%",
    paddingLeft: "3%",
    paddingTop: "1%",
    paddingBottom: "2%",
  },
  resetPassword: {
    paddingRight: "3%",
    paddingLeft: "3%",
    paddingTop: "2%",
    paddingBottom: "5%",
  },
  passwordText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    fontSize: 11,
    color: "#8D8D8D",
    //paddingTop:'2%'
  },
  buttonWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: "3%",
    paddingRight: "3%",
    // paddingBottom:'6%',
    // paddingTop:'3%'
  },
  cPassword: {
    marginTop: -12,
    flexDirection: "row",
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  numberEnter: {
    fontSize: 18,
    color: "#242424",
    marginLeft: -4,
    // lineHeight: 9,
    alignItems: "center",
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    flex: 1,
  },
  imageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: "stretch",
    alignItems: "center",
  },
  pwdOne: {
    paddingTop: "5%",
  },
  shadowContainerStyle: {
    //<--- Style with elevation
    // borderWidth: 1,
    // borderRadius: 1,
    // borderColor: "#ddd",
    // borderBottomWidth: 0,
    // shadowColor: "#000000",
    // shadowOffset: { width: 0, height: 2 },
    // shadowOpacity: 0.9,
    // shadowRadius: 2,
    // elevation: 1.7,
    borderWidth: 0.5,
    borderRadius: 1,
    borderColor: '#ddd',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 2,
    paddingLeft: "5%",
    paddingRight: "5%",
  },
  delivreyAddress: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: "4%",
    //alignItems:'center'
  },
  billingAddress: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: "5%",
    alignItems: "center",
  },
  descritpionText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
    fontSize: 11,
  },
  notificationText:{
    paddingLeft: "5%",
    paddingRight: "5%",
    paddingTop: "5%",
    paddingBottom: "5%",
  },
  descritpionView:{
    width: "80%", paddingTop: "2%", paddingBottom: "3%"
  },
  settingText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinssemiBold,
  fontSize: 14,
  },
  timeText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    fontSize: 11,
    color:'#919191'
  },
  completedescritpionText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
  fontSize: 11,
  color:'#919191'
  },
  addnewButton: {
    backgroundColor: globals.COLOR.themeGreen,
    width: 80,
    height: 32,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    fontSize: 10,
  },
  delivreyaddressDetail: {
    paddingTop: "5%",
  },
  nameandIcon: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  billingnameandIcon: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingTop: "3%",
  },
  iconList: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  nameText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color: "#686868",
  },
  placeText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    fontSize: 12,
    color: "#686868",
  },
  userDetails: {
    paddingTop: "1%",
    borderBottomColor: "#CFCFCF",
    borderBottomWidth: 1,
    paddingBottom: "6%",
  },
  billinguserDetails: {
    paddingTop: "3%",
    paddingBottom: "6%",
  },
  nameandiconSecond: {
    flexDirection: "row",
    paddingTop: "6%",
    justifyContent: "space-between",
    alignItems: "center",
  },
  arrowicon: {
    width: 15,
    height: 15,
  },
  cancelButton: {
    //backgroundColor: globals.COLOR.themeGreen,
    borderColor: "#CFCFCF",
    borderWidth: 1,
    width: 134,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
  },
  submitButton: {
    backgroundColor: globals.COLOR.themeGreen,
    width: 134,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
  },
});

export { images, styles };
