import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  I18nManager,Image
} from "react-native";
import globals from "../../lib/globals";
import { styles,images } from "./styles";
import Modal from "react-native-modal";
import Header from "../../components/Header";
import PropTypes from "prop-types";
import appTexts from "../../lib/appTexts";
import DropDownPicker from "react-native-dropdown-picker";
import Icon from "react-native-vector-icons/AntDesign";
import RatingComponent from "../../components/StarRating";
const MyOrdersView = (props) => {
  const {
    isHelpModalVisible,
    toggleHelpModal,
    toggleRatingModal,
    isRatingModalVisible,
  } = props;

  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        headerTitle={appTexts.MY_ORDERS.MyOrders}
        isBackButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <View style={styles.screenContainer}>
        <TouchableOpacity
          onPress={() => {
            toggleHelpModal();
          }}
        >
          <Text>MyOrdersViewCLCIk</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            toggleRatingModal();
          }}
        >
          <Text style={{ paddingTop: "5%" }}>Rating</Text>
        </TouchableOpacity>
      </View>
      {/* help modal */}
      <Modal isVisible={isHelpModalVisible} style={styles.modalMaincontentHelp}>
        <View style={styles.modalmainView}>
          <TouchableOpacity
            style={styles.buttonwrapper}
            onPress={() => {
              toggleHelpModal();
            }}
          >
            <Text style={styles.closeButton}>X</Text>
            <Text style={styles.helpText}>{appTexts.SUPPORT.Message}</Text>
          </TouchableOpacity>

          <View>
            <View style={styles.contactView}>
              <View style={styles.bookdetailscontentsright}>
                <Text style={styles.contactText}>
                  {appTexts.SUPPORT.contact}
                </Text>
              </View>

                <View style={styles.leftSide}>
                <View style={styles.socailArea}>
                   <Image source={images.whatsappIcon} style={styles.arrowImage} />
                  <Text style={styles.numText}>{"00245 90 90"}</Text>
                </View>
                <View style={[styles.socailArea]}>
                 <Image source={images.mailIcon} style={styles.arrowImage} />
                  <Text style={styles.numText}>{"support.emtyaz.com"}</Text>
                </View>
              </View>
            </View>
          </View>

          <View style={{ paddingBottom: "5%", paddingTop: "2%" }}>
            <DropDownPicker
              items={[
                { label: "Thrissur", value: "Thrissur" },
                { label: "Palakkad", value: "Palakkad" },
              ]}
              placeholder={appTexts.SUPPORT.orderissue}
              labelStyle={{
                fontSize: 13,
                fontFamily: I18nManager.isRTL
                  ? globals.FONTS.notokufiArabic
                  : globals.FONTS.poppinsRegular,
                textAlign: "left",
                color: "#242424",
              }}
              containerStyle={{ height: 30 }}
              style={{
                backgroundColor: "white",
                borderTopLeftRadius: 0,
                borderTopRightRadius: 0,
                borderBottomLeftRadius: 0,
                borderBottomRightRadius: 0,
              }}
              itemStyle={{
                justifyContent: "flex-start",
              }}
              dropDownStyle={{ backgroundColor: "white" }}
            />
            <TextInput
              style={styles.inputmessage}
              underlineColorAndroid="transparent"
              placeholder={appTexts.SUPPORT.Type}
              placeholderTextColor="#C9C9C9"
              autoCapitalize="none"
            />
          </View>
          <View style={styles.buttonWrappers}>
            <TouchableOpacity onPress={() => console.log("click")}>
              <View style={styles.submitButton}>
                <Text style={styles.sendbuttonText}>
                  {appTexts.SUPPORT.Send}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      {/* help modal */}

      {/* Rating modal */}
      <Modal
        isVisible={isRatingModalVisible}
        style={styles.modalMaincontentHelp}
      >
        <View style={styles.modalmainView}>
          <TouchableOpacity
            style={styles.buttonwrapper}
            onPress={() => {
              toggleRatingModal();
            }}
          >
            <Text style={styles.closeButton}>X</Text>
            <Text style={styles.helpText}>{appTexts.RATING.Heading}</Text>
          </TouchableOpacity>

          <View
            style={styles.ratingBlock}
          >
            <View>
              <Text style={styles.RatingText}>{appTexts.RATING.Quality}</Text>
              <Text style={styles.RatingText}>{appTexts.RATING.Supportfromteam}</Text>
              <Text style={styles.RatingText}>{appTexts.RATING.DeliveryTime}</Text>
            </View>

            <View>
              <RatingComponent />
              <RatingComponent />
              <RatingComponent />
            </View>
          </View>

          <View style={{ paddingBottom: "5%", paddingTop: "2%" }}>
          <Text style={styles.reviewText}>{appTexts.RATING.Yourreview}</Text>
            <TextInput
              style={styles.inputmessage}
              underlineColorAndroid="transparent"
              placeholderTextColor="#C9C9C9"
              autoCapitalize="none"
            />
          </View>
          <View style={styles.buttonWrappers}>
            <TouchableOpacity onPress={() => console.log("click")}>
              <View style={styles.submitButton}>
                <Text style={styles.sendbuttonText}>
                  {appTexts.SUPPORT.Send}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      {/* Rating modal */}
    </View>
  );
};

MyOrdersView.propTypes = {
  toggleHelpModal: PropTypes.func,
  isHelpModalVisible: PropTypes.bool,
  toggleRatingModal: PropTypes.func,
  isRatingModalVisible: PropTypes.bool,
};

export default MyOrdersView;
