import React from "react";
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  I18nManager,
  TextInput,
} from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";
import PropTypes from "prop-types";
import Header from "../../components/Header";
import QuotationCard from "../../components/QuotationCard";
import QuotationsarCard from "../../components/QuotationsarCard";
import DividerLine from "../../components/DividerLine";
import Modal from "react-native-modal";
import DropDownPicker from "react-native-dropdown-picker";
import appTexts from "../../lib/appTexts";
const QuotationView = (props) => {
  const {
    onCartButtonPress,
    itemClick,
    isHelpModalVisible,
    toggleHelpModal,
    onBackButtonPress,
    qutotationSummery
  } = props;
  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        headerTitle={appTexts.MYQUATION.MyQuotations}
        isBackButtonRequired={true}
        onBackButtonPress={onBackButtonPress}
        onCartButtonPress={onCartButtonPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />

      <ScrollView
        style={styles.screenContainerScrollView}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.screenDesignContainer}>
          <View style={styles.screenContainerWithMargin}>
            <View style={styles.statusSection}>
              <Text style={styles.orderID}>QUOID12347878</Text>
              <View style={styles.dotsSection}>
                {/* <View style={styles.redDot} />
                  <Text style={styles.statusText}>
                  {appTexts.MYQUATION.Requested}
                </Text> */}
                <View style={styles.greenDot} />
                  <Text style={styles.approveText}>
                  {appTexts.MYQUATION.Approved}
                </Text>
              </View>
            </View>
            <View style={styles.itemSection}>
              <Text style={styles.itemText}>2 {appTexts.MYQUATION.Items}</Text>
            </View>
          </View>
          <View style={styles.promotionalContainer}>
            <QuotationCard
              itemImage={require("../../assets/images/home/product_01.png")}
               />
            <QuotationCard
              itemImage={require("../../assets/images/home/product_02.png")}
              />
         {/* <QuotationsarCard
            itemImage={require("../../assets/images/home/product_02.png")} /> */}
          </View>
          <DividerLine />

            {/* first screen starting */}

          <View style={styles.deliveryArea}>
            <View style={styles.deliveryLocation}>
              <Text style={styles.deliverylocationText}>
                {appTexts.MYQUATION.DeliveryLocation}
              </Text>
              <Text style={styles.deliverylocationPlace}>
                {"Jedda, Al Nameem"}
              </Text>
            </View>
            <View style={styles.summaryView}>
              <View style={styles.summaryHeadingView}>
                <Text style={styles.summaryHeadingStyle}>{appTexts.QUOTATIONS_SUMMERY.QuotationSummary}</Text>
              </View>
              <View style={styles.summaryBodyView}>
                <Text style={styles.summaryBodyText}>{appTexts.QUOTATIONS_SUMMERY.Subtotal}</Text>
                <Text style={styles.subtotalPrice}>SAR 15000.00</Text>
              </View>
              <View style={styles.summaryBodyView}>
                <Text style={styles.summaryBodyText}>{appTexts.QUOTATIONS_SUMMERY.DeliveryCharge}</Text>
                <Text style={styles.subtotalPrice}>SAR 10</Text>
              </View>
              <View style={styles.summaryBodyView}>
                <Text style={styles.summaryBodyText}>{appTexts.QUOTATIONS_SUMMERY.TotalInclusivefVAT}</Text>
                <Text style={styles.subtotalPriceVAT}>SAR 15010.00</Text>
              </View>
            </View>
            <View style={styles.QuotationNote}>
              <Text style={styles.deliverylocationText}>
                {appTexts.MYQUATION.QUNOTE}
              </Text>
              <Text style={styles.qtyText}>
                {
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
                }
              </Text>
            </View>

            <View style={styles.expectedOrder}>
              <Text style={styles.deliverylocationText}>
                {appTexts.MYQUATION.ExpectedOrderDeliveryDate}
              </Text>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.qtyText}>{"20-August-2020"}</Text>
              </View>
            </View>
            <View style={styles.noteBox}>
              <View style={styles.noteText}>
                <Text style={styles.noteTextStyle}>{appTexts.QUOTATIONS_SUMMERY.AddQuotationnoteOptional}</Text>
              </View>
            </View>

            <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => {qutotationSummery()}}>
                <View style={styles.yellowButton}>
                  <Text style={styles.leftbuttonText}>
                    {appTexts.MYQUATION.MODIFYQUOTAION}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  toggleHelpModal();
                }}
              >
                <View style={styles.greenButton}>
                  <Text style={styles.RightbuttonText}>
                    {appTexts.MYQUATION.CANCELQUOTATION}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
  {/* first screen starting */}

          {/* seocnd screen starting */}

          {/* <View style={styles.deliveryArea}>
            <View style={styles.deliveryLocation}>
              <Text style={styles.deliverylocationText}>
                {appTexts.MYQUATION.DeliveryLocation}
              </Text>
              <Text style={styles.deliverylocationPlace}>
                {"Jedda, Al Nameem"}
              </Text>
            </View>

            <View style={styles.QuotationNote}>
              <Text style={styles.deliverylocationText}>
                {appTexts.MYQUATION.QuotationSummary}
              </Text>

              <View style={styles.billwrapper}>
                <View style={styles.bookdetailscontentsright}>
                  <Text style={styles.sarsText}>
                    {appTexts.THANKUORDER_VIEW.Subtotal}
                  </Text>
                  <Text style={styles.sarsText}>
                    {appTexts.THANKUORDER_VIEW.DeliveryCharge}
                  </Text>
                  <Text style={styles.sarsText}>
                    {appTexts.THANKUORDER_VIEW.TotalInclusivefVAT}
                  </Text>
                </View>

                <View style={{ alignItems: "flex-end" }}>
                  <Text style={styles.sarText}>SAR 200.00</Text>
                  <Text style={styles.sarText}>SAR 10.00</Text>
                  <Text style={styles.sarboldText}>SAR 210.00</Text>
                </View>
              </View>
            </View>

            <View style={styles.QuotationNote}>
              <Text style={styles.deliverylocationText}>
                {appTexts.MYQUATION.QUNOTE}
              </Text>
              <Text style={styles.qtyText}>
                {
                  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "
                }
              </Text>
            </View>

            <View style={styles.expectedOrder}>
              <Text style={styles.deliverylocationText}>
                {appTexts.MYQUATION.ExpectedOrderDeliveryDate}
              </Text>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.qtyText}>{"20-August-2020"}</Text>
              </View>
            </View>
            <View>
              <TextInput
                style={styles.inputmessage}
                underlineColorAndroid="transparent"
                placeholder={appTexts.MYQUATION.AddQuotationnote}
                placeholderTextColor="#C9C9C9"
                autoCapitalize="none"
              />
            </View>

            <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={() => {}}>
                <View style={styles.yellowButton}>
                  <Text style={styles.leftbuttonText}>
                    {appTexts.MYQUATION.MODIFYQUOTAION}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <View style={styles.themeButton}>
                  <Text style={styles.RightbuttonText}>
                    {appTexts.MYQUATION.PayNow}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View> */}
          
          {/* second screen ending */}

        </View>

        {/* help modal start */}
        <Modal
          isVisible={isHelpModalVisible}
          style={styles.modalMaincontentHelp}
        >
          <View style={styles.modalmainView}>
            <TouchableOpacity
              style={styles.buttonwrapper}
              onPress={() => {
                toggleHelpModal();
              }}
            >
              <Text style={styles.closeButton}>X</Text>
              <View style={{flexDirection:'row'}}>
              <Text style={styles.helpText}>
                
                {appTexts.CANCELMODAL.Heading}
              </Text>
              </View>
            </TouchableOpacity>

            <View style={{ paddingBottom: "5%", paddingTop: "2%" }}>
              <DropDownPicker
                items={[
                  { label: "Thrissur", value: "Thrissur" },
                  { label: "Palakkad", value: "Palakkad" },
                ]}
                placeholder={appTexts.CANCELMODAL.Reason}
                labelStyle={{
                  fontSize: 13,
                  fontFamily: I18nManager.isRTL
                    ? globals.FONTS.notokufiArabic
                    : globals.FONTS.poppinsRegular,
                  textAlign: "left",
                  color: "#242424",
                }}
                containerStyle={{ height: 30 }}
                style={{
                  backgroundColor: "white",
                  borderTopLeftRadius: 0,
                  borderTopRightRadius: 0,
                  borderBottomLeftRadius: 0,
                  borderBottomRightRadius: 0,
                }}
                itemStyle={{
                  justifyContent: "flex-start",
                }}
                dropDownStyle={{ backgroundColor: "white" }}
              />
              <TextInput
                style={styles.inputmessage}
                underlineColorAndroid="transparent"
                placeholder={appTexts.CANCELMODAL.Typereason}
                placeholderTextColor="#C9C9C9"
                autoCapitalize="none"
              />
            </View>
            <View style={styles.buttonWrappers}>
              <TouchableOpacity onPress={() => console.log("click")}>
                <View style={styles.submitButton}>
                  <Text style={styles.sendbuttonText}>
                    {appTexts.CANCELMODAL.Done}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        {/* help modal end */}
      </ScrollView>
    </View>
  );
};

QuotationView.propTypes = {
  toggleHelpModal: PropTypes.func,
  isHelpModalVisible: PropTypes.bool,
};

export default QuotationView;
