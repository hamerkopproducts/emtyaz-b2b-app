import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainerScrollView:{
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height
  },
  screenDesignContainer:{
    width: globals.SCREEN_SIZE.width,
    paddingBottom: globals.INTEGER.screenPaddingFromFooter
  },
  screenContainerWithMargin:{
    width: globals.INTEGER.screenWidthWithMargin,
    marginLeft: globals.INTEGER.leftRightMargin,
    //backgroundColor:'red'
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  promotionalContainer:{
    marginBottom:5,
    //backgroundColor:'yellow'
  },
  categoryItemRow:{
    width:'100%',
    flexDirection:'row',
    flexWrap:'wrap'
  },
  statusSection:{
    flexDirection:'row',
    justifyContent:'space-between',
    marginTop:'4%'
  },dotsSection:{
    flexDirection:'row',
    alignItems:"center"
  },
  itemSection:{
    flexDirection:'row',
    marginTop:'2%'
  },
  redDot:{
    width: 6, height: 6,backgroundColor:'#A11818',borderRadius:20
  },
  greenDot:{
    width: 6, height: 6,backgroundColor:globals.COLOR.themeGreen,borderRadius:20
  },
  orderID:{
    color: globals.COLOR.addressLabelColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 11.5
  },statusText:{
    color:'#A11818',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 11.5,
    paddingLeft:'1%',
    paddingRight:'2%'

  },
  approveText:{
color:globals.COLOR.themeGreen,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 11.5,
    paddingLeft:'1%',
    paddingRight:'2%'
  },
  itemText:{
    color:'#565656',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 11.5,
    paddingBottom:'1%'
  },
  deliveryArea:{
    paddingLeft:'5%',
    paddingRight:'5%'
  },
  deliveryLocation:{
    flexDirection:'row',
    alignItems:'center',
    paddingTop:'8%',
    paddingBottom:'2%'
    
  },
  expectedOrder:{
    paddingTop:'2%',
    paddingBottom:'2%',
    
  },
  deliverylocationText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
    fontSize: 14,
    color: globals.COLOR.lightBlack,
  },
  deliverylocationPlace:{
   
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
      fontSize: 12,
      color: globals.COLOR.lightBlack,
  },
  summaryHeadingView:{
    paddingBottom:'4%',
  },
  summaryHeadingStyle:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    fontSize: 14,
    color: globals.COLOR.lightBlack,
  },
  summaryBodyView:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingBottom:'4%',
  },
  summaryBodyText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 12,
    color: globals.COLOR.lightBlack,
  },
  subtotalPrice:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 12,
    color: globals.COLOR.lightBlack,
  },
  subtotalPriceVAT:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinssemiBold,
    fontSize: 12,
    color: globals.COLOR.lightBlack,

  },
  qtyText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 12,
    color: globals.COLOR.lightBlack,
    paddingTop:'2%',
    paddingBottom:'1%'
  },
  buttonWrapper:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingTop:'6%',
    paddingBottom:'6%'
  },
  greenButton: {
    backgroundColor: '#A11818',
    width: 156,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
  },
  yellowButton: {
    backgroundColor: '#F0F0F0',
    width: 156,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
    borderColor: globals.COLOR.lightGray,
    borderWidth:1
  },
  themeButton:{
    backgroundColor: '#3fb851',
    width: 156,
    height: 48,
    justifyContent: "center",
    alignItems: "center",
  },
  leftbuttonText: {
    color:'black',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13,
  },
  RightbuttonText: {
    color: globals.COLOR.white,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13,
  },
  modalMaincontentHelp:{
  
    justifyContent: "center",
    //justifyContent: "flex-end",
   // margin: 0,
},
modalmainView: {
  backgroundColor: "white",
  //width: wp("90%"),
  padding: "4%",
  //borderRadius: 10,
  borderTopRightRadius: 0,
  borderTopLeftRadius: 0,
  borderColor: "rgba(0, 0, 0, 0.1)",
},
closeButton:{
  alignSelf:'flex-end',
  paddingRight:'3%',
  paddingLeft:'3%',
  paddingTop:'2%',
  paddingBottom:'2%',
  color:'#707070',
  fontSize:16
},
helpText:{
  fontFamily: I18nManager.isRTL
  ? globals.FONTS.notokufiArabic
  : globals.FONTS.poppinssemiBold,
  fontSize:14, 
  paddingBottom:'3%',
  flexDirection: 'row',
  paddingLeft:'4%',
},
inputmessage: {
  height: 100,
  marginTop: "3%",
  borderColor: "#DEDEDE",
  //backgroundColor: "#E8E8E8",
  borderWidth: 1,
  borderRadius: 1,
  textAlignVertical: "top",
  textAlign: I18nManager.isRTL ? "right" : "left",
  fontFamily: I18nManager.isRTL
  ? globals.FONTS.notokufiArabic
  : globals.FONTS.poppinsRegular,
  //fontFamily: "Montserrat-Medium",
  // textAlign: isRTL ? "right" : "left",
  // fontFamily: isRTL ? "Noto Kufi Arabic" : "Montserrat-Medium",
  fontSize: 14,
  padding: 4,
  paddingLeft:'4%',
},
buttonWrappers:{
  // paddingLeft:'3%',
  // paddingRight:'3%',
  paddingBottom:'3%',
  paddingTop:'3%',
  alignSelf:'center'
},
buttonWrappers:{
  // paddingLeft:'3%',
  // paddingRight:'3%',
  paddingBottom:'3%',
  paddingTop:'3%',
  alignSelf:'center'
},
submitButton: {
  backgroundColor:  globals.COLOR.themeGreen,
  width: 144,
  height: 48,
  justifyContent: "center",
  alignItems: "center",
},
sendbuttonText:{
  color: globals.COLOR.white,
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  fontSize: 15, 
},
billwrapper:{
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  paddingBottom:'2%',
  paddingTop:'2%'
},
sarText: {
  fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    fontSize: 12,
    color: globals.COLOR.lightBlack,
  paddingTop: "2%",
  //paddingLeft:'5%'
},
sarboldText:{
  fontFamily: I18nManager.isRTL
  ? globals.FONTS.notokufiArabic
  : globals.FONTS.poppinsMedium,
  fontSize: 12,
  color: globals.COLOR.lightBlack,
paddingTop: "2%",
},
sarsText: {
  fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,

  paddingTop: "2.5%",
  paddingBottom:'1%',
  fontSize: 12,
  color: globals.COLOR.lightBlack,
},
noteBox:{
  width:'100%',
  height:140,
  borderWidth:1,
  borderColor:globals.COLOR.lightGray,
},
noteText:{
  paddingLeft:'3%',
  paddingTop:'2%',
},
noteTextStyle:{
  fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    fontSize:12,
    color:globals.COLOR.lightGray,
},
  
});

export { images, styles };
