import React, { useEffect, useState } from "react";
import { BackHandler, Keyboard } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import ProfileView from "./ProfileView";
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";
import * as SignUpActions from "../../actions/SignUpActions";
import * as EditUserActions from "../../actions/EditUserActions";
import * as AddressListActions from "../../actions/AddressListActions";
import globals from "../../lib/globals";
import functions from "../../lib/functions";
import _ from 'lodash'

import { number } from "prop-types";

const ProfileScreen = (props) => {
   //Initialising states
   let initialRegion = '';
  let initialRegionName = 'Select Your City';
   const [isModalVisible, setIsModalVisible] = useState(false);
   const [isHelpModalVisible, setIsHelpModalVisible] = useState(false);
   const [isLogoutpModalVisible, setIsLogoutModalVisible] = useState(false);
   const [isUpdateModalVisible, setIsUpdateModalVisible] = useState(false);
   const [isRequestModalVisible, setIsRequestModalVisible] = useState(false);
   const [ishidePass, setIsHidePass] = useState(true);
   const [ishideNewPass, setIsHideNewPass] = useState(true);
   const [isconfirmHidePass, setIsConfirmHidePass] = useState(true);

   const [name, setName] = useState(props.customername);
   const [email, setEmail] = useState(props.customeremail);
   const [phone, setPhone] = useState(props.customerphone);
   const [cname, setCname] = useState(props.companyName);
   const [citySelected, setcitySelected] = useState(props.cityNameId);
   const [regionID, setRegionid] = useState('');

   
  const [nameerrorflag, setnameerrorflag] = useState(false);
  const [emailerrorflag, setemailerrorflag] = useState(false);
  const [cnameerrorflag, setcnameerrorflag] = useState(false);

  const [regionerrorflag, setRegionerrorflag] = useState(false);
  const [isRegionModalVisible, setIsRegionModalVisible] = useState(false);
  const [selectedRegion, setSelectedRegion] = useState(initialRegion);
  const [selectedRegionName, setSelectedRegionName] = useState(citySelected);
   
  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
          
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        props.getUserDetails(props.userData.data.access_token);
        props.getRegion(props.languageSelected,props.userData.data.access_token);
        //props.addressList(props.userData.data.access_token);


        // setName(props.userDetails.data.user.cust_name)
        // setEmail(props.userDetails.data.user.email)
        // setPhone(props.userDetails.data.user.phone)
        // setCname(props.userDetails.data.user.company)
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  };

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated(
    
  ));

    const handleComponentUpdated = () => {

      console.debug('userDetailsUpdated', props.userDetailsUpdated)
      if(props.userDetailsUpdated.success)
      {
        props.getUserDetails(props.userData.data.access_token);
      functions.displayToast(
        "success",
        "top",
        "User details Updated "
      );
      props.getRegion(props.languageSelected,props.userData.data.access_token);
      }
      if(props.userDetails.success === true)
      if (props.isLoggedOut) {
        props.navigation.navigate('SigninScreen');
      }
    };
 

  const logoutButtonPress = () => {
    // props.navigation.navigate('TermsScreen');
    props.doLogout();
    props.doLogoutFromSignup();
  };

  const toggleModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  const toggleHelpModal = () => {
    setIsHelpModalVisible(!isHelpModalVisible)
  };
  const toggleLogoutModal = () => {
    setIsLogoutModalVisible(!isLogoutpModalVisible)
  };
  const toggleUpdateModal = () => {
    setIsUpdateModalVisible(!isUpdateModalVisible)
  };
  const openRegionModal = () => {
    setIsRegionModalVisible(true);
  };
  const toggleRegionModal = () => {
    setIsRegionModalVisible(false)
  };
  const fetchUserDetails = () => {
     setName(props.userDetails.data.user.cust_name)
        setEmail(props.userDetails.data.user.email)
        setPhone(props.userDetails.data.user.phone)
        setCname(props.userDetails.data.user.company)
        setcitySelected(props.userDetails.data.user.email)
        
    props.getUserDetails(props.userData.data.access_token);
  };
  const toggleRequestModal = () => {
    setIsRequestModalVisible(!isRequestModalVisible)
  };
  const addressDetails = () => {
    props.addressList(props.userData.data.access_token);
    props.navigation.navigate("AddressdetailsScreen");
  };

  
  const setHidePass =() => {
    setIsHidePass(!ishidePass)
  }
  const setHideNewPass =() => {
    setIsHideNewPass(!ishideNewPass)
  }

  const setHideConfirmPass =() => {
    setIsConfirmHidePass(!isconfirmHidePass)
  }
  const favouritemsClicked = () => {
    props.navigation.navigate("FavouritesScreen");
  };
  const faqPress = () => {
    props.navigation.navigate("FaqScreen");
  };
  const privacyPress = () => {
    props.navigation.navigate("PrivacyScreen");
  };
  const termsPress = () => {
    props.navigation.navigate("TermsScreen");
  };
  const supportPress = () => {
    props.navigation.navigate("");
  };
  const sharePress = () => {
    props.navigation.navigate("");
  };
  const logoutPress = () => {
    
  };
  let usereditdetails =''

  try {
    usereditdetails = props.userDetails.data.user

  } catch (error) {
    usereditdetails = ''
  }


  const validatename = flag => {
    setnameerrorflag(flag);
  };
  const validateEmail = flag => {
    setemailerrorflag(flag);
  };
  const validateCname = flag => {
    setcnameerrorflag(flag);
  };
  const validateregion = (flag) => {
    setRegionerrorflag(flag);
  };


  const updateUserDetails = () => {

    if (name.trim() === '')
    {
      validatename()
      functions.displayToast('error', 'top', 'Name can not be empty');
    } 
    else if(email.trim() == '' ){
      validateEmail()
      functions.displayToast('error', 'top', 'Email can not be empty');
    } 
     else if( cname.trim() == '') {
       validateCname()
       functions.displayToast('error', 'top', 'Company name can not be empty');
    }
    else if (selectedRegion === ''  || selectedRegion === null) {
      validateregion();
      functions.displayToast('error', 'top',appTexts.ALERT_MESSAGES.error, 'City can not be empty');
    }
    else {
      Keyboard.dismiss()
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          let apiParam = {
            "name": name.trim(),
            "email": email.trim(),
            "company_name": cname.trim(),
            "region_id": selectedRegion,
            "language": props.languageSelected
          }
          props.doUpdateUserDetails(apiParam,props.userData.data.access_token);
        } else {
          functions.displayToast('error', 'top', appTexts.VALIDATION_MESSAGE.Type, appTexts.VALIDATION_MESSAGE.Mobile_error_msg);
        }
      });
    }
  };


  return (
    <ProfileView
      isLoading={props.isLoading}
      logoutButtonPress={logoutButtonPress}
      faqPress={faqPress}
      privacyPress={privacyPress}
      termsPress={termsPress}
      supportPress={supportPress}
      sharePress={sharePress}
      logoutPress={logoutPress}
      isModalVisible={isModalVisible}
      toggleModal={toggleModal}
      toggleHelpModal={toggleHelpModal}
      isHelpModalVisible={isHelpModalVisible} 
      toggleLogoutModal={toggleLogoutModal}
      isLogoutpModalVisible={isLogoutpModalVisible}
      toggleUpdateModal={toggleUpdateModal}
      fetchUserDetails={fetchUserDetails}
      addressDetails={addressDetails}
      isUpdateModalVisible={isUpdateModalVisible}
      toggleRequestModal={toggleRequestModal}
      favouritemsClicked={favouritemsClicked}
      isRequestModalVisible={isRequestModalVisible}
      ishidePass={ishidePass}
      setHidePass={setHidePass}
      ishideNewPass={ishideNewPass}
      setHideNewPass={setHideNewPass}
      setHideConfirmPass={setHideConfirmPass}
      isconfirmHidePass={isconfirmHidePass}
      usereditdetail={
        usereditdetails
      }
      regionList={props.regionList}

      customerName={props.customername}
      customerEmail={props.customeremail}
      customerPhone={props.customerphone}

      name={name}
    email={email}
    phone={phone}
    cname={cname}
      setName={setName}
      validatename={validatename}
      validateEmail={validateEmail}
      validateCname={validateCname}

      setEmail={setEmail}
      setPhone={setPhone}
      setCname={setCname}
      setcitySelected={setcitySelected}
      
      updateUserDetails={updateUserDetails}

      nameerrorflag={nameerrorflag}
      emailerrorflag={emailerrorflag}
      cnameerrorflag={cnameerrorflag}
      openRegionModal={openRegionModal}
      setSelectedRegion={setSelectedRegion}
      setSelectedRegionName={setSelectedRegionName}
      isRegionModalVisible={isRegionModalVisible}

      selectedRegion={selectedRegion}
      selectedRegionName={selectedRegionName}
      toggleRegionModal={toggleRegionModal}


    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    isLoading: state.editUserDetailsReducer.isLoading,
    isLoggedOut: state.loginReducer.isLoggedOut,
    userData: state.loginReducer.userData,
    currentScreen: state.loginReducer.currentScreen,
    profileUpdate: state.loginReducer.profileUpdate,
    userDetails: _.get(state, 'editUserDetailsReducer.userDetails', ''),
    customername: _.get(state, 'editUserDetailsReducer.userDetails.data.user.cust_name', ''),
    customeremail: _.get(state, 'editUserDetailsReducer.userDetails.data.user.email', ''),
    customerphone: _.get(state, 'editUserDetailsReducer.userDetails.data.user.phone', ''),
    companyName: _.get(state, 'editUserDetailsReducer.userDetails.data.user.company', ''),
    cityNameId: _.get(state, 'editUserDetailsReducer.userDetails.data.user.region.lang[0].name', ''),
    languageSelected:  _.get(state, 'loginReducer.languageSelected', ''),
    regionList:  _.get(state, 'editUserDetailsReducer.regionList', []),
    userDetailsUpdated:  _.get(state, 'editUserDetailsReducer.updateduserdetails', ''),
    
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      doLogout: LoginActions.doLogout,
      doLogoutFromSignup: SignUpActions.doLogoutFromSignup,
      getUserDetails: EditUserActions.doUserDetailsEdit,
      doUpdateUserDetails: EditUserActions.doUpdateUserDetails,
      getRegion: EditUserActions.doRegionDropdown,
      addressList: AddressListActions.doAddressDetailsFetch,
    },
    dispatch
  );
};

const profileScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileScreen);

profileScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default profileScreenWithRedux;
