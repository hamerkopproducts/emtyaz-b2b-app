import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  I18nManager,
  Platform,
} from "react-native";
import globals from "../../lib/globals";
import { styles, images } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import ProfileItem from "./ProfileItem";
import Modal from "react-native-modal";
import CustomGreenLargeButton from "../../components/CustomGreenLargeButton";
import Icon from "react-native-vector-icons/Ionicons";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DropDownPicker from "react-native-dropdown-picker";
import Loader from '../../components/Loader';
// import { Dropdown } from 'react-native-material-dropdown';
import RegionModal from "../../components/RegionModal";
import {dropDownRegionMapper} from '../../lib/HelpperMethods'

const ProfileView = (props) => {
  const {
    faqPress,
    privacyPress,
    termsPress,
    sharePress,
    logoutButtonPress,
    logoutPress,
    toggleModal,
    isModalVisible,
    isHelpModalVisible,
    toggleHelpModal,
    isLogoutpModalVisible,
    toggleLogoutModal,
    toggleUpdateModal,
    fetchUserDetails,
    addressDetails,
    isUpdateModalVisible,
    toggleRequestModal,
    isRequestModalVisible,
    ishidePass,
    setHidePass,
    ishideNewPass,
    setHideNewPass,
    isconfirmHidePass,
    setHideConfirmPass,
    favouritemsClicked,
    usereditdetail,
    regionList,
    openRegionModal,
    setSelectedRegion,
    setSelectedRegionName,
    selectedRegion,
    selectedRegionName,
    selectedRegionId,
    isRegionModalVisible,
    toggleRegionModal,

    name,
    email,
    phone,
    cname,
    setName,
    setEmail,
    setPhone,
    setCname,
    customerName,
    customerEmail,
    customerPhone,

    validateCname,
    validateEmail,
    validatename,
    isLoading,
    updateUserDetails
  } = props;


  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        headerTitle={appTexts.PWD_CHANGE.account}
        isLanguageButtonRequired={false}
        isRightButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <ScrollView>
        <View style={styles.screenContainer}>
          <View style={styles.profileScreen}>
            <View style={styles.editArrow}>
              <TouchableOpacity
                onPress={() => {
                  fetchUserDetails();
                  toggleUpdateModal();
                }}>
                <Image
                  source={require("../../assets/images/profileItem/edit.png")}
                  resizeMode="contain"
                  style={styles.arrowicon}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
                onPress={() => {
                  addressDetails();
                  
                }}
              >

              </TouchableOpacity>
            <View style={styles.profileEdit}>
              
              <View style={styles.profileContent}>
                <Image
                  source={images.personIcon}
                  resizeMode="contain"
                  style={styles.arrowicon}
                />

                <View style={styles.profileInformation}>
                  <Text style={styles.name}>{customerName}</Text>
                  <Text style={styles.address}>
                    {customerEmail}
                  </Text>
                  <Text style={styles.place}>{customerPhone}</Text>
                </View>
              </View>


              <TouchableOpacity
                onPress={() => {
                  addressDetails();

                }}
              >
                <View style={styles.ArrowView}>
                  <Image
                    source={images.arrowIcon}
                    resizeMode="contain"
                    style={styles.arrowicons}
                  />
                </View>
              </TouchableOpacity>

            </View>
          </View>
          <View style={styles.profileItem}>
            <ProfileItem
              itemImage={images.favIcon}
              itemText={appTexts.PROFILEITEM.Favorites}
              arrowImage={images.arrowIcon}
              onItemClick={favouritemsClicked}
            />
            <ProfileItem
              itemImage={images.requestIcon}
              itemText={appTexts.PROFILEITEM.Requestproduct}
              arrowImage={images.arrowIcon}
              onItemClick={toggleRequestModal}
            />

            <ProfileItem
              itemImage={images.faqIcon}
              itemText={appTexts.PROFILEITEM.FAQs}
              arrowImage={images.arrowIcon}
              onItemClick={faqPress}
            />
            <ProfileItem
              itemImage={images.privacyIcon}
              itemText={appTexts.PROFILEITEM.PrivacyPolicy}
              arrowImage={images.arrowIcon}
              onItemClick={privacyPress}
            />
            <ProfileItem
              itemImage={images.termsIcon}
              itemText={appTexts.PROFILEITEM.Terms}
              arrowImage={images.arrowIcon}
              onItemClick={termsPress}
            />
            <ProfileItem
              itemImage={images.supportIcon}
              itemText={appTexts.PROFILEITEM.Support}
              arrowImage={images.arrowIcon}
              onItemClick={toggleHelpModal}
            />
            <ProfileItem
              itemImage={images.shareIcon}
              itemText={appTexts.PROFILEITEM.Share}
              arrowImage={images.arrowIcon}
              onItemClick={toggleRequestModal}
            />
            <ProfileItem
              itemImage={images.logoutIcon}
              itemText={appTexts.PROFILEITEM.Logout}
              arrowImage={images.arrowIcon}
              onItemClick={toggleLogoutModal}
            />
          </View>

          {/* modalpopup Reset Password */}
          <Modal isVisible={isModalVisible}
            animationType="fade"
            style={styles.modalMainContent}>
            <View style={styles.modalmainView}>
              <TouchableOpacity
                style={styles.buttonwrapper}
                onPress={() => {
                  toggleModal();
                }}
              >
                <Text style={styles.closeButton}>X</Text>
              </TouchableOpacity>
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.restText}>{appTexts.PWD_CHANGE.Rest}</Text>
              </View>
              <KeyboardAwareScrollView>
                <View style={styles.resetPassword}>
                  <View style={styles.pwdOne}>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.passwordText}>
                        {appTexts.PWD_CHANGE.cpassword}
                      </Text>
                    </View>
                    <View style={styles.cPassword}>
                      <TextInput
                        style={styles.numberEnter}
                        underlineColorAndroid="transparent"
                        secureTextEntry={ishidePass ? true : false}
                      />
                      <Icon
                        name={ishidePass ? 'eye' : 'eye-off'}
                        style={{ fontSize: 22, color: "#ACACAC" }}
                        onPress={() => setHidePass(!ishidePass)}
                      />
                    </View>
                  </View>
                  <View style={styles.pwdOne}>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.passwordText}>
                        {appTexts.PWD_CHANGE.npassword}
                      </Text>
                    </View>
                    <View style={styles.cPassword}>
                      <TextInput
                        style={styles.numberEnter}
                        underlineColorAndroid="transparent"
                        secureTextEntry={ishideNewPass ? true : false}
                      />
                      <Icon
                        name={ishideNewPass ? 'eye' : 'eye-off'}
                        style={{ fontSize: 22, color: "#ACACAC" }}
                        onPress={() => setHideNewPass(!ishideNewPass)}
                      />
                    </View>
                  </View>

                  <View style={styles.pwdOne}>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.passwordText}>
                        {appTexts.PWD_CHANGE.conpassword}
                      </Text>
                    </View>
                    <View style={styles.cPassword}>
                      <TextInput
                        style={styles.numberEnter}
                        underlineColorAndroid="transparent"
                        secureTextEntry={isconfirmHidePass ? true : false}
                      />
                      <Icon
                        name={isconfirmHidePass ? 'eye' : 'eye-off'}
                        style={{ fontSize: 22, color: "#ACACAC" }}
                        onPress={() => setHideConfirmPass(!isconfirmHidePass)}
                      />
                    </View>
                  </View>

                </View>
              </KeyboardAwareScrollView>
              <View style={styles.buttonWrapper}>
                <TouchableOpacity onPress={() => console.log("click")}>
                  <CustomGreenLargeButton
                    buttonText={appTexts.PWD_CHANGE.updatepassword}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          {/* modalpopup Reset Password */}

          {/* modalpopup Support */}
          <Modal
            isVisible={isHelpModalVisible}
            style={styles.modalMaincontentHelp}
          >
            <View style={styles.modalmainView}>
              <TouchableOpacity
                style={styles.buttonwrapper}
                onPress={() => {
                  toggleHelpModal();
                }}
              >
                <Text style={styles.closeButton}>X</Text>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.helpText}>{appTexts.SUPPORT.Message}</Text>
                </View>
                <View style={styles.contacts}>
                  <Text style={styles.cont}>
                    {appTexts.SUPPORT.contact}
                  </Text>
                  <View>
                    <View style={styles.contactTitle}>
                      <Image source={require("../../assets/images/myOrders/call_icon.png")} />

                      <Text style={styles.contactdetails}>
                        00002345676567
                    </Text>
                    </View>
                    <View style={styles.contactTitle}>
                      <Image source={require("../../assets/images/myOrders/mail_icon.png")} />

                      <Text style={styles.contactdetails}>
                        support.emtyaz.com
                    </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
              <View style={{ paddingBottom: "5%", paddingTop: "2%" }}>
                {/* <DropDownPicker
                  items={
                    [{ label: "Damaged", value: "Damaged" }, { label: "Differnet Product", value: "Differnet" }]}
                  dropDownStyle={{ placeholderTextColor: "red" }}
                  placeholder={appTexts.SUPPORT.orderissue}
                  labelStyle={{
                    fontSize: 13,
                    fontFamily: I18nManager.isRTL
                      ? globals.FONTS.notokufiArabic
                      : globals.FONTS.poppinsRegular,
                    textAlign: "left",
                    color: '#242424',
                  }}
                  containerStyle={{ height: 30 }}
                  style={styles.dropDownStyle}
                  itemStyle={{
                    justifyContent: "flex-start",
                  }}
                  dropDownStyle={{ backgroundColor: "white" }}
                /> */}
                <TextInput
                  style={styles.inputmessage}
                  underlineColorAndroid="transparent"
                  placeholder={appTexts.SUPPORT.Type}
                  placeholderTextColor="#C9C9C9"
                  autoCapitalize="none"
                />
              </View>
              <View style={styles.buttonWrappers}>
                <TouchableOpacity onPress={() => console.log("click")}>
                  <View style={styles.submitButton}>
                    <Text style={styles.sendbuttonText}>
                      {appTexts.SUPPORT.Send}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          {/* modalpopup Support */}

          {/* modalpopup logout */}
          <Modal
            isVisible={isLogoutpModalVisible}
            style={styles.modalMaincontentHelp}
          >
            <View style={styles.modalmainView}>
              <TouchableOpacity
                style={styles.buttonwrapper}
                onPress={() => {
                  toggleLogoutModal();
                }}
              >
                <Text style={styles.closeButton}>X</Text>
                <Text style={styles.logoutText}>{appTexts.LOGOUT.Message}</Text>
              </TouchableOpacity>
              <View style={styles.logoutWrappers}>
                <TouchableOpacity onPress={() => logoutButtonPress()}>
                  <View style={styles.yesButton}>
                    <Text style={styles.sendbuttonText}>
                      {appTexts.LOGOUT.Yes}
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => toggleLogoutModal()}>
                  <View style={styles.noButton}>
                    <Text style={styles.sendbuttonText}>
                      {appTexts.LOGOUT.No}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          {/* modalpopup logout */}

          {/* modalpopup update profile */}
          <Modal
            isVisible={isUpdateModalVisible}
            style={styles.modalMainContent}
            animationOutTiming = {900}
            animationType = 'fade'
              >
            {isLoading && <Loader />}
            <View style={styles.modalmainView}>
              <KeyboardAwareScrollView
                scrollEnabled={false}
                contentContainerStyle={{ justifyContent: "flex-end" }}
              >
                <TouchableOpacity
                  style={styles.buttonwrapper}
                  onPress={() => {
                    toggleUpdateModal();
                  }}
                >
                  <Text style={styles.closeButton}>X</Text>
                </TouchableOpacity>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.restText}>
                    {appTexts.EDIT_PROFILE.Heading}
                  </Text>
                </View>
                <View style={styles.resetPassword}>
                  <View style={styles.pwdOne}>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.passwordText}>
                        {appTexts.EDIT_PROFILE.Fullname}
                      </Text>
                    </View>
                    <View style={styles.cPassword}>
                      <TextInput
                        style={styles.numberEnter}
                        underlineColorAndroid="transparent"
                        value={name}
                        onChangeText={val => {
                          validatename(false)
                          setName(val);
                        }}
                      />
                    </View>
                  </View>
                  <View style={{ paddingTop: "5%", flexDirection: 'column' }}>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.passwordText}>
                        {appTexts.EDIT_PROFILE.Email}
                      </Text>
                    </View>
                    <View style={styles.cPassword}>
                      <TextInput
                        style={styles.numberEnter}
                        underlineColorAndroid="transparent"
                        value={email}
                        onChangeText={val => {
                          validateEmail(false)
                          setEmail(val);
                        }}
                      />
                    </View>
                  </View>
                  <View style={{ paddingTop: "5%", flexDirection: 'column' }}>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.passwordText}>
                        {/* {regionList} */}
                        {appTexts.EDIT_PROFILE.compnayname}
                      </Text>
                    </View>
                    <View style={styles.cPassword}>
                      <TextInput
                        style={styles.numberEnter}
                        underlineColorAndroid="transparent"
                        value={cname}x
                        onChangeText={val => {
                          validateCname(false)
                          setCname(val);
                        }}
                      />
                    </View>
                  </View>
                  <View style={{ paddingTop: "3%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Region}
                  </Text>
                  <View style={styles.cpPassword}>
                    <TouchableOpacity
                      onPress={() => {
                        openRegionModal();
                      }}
                    >
                      <View style={styles.regionSelectContainer}>
                        <View style={styles.regionSelectView}>
                          <Text
                            style={styles.regionText}>
                            {selectedRegionName}
                          </Text>
                          <Image
                            style={styles.dropDownArrow}
                            source={require("../../assets/images/cartScreenIcons/downArrow.png")}
                          />
                        </View>
                      </View>
                    </TouchableOpacity>
                    <RegionModal
                      setSelectedRegion={setSelectedRegion}
                      setSelectedRegionName={setSelectedRegionName}
                      selectedRegionId={selectedRegion}
                      regionList={regionList}
                      isRegionModalVisible={isRegionModalVisible}
                      toggleRegionModal={toggleRegionModal}
                    />
                  </View>
                </View>

                  <View style={styles.phoneScetion}>
                    <View style={styles.firstSection}>
                      <Image
                        source={require("../../assets/images/Icons/flag.png")}
                        style={{ height: 18, width: 18 }}
                      />
                      <View style={{ flexDirection: 'row', marginLeft: 2 }}>
                        <TextInput
                          editable={false}
                          style={styles.disableText}
                          placeholder={"+966"}
                          placeholderTextColor="#282828"/>
                      </View>
                    </View>
                    <View style={styles.secondSection}>
                      <TextInput
                        style={styles.enableText}
                        placeholder={appTexts.SIGNIN.PHONE}
                        placeholderTextColor="#242424"
                        keyboardType="number-pad"
                        autoFocus={true}
                        value={phone}
                        editable={false}

                      />
                    </View>
                  </View>
                </View>
                <View style={styles.buttonWrapper}>
                  <TouchableOpacity 
                    onPress={() => {updateUserDetails();
                      toggleUpdateModal();
                    }}
                  >
                    <CustomGreenLargeButton
                      buttonText={appTexts.EDIT_PROFILE.Save}
                    />
                  </TouchableOpacity>
                </View>
              </KeyboardAwareScrollView>
            </View>
          </Modal>
          {/* modalpopup update profile */}

          {/* modalpopup Request product */}
          <Modal
            isVisible={isRequestModalVisible}
            style={styles.modalRequest}
          >
            <View style={styles.modalRequestView}>
              <TouchableOpacity
                style={styles.buttonwrapper}
                onPress={() => {
                  toggleRequestModal();
                }}
              >
                <Text style={styles.closeButton}>X</Text>
              </TouchableOpacity>
              {/* <Text style={styles.helpText}>{appTexts.SUPPORT.Message}</Text> */}
              <KeyboardAwareScrollView>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={styles.requestTextHeading}>{appTexts.REQUEST.Request}</Text>
                </View>
                <View style={styles.nameView}>
                  <Text style={styles.nameStyleText}>{appTexts.REQUEST.Name}</Text>
                </View>
                <View style={styles.nameInput}>
                  <TextInput style={styles.nameInputStyle}
                    placeholder={appTexts.REQUEST.Nishad}
                    placeholderTextColor='grey' />
                </View>
                <View style={styles.profileBorder} />
                <View style={styles.nameView}>
                  <Text style={styles.nameStyleText}>{appTexts.REQUEST.Phone}</Text>
                </View>
                <View style={styles.nameInput}>
                  <TextInput style={styles.nameInputStyle}
                    placeholder={appTexts.REQUEST.PhoneNo}
                    placeholderTextColor='grey' />
                </View>
                <View style={styles.profileBorder} />
                <View style={styles.nameView}>
                  <Text style={styles.nameStyleText}>{appTexts.REQUEST.product}</Text>
                </View>
                <View style={styles.nameInput}>
                  <TextInput style={styles.nameInputStyle}
                    placeholder={appTexts.REQUEST.Vanilla}
                    placeholderTextColor='grey' />
                </View>
                <View style={styles.profileBorder} />
                <View style={styles.imageTextView}>
                  <Image style={styles.imageStyle} source={require('../../assets/images/home/product_03.png')}></Image>

                  <Image style={styles.iconImageStyle} source={require('../../assets/images/profileItem/add_image.png')}></Image>
                  <Text style={styles.iconText}>{appTexts.REQUEST.Add}</Text>

                </View>

                <View style={styles.buttonWrappers}>
                  <TouchableOpacity onPress={() => console.log("click")}>
                    <View style={styles.submitButton}>
                      <Text style={styles.sendbuttonText}>
                        {appTexts.REQUEST.Save}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </KeyboardAwareScrollView>
            </View>

          </Modal>
          {/* modalpopup  Request product */}
        </View>
      </ScrollView>
    </View>
  );
};

ProfileView.propTypes = {

  favouritemsClicked: PropTypes.func,
  faqPress: PropTypes.func,
  privacyPress: PropTypes.func,
  termsPress: PropTypes.func,
  supportPress: PropTypes.func,
  sharePress: PropTypes.func,
  logoutButtonPress: PropTypes.func,
  logoutPress: PropTypes.func,
  toggleModal: PropTypes.func,
  isModalVisible: PropTypes.bool,
  toggleHelpModal: PropTypes.func,
  isHelpModalVisible: PropTypes.bool,
  isLogoutpModalVisible: PropTypes.bool,
  toggleLogoutModal: PropTypes.func,
  toggleUpdateModal: PropTypes.func,
  fetchUserDetails: PropTypes.func,
  addressDetails: PropTypes.func,
  isUpdateModalVisible: PropTypes.bool,
  toggleRequestModal: PropTypes.func,
  isRequestModalVisible: PropTypes.bool,
  ishidePass: PropTypes.bool,
  setHidePass: PropTypes.func,
  ishideNewPass: PropTypes.bool,
  setHideNewPass: PropTypes.func,
  isconfirmHidePass: PropTypes.bool,
  setHideConfirmPass: PropTypes.func,

  setName: PropTypes.func,
  setEmail: PropTypes.func,
  setCname: PropTypes.func,
  setPhone: PropTypes.func,

  validatename: PropTypes.func,
  validateEmail: PropTypes.func,
  validateCname: PropTypes.func,

  toggleRegionModal: PropTypes.func,
  openRegionModal: PropTypes.func,

  updateUserDetails: PropTypes.func,
};

export default ProfileView;