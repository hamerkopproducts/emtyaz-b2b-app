import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import GMapView from './MapView';

import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions"

const MapScreen = (props) => {

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if(props.map_data && props.map_data.success == true) {
      functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, props.map_data.msg);
      props.resetMapData({});
      props.navigation.navigate('TabNavigator');
    }
    if(props.error && props.error.msg) {
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, props.error.msg);
      props.resetError({});
    }
  };
  
  const logoutButtonPress = () => {
    props.doLogout();
  };
  const onBackButtonPress = () => {
    props.navigation.goBack()
   };
  const locationPress = (location_lat_lng, location) => {
    const data = {location: {
        "lat": location_lat_lng.latitude,
        "lng": location_lat_lng.longitude,
        "name":location
      }
    }
    props.saveLocation(data, props.userData.data.access_token);
   };

  return (
    <GMapView 
      logoutButtonPress={logoutButtonPress}
      onBackButtonPress={onBackButtonPress}
      locationPress={locationPress}
      isLoading={props.map_isLoading}
      map_local_data={props.map_local_data}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    map_data: state.loginReducer.map_data,
    error: state.loginReducer.map_error,
    userData: state.loginReducer.userData,
    map_isLoading: state.loginReducer.map_isLoading,
    map_local_data: state.loginReducer.map_local_data
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    doLogout: LoginActions.doLogout,
    saveLocation: LoginActions.saveMapLocation,
    resetMapData: LoginActions.map_success,
    resetError: LoginActions.map_error
  }, dispatch)
};

const MapScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(MapScreen);

MapScreenWithRedux.navigationOptions = ({ navigation }) => ({
    header: null
  });
  
  export default MapScreenWithRedux;