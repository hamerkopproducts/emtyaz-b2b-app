import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image
} from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";

import Header from "../../components/Header";
import PropTypes from "prop-types";
import appTexts from "../../lib/appTexts";

import MapView, { Marker, Callout } from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import Loader from "../../components/Loader";
import { map_loading } from "../../actions/LoginActions";
Geocoder.init("AIzaSyDEvpyS_qklkSCHdUd4413z1QUchN4DBRM");

const GMapView = (props) => {

  const {
    onBackButtonPress,
    locationPress,
    isLoading,
    map_local_data
  } = props;

  let x = {
    latitude: 24.7136,
    longitude: 46.6753
  }
  let loc = 'Riyadh, SA';

  if(map_local_data && map_local_data.location) {
    console.debug(map_local_data)
    x = {
      latitude: map_local_data.location.lat,
      longitude: map_local_data.location.lng
    }
    loc = map_local_data.location.name;
  }
  const [location_lat_lng, setLocationLatLng] = useState(x);
  const [location, setLocation] = useState(loc);

  const getLocation = (cordinates) => {
    setLocationLatLng(cordinates);
    Geocoder.from(cordinates)
      .then(json => {
        const location_formatted = json.results[0].formatted_address;
        setLocation( location_formatted );
      })
      .catch(error => {
        console.debug(error);
      });
  }

  return (
    <View style={styles.mapScreenView}>

    {isLoading && <Loader /> }

      <Header
        navigation={props.navigation}
        headerTitle={appTexts.STRING.Location}
        isRightButtonRequired={false}
        isBackButtonRequired={true}
        isSearchButtonRequired={false}
        onBackButtonPress={onBackButtonPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor
        }}
      />

      <MapView
        onPress={({ nativeEvent }) => {
          getLocation(nativeEvent.coordinate);
        }}
        style={{ width: '100%', height: '98%', marginTop: 5 }}
        initialRegion={{
          latitude: 24.7136,
          longitude: 46.6753,
          latitudeDelta: 0.922,
          longitudeDelta: 0.221,
        }} >
        <Callout>
          <Marker
            coordinate={location_lat_lng}
          />
        </Callout>
      </MapView>

      <TouchableOpacity style={styles.floating} onPress={() => {
          locationPress(location_lat_lng, location);
        }} >
        <View style={styles.bottonBoxView}>
          <View style={styles.leftWrapper}>
            <Image style={styles.mapImage} source={require('../../assets/images/map/map_white2.png')}></Image>
            <Text numberOfLines={1} style={[
              styles.locationText,
              { width: 250}
            ]}>{ location }</Text>
          </View>
          <View style={styles.iconStyle}>
            <Image style={styles.mapIcon} source={require('../../assets/images/map/gps.png')}></Image>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

GMapView.propTypes = {
  locationPress: PropTypes.func,
};

export default GMapView;