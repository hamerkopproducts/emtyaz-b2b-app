import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  mapScreenView:{
      flex:1,
      backgroundColor:'white',
      
  },
  floating:{
    position:'absolute',
    width:'90%',
    height:60,
    //paddingLeft:'5%',
    backgroundColor:'orange',
    marginLeft:20,
    top:550,
     
  },
  bottonBoxView:{
     flexDirection:'row',
     justifyContent:'space-between',
     //alignSelf:'center',
     paddingLeft:'4%',
     paddingTop:'4%',
     
     
    
  },
  leftWrapper:{
    flexDirection:'row',
    justifyContent:'center',
    
  },
  mapIcon:{
    paddingRight:'2%',
    
  },
  iconStyle:{
    //alignSelf:'flex-end',
    paddingRight:'4%',
  },
  mapImage:{
    marginRight:'5%',
    width:24,
    height:24,
    resizeMode:'contain'
  },
  locationText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS. poppinssemiBold,
    fontSize:16,
    color:'white',
  },
});

export { images, styles };