import React, { useState, useEffect } from "react";
import { BackHandler, Keyboard } from "react-native";
import { connect } from "react-redux";
import SignupView from "./SignupView";
import { bindActionCreators } from "redux";
import functions from "../../lib/functions";
import appTexts from "../../lib/appTexts";
import * as SignUpActions from "../../actions/SignUpActions";
import * as LoginActions from "../../actions/LoginActions";

import NetInfo from "@react-native-community/netinfo";


const SignupScreen = (props) => {

  //Initialising states
  const [fullName, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [companyRegNo, setCompanyRegNo] = useState('');
  const [vatRegNo, setVatRegNo] = useState('');

  const [termsCheckbox, settermsCheckboxflag] = useState(false);
  const [nameerrorflag, setnameerrorflag] = useState(false);
  const [emailerrorflag, setemailerrorflag] = useState(false);
  const [numbererrorflag, setnumbererrorflag] = useState(false);
  const [cnameerrorflag, setcnameerrorflag] = useState(false);
  const [cregerrorflag, setcregerrorflag] = useState(false);
  const [vatregerrorflag, setvatregerrorflag] = useState(false);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener(
      "hardwareBackPress",
      (backPressed = () => {
        BackHandler.exitApp();
        return true;
      })
    );
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
    if (props.otpAPIReponse && props.otpAPIReponse.success) {
      props.loginServiceActionSuccess(null, phoneNumber.trim());
      props.navigation.navigate('OTPScreen', { phone: props.phone })
    }
    else if (props.error && props.error.msg) {
      functions.displayToast('error', 'top', props.error.msg);
      props.resetError();
    }
  };
  const validateFname = flag => {
    setnameerrorflag(flag);
  };
  const validateEmail = flag => {
    setemailerrorflag(flag);
  };
  const validatePhone = flag => {
    setnumbererrorflag(flag);
  };
  const validateCname = flag => {
    setcnameerrorflag(flag);
  };
  const validateCRegno = flag => {
    setcregerrorflag(flag);
  };
  const validateVatRegno = flag => {
    setvatregerrorflag(flag);
  };
  const checkBoxClick = flag => {
    settermsCheckboxflag(flag);
  };
  const signupNext = () => {
    if (fullName.trim() === '')
    {
      validateFname()
      functions.displayToast('error', 'top', 'First name can not be empty');
    } 
    else if(email.trim() == '' ){
      validateEmail()
      functions.displayToast('error', 'top', 'Email can not be empty');
    } 
    else if(phoneNumber.trim() == ''){
      validatePhone()
      functions.displayToast('error', 'top', 'Phone number can not be empty');

    }
     else if( companyName.trim() == '') {
       validateCname()
       functions.displayToast('error', 'top', 'Company name can not be empty');
     } 
     else if(companyRegNo.trim() == ''){
       validateCRegno()
       functions.displayToast('error', 'top', 'Company Registration number can not be empty');
     } 
     else if(vatRegNo.trim() == '') {
       validateVatRegno()
       functions.displayToast('error', 'top', 'Vat Registration number can not be empty');
    }
     else if(termsCheckbox == false) {
       functions.displayToast('error', 'top', 'Please accept tems and conditions');
    }
    else {
      Keyboard.dismiss()
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          let apiParam = {
            "name": fullName.trim(),
            "email": email.trim(),
            "phone_no": phoneNumber.trim(),
            "company_name": companyName.trim(),
            "company_reg_no": companyRegNo.trim(),
            "country_code": "+966",
            "vat_reg_no": vatRegNo.trim(),
          }
          props.doSignUp(apiParam);
        } else {
          functions.displayToast('error', 'top', appTexts.VALIDATION_MESSAGE.Type, appTexts.VALIDATION_MESSAGE.Mobile_error_msg);
        }
      });
    }
  };
  const termsandconditions = () => {
    props.navigation.navigate('TermsScreen');
  };
  const backButtonpress = () => {
    props.navigation.goBack()
  };
  return (
    <SignupView
      signupNext={signupNext}
      termsandconditions={termsandconditions}
      backButtonpress={backButtonpress}

      setFullName={setFullName}
      validateFname={validateFname}
      setEmail={setEmail}
      validateEmail={validateEmail}
      setCompanyName={setCompanyName}
      validatePhone={validatePhone}
      setCompanyRegNo={setCompanyRegNo}
      validateCRegno={validateCRegno}
      setPhoneNumber={setPhoneNumber}
      validateCname={validateCname}
      setVatRegNo={setVatRegNo}
      validateVatRegno={validateVatRegno}
      checkBoxClick={checkBoxClick}
      


      fullName={fullName}
      nameerrorflag={nameerrorflag}
      email={email}
      emailerrorflag={emailerrorflag}
      companyName={companyName}
      cnameerrorflag={cnameerrorflag}
      companyRegNo={companyRegNo}
      cregerrorflag={cregerrorflag}
      vatRegNo={vatRegNo}
      vatregerrorflag={vatregerrorflag}
      phoneNumber={phoneNumber}
      numbererrorflag={numbererrorflag}
      termsCheckbox={termsCheckbox}
      isLoading={props.isLoading}

    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    isLoading: state.loginReducer.isLoading,
    error: state.signUpReducer.error,
    otpAPIReponse: state.signUpReducer.otpAPIReponse,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      doSignUp: SignUpActions.doSignUp,
      loginServiceActionSuccess: LoginActions.loginServiceActionSuccess,
      resetError: LoginActions.resetError,
    },
    dispatch
  );
};

const SignUpWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupScreen);

SignUpWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default SignUpWithRedux;
