import React, { useRef } from "react";
import PropTypes from "prop-types";
import {
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  Image,
  TextInput, ScrollView, CheckBox
} from "react-native";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { images, styles } from "./styles";
import CustomGreenLargeButton from "../../components/CustomGreenLargeButton";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Loader from '../../components/Loader';
import Modal from "react-native-modal";

const SignupView = (props) => {
  const fullNameInput = useRef(null);
  const emailInput = useRef(null);
  const phoneNumberInput = useRef(null);
  const companyNameInput = useRef(null);
  const companyRegNoInput = useRef(null);
  const vatRegNoInput = useRef(null);
  const {
    setFullName,
    validateFname,
    setEmail,
    validateEmail,
    setPhoneNumber,
    validatePhone,
    setCompanyName,
    validateCname,
    setCompanyRegNo,
    validateCRegno,
    setVatRegNo,
    validateVatRegno,
    checkBoxClick,

    signupNext,
    backButtonpress,
    termsandconditions,
    nameerrorflag,
    emailerrorflag,
    numbererrorflag,
    cnameerrorflag,
    cregerrorflag,
    vatregerrorflag,
    termsCheckbox,
    isLoading

  } = props;

  return (
    <KeyboardAwareScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps='always'>
      <View style={styles.screenMain}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor={globals.COLOR.screenBackground}
        />
        
        {isLoading && <Loader/>}
       

        <View style={styles.arrowWrapper}>
          <TouchableOpacity
            onPress={() => {
              backButtonpress();
            }}
          >
            <Image
              source={images.backIcon}
              resizeMode="contain"
              style={styles.arrowicon}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.logoWrapper}>
          <Image
            source={images.logoImage}
            resizeMode="contain"
            style={styles.logoImage}
          />
        </View>
        <View style={styles.contentWrapper}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={styles.contentHeader}>{appTexts.SIGNIN.Heading}</Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={styles.contentDescription}>
              {appTexts.SIGNUP.Description}
            </Text>
          </View>
        </View>

        <View style={styles.fullName}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.formcontentDescription}>
              {appTexts.SIGNUP.Fullname}
            </Text>
          </View>
          <View style={{ marginTop: -12 }}>
            <TextInput
              style={nameerrorflag == false ? styles.formInput : styles.formInputerror}
              // style={styles.formInput}
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              autoFocus={true}
              ref={fullNameInput}
              onChangeText={val => {
                validateFname(false);
                setFullName(val);
              }}
            />
          </View>
        </View>
        <View style={styles.emailAddress}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.formcontentDescription}>
              {appTexts.SIGNUP.Email}
            </Text>
          </View>
          <View style={{ marginTop: -12 }}>
            <TextInput

              style={emailerrorflag == false ? styles.formInput : styles.formInputerror}
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              ref={emailInput}
              onChangeText={val => {
                validateEmail(false)
                setEmail(val);
              }}
            />
          </View>
        </View>

        <View style={styles.emailAddress}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.formcontentDescription}>
              {appTexts.SIGNUP.PHONE}
            </Text>
          </View>
          <View style={styles.phoneScetion}>
            <View style={styles.firstSection}>
              <Image
                source={require("../../assets/images/Icons/flag.png")}
                style={{ height: 18, width: 18 }}
              />
              <View style={{ flexDirection: 'row', marginLeft: 2 }}>
                <TextInput
                  editable={false}
                  style={styles.disableText}
                  placeholder={"+966"}
                  placeholderTextColor="#282828"


                />
              </View>
            </View>
            <View style={numbererrorflag == false ? styles.secondSection : styles.secondSectionError}>
              <TextInput
                style={styles.formInputPhonenumber}
                placeholder={appTexts.SIGNIN.PHONE}
                placeholderTextColor="#242424"
                keyboardType="number-pad"
                autoFocus={true}
                ref={phoneNumberInput}
                onChangeText={val => {
                  validatePhone(false)
                  setPhoneNumber(val)
                }}
              // value={number}

              />
            </View>
          </View>

        </View>

        <View style={styles.companyName}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.formcontentDescription}>
              {appTexts.SIGNUP.compnayname}
            </Text>
          </View>
          <View style={{ marginTop: -12 }}>
            <TextInput
              style={cnameerrorflag == false ? styles.formInput : styles.formInputerror}
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              ref={companyNameInput}
              onChangeText={val => {
                validateCname(false)
                setCompanyName(val)
              }}
            />
          </View>
        </View>

        <View style={styles.companyName}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.formcontentDescription}>
              {appTexts.SIGNUP.companynumber}
            </Text>
          </View>
          <View style={{ marginTop: -12 }}>
            <TextInput
              style={cregerrorflag == false ? styles.formInput : styles.formInputerror}
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              ref={companyRegNoInput}
              onChangeText={val => {
                validateCRegno(false)
                setCompanyRegNo(val)
              }}
            />
          </View>
        </View>

        <View style={styles.companyName}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.formcontentDescription}>
              {appTexts.SIGNUP.VAT}
            </Text>
          </View>
          <View style={{ marginTop: -12 }}>
            <TextInput
              style={vatregerrorflag == false ? styles.formInput : styles.formInputerror}
              placeholderTextColor="#9a73ef"
              autoCapitalize="none"
              ref={vatRegNoInput}
              onChangeText={val => {
                validateVatRegno(false)
                setVatRegNo(val)
              }}
            />
          </View>
        </View>

        <View style={styles.acceptSection}>
          <TouchableOpacity onPress={() => termsCheckbox == false? checkBoxClick(true): checkBoxClick(false)}>
            <View style={styles.checkBoxView}>
              <Text style={styles.checkboxText}>
              {termsCheckbox == true? '✓': ''}
              </Text>
            </View>
            {/* isCheck ? 
          <Image style={styles.imageCheck} source={require('../../assets/images/signup/uncheck.png')}/> :
        <Image style={styles.imageCheck} source={require('../../assets/images/signup/checked_box.png')}/>  */}
            {/* <Image style={styles.imageCheck} source={checkBoxClick ? require("../../assets/images/signup/checked_box.png")
              : require("../../assets/images/signup/uncheck.png")} /> */}
          </TouchableOpacity>
         
          <Text style={styles.conitionText}> {appTexts.SIGNUP.Terms}</Text>
          <TouchableOpacity onPress={() => { termsandconditions() }}>
            <Text style={styles.termsText}> {appTexts.SIGNUP.Condition}</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            onPress={() => signupNext()}>
            <CustomGreenLargeButton buttonText={appTexts.SIGNUP.NEXT} />
          </TouchableOpacity>
        </View>
      </View>
    </KeyboardAwareScrollView>
  );
};

SignupView.propTypes = {
  setFullName: PropTypes.func,
  validateFname: PropTypes.func,
  setEmail: PropTypes.func,
  validateEmail: PropTypes.func,
  setPhoneNumber: PropTypes.func,
  validatePhone: PropTypes.func,
  setCompanyName: PropTypes.func,
  validateCname: PropTypes.func,
  setCompanyRegNo: PropTypes.func,
  validateCRegno: PropTypes.func,
  setVatRegNo: PropTypes.func,
  validateVatRegno: PropTypes.func,
  checkBoxClick: PropTypes.func,
  signupNext: PropTypes.func,

  nameerrorflag: PropTypes.bool,
  emailerrorflag: PropTypes.bool,
  numbererrorflag: PropTypes.bool,
  cnameerrorflag: PropTypes.bool,
  cregerrorflag: PropTypes.bool,
  vatregerrorflag: PropTypes.bool,
  termsCheckbox: PropTypes.bool,
  isLoading: PropTypes.bool,

};

export default SignupView;
