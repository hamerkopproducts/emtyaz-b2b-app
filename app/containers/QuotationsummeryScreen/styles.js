import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;

const images = {};

const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    //flexDirection:'column',
    backgroundColor: globals.COLOR.screenBackground,
  },
  screenContainer: {
    flex: 1,
    marginTop: globals.INTEGER.heightTen,
    flexDirection: "column",
    backgroundColor: globals.COLOR.white,
    alignItems: "center",
    justifyContent: "center",
  },
  headerButtonContianer: {
    flexDirection: "row",
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: "center",
  },
  headerButton: {
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty,
  },
  headerButtonMargin: {
    marginLeft: globals.MARGIN.marginTen,
  },
  contentWrapper: {
    //backgroundColor:'red',
    flex: 1,
  },
  privacyContent: {
    paddingLeft: "5%",
    paddingRight: "5%",
    paddingTop: hp("2%"),
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakBlack,
    fontSize: 13,
  },
  cardWrapper: {
    paddingLeft: "5%",
    paddingRight: "5%",
    paddingTop: "5%",
    paddingBottom: "5%",
  },
  deliveryWrapper: {
    paddingLeft: "5%",
    paddingRight: "5%",
    paddingTop: "1%",
    paddingBottom: "5%",
  },
  descriptionContainer: {
    borderWidth: 0,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 1: 3,
    backgroundColor:  '#ffffff',
  },
  descriptionView: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: "4%",
    paddingRight: "4%",
    paddingTop: "3%",
  },
  bookdetailscontents: {
    alignItems: "flex-end",
  },
  quotationText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
    color: globals.COLOR.drakBlack,
    fontSize: 14,
  },
  sarsText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsLight,

    paddingTop: "1.3%",
    //paddingBottom:'1%',
    fontSize: 13,
    color: globals.COLOR.lightBlack,
  },
  sarText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsLight,
    fontSize: 13,
    color: globals.COLOR.lightBlack,
    paddingTop: "1.3%",
    //paddingLeft:'5%'
  },
  boldText: {
    fontFamily: I18nManager.isRTL
      ?globals.FONTS.poppinsMedium
      : globals.FONTS.poppinsMedium,
    fontSize: 13,
    color: globals.COLOR.lightBlack,
    paddingTop: "1.3%",
  },
  itemText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color: "#CFCFCF",
  },
  placeText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    fontSize: 12,
    color: "#686868",
  },
  schedText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
    fontSize: 12,
    color: globals.COLOR.drakBlack,
  },
  nameText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color: "#383838",
  },
  greenText: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    fontSize: 12,
    color: "#3fb851",
    marginRight:6
  },
  arrowIcon: {
    width: 15,
    height: 15,
  },
  editIcon: {
    width: 15,
    height: 15,
  },
  timeContent: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 1,
    paddingBottom: 2,
  },
  itemView: {
    flexDirection: "row",
    paddingLeft: "4%",
    paddingRight: "4%",
  },
  billView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: "4%",
    paddingRight: "4%",
    paddingTop: "2%",
    paddingBottom: "3%",
  },
  vatView: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: "4%",
    paddingRight: "4%",
    paddingTop: "3%",
    paddingBottom: "3%",
  },
  deliveryView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: "4%",
    paddingRight: "4%",
    paddingTop: "2%",
    paddingBottom: "3%",
  },
  netbankView:{
    paddingLeft: "4%",
    paddingRight: "4%",
    paddingTop: "3%",
    paddingBottom: "3%",
  },
  buttonWrapper:{
    flexDirection:'row',
    justifyContent:'space-between',
     paddingLeft:'2%',
     paddingRight:'2%',
     // backgroundColor:'red',
      //paddingTop:'5%',
      //paddingBottom:'10%'
    //  marginBottom:10
     paddingBottom:'5%',
     paddingTop:'5%'
  },
  cancelButton: {
    backgroundColor: '#FBFBFB',
    borderColor:'#CFCFCF',
borderWidth:1,
    width: 170,
    height: 50,
    justifyContent: "center",
    alignItems: "center",},submitButton: {
      backgroundColor: globals.COLOR.themeGreen,
      width: 170,
      height: 50,
      justifyContent: "center",
      alignItems: "center",},
      saveText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
        fontSize: 13,
        color:'white'
      },cancelText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
        fontSize: 11,
        color:'#242424'
      },
      timeslotView:{
        flexDirection:'row',
        paddingTop:'2%'
      },
      sarboldText:{
        fontFamily: I18nManager.isRTL
        ?globals.FONTS.poppinsMedium
        : globals.FONTS.poppinsMedium,
      fontSize: 11,
      },
      whiteText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
        fontSize: 10,
      },
      greensText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsMedium,
        fontSize: 10,
        color:'white'
      }
      
  
});

export { images, styles };
