import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import SummeryView from './SummeryView';
import { bindActionCreators } from "redux";
import * as LoginActions from "../../actions/LoginActions";
import globals from "../../lib/globals";
import functions from "../../lib/functions"


const QuotationsummeryScreen = (props) => {
  const [showDescription, setShowDescription] = useState(true);
  const [showDelivery, setShowDelivery] = useState(true);
  const [showCalender, setShowCalender] = useState(false); 
  

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };

  const openHideDescription = () => {
    setShowDescription(!showDescription)
  };

  const openHideDelivery = () => {
    setShowDelivery(!showDelivery)
  };

  const openHideCalender = () =>{
    setShowCalender(!showCalender)
  };
  
  const logoutButtonPress = () => {
    props.doLogout();
  };

  const onBackButtonPress = () => {
    props.navigation.goBack()
   };

  return (
    <SummeryView logoutButtonPress={logoutButtonPress}
    openHideDescription={openHideDescription}
    showDescription={showDescription}
    showDelivery={showDelivery}
    onBackButtonPress={onBackButtonPress}
    openHideDelivery={openHideDelivery}
    openHideCalender={openHideCalender}
    showCalender={showCalender}/>
  );

};


const mapStateToProps = (state, props) => {
  return {
    
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    doLogout: LoginActions.doLogout
  }, dispatch)
};

const profileScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(QuotationsummeryScreen);

profileScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default profileScreenWithRedux;
