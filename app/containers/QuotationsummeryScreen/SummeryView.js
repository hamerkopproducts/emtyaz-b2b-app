import React from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity, Image, ScrollView,I18nManager } from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/AntDesign";
const SummeryView = (props) => {
  const { showDescription, openHideDescription,onBackButtonPress } = props;
  const { showDelivery, openHideDelivery } = props;
  const { showCalender, openHideCalender } = props;

  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        isBackButtonRequired={true}
        onBackButtonPress={onBackButtonPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <ScrollView>
        <View style={styles.cardWrapper}>
          <View style={[styles.descriptionContainer, { width: "100%" }]}>
            <View style={styles.descriptionView}>
              <Text style={styles.quotationText}>
                {appTexts.QUOTATIONS_SUMMERY.QuotationSummary}
              </Text>
              <TouchableOpacity
                style={styles.arrowContainer}
                onPress={() => {
                  openHideDescription();
                }}
              >
                <Image
                  source={
                    showDescription
                      ? require("../../assets/images/home/down-arrown.png")
                      : require("../../assets/images/home/up-arrown.png")
                  }
                  style={styles.arrowIcon}
                />
              </TouchableOpacity>
            </View>

            {showDescription && (
              <View>
                <View style={styles.itemView}>
                  <Text style={styles.itemText}>
                  {(I18nManager.isRTL ? '2 سلع' :'2 Items')}
                   </Text>
                </View>
                <View style={styles.billView}>
                  <View style={styles.bookdetailscontentsright}>
                    <Text style={styles.sarsText}>
                      {appTexts.QUOTATIONS_SUMMERY.Subtotal}
                    </Text>
                    <Text style={styles.sarsText}>
                      {appTexts.QUOTATIONS_SUMMERY.Discount}
                    </Text>
                    <Text style={styles.sarsText}>
                      {appTexts.QUOTATIONS_SUMMERY.DeliveryCharge}
                    </Text>
                  </View>

                  <View style={styles.bookdetailscontents}>
                    <Text style={styles.sarText}>SAR 200.00</Text>
                    <Text style={styles.sarText}>SAR 10.00</Text>
                    <Text style={styles.sarText}>SAR 210.00</Text>
                  </View>
                </View>
                <View
                  style={{ borderBottomColor: "#CFCFCF", borderBottomWidth: 1 }}
                />
                <View style={styles.vatView}>
                  <View style={styles.bookdetailscontentsright}>
                    <Text style={styles.sarsText}>
                      {appTexts.QUOTATIONS_SUMMERY.TotalInclusivefVAT}
                    </Text>
                  </View>

                  <View style={styles.bookdetailscontents}>
                    <Text style={styles.boldText}>SAR 900.00</Text>
                  </View>
                </View>
              </View>
            )}
          </View>
        </View>

        <View style={styles.deliveryWrapper}>
          <View style={[styles.descriptionContainer, { width: "100%" }]}>
            <View style={styles.descriptionView}>
              <Text style={styles.quotationText}>
                {appTexts.QUOTATIONS_SUMMERY.Deliveryaddress}
              </Text>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <View style={{ marginRight: 8 }}>
                  <Image
                    source={require("../../assets/images/profileItem/edit.png")}
                    style={[styles.editIcon]}
                  />
                </View>
                <TouchableOpacity
                  style={styles.arrowContainer}
                  onPress={() => {
                    openHideDelivery();
                  }}
                >
                  <Image
                    source={
                      showDescription
                        ? require("../../assets/images/home/down-arrown.png")
                        : require("../../assets/images/home/up-arrown.png")
                    }
                    style={[styles.arrowIcon]}
                  />
                </TouchableOpacity>
              </View>
            </View>

            {showDelivery && (
              <View>
                <View style={styles.deliveryView}>
                  <View style={styles.userDetails}>
                    <View style={styles.textContent}>
                      <View style={{ paddingLeft: "0%" }}>
                        <View style={styles.textContent}>
                          <Text style={styles.nameText}>John Mathew</Text>
                        </View>
                        <View style={styles.textContent}>
                          <Text style={styles.placeText}>Al Naemia Street</Text>
                        </View>
                        <View style={styles.textContent}>
                          <Text style={styles.placeText}>PO Box: 2211</Text>
                        </View>
                        <View style={styles.textContent}>
                          <Text style={styles.placeText}>Al Nameem,Jaddah</Text>
                        </View>
                        <View style={styles.textContent}>
                          <Text style={styles.placeText}>+966 00000</Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                <View
                  style={{ borderBottomColor: "#CFCFCF", borderBottomWidth: 1 }}
                />

                <View style={styles.descriptionView}>
                  <Text style={styles.quotationText}>
                    {appTexts.QUOTATIONS_SUMMERY.Billing}
                  </Text>

                  <Image
                    source={require("../../assets/images/profileItem/edit.png")}
                    style={[styles.editIcon]}
                  />
                </View>
                <View style={styles.deliveryView}>
                  <View style={styles.userDetails}>
                    <View style={styles.textContent}>
                      <View style={{ paddingLeft: "0%" }}>
                        <View style={styles.textContent}>
                          <Text style={styles.nameText}>John Mathew</Text>
                        </View>
                        <View style={styles.textContent}>
                          <Text style={styles.placeText}>Al Naemia Street</Text>
                        </View>
                        <View style={styles.textContent}>
                          <Text style={styles.placeText}>PO Box: 2211</Text>
                        </View>
                        <View style={styles.textContent}>
                          <Text style={styles.placeText}>Al Nameem,Jaddah</Text>
                        </View>
                        <View style={styles.textContent}>
                          <Text style={styles.placeText}>+966 00000</Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            )}
          </View>
        </View>

        <View style={styles.deliveryWrapper}>
          <View style={[styles.descriptionContainer, { width: "100%" }]}>
            <View style={styles.descriptionView}>
              <Text style={styles.quotationText}>
                {appTexts.QUOTATIONS_SUMMERY.Scheduled}
              </Text>
            </View>

            <View style={styles.deliveryView}>
              <View style={styles.userDetails}>
                <View style={styles.textContent}>
                  <View style={{ paddingLeft: "0%" }}>
                    <View style={styles.timeContent}>
                      <Text style={styles.schedText}>
                        {appTexts.QUOTATIONS_SUMMERY.Delivery}
                      </Text>
                      <Text style={styles.schedText}>Jedda, Al Nameem</Text>
                    </View>
                    <View style={styles.timeContent}>
                      <Text style={styles.schedText}>
                        {appTexts.QUOTATIONS_SUMMERY.Deliverytime}
                      </Text>
                      <Text style={styles.greenText}>
                        {" "}
                        12 July 2020, 2pm-5pm
                      </Text>
                      <TouchableOpacity
                        style={styles.arrowContainer}
                        onPress={() => {
                            // alert('hai welcome');
                           openHideCalender();
                        }}
                      >
                        <Image
                          source={
                            showCalender
                              ? require("../../assets/images/home/time_slot_dn.png")
                              : require("../../assets/images/home/time_slot_up.png")
                          }
                          style={[styles.editIcon, { marginLeft: "3%" }]}
                        />
                      </TouchableOpacity>
                    </View>
                    {showCalender && (
                      <View>
             <View style={styles.calenderView}>
               {/* <Text style={[styles.greenText],{color:'red'}}>
                    calenderview
                      </Text> */}
             </View>
             <View style={styles.timeslotView}>
             <View
                style={{
                  backgroundColor: "#EBEBEB",
                  width: 85,
                  height: 32,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Text style={styles.whiteText}>10 am - 1 pm</Text>
              </View>
              <View
                style={{
                  marginLeft:8,
                  backgroundColor: "#3fb851",
                  width: 85,
                  height: 32,
                  justifyContent: "center",
                  alignItems: "center",
                  
                }}
              ><Text style={styles.greensText}>2 pm - 5 pm</Text>
              </View>
               
             </View>
             </View>
             
            )}
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>

        <View style={styles.deliveryWrapper}>
          <View style={[styles.descriptionContainer, { width: "100%" }]}>
            <View style={styles.descriptionView}>
              <Text style={styles.quotationText}>
                {appTexts.QUOTATIONS_SUMMERY.SelectPaymentMethod}
              </Text>
            </View>

            <View style={styles.netbankView}>
              <View
                style={{
                  backgroundColor: "#f2f2f2",
                  width: 297,
                  height: 80,
                  marginBottom: "5%",
                }}
              ></View>
              <View
                style={{
                  backgroundColor: "#f2f2f2",
                  width: 297,
                  height: 80,
                  marginBottom: "5%",
                }}
              ></View>
              <View
                style={{
                  backgroundColor: "#f2f2f2",
                  width: 297,
                  height: 80,
                  marginBottom: "5%",
                }}
              ></View>
            </View>
          </View>
        </View>
      </ScrollView>

      <View style={styles.buttonWrapper}>
        <View style={styles.cancelButton}>
          <Text style={styles.cancelText}>
            {appTexts.QUOTATIONS_SUMMERY.TOTALPAYABLEAMOUNT}
            {"\n"}
            <Text style={styles.sarboldText}>{"SAR 160.00"}</Text>
            {/* {"SAR 160.00"} */}
          </Text>
        </View>
        <View style={styles.submitButton}>
          <Text style={styles.saveText}>
            {appTexts.QUOTATIONS_SUMMERY.Confirm}
          </Text>
        </View>
      </View>
    </View>
  );
};

SummeryView.propTypes = {
  logoutButtonPress: PropTypes.func,
};

export default SummeryView;
