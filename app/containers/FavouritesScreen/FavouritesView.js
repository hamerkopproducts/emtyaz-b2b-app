import React from 'react';
import { View, FlatList, Text } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";

import PromotionalProductCard from "../../components/PromotionalProductCard";
import Header from "../../components/Header";
import appTexts from "../../lib/appTexts";

const FavouritesView = (props) => {

	const {
		itemClick,
		onCartButtonPress,
		onBackButtonPress,
		wishList
	} = props;

	let listData = [
		{
			id: 1,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: true,
			image: require('../../assets/images/home/product_01.png')

		},
		{
			id: 2,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: true,
			image: require('../../assets/images/home/product_02.png')

		},
		{
			id: 3,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: true,
			image: require('../../assets/images/home/product_03.png')

		},
		{
			id: 4,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: true,
			image: require('../../assets/images/home/product_04.png')

		},
		{
			id: 5,
			name: 'Novalac Genio Groving-Up Vanila Flavour Milk Shake Powder',
			isFav: true,
			image: require('../../assets/images/home/product_05.png')

		}]

	const renderItem = ({ item, index }) => <PromotionalProductCard item={item} itemClick={itemClick} />;

	return (
		
		<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isBackButtonRequired={true}
				onBackButtonPress={onBackButtonPress}
				headerTitle={appTexts.STRING.favourites}
				isLanguageButtonRequired={false}
				isRightButtonRequired={true}
				onCartButtonPress={onCartButtonPress}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
			<View style={styles.screenContainer}>
		
				<View style={styles.listContainer}>
					{listData.length === 0 ? <View style={styles.noDataContainer}><Text style={styles.noDataText}>{appTexts.STRING.nodata}</Text></View> :

						<FlatList
							style={styles.flatListStyle}
							data={listData}
							extraData={listData}
							keyExtractor={(item, index) => index.toString()}
							/*onEndReachedThreshold={0.5}
							onEndReached={({ distanceFromEnd }) => {
								if (listData.length >= (currentPage * pageLimit)) {
									loadMoreItems();
								}
							}}*/
							//onRefresh={() => { onRefresh() }}
							//refreshing={isRefreshing}
							showsVerticalScrollIndicator={false}
							renderItem={renderItem} />}
				</View>
			
		</View>
		</View>
			

	);
};

FavouritesView.propTypes = {
	
};

export default FavouritesView;
