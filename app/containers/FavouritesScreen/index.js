import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import FavouritesView from './FavouritesView';
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import functions from "../../lib/functions"
import * as HomePageActions from "../../actions/HomePageActions";

const FavouritesScreen = (props) => {
  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.wishListData(props.userData.data.access_token);
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  const onCartButtonPress = () => {
    props.navigation.navigate('CartScreen')
  };
  const showDetails = () => {
    props.navigation.navigate('DetailsScreen')
  };
  const onBackButtonPress = () => {
    props.navigation.goBack()
   };

  return (
    <FavouritesView 
      onCartButtonPress={onCartButtonPress} 
      itemClick={showDetails} 
      onBackButtonPress={onBackButtonPress}
      wishList={props.wishList}
    />
  );

};

const mapStateToProps = (state, props) => {
  return {
    userData: state.loginReducer.userData,
    wishList: state.homepageReducer.wl_data,
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    wishListData: HomePageActions.wishListData
  }, dispatch)
};

const favouriteScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(FavouritesScreen);

favouriteScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default favouriteScreenWithRedux;
