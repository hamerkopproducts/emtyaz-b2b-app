import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import DetailsView from './DetailsView';
import { bindActionCreators } from "redux";

import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions";
import * as HomePageActions from "../../actions/HomePageActions";


const DetailsScreen = (props) => {

  const [isBannerModalVisible, setIsBannerModalVisible] = useState(false);
  const [showDescription, setShowDescription] = useState(false);
  const [showSpecification, setShowSpecification] = useState(false);

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        const slug = props.route.params.slug;
        props.resetData({});
        props.productDetails(slug, props.userData.data.access_token);
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if (props.error && props.error.msg) {
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, props.error.msg);
      props.resetError();
    }
    if (props.fav_data && props.fav_data.success == true) {
      functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, props.fav_data.msg);
      if(props.resetFavSuccess) {
        props.resetFavSuccess();
      }
    }
  };
  const showDetails = () => {
    props.navigation.navigate('DetailsScreen')
  };
  const onCartButtonPress = () => {
    props.navigation.navigate('CartScreen')
  };
  const onBackButtonPress = () => {
    props.navigation.goBack()
  };
  const closeBannerModal = () => {
    setIsBannerModalVisible(false)
  };
  const openBannerModal = () => {
    setIsBannerModalVisible(true)
  };
  const openHideDescription = () => {
    setShowDescription(!showDescription)
  };
  const openHideSpecification = () => {
    setShowSpecification(!showSpecification)
  };

  const addRemoveFavProduct = (id) => {
    const apiParam = {
      product_id: id
    };
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        props.addRemoveFav(apiParam, props.userData.data.access_token);
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  return (
    <DetailsView onCartButtonPress={onCartButtonPress} 
      itemClick={showDetails} 
      onBackButtonPress={onBackButtonPress} 
      isBannerModalVisible={isBannerModalVisible} 
      closeBannerModal={closeBannerModal} 
      openBannerModal={openBannerModal} 
      openHideDescription={openHideDescription} 
      showDescription={showDescription}
      openHideSpecification={openHideSpecification} 
      showSpecification={showSpecification}
      details={props.product_details}
      isLoading={props.isLoading||props.f_isLoading}
      navigation={props.navigation}
      addToCart={(item)=> {
        let existing = false;
        let all = props.cartItems ? [...props.cartItems] : [];
        if(props.cartItems) {
          for(let inc=0; inc<all.length; inc++) {
            const _itm = all[inc];
            if(_itm.type ==  item.type &&
              _itm.variant_id ==  item.variant_id &&
              _itm.quantityId ==  item.quantityId &&
              _itm.product_id ==  item.product_id
              ) {
                existing = true;
                all[inc].quantity += 1;
                props.addItemsToCart(all);
              }
          }
        }
        if(existing == false) {
          all.push(item);
          props.addItemsToCart(all);
        }
      }}
      addRemoveFav={addRemoveFavProduct}
      wishList={props.wishList}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    userData: state.loginReducer.userData,
    product_details: state.homepageReducer.product_details,
    isLoading: state.homepageReducer.d_isLoading,
    error: state.homepageReducer.d_error,
    cartItems: state.homepageReducer.cartItems,
    fav_data: state.homepageReducer.fav_data,
    wishList: state.homepageReducer.wl_data,
    f_isLoading: state.homepageReducer.f_isLoading,
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    productDetails: HomePageActions.productDetails,
    resetError: HomePageActions.detail_apiServiceActionErrorReset,
    addItemsToCart: HomePageActions.addItemsToCart,
    addRemoveFav: HomePageActions.addRemoveFav,
    resetData: HomePageActions.detail_listServiceActionSuccess,
    wishListData: HomePageActions.wishListData,
    addRemoveFav: HomePageActions.addRemoveFav,
  }, dispatch)
};

const detailsScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(DetailsScreen);

detailsScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default detailsScreenWithRedux;
