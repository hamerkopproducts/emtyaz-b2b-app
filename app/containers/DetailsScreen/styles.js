import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainerScrollView:{
    backgroundColor: globals.COLOR.screenBackground,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height - 120,
  },
  bottomButtonContainer:{
    height: 120,
    marginLeft: globals.INTEGER.leftRightMargin,
    width: globals.INTEGER.screenWidthWithMargin,
    alignItems:'center',
    justifyContent:'center'
  },
  horizontalView:{
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: globals.INTEGER.screenWidthWithMargin,
    flexDirection:'row'
  },
  buttonView: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    width: globals.INTEGER.screenWidthWithMargin,
    backgroundColor: globals.COLOR.themeGreen
  },
  imageScroller: {
    backgroundColor: '#F9F9F9',
    width: globals.SCREEN_SIZE.width,
  },
  screenDesignContainer:{
    backgroundColor: globals.COLOR.screenBackground,
    width: globals.SCREEN_SIZE.width,
    paddingBottom: globals.INTEGER.screenPaddingFromFooter
  },
  screenContainerWithMargin:{
    width: globals.INTEGER.screenWidthWithMargin,
    marginLeft: globals.INTEGER.leftRightMargin,
    backgroundColor: globals.COLOR.screenBackground
  },
  deliveryDetailsContainer:{
    width: globals.INTEGER.screenWidthWithMargin,
    marginLeft: globals.INTEGER.leftRightMargin,
    backgroundColor: globals.COLOR.screenBackground,
    marginBottom:20,
    
  },
  descriptionContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    marginLeft: globals.INTEGER.leftRightMargin,
    backgroundColor: globals.COLOR.screenBackground,
    marginTop: 20,
    marginBottom: 20,
  },
  similarProductContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    marginLeft: globals.INTEGER.leftRightMargin,
    backgroundColor: globals.COLOR.screenBackground,
    marginTop: 20,
    marginBottom: 20,
    
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  promotionalContainer:{
    marginBottom:20
  },
  categoryItemRow:{
    width:'100%',
    flexDirection:'row',
    flexWrap:'wrap'
  },
  itemNameText: {
    color: globals.COLOR.addressLabelColor,
    fontFamily:  I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14,
    flexDirection: 'row'
  },
  itemNameText1: {
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinssemiBold,
    fontSize: 14
  },
  itemNameText2: {
    
    color: globals.COLOR.textColor,
    fontFamily:  I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14
  },
  itemNameText4: {
    width: '90%',
    fontFamily:  I18nManager.isRTL ? globals.FONTS.notokufiArabic :globals.FONTS.poppinssemiBold,
    fontSize: 15
  },
  similarProductList: {
    marginTop: 10,
    flex:1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  itemNameText3: {
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 14
  },
  descriptionView:{
    flexDirection:'row',
    justifyContent: 'space-between'
  },
  similarProductView:{
    flexDirection:'column',
  },
  buttonText: {
    textAlign: "center",
    color: globals.COLOR.white,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15
  },
  arrowContainer:{
    width:'10%',
    height:30,
    justifyContent:'center',
    alignItems:'flex-end'
  },
  shareTextView:{
    width:'48%'
  },
  shareText: {
    textAlign: "center",
    color: globals.COLOR.themeGreen,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 15
  },
  verticalDivider: {
    width: 1.5,
    height: '40%',
    backgroundColor: globals.COLOR.themeGreen,
  }
});

export { images, styles };
