import React, { useState } from 'react';
import { View, ScrollView, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";
import PropTypes from 'prop-types';
import Header from "../../components/Header";
import ImageItem from "./ImageItem";

import BannerModal from "../../components/BannerModal";
import ItemCard from "./ItemCard";
import DividerLine from "../../components/DividerLine";
import { SliderBox } from 'react-native-image-slider-box';
import appTexts from "../../lib/appTexts";
import PromotionalProductCard from "../../components/PromotionalProductCard";
import Loader from "../../components/Loader";

const DetailsView = (props) => {
	const {
		itemClick,
		onCartButtonPress,
		onBackButtonPress,
		isBannerModalVisible,
		closeBannerModal,
		openBannerModal,
		showDescription,
		openHideDescription,
		showSpecification,
		openHideSpecification,
		details,
    isLoading,
    wishList
  } = props;

  let WishListItems = [];
  if(wishList.data) {
    WishListItems = wishList.data.map(item => {
      return item.product_id;
    });
  }

  let similarProducts = [];
  try {
    similarProducts = details.data.similarProduct;
  } catch(err) {
    similarProducts = [];
  }

	let images = [];
	try {
		for(let inc=0; inc < details.data.product.produc_images.length; inc++) {
			images.push(details.data.product.produc_images[inc].path);
		}
	} catch(err) {
		images = [];
	}

	let description = '';
	try {
		description = details.data.product.lang[0].description;
	} catch(err) {
		description = '';
	}
  let speccification_data = '';

  let product_id = '';
	try {
		product_id = details.data.product.id;
	} catch(err) {
		product_id = '';
  }
  
  let product_name = '';
	try {
		product_name = details.data.product.lang[0].name;
	} catch(err) {
		product_name = '';
  }
  
  let c_image = '';
	try {
		c_image = details.data.product.cover_image;
	} catch(err) {
		c_image = '';
  }
  
  let product_type = '';
	try {
		product_type = details.data.product.product_type;
	} catch(err) {
		product_type = '';
	}
  
  const [selectedVariant, setSelectedVariant] = useState('');
  const [selectedVariantName, setSelectedVariantName] = useState('');
  const [selectedQuantity, setSelectedQuantity] = useState('');
  const [selectedQuantityCount, setSelectedQuantityCount] = useState('');
  const [quantitiesOfSelectedvariant, setQuantitiesOfSelectedvariant] = useState([]);

  let initialVariant = '';
  let initialVariantName = '';
  try {
    initialVariant = details.data.product.b2bpricevariant[0].attribute_variant_id;
    initialVariantName = details.data.product.b2bpricevariant[0].variant.name;
  } catch(err) {
    initialVariant = '';
    initialVariantName  = '';
  }

  if(selectedVariant == '' && initialVariant != selectedVariant) {
    setSelectedVariant(initialVariant);
    setSelectedVariantName(initialVariantName);
  }

  const addTocart = () => {
    props.addToCart({
      variant_id: selectedVariant,
      variant_name: selectedVariantName,
      quantityId: selectedQuantity,
      product_id: product_id,
      name: product_name,
      quantity: 1,
      type: product_type,
      image: c_image,
      quantity_count: selectedQuantityCount
    });
    openBannerModal(true);
  };
	
		const renderItem1 = ({ item, index }) => 
      <PromotionalProductCard
        wishListItems={WishListItems}
        item={item} 
        itemClick={itemClick}
        navigation={props.navigation}
        addToCart={props.addToCart}
        addRemoveFav={props.addRemoveFav}
      />;
	
	return (
    <View style={styles.screenMain}>
      {isLoading && <Loader />}
      <Header
        navigation={props.navigation}
        isLogoRequired={true}
        isBackButtonRequired={true}
        onBackButtonPress={onBackButtonPress}
        isRightButtonRequired={true}
        onCartButtonPress={onCartButtonPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />

      <ScrollView
        style={styles.screenContainerScrollView}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.imageScroller}>
          <SliderBox
            images={images}
            sliderBoxHeight={200}
            dotColor={"#3fb851"}
            inactiveDotColor={"#96D8AA"}
            ImageComponentStyle={{
              borderRadius: 2,
              width: 200,
              marginTop: 20,
              marginBottom: 40,
            }}
          />
        </View>
        <View style={styles.screenDesignContainer}>
          {/*<BannerImage bannerImage={require('../../assets/images/temp/banner.png')} />*/}
          <View style={styles.screenContainerWithMargin}>
            <View style={styles.categoryItemRow}>
              <FlatList
                data={images}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item) => images.indexOf(item).toString()}
                showsVerticalScrollIndicator={true}
                renderItem={({ item, index }) => (
                  <ImageItem itemImage={{ uri: item }} />
                )}
              />
            </View>
          </View>
          <View style={styles.promotionalContainer}>
            {details.data && 
            <ItemCard 
              item={details.data} 
              selectedVariant={selectedVariant}
              setSelectedVariant={setSelectedVariant}
              selectedVariantName={selectedVariantName}
              setSelectedVariantName={setSelectedVariantName}
              selectedQuantity={selectedQuantity}
              setSelectedQuantity={setSelectedQuantity}
              selectedQuantityCount={selectedQuantityCount}
              setSelectedQuantityCount={setSelectedQuantityCount}
              quantitiesOfSelectedvariant={quantitiesOfSelectedvariant}
              setQuantitiesOfSelectedvariant={setQuantitiesOfSelectedvariant}
            />
            }
          </View>
          <View style={styles.deliveryDetailsContainer}>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.itemNameText}>
                {appTexts.DETAILSSCREEN.Location}
                <Text style={styles.itemNameText1}>{"Jedda, Al,Nameem"}</Text>
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.itemNameText}>
                {appTexts.DETAILSSCREEN.Charge}
                <Text style={styles.itemNameText1}>{"SAR 5"}</Text>
              </Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.itemNameText}>
                {appTexts.DETAILSSCREEN.Time}
                <Text style={styles.itemNameText1}>
                  {"Today25 August, 5pm-7pm"}
                </Text>
              </Text>
            </View>
          </View>
          <DividerLine />
          <View style={styles.descriptionContainer}>
            <View style={styles.descriptionView}>
              <View style={{ flexDirection: "row" }}>
                <Text style={styles.itemNameText2}>
                  {appTexts.DETAILSSCREEN.Description}
                </Text>
              </View>
              <TouchableOpacity
                style={styles.arrowContainer}
                onPress={() => {
                  openHideDescription();
                }}
              >
                <Image
                  source={
                    showDescription
                      ? require("../../assets/images/home/down-arrown.png")
                      : require("../../assets/images/home/up-arrown.png")
                  }
                />
              </TouchableOpacity>
            </View>
            {showDescription && (
              <Text style={styles.itemNameText3}>{description}</Text>
            )}
          </View>
          <DividerLine />
          {speccification_data != "" && (
            <>
              <View style={styles.descriptionContainer}>
                <View style={styles.descriptionView}>
                  <Text style={styles.itemNameText2}>
                    {appTexts.DETAILSSCREEN.Specification}
                  </Text>
                  <TouchableOpacity
                    style={styles.arrowContainer}
                    onPress={() => {
                      openHideSpecification();
                    }}
                  >
                    <Image
                      source={
                        showSpecification
                          ? require("../../assets/images/home/down-arrown.png")
                          : require("../../assets/images/home/up-arrown.png")
                      }
                    />
                  </TouchableOpacity>
                </View>
                {showSpecification && (
                  <Text style={styles.itemNameText3}>
                    {
                      "Spec"
                    }
                  </Text>
                )}
              </View>
              <DividerLine />
            </>
          )}
          <View style={styles.similarProductContainer}>
            <View style={styles.similarProductView}>
              <Text style={styles.itemNameText4}>
                {appTexts.MOREITEM.SimilarProducts}
              </Text>

              <View style={styles.similarProductList}>
                {similarProducts.length === 0 ? (
                  <View style={styles.noDataContainer}>
                    <Text style={styles.noDataText}>
                      {appTexts.STRING.nodata}
                    </Text>
                  </View>
                ) : (
                  <FlatList
                    style={styles.flatListStyle}
                    data={similarProducts}
                    extraData={similarProducts}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    renderItem={renderItem1}
                  />
                )}
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <View style={styles.bottomButtonContainer}>
        <View style={styles.horizontalView}>
          {WishListItems.indexOf(product_id) == -1 &&
            <TouchableOpacity style={styles.shareTextView} onPress={() => props.addRemoveFav(product_id) }>
              <Text style={styles.shareText}>{appTexts.DETAILSSCREEN.Add}</Text>
            </TouchableOpacity>
          }
          {WishListItems.indexOf(product_id) != -1 &&
            <View style={styles.shareTextView}>
              <Text style={[styles.shareText, {color: 'grey'}]}>{appTexts.DETAILSSCREEN.Add}</Text>
            </View>
          }
          <View style={styles.verticalDivider} />
          <TouchableOpacity style={styles.shareTextView}>
            <Text style={styles.shareText}>{appTexts.DETAILSSCREEN.Share}</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={styles.buttonView}
          onPress={() => {
            addTocart();
          }}
        >
          <Text style={styles.buttonText}>{appTexts.DETAILSSCREEN.Cart}</Text>
        </TouchableOpacity>
      </View>
      <BannerModal
        isBannerModalVisible={isBannerModalVisible}
        closeModal={closeBannerModal}
        bannerText={appTexts.BANNER.CartAdded}
        navigate={()=> {
          props.navigation.navigate('CartScreen');
          closeBannerModal();
        }}
      />
    </View>
  );
};

DetailsView.propTypes = {
	onCartButtonPress: PropTypes.func
};

export default DetailsView;
