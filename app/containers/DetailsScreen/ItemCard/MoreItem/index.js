import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Image, Text, TouchableOpacity } from "react-native";
import { styles } from "./styles";

const MoreItem = (props) => {
  const { onItemClick } = props;

  return (
    <TouchableOpacity onPress={() => onItemClick() }>
    <View style={styles.sizeSelectView}>
      <Text style={styles.symbolText}>{"More"}</Text>
      <Image
        source={require("../../../../assets/images/cartScreenIcons/refresh/refresh.png")}
      />
    </View>
    </TouchableOpacity>
  );
};
MoreItem.propTypes = {
  itemImage: PropTypes.number,
  nameLabel: PropTypes.string,
};

export default MoreItem;
