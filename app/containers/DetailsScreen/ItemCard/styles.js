import { StyleSheet } from "react-native";
import globals from "../../../lib/globals";
import { I18nManager } from "react-native";

const styles = StyleSheet.create({
  fullWidthRowContainer:{
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowContainer: {
    width: globals.INTEGER.screenWidthWithMargin,
    justifyContent: 'center',
    alignItems: 'center'
  },
  firstContainer:{
    width: '100%',
    paddingTop:10,
    paddingBottom:10,
    flexDirection:'row'
  },
  secondContainer: {
    width: '100%',
    height: 40,
    flexDirection: 'row'
  },
  labelContainer:{
    width: globals.INTEGER.screenWidthWithMargin,
  },
  labelView:{
    width: globals.INTEGER.screenWidthWithMargin,
    flexDirection:'row'
  },
  sizeSelectContainer: {
    width: globals.INTEGER.screenWidthWithMargin - 100,
    height: 40
  },
  sizeSelectView:{
    marginLeft:20,
    width: 80,
    height: 30,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: globals.COLOR.lightGray,
    borderWidth: 1
  },
  itemNameText:{
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 16
  },
  sizeText:{
    marginRight:20,
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 12
  },
  dropDownArrow: {
    width:12,
    height:10,
  },
  textContainer:{
    marginLeft: 0,
    width: globals.INTEGER.screenWidthWithMargin - 100
  },
  nameLabel:{
    top:0,
    color: globals.COLOR.addressLabelColor,
    fontFamily: globals.FONTS.poppinsRegular,
    fontSize: 11,
    height: 20
  },
  
    
});

export { styles };
