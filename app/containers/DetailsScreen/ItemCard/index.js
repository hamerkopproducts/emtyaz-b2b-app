import React, { useState, useEffect } from "react";
import { View, Image, Text, TouchableOpacity, FlatList } from "react-native";
import { styles } from "./styles";

import DotedContainerItem from "./DotedContainerItem";
import MoreItem from "./MoreItem";
import DividerLine from "../../../components/DividerLine";

import appTexts from "../../../lib/appTexts";
import QuantityModal from "../../../components/QuantityModal";
import PackModal from "../../../components/PackModal";

import BannerModal from "../../../components/BannerModal";
import PackSizeModal from "../../../components/PackSizeModal";

const ItemCard = (props) => {
  const { 
    item,
    selectedVariant,
    setSelectedVariant,
    selectedVariantName,
    setSelectedVariantName,
    selectedQuantity,
    setSelectedQuantity,
    selectedQuantityCount,
    setSelectedQuantityCount,
    quantitiesOfSelectedvariant,
    setQuantitiesOfSelectedvariant
  } = props;

  let name = '';
  try {
    name = item.product.lang[0].name;
  } catch(err) {
    name = '';
  }

  // let initialVariant = '';
  // let initialVariantName = '';
  // try {
  //   initialVariant = item.product.b2bpricevariant[0].attribute_variant_id;
  //   initialVariantName = item.product.b2bpricevariant[0].variant.name;
  // } catch(err) {
  //   initialVariant = '';
  //   initialVariantName  = '';
  // }

  // if(initialVariant != selectedVariant) {
  //   setSelectedVariant(initialVariant);
  //   setSelectedVariantName(initialVariantName);
  // }

  const type = item.product.product_type;
  const slug = item.product.slug;
  const product_id = item.product.id;
  const b2bpricevariant = item.product.b2bpricevariant;
  const b2bprice = item.product.b2bprice;

  useEffect(() => {
    let quantities = [];
    for(let incr=0; incr < b2bprice.length; incr++ ) {
      if(b2bprice[incr].variant_id == selectedVariant) {
        quantities.push(b2bprice[incr]);
      }
    }
    if(type == 'simple') {
      quantities = b2bprice;
    }
    setQuantitiesOfSelectedvariant(quantities);

    let initialQty = '';
    let initialQtyCount = '';
    try {
      initialQty = quantities[0].id;
      initialQtyCount = quantities[0].quantity;
    } catch(err) {
      initialQty = '';
      initialQtyCount = '';
    }
    console.debug('initial->', initialQty)
    setSelectedQuantity(initialQty);
    setSelectedQuantityCount(initialQtyCount);
  },[selectedVariant]);

  const qtySlicedFirstThree = quantitiesOfSelectedvariant.slice(0,3);

  const [isPackModalVisible, setIsPackModalVisible] = useState(false);
  const [isPackSizeModalVisible, setIsPackSizeModalVisible] = useState(false);
  const [isBannerModalVisible, setIsBannerModalVisible] = useState(false);
  const [isQuantityModalVisible, setIsQuantityModalVisible] = useState(false);

  const toggleQuantityModal = () => {
    setIsQuantityModalVisible(false);
  };
  const openQuantityModal = () => {
    setIsQuantityModalVisible(true);
  };

  const togglePackModal = () => {
    setIsPackModalVisible(!isPackModalVisible);
  };
  const openPackModal = () => {
    setIsPackModalVisible(true);
  };

  const togglePackSizeModal = () => {
    setIsPackSizeModalVisible(false)
  };

  const openPackSizeModal = () => {
    setIsPackSizeModalVisible(true)
  };

  const closeBannerModal = () => {
    setIsBannerModalVisible(false);
  };

  return (
    <View style={styles.fullWidthRowContainer}>
      <View style={styles.rowContainer}>
        <View style={styles.firstContainer}>
          <View style={styles.labelContainer}>
            <View style={styles.labelView}>
              <View style={styles.textContainer}>
                <Text style={styles.itemNameText}>
                  {name}
                </Text>
              </View>
              {type == 'complex' &&
                <TouchableOpacity
                  onPress={() => {
                    openQuantityModal();
                  }}
                >
                  <View style={styles.sizeSelectView}>
                    <Text style={styles.sizeText}>{ selectedVariantName }</Text>
                    <Image
                      style={styles.dropDownArrow}
                      source={require("../../../assets/images/cartScreenIcons/downArrow.png")}
                    />
                  </View>
                </TouchableOpacity>
              }
              <QuantityModal
                setSelectedVariant={setSelectedVariant}
                setSelectedVariantName={setSelectedVariantName}
                selectedVariantId={selectedVariant}
                b2bQuantity={b2bpricevariant}
                isQuantityModalVisible={isQuantityModalVisible}
                toggleQuantityModal={toggleQuantityModal}
              />
            </View>
          </View>
        </View>

        <View style={styles.secondContainer}>
          
          <FlatList
            data={qtySlicedFirstThree}
            horizontal
            keyExtractor={(item) => qtySlicedFirstThree.indexOf(item).toString()}
            showsVerticalScrollIndicator={false}
            renderItem={({ item, index }) => (
              <>
              <DotedContainerItem
                color={selectedQuantity==item.id ? 'green' : ''}
                name={item}
                id={item.id}
                onEventClick={(id) => {
                  // setSelectedQuantity(id);
                  // setSelectedQuantityCount(item.quantity);
                  openPackSizeModal();
                }} 
              />
              {(quantitiesOfSelectedvariant.length > 3 && index == 2) &&
                <MoreItem onItemClick={openPackModal} />
              }
              </>
            )}
          />

          <PackModal
            isPackModalVisible={isPackModalVisible} 
            togglePackModal={togglePackModal}
            packSizes={quantitiesOfSelectedvariant}
            selectedQuantity={selectedQuantity}
            setSelectedQuantity={(id, quantity) => {
              setSelectedQuantity(id);
              setSelectedQuantityCount(quantity);
              togglePackModal();
              setTimeout(() => {
                openPackSizeModal();
              }, 400);
            }}
          />

        {isPackSizeModalVisible &&
          <PackSizeModal
            packSizes={quantitiesOfSelectedvariant}
            selectedQuantity={selectedQuantity}
            selectedQuantityCount={selectedQuantityCount}
            setSelectedQuantity={setSelectedQuantity}
            setSelectedQuantityCount={setSelectedQuantityCount}
            isPackSizeModalVisible={isPackSizeModalVisible} 
            togglePackSizeModal={togglePackSizeModal} 
          />
        }
        <BannerModal 
          isBannerModalVisible={isBannerModalVisible} 
          closeModal={closeBannerModal} 
          bannerText={appTexts.BANNER.CartAdded}
          navigate={()=> {
            props.navigation.navigate('CartScreen');
            closeBannerModal();
          }}
        />

        </View>
      </View>
      <DividerLine />
    </View>
  );
};
ItemCard.propTypes = {};

export default ItemCard;
