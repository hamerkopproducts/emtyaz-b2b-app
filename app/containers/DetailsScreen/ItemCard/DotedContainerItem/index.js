import React from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Text } from "react-native";
import { styles } from "./styles";

const DotedContainerItem = (props) => {
  const { name, onEventClick, color, id } = props;

  return (
    <TouchableOpacity onPress={() => onEventClick(id)}>
      <View style={[
        styles.sizeSelectView,
        color && {borderColor: color}
        ]}>
        <Text style={styles.symbolText}>{"X"}</Text>
        <Text style={styles.countText}>{name.quantity}</Text>
      </View>
    </TouchableOpacity>
  );
};
DotedContainerItem.propTypes = {
  itemImage: PropTypes.number,
  nameLabel: PropTypes.string,
};

export default DotedContainerItem;
