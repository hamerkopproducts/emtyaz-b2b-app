import { StyleSheet } from "react-native";
import globals from "../../../lib/globals"

const styles = StyleSheet.create({
  imageContainer:{
    width: 100,
    height: 100,
    marginRight:10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  itemImage: {
    width: 100,
    height: 100
  }
});

export { styles };
