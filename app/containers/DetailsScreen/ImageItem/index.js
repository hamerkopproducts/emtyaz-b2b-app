import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text} from 'react-native';
import { styles } from "./styles";

const ImageItem = (props) => {
  const {
        itemImage
      } = props;

  return (
      <View style={styles.imageContainer}>
        <Image style={styles.itemImage} resizeMode="contain" source={itemImage}/>
      </View>
  );
};
ImageItem.propTypes = {
  
};

export default ImageItem;