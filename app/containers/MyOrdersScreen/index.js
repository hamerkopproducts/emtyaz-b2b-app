import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import MyOrdersView from './MyOrdersView';
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import functions from "../../lib/functions"
import * as OrderActions from "../../actions/OrderActions";
import orderReducer from '../../reducers/orderReducer';


const MyOrdersScreen = (props) => {

  const [isHelpModalVisible, setIsHelpModalVisible] = useState(false);
  const [isRatingModalVisible, setIsRatingModalVisible] = useState(false);
  const [tabIndex, setTabIndex] = useState(0);
  const [isMyOrderFilterModalVisible, setIsMyOrderFilterModalVisible] = useState(false);
  

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.getOrders({});
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  const toggleHelpModal = () => {
    setIsHelpModalVisible(!isHelpModalVisible)
  };
  const toggleRatingModal = () => {
    setIsRatingModalVisible(!isRatingModalVisible)
  };


  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  const onOrderFilterPress = () => {
    setIsMyOrderFilterModalVisible(true)
  };
  const closeFiltermodal = () => {
    setIsMyOrderFilterModalVisible(false)
  };
 
  const onCartButtonPress = () => {
    props.navigation.navigate('CartScreen')
  };
  const showDetails = () => {
    //props.navigation.navigate('QuotationScreen')
    if(tabIndex === 0){
      props.navigation.navigate('OrderStatusDetails')
    }
    else{
      props.navigation.navigate('QuotationScreen')
    }
  };
  const onOrderStatusPress = () => {
  
    props.navigation.navigate('CartScreen')
  };
  const orderClick = () =>{
    
  }
  
  const changeTab = (index) => {
    if (tabIndex !== index){
      setTabIndex(index);
    }
    // //alert(index);
    // if(index === 0){
    //   alert('orderTab')
    // }
    // else if(index === 1){
    //   alert('QuotationTab')
    // }
  };

  return (
    <MyOrdersView 
    isMyOrderFilterModalVisible = {isMyOrderFilterModalVisible}
    closeFiltermodal={closeFiltermodal}
    onOrderFilterPress={onOrderFilterPress}
    changeTab={changeTab} 
    tabIndex={tabIndex}
    onCartButtonPress={onCartButtonPress}
    onOrderStatusPress={onOrderStatusPress}
    // itemClick={itemClick}
    navigation={props.navigation}
    itemClick={showDetails} 
    orderClick={orderClick}
    // listData={props.orderData && props.orderData.length> 0 ? props.orderData:[]}
    // listData={tabIndex === 0 ? props.orderData !== null &&  props.orderData:[]
      listData={tabIndex === 0 ? props.orderData !== null  && props.orderData ? props.orderData : [] :
      tabIndex === 1 ? props.orderData !== null  && props.orderData ? props.orderData : [] : 'null'}  
        
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    orderData: state.orderReducer.orderData
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ getOrders: OrderActions.getOrders}, dispatch)
};

const myOrdersScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(MyOrdersScreen);

myOrdersScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default myOrdersScreenWithRedux;