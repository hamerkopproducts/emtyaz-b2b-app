import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;

const images = {
  binIcon: require("../../assets/images/addressDetails/bin.png"),
  editIcon: require("../../assets/images/profileItem/edit.png"),
  heartIcon: require("../../assets/images/addressDetails/heart-active.png"),


};

const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: globals.COLOR.screenBackground,
  },
  screenContainer: {
    flex: 1,
    marginTop: globals.INTEGER.heightTen,
    flexDirection: "column",
    backgroundColor: globals.COLOR.white,
    // alignItems: 'center',
    // justifyContent:'center'
  },
  headerButtonContianer: {
    flexDirection: "row",
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: "center",
  },
  headerButton: {
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty,
  },
  headerButtonMargin: {
    marginLeft: globals.MARGIN.marginTen,
  },
  profileScreen: {
    //backgroundColor:'red',
    paddingLeft: "5%",
    paddingRight: "5%",
  },
  editArrow: {
    alignSelf: "flex-end",
    paddingTop: "4%",
  },
  profileEdit: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: "1%",
    paddingBottom: "5%",
    alignItems: "center",
    //backgroundColor:'red'
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
  },
  checkboxText:{
    textAlign: 'center', fontWeight: 'bold',
  },

checkBoxView: {
  width: 18,
  height: 18,
  borderWidth: 1,
  borderColor: 'black', 
  alignItems: 'center',
  justifyContent: 'center'},
  profileContent: {
    flexDirection: "row",
  },
  conitionText:{fontSize:13,
    color:globals.COLOR.drakBlack,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
  },
  profileInformation: {
    flexDirection: "column",
    flexWrap: "wrap",
    justifyContent: "center",
    paddingLeft: "6%",
  },
  name: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.lightBlack,
    fontSize: 16,
  },
  address: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakGray,
    fontSize: 12,
  },
  place: {
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
    color: globals.COLOR.drakGray,
    fontSize: 12,
  },
  profileItem: {
    paddingTop: "2%",
  },
  modalMainContent: {
    marginTop: 50,
    
  },
  regionSelectContainer: {
    flexDirection: 'row',
    width: globals.INTEGER.screenWidthWithMargin - 45,
    height: 40
  },
  dropDownArrow: {
    width:12,
    height:10, 
   },
  regionSelectView:{
    marginLeft: 0,
    flex: 1,
    //height: 30,
    flexDirection:'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  regionText:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    color: globals.COLOR.addressLabelColor,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 14
  },
  modalmainView: {
    backgroundColor: "white",
    padding: "2%",
    borderColor: "rgba(0, 0, 0, 0.1)",
    
  },
  closeButton:{
    alignSelf:'flex-end',
    paddingRight:'3%',
    paddingLeft:'3%',
    // paddingTop:'2%',
    // paddingBottom:'1%',
    color:'#707070',
    fontSize:16
  },
  restText:{
   alignItems:'flex-start' ,
   fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsMedium,
      fontSize:16,
   paddingRight:'3%',
    paddingLeft:'3%',
    paddingBottom:'1.3%'
  },
  resetPassword:{
    paddingRight:'3%',
    paddingLeft:'3%',
  },
  passwordText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsRegular,
    fontSize:13,
    color:'#8D8D8D',
  },
  buttonWrapper:{
    flexDirection:'row',
    justifyContent:'space-between',
     paddingLeft:'3%',
     paddingRight:'3%',
     paddingBottom:'5%',
     paddingTop:'1%'
  },
  cPassword:{
flexDirection:'row',
borderBottomColor: "#EEEEEE",
borderBottomWidth: 1,
justifyContent: 'center',
    alignItems: 'center',

  },
  numberEnter:{
    fontSize: 14,
    color: "#242424",
    marginTop: Platform.OS === 'ios' ? 20: 0,
    textAlign: I18nManager.isRTL ? "right" : "left",
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.poppinsRegular,
      flex:1
   
  },imageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: 'stretch',
    alignItems: 'center',
  },
  pwdOne:{
  },
  shadowContainerStyle: {   //<--- Style with elevation
    borderWidth: 0,
    shadowOffset: {
      width: 1,
      height: 1,
    },
    shadowOpacity: 0.2,
    elevation: Platform.OS === 'ios' ? 1: 3,
    backgroundColor:  '#ffffff',
    paddingLeft:'5%',
    paddingRight:'5%'
  },
  delivreyAddress:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingTop:'4%',
    alignItems:'center'
  },
  billingAddress:{
    flexDirection:'row',
    justifyContent:'space-between',
    paddingTop:'5%',
    alignItems:'center'
  },
  deliveryText:{
    fontFamily: I18nManager.isRTL
    ? globals.FONTS.notokufiArabic
    : globals.FONTS.poppinsMedium,
    fontSize:I18nManager.isRTL?14:14,
  },addnewButton: {
    backgroundColor: globals.COLOR.themeGreen,
    width: 80,
    height: 32,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    color: globals.COLOR.white,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 11,
  },
  delivreyaddressDetail:{
    paddingTop:'5%',
    paddingBottom: '5%'

  },nameandIcon:{
    flexDirection:'row',
    justifyContent:'space-between',alignItems:'center'
  },
  billingnameandIcon:{
    flexDirection:'row',
    justifyContent:'space-between',alignItems:'center',paddingTop:'3%'
  },iconList:{
    flexDirection:'row',
    justifyContent:'space-between'
  },
  nameText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 13,
    color:'#383838',
  }, placeText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    fontSize: 12,
    color:'#686868'
  },
  userDetails:{
paddingTop:'1%',
borderBottomColor:'#CFCFCF',
borderBottomWidth:1,
paddingBottom:'6%'
  },
  billinguserDetails:{
    paddingTop:'3%',
    paddingBottom:'6%',
  },
  nameandiconSecond:{
    flexDirection:'row',
    paddingTop:'6%',
    justifyContent:'space-between',alignItems:'center'
  },
  arrowicon:{
    width:15,
    height:15
  },cancelButton: {
    backgroundColor: '#FBFBFB',
    borderColor:'#CFCFCF',
borderWidth:1,
    width: 134,
    height: 45,
    justifyContent: "center",
    alignItems: "center",},submitButton: {
      backgroundColor: globals.COLOR.themeGreen,
      width: 134,
      height: 45,
      justifyContent: "center",
      alignItems: "center",},
      saveText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
        fontSize: 13,
        color:'white'
      },cancelText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
        fontSize: 13,
        color:'#242424'
      },
      resendWrapper: {
        flexDirection: "row",
        justifyContent: "center",
        paddingLeft: "3%",
        paddingRight: "3%",
        paddingTop:'4%',
        paddingBottom:'4%'
        //  paddingTop: hp("3%"),
        //  paddingBottom: hp("2%"),
        //padding:'10%'
      },
      flagsectionWrapper: {
        //flexBasis: "30%",
        width:'30%',
        //backgroundColor: "pink",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        borderBottomColor: "#EEEEEE",
        borderBottomWidth: 1,
      },
      acceptSection:{
        
        flexDirection:'row',
        alignItems:'center',
        paddingLeft:'5%',
        paddingTop:hp('1.3%'),
        marginBottom: 10
      
      },
      numbersectionWrapper: {
        paddingTop: '3%',
        width:'75%',
        marginLeft: "2%",
      },
      numberEnters: {

    width: "100%",
    fontSize: 14,
    lineHeight: 24,
    alignItems: "center",
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.poppinsRegular,
    color: '#242424',
    textAlign: I18nManager.isRTL ? "right" : "left",
    borderBottomColor: "#EEEEEE",
    borderBottomWidth: 1,
    color: "#242424",
      },
      setasText:{
        fontFamily: I18nManager.isRTL
        ? globals.FONTS.notokufiArabic
        : globals.FONTS.poppinsRegular,
        fontSize:12,
        color:'#242424',
        paddingLeft:'2%',
        paddingRight:'2%'
      },
      textContent:{
        flexDirection:'column',
        width:"70%",
      }

});

export { images, styles };
