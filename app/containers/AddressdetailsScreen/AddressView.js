import React from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  ScrollView,
  CheckBox,
  I18nManager,
  FlatList,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import globals from "../../lib/globals";
import { styles, images } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import Modal from "react-native-modal";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Icon from "react-native-vector-icons/FontAwesome";
import Loader from "../../components/Loader";
import DeliveryAddress from "../../components/DeliveryAddress";
import BillingAddress from "../../components/BillingAddress";
import { dropDownRegionMapper } from "../../lib/HelpperMethods";
import RegionModal from "../../components/RegionModal";
const AddressView = (props) => {
  const {
    toggleModal,
    clearFileds,
    isModalVisible,
    onBackButtonPress,
    defaultAddressCheckbox,
    checkBoxClick,
    setAsdefailtAddress,

    name,
    appartment,
    street,
    pobox,
    landmark,
    phone,
    setName,
    setAppartment,
    setStreet,
    setPobox,
    setLandmark,
    setPhone,
    setRegion,
    validatename,
    validateAddressType,
    validateNewOrUpdate,
    spredDataDeliveryAdrs,
    editAddress,
    deleteAddress,
    setAsDefaultAddress,
    spredDataBillingAdrs,
    validateappartment,
    validatestreet,
    validatepobox,
    validatelandmark,
    validatephone,
    saveAddressDetails,
    regionList,
    billingAddress,
    deliveryAddress,
    isLoading,
    addressLoader,
    openRegionModal,
    isRegionModalVisible,
    setSelectedRegion,
    setSelectedRegionName,
    selectedRegion,
    selectedRegionName,
    selectedRegionId,
    toggleRegionModal,
  } = props;

  return (
    // <ScrollView>

    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        headerTitle={appTexts.ADDRESSDETAILS.Heading}
        isBackButtonRequired={true}
        onBackButtonPress={onBackButtonPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <View style={styles.screenContainer}>
        {isLoading && <Loader />}
        <ScrollView keyboardShouldPersistTaps='handled'>
          {/* {isLoading && <Loader />} */}
          <View
            style={{
              paddingLeft: "5%",
              paddingRight: "5%",
              paddingTop: "5%",
              paddingBottom: "5%",
            }}
          >
            <View style={[styles.shadowContainerStyle, { width: "100%" }]}>
              <View style={styles.delivreyAddress}>
                <Text style={styles.deliveryText}>
                  {appTexts.ADDRESSDETAILS.DeliveryAddress}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    clearFileds();
                    validateAddressType(false);
                    validateNewOrUpdate(true);
                    toggleModal();
                  }}
                >
                  <View style={styles.addnewButton}>
                    <Text style={styles.buttonText}>
                      {appTexts.ADDRESSDETAILS.AddNew}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={styles.delivreyaddressDetail}>
                <FlatList
                  data={deliveryAddress}
                  keyExtractor={(item) => deliveryAddress.indexOf(item).toString()}
                  showsVerticalScrollIndicator={true}
                  renderItem={({ item, index, separators }) => (
                    <DeliveryAddress
                      item={item}
                      toggleModal={toggleModal}
                      validateAddressType={validateAddressType}
                      validateNewOrUpdate={validateNewOrUpdate}
                      spredDataDeliveryAdrs={spredDataDeliveryAdrs}
                      editAddress={editAddress}
                      deleteAddress={deleteAddress}
                      setAsDefaultAddress={setAsDefaultAddress}
                      clearFileds={clearFileds}
                    />
                  )}
                />

                <View style={styles.billingAddress}>
                  <Text style={styles.deliveryText}>
                    {appTexts.ADDRESSDETAILS.BillingAddress}
                  </Text>
                  <TouchableOpacity
                    onPress={() => {
                      clearFileds();
                      validateAddressType(true);
                      validateNewOrUpdate(true);
                      toggleModal();
                    }}
                  >
                    <View style={styles.addnewButton}>
                      <Text style={styles.buttonText}>
                        {appTexts.ADDRESSDETAILS.AddNew}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <FlatList
                  data={billingAddress}
                  keyExtractor={(item) => billingAddress.indexOf(item).toString()}
                  showsVerticalScrollIndicator={true}
                  renderItem={({ item, index, separators }) => (
                    <BillingAddress
                      item={item}
                      toggleModal={toggleModal}
                      validateAddressType={validateAddressType}
                      validateNewOrUpdate={validateNewOrUpdate}
                      spredDataBillingAdrs={spredDataBillingAdrs}
                      editAddress={editAddress}
                      deleteAddress={deleteAddress}
                      isLoading={isLoading}
                      clearFileds={clearFileds}
                      setAsDefaultAddress={setAsDefaultAddress}
                    />
                  )}
                />
              </View>
            </View>
          </View>
        </ScrollView>
        {/* modalpopup starting */}
        <Modal
          isVisible={isModalVisible}
          style={styles.modalMainContent}
          animationIn= 'fadeIn'
          animationOut= 'fadeOut'
          
        >
          <View style={styles.modalmainView}>
          <KeyboardAwareScrollView
            scrollEnabled={false}
            contentContainerStyle={{ justifyContent: "flex-end" }}
          >
            {/* <DismissKeyboard> */}
            <View style={{ justifyContent: "center" }}>
              {addressLoader && <Loader />}
              <TouchableOpacity
                style={styles.buttonwrapper}
                onPress={() => {
                  clearFileds();
                  toggleModal();
                }}
              >
                <Text style={styles.closeButton}>X</Text>
              </TouchableOpacity>
              <Text style={styles.restText}>
                {appTexts.ADDRESSDETAILS.AddNewAddress}
              </Text>
              <View style={styles.resetPassword}>
                <View style={styles.pwdOne}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Name}
                  </Text>
                  <View style={styles.cPassword}>
                    <TextInput
                      style={styles.numberEnter}
                      underlineColorAndroid="transparent"
                      value={name}
                      onChangeText={(val) => {
                        validatename(false);
                        setName(val);
                      }}
                    />
                  </View>
                </View>
                <View style={{ paddingTop: "3%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Apartment}
                  </Text>
                  <View style={styles.cPassword}>
                    <TextInput
                      style={styles.numberEnter}
                      underlineColorAndroid="transparent"
                      value={appartment}
                      onChangeText={(val) => {
                        validateappartment(false);
                        setAppartment(val);
                      }}
                    />
                  </View>
                </View>
                <View style={{ paddingTop: "3%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Street}
                  </Text>
                  <View style={styles.cPassword}>
                    <TextInput
                      style={styles.numberEnter}
                      underlineColorAndroid="transparent"
                      value={street}
                      onChangeText={(val) => {
                        validatestreet(false);
                        setStreet(val);
                      }}
                    />
                  </View>
                </View>
                <View style={{ paddingTop: "3%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.POBox}
                  </Text>
                  <View style={styles.cPassword}>
                    <TextInput
                      style={styles.numberEnter}
                      underlineColorAndroid="transparent"
                      value={pobox}
                      onChangeText={(val) => {
                        validatepobox(false);
                        setPobox(val);
                      }}
                    />
                  </View>
                </View>
                <View style={{ paddingTop: "3%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Landmark}
                  </Text>
                  <View style={styles.cPassword}>
                    <TextInput
                      style={styles.numberEnter}
                      underlineColorAndroid="transparent"
                      value={landmark}
                      onChangeText={(val) => {
                        validatelandmark(false);
                        setLandmark(val);
                      }}
                    />
                  </View>
                </View>
                <View style={{ paddingTop: "3%" }}>
                  <Text style={styles.passwordText}>
                    {appTexts.ADDRESSDETAILS.Region}
                  </Text>
                  <View style={styles.cPassword}>
                    <TouchableOpacity
                      onPress={() => {
                        openRegionModal();
                      }}
                    >
                      <View style={styles.regionSelectContainer}>
                        <View style={styles.regionSelectView}>
                          <Text
                            style={styles.regionText}>
                              
                            {selectedRegionName}
                          </Text>
                          <Image
                            style={styles.dropDownArrow}
                            source={require("../../assets/images/cartScreenIcons/downArrow.png")}
                          />
                        </View>
                      </View>
                    </TouchableOpacity>
                    <RegionModal
                      setSelectedRegion={setSelectedRegion}
                      setSelectedRegionName={setSelectedRegionName}
                      selectedRegionId={selectedRegion}
                      regionList={regionList}
                      isRegionModalVisible={isRegionModalVisible}
                      toggleRegionModal={toggleRegionModal}
                    />
                  </View>
                </View>
                <View style={styles.resendWrapper}>
                  <View style={styles.flagsectionWrapper}>
                    <Image
                      source={require("../../assets/images/Icons/flag.png")}
                      style={{ height: 20, width: 20 }}
                    />
                    <TextInput
                      editable={false}
                      style={{
                        fontSize: 14,
                        alignItems: "center",
                        color: "#7D7B7D",
                      }}
                      placeholder={"+966"}
                      placeholderTextColor="#7D7B7D"
                    />
                  </View>

                  <View style={styles.numbersectionWrapper}>
                    <TextInput
                      keyboardType="number-pad"
                      style={styles.numberEnters}
                      placeholderTextColor="#242424"
                      value={phone}
                      onChangeText={(val) => {
                        validatephone(false);
                        setPhone(val);
                      }}
                    />
                  </View>
                </View>
              </View>
              <View style={styles.acceptSection}>
                <View style={{height: 30,width: 30,alignItems: 'center',justifyContent: 'center'}}>
                <TouchableOpacity
                  onPress={() =>
                    defaultAddressCheckbox == false
                      ? checkBoxClick(true)
                      : checkBoxClick(false)
                  }
                >
                  <View style={styles.checkBoxView}>
                    <Text style={styles.checkboxText}>
                      {defaultAddressCheckbox == true ? "✓" : ""}
                    </Text>
                  </View>
                  {/* <Text style={styles.termsText}> </Text> */}
                </TouchableOpacity>
                </View>
                <Text style={styles.conitionText}> Set as default address</Text>
              </View>
              <View style={styles.buttonWrapper}>
              <TouchableOpacity
                onPress={() => {
                  clearFileds();
                  toggleModal();
                }}
              >
                <View style={styles.cancelButton}>
                  <Text style={styles.cancelText}>
                    {appTexts.ADDRESSDETAILS.Cancel}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => saveAddressDetails()}>
                <View style={styles.submitButton}>
                  <Text style={styles.saveText}>
                    {appTexts.ADDRESSDETAILS.Save}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            </View>
           
          </KeyboardAwareScrollView>
          </View>
        </Modal>
        {/* modalpopup ending */}
      </View>
    </View>

    // </ScrollView>
  );
};

AddressView.propTypes = {
  toggleModal: PropTypes.func,
  clearFileds: PropTypes.func,
  isModalVisible: PropTypes.bool,

  setName: PropTypes.func,
  setAppartment: PropTypes.func,
  setStreet: PropTypes.func,
  setPobox: PropTypes.func,
  setLandmark: PropTypes.func,
  setPhone: PropTypes.func,

  validatename: PropTypes.func,
  validateappartment: PropTypes.func,
  validatestreet: PropTypes.func,
  validatepobox: PropTypes.func,
  validatelandmark: PropTypes.func,
  validatephone: PropTypes.func,
  validateAddressType: PropTypes.func,
  validateNewOrUpdate: PropTypes.func,
  spredDataDeliveryAdrs: PropTypes.func,
  spredDataBillingAdrs: PropTypes.func,
  editAddress: PropTypes.func,
  deleteAddress: PropTypes.func,
  setAsDefaultAddress: PropTypes.func,
  defaultAddressCheckbox: PropTypes.bool,
  checkBoxClick: PropTypes.func,
  openRegionModal: PropTypes.func,
  toggleRegionModal: PropTypes.func,

  saveAddressDetails: PropTypes.func,
  isLoading: PropTypes.bool,
  addressLoader: PropTypes.bool,
};

export default AddressView;
