import React, { useEffect, useState } from "react";
import { BackHandler, Keyboard } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import AddressView from "./AddressView";
import { bindActionCreators } from "redux";
import * as AddressListActions from "../../actions/AddressListActions";
import globals from "../../lib/globals";
import functions from "../../lib/functions";
import appTexts from "../../lib/appTexts";
import _ from "lodash";

const AddressdetailsScreen = (props) => {
  //Initialising states
  let initialRegion = '';
  let initialRegionName = 'Select Your City';
  const [isModalVisible, setIsModalVisible] = useState(false);

  const [name, setName] = useState('');
  const [appartment, setAppartment] = useState('');
  const [street, setStreet] = useState('');
  const [landmark, setLandmark] = useState('');
  const [pobox, setPobox] = useState('');
  const [phone, setPhone] = useState('');
  const [addressid, setAddressid] = useState('');
  const [regionID, setRegionid] = useState('');


  const [nameerrorflag, setnameerrorflag] = useState(false);
  const [appartmenterrorflag, setappartmenterrorflag] = useState(false);
  const [streeterrorflag, setstreeterrorflag] = useState(false);
  const [landmarkerrorflag, setlandmarkerrorflag] = useState(false);
  const [poboxerrorflag, setpoboxerrorflag] = useState(false);
  const [phoneerrorflag, setphoneerrorflag] = useState(false);
  const [regionerrorflag, setRegionerrorflag] = useState(false);
  const [isRegionModalVisible, setIsRegionModalVisible] = useState(false);

  const [addressType, setaddressTypeflag] = useState(false);
  const [AddresNewOrOld, setaddressNewOrUpdateflag] = useState(false);
  const [defaultAddressCheckbox, setdefaultAddressCheckboxflag] = useState(
    false
  );

  const [selectedRegion, setSelectedRegion] = useState(initialRegion);
  const [selectedRegionName, setSelectedRegionName] = useState(initialRegionName);
  

  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        props.addressList(props.userData.data.access_token);
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  };

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    console.log("EDIT ADDRESS",props.editaddressDetailsList)
    //setRegionid(selectedRegion);
    if(props.deleteAddressDetails.success)
    {
      props.addressList(props.userData.data.access_token);
      functions.displayToast(
        "success",
        "top",
        "Addresss deleted successfully"
      );
    }
    if(props.defaultAddessDetails.success)
    {
      props.addressList(props.userData.data.access_token);
      // functions.displayToast(
      //   "success",
      //   "top",
      //   "Default Address set "
      // );
    }
    if (props.addressSaved.success) {
      props.addressList(props.userData.data.access_token);
      functions.displayToast("success", "top", "Address saved ");
    } else if (props.addressSaved.error) {
      props.addressList(props.userData.data.access_token);
      functions.displayToast(
        "error",
        "top",
        "Something went wrong.. Please try again"
      );
      clearFileds();
    }
  };

  const logoutButtonPress = () => {
    props.doLogout();
  };

  const toggleModal = () => {
    setIsModalVisible(!isModalVisible);
  };
  const clearFileds = () => {
    setName(''), setLandmark('');
    setAppartment('');
    setStreet('');
    setPobox('');
    setPhone('');
    setSelectedRegionName(initialRegionName)
    checkBoxClick()
  };

  const onBackButtonPress = () => {
    props.navigation.goBack();
  };
  const editAdClicked = () => {
    props.navigation.navigate("DeliverybillingaddressScreen");
  };

  const validatename = (flag) => {
    setnameerrorflag(flag);
  };
  const validateappartment = (flag) => {
    setappartmenterrorflag(flag);
  };
  const validatestreet = (flag) => {
    setstreeterrorflag(flag);
  };
  const validatepobox = (flag) => {
    setpoboxerrorflag(flag);
  };
  const validatelandmark = (flag) => {
    setlandmarkerrorflag(flag);
  };
  const validatephone = (flag) => {
    setphoneerrorflag(flag);
  };
  const validateAddressType = (flag) => {
    setaddressTypeflag(flag);
  };
  const validateNewOrUpdate = (flag) => {
    setaddressNewOrUpdateflag(flag);
  };
  const validateregion = (flag) => {
    setRegionerrorflag(flag);
  };
  const checkBoxClick = (flag) => {
    setdefaultAddressCheckboxflag(flag);
  };
  const openRegionModal = () => {
    setIsRegionModalVisible(true);
  };
  const toggleRegionModal = () => {
    setIsRegionModalVisible(false)
  };
  const fetcheditAddress = (id) => {
    props.editAddresData(id, props.userData.data.access_token);
  }
  const editAddress = (id) => {
    console.log("ID", id)
    props.editAddresData(id, props.userData.data.access_token);
   // console.log("ID from ADDRESS FETCH",props.editaddressDetailsList[0].id)
    
      spredDataDeliveryAdrs();
      spredDataBillingAdrs();
    
      // props.editAddresData(id, props.userData.data.access_token);
      // console.log("FETCH",props.editaddressDetailsList[0].id)
    
    setAddressid(id);
    setRegionid(selectedRegion);
    
  };
  const deleteAddress = (id) => {
    //console.log("******************************************",id ,props.userData.data.access_token)
      props.deleteAddressData(id, props.userData.data.access_token);
  };
  const setAsDefaultAddress = (id) => {
    let apiParam = {
      id: id
    }
      props.setAsDefaultAdrs(apiParam, props.userData.data.access_token);
  };
  const onChangeText = () => {
  
  };
  
  const spredDataDeliveryAdrs = () => {
    
    setName(_.get(props, "editaddressDetailsList[0].name", "")),
      setLandmark(_.get(props, "editaddressDetailsList[0].landmark", ""));
    setAppartment(_.get(props, "editaddressDetailsList[0].apartment_name", ""));
    setStreet(_.get(props, "editaddressDetailsList[0].street_name", ""));
    setPobox(_.get(props, "editaddressDetailsList[0].postal_code", ""));
    setPhone(_.get(props, "editaddressDetailsList[0].phone", ""));
    setSelectedRegionName(_.get(props, "editaddressDetailsList[0].region.lang[0].name", ""));
    if ((_.get(props, "editaddressDetailsList[0].is_default", "") === "Y"))
     {checkBoxClick(true)}
     else checkBoxClick(false)
    
  };
  const spredDataBillingAdrs = () => {
    setName(_.get(props, "editaddressDetailsList[0].name", "")),
      setLandmark(_.get(props, "editaddressDetailsList[0].landmark", ""));
    setAppartment(_.get(props, "editaddressDetailsList[0].apartment_name", ""));
    setStreet(_.get(props, "editaddressDetailsList[0].street_name", ""));
    setPobox(_.get(props, "editaddressDetailsList[0].postal_code", ""));
    setPhone(_.get(props, "editaddressDetailsList[0].phone", ""));
    setSelectedRegionName(_.get(props, "editaddressDetailsList[0].region.lang[0].name", ""));
    if ((_.get(props, "editaddressDetailsList[0].is_default", "") === "Y"))
     {checkBoxClick(true)}
     else checkBoxClick(false)
  };

  const saveAddressDetails = () => {
    if (name.trim() === ''  || name === null) {
      validatename();
      //functions.displayAlert(appTexts.ALERT_MESSAGES.error,'name can not be empty');
      functions.displayToast('error', 'top',appTexts.ALERT_MESSAGES.error,'Name can not be empty');
    } else if (landmark.trim() === "") {
      validatelandmark();
      functions.displayToast('error', 'top',appTexts.ALERT_MESSAGES.error, 'Landmark can not be empty');
    } else if (appartment.trim() === '' || appartment === null) {
      validateappartment();
      functions.displayToast(
        'error', 'top','error',
        'Appartment name can not be empty'
      );
    } else if (street.trim() === ''  || street === null) {
      validatestreet();
      functions.displayAlert(appTexts.ALERT_MESSAGES.error,'Street name can not be empty');
      functions.displayToast('error', 'top',appTexts.ALERT_MESSAGES.error,'Street name can not be empty');
    } else if (pobox.trim() === ''  || pobox === null) {
      validatepobox();
      functions.displayToast('error', 'top',appTexts.ALERT_MESSAGES.error,'Pobox can not be empty');
    } else if (phone.trim() === '' || phone === null) {
      validatephone();
      functions.displayToast('error', 'top',appTexts.ALERT_MESSAGES.error,'Phone Numer can not be empty');
    } 
    else if (selectedRegion === ''  || selectedRegion === null) {
      validateregion();
      functions.displayToast('error', 'top',appTexts.ALERT_MESSAGES.error, 'City can not be empty');
    }
    else {
      Keyboard.dismiss();
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
          console.log("ADDRESS TYPE", addressType);
          let apiParam = {
            name: name.trim(),
            apartment: appartment.trim(),
            street: street.trim(),
            landmark: landmark.trim(),
            phone: phone.trim(),
            po_box: pobox.trim(),
            region: selectedRegion,
            type: addressType ? "billing" : "delivery",
            is_default: defaultAddressCheckbox ? "Y" : "N",
            id: !AddresNewOrOld ? addressid : "",
          };
          if (AddresNewOrOld === true) {
            props.doSaveAddressDetails(
              apiParam,
              props.userData.data.access_token
            );

            toggleModal();
          } else {
            props.saveEditedAddressDetails(
              apiParam,
              props.userData.data.access_token
            );
            toggleModal();
          }
        } else {
          functions.displayToast(
            "error",
            "top",
            appTexts.VALIDATION_MESSAGE.Type,
            appTexts.VALIDATION_MESSAGE.Mobile_error_msg
          );
        }
      });
    }
  };

  return (
    <AddressView
      isModalVisible={isModalVisible}
      toggleModal={toggleModal}
      clearFileds={clearFileds}
      onBackButtonPress={onBackButtonPress}
      name={name}
      appartment={appartment}
      street={street}
      regionID={regionID}
      pobox={pobox}
      landmark={landmark}
      phone={phone}
      setName={setName}
      setAppartment={setAppartment}
      setStreet={setStreet}
      setPobox={setPobox}
      setPhone={setPhone}
      setLandmark={setLandmark}
      setRegionid={setRegionid}
      validatename={validatename}
      validateappartment={validateappartment}
      validatestreet={validatestreet}
      validatepobox={validatepobox}
      validatelandmark={validatelandmark}
      validatephone={validatephone}
      validateAddressType={validateAddressType}
      validateNewOrUpdate={validateNewOrUpdate}
      spredDataDeliveryAdrs={spredDataDeliveryAdrs}
      editAddress={editAddress}
      fetcheditAddress={fetcheditAddress}
      deleteAddress={deleteAddress}
      setAsDefaultAddress={setAsDefaultAddress}
      onChangeText={onChangeText}
      spredDataBillingAdrs={spredDataBillingAdrs}
      saveAddressDetails={saveAddressDetails}
      regionList={props.regionList}
      billingAddress={props.addressDetailsList.billingAddress}
      deliveryAddress={props.addressDetailsList.deliveryAddress}
      defaultAddressCheckbox={defaultAddressCheckbox}
      isRegionModalVisible={isRegionModalVisible}
      checkBoxClick={checkBoxClick}
      isLoading={props.isLoading}
      addressLoader={props.addressLoader}
      openRegionModal={openRegionModal}
      setSelectedRegion={setSelectedRegion}
      setSelectedRegionName={setSelectedRegionName}
      selectedRegion={selectedRegion}
      selectedRegionName={selectedRegionName}
      toggleRegionModal={toggleRegionModal}

    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    addressLoader: _.get(state, "addressDetailsReducer.addressspreadLoader", ""),
    isLoading: _.get(state, "addressDetailsReducer.isLoading", ""),
    userData: _.get(state, "loginReducer.userData", ""),
    regionList: _.get(state, "editUserDetailsReducer.regionList", []),
    addressDetailsList: _.get(
      state,
      "addressDetailsReducer.addressDetails",
      []
    ),
    deleteAddressDetails: _.get(
      state,
      "addressDetailsReducer.deleteAddressDetails",
      []
    ),
    addressDetailsListSaved: _.get(
      state,
      "addressDetailsReducer.addressDetailsSaved",
      []
    ),
    editaddressDetailsList: _.get(
      state,
      "addressDetailsReducer.editAddressDetails",
      []
    ),
    defaultAddessDetails: _.get(
      state,
      "addressDetailsReducer.defaultAddessDetails",
      []
    ),
    addressSaved: _.get(state, "addressDetailsReducer.addressDetailsSaved", []),
    error: _.get(state, "addressDetailsReducer.error", []),
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      addressList: AddressListActions.doAddressDetailsFetch,
      doSaveAddressDetails: AddressListActions.doAddressDetailSave,
      saveEditedAddressDetails: AddressListActions.doEditedAddressDetailSave,
      editAddresData: AddressListActions.doEditAddressDetailsFetch,
      deleteAddressData: AddressListActions.doDeleteAddress,
      setAsDefaultAdrs: AddressListActions.setAddressAsDefault,
    },
    dispatch
  );
};

const addressScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(AddressdetailsScreen);

addressScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default addressScreenWithRedux;
