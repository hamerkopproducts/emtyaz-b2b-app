import React,{useState} from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TouchableOpacity,
  CheckBox,Image,
  ScrollView,TextInput,I18nManager
} from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import CustomGreenLargeButton from "../../components/CustomGreenLargeButton";
import Customradiobutton from "../../components/CustomRadioButton";
import Modal from 'react-native-modal';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DropDownPicker from "react-native-dropdown-picker";

const AddressView = (props) => {
  const { logoutButtonPress,toggleModal,isModalVisible,onBackButtonPress } = props;
   return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        isBackButtonRequired={true}
        onBackButtonPress={onBackButtonPress}
        // isRightButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />

      <View style={styles.contentWrapper}>
        <Text style={styles.deliveryText}>
          {appTexts.BILLINGDELIVERY.Delivery}
          {/* {appTexts.BILLINGDELIVERY.Billing} */}
        </Text>
        <View style={styles.billingnameandIcon}>
          <Text style={styles.nameText}>John Mathew</Text>
          {/* <CheckBox /> */}
          <Customradiobutton  />
        </View>

        <View style={styles.userDetails}>
          {/* <Text style={styles.userText}> {appTexts.USER_DATA.name}</Text>
          <Text style={styles.userText}> {appTexts.USER_DATA.po}</Text>
          <Text style={styles.userText}> {appTexts.USER_DATA.place}</Text>
          <Text style={styles.userText}> {appTexts.USER_DATA.number}</Text>
          <Text style={styles.userText}> {appTexts.USER_DATA.name}</Text> */}
          <View style={styles.textContent}>
            <View style={{ paddingLeft: "0%" }}>
              <View style={styles.textContent}>
                <Text style={styles.userText}>Al Naemia Street</Text>
              </View>
              <View style={styles.textContent}>
                <Text style={styles.userText}>PO Box: 2211</Text>
              </View>
              <View style={styles.textContent}>
                <Text style={styles.userText}>Al Nameem,Jaddah</Text>
              </View>
              <View style={styles.textContent}>
                <Text style={styles.userText}>+966 00000</Text>
              </View>
              <View style={styles.textContent}>
                <Text style={styles.userText}>Al Nameem,Jaddah</Text>
              </View>
              <View style={styles.textContent}>
                <Text style={styles.userText}>+966 00000</Text>
              </View>
            </View>
          </View>
        </View>
        
        <View style={styles.auserDetails}>
        <View style={styles.billingnameandIcon}>
          <Text style={styles.nameText}>John Mathew</Text>
          {/* <CheckBox /> */}
          <Customradiobutton/>
        </View>
          <View style={{ paddingLeft: "0%" }}>
            <View style={styles.textContent}>
              <Text style={styles.userText}>Al Naemia Street</Text>
            </View>
            <View style={styles.textContent}>
              <Text style={styles.userText}>PO Box: 2211</Text>
            </View>
            <View style={styles.textContent}>
              <Text style={styles.userText}>Al Nameem,Jaddah</Text>
            </View>
            <View style={styles.textContent}>
              <Text style={styles.userText}>+966 00000</Text>
            </View>
            <View style={styles.textContent}>
              <Text style={styles.userText}>Al Nameem,Jaddah</Text>
            </View>
            <View style={styles.textContent}>
              <Text style={styles.userText}>+966 00000</Text>
            </View>
          </View>
        </View>
        <Modal isVisible={isModalVisible} style={styles.modalMainContent}>
        <KeyboardAwareScrollView scrollEnabled={false}>
          <View style={styles.modalmainView}>
            <TouchableOpacity
              style={styles.buttonwrapper}
              onPress={() => {
                toggleModal();
              }}
            >
              <Text style={styles.closeButton}>X</Text>
            </TouchableOpacity>
            <Text style={styles.restText}>
              {appTexts.ADDRESSDETAILS.AddNewAddress}
            </Text>
            <View style={styles.resetPassword}>
              <View style={styles.pwdOne}>
                <Text style={styles.passwordText}>
                  {appTexts.ADDRESSDETAILS.Name}
                </Text>
                <View style={styles.cPassword}>
                  <TextInput
                    style={styles.numberEnter}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>
              <View style={{ paddingTop: "2%" }}>
                <Text style={styles.passwordText}>
                  {appTexts.ADDRESSDETAILS.Apartment}
                </Text>
                <View style={styles.cPassword}>
                  <TextInput
                    style={styles.numberEnter}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>
              <View style={{ paddingTop: "2%" }}>
                <Text style={styles.passwordText}>
                  {appTexts.ADDRESSDETAILS.Street}
                </Text>
                <View style={styles.cPassword}>
                  <TextInput
                    style={styles.numberEnter}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>
              <View style={{ paddingTop: "2%" }}>
                <Text style={styles.passwordText}>
                  {appTexts.ADDRESSDETAILS.POBox}
                </Text>
                <View style={styles.cPassword}>
                  <TextInput
                    style={styles.numberEnter}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>
              <View style={{ paddingTop: "2%" }}>
                <Text style={styles.passwordText}>
                  {appTexts.ADDRESSDETAILS.Landmark}
                </Text>
                <View style={styles.cPassword}>
                  <TextInput
                    style={styles.numberEnter}
                    underlineColorAndroid="transparent"
                  />
                </View>
              </View>
              <View style={{ paddingTop: "2%" }}>
                <Text style={styles.passwordText}>
                  {appTexts.ADDRESSDETAILS.Region}
                </Text>
                <View style={styles.cdPassword}>
                  <DropDownPicker
                    items={[
                      { label: "Thrissur", value: "Thrissur" },
                      { label: "Palakkad", value: "Palakkad" },
                    ]}
                    //placeholder="Select your city"
                    placeholder={[]}
                    labelStyle={{
                      fontSize: 13,
                      fontFamily: I18nManager.isRTL
                        ? globals.FONTS.notokufiArabic
                        : globals.FONTS.poppinsRegular,
                      textAlign: "left",
                      color: "#242424",
                    }}
                    containerStyle={{ height: 30 }}
                    style={{
                      backgroundColor: "white",
                      borderBottomColor: "#EEEEEE",
                      borderTopWidth: 0,
                      borderBottomWidth: 1,
                      borderLeftWidth: 0,
                      borderRightWidth: 0,
                      borderLeftColor: "red",
                      borderTopLeftRadius: 0,
                      borderTopRightRadius: 0,
                      borderBottomLeftRadius: 0,
                      borderBottomRightRadius: 0,
                    }}
                    itemStyle={{
                      justifyContent: "flex-start",
                    }}
                    dropDownStyle={{ backgroundColor: "white" }}
                  />
                </View>
              </View>

              <View style={styles.resendWrapper}>
                <View style={styles.flagsectionWrapper}>
                  <Image
                    source={require("../../assets/images/Icons/flag.png")}
                    style={{ height: 20, width: 20 }}
                  />
                  <TextInput
                    editable={false}
                    style={{
                      //height: 40,
                      fontSize: 14,
                      alignItems: "center",
                      //fontFamily: "Montserrat-Regular",
                      //fontFamily:globals.FONTS.poppinsRegular,
                      color: "#7D7B7D",
                    }}
                    placeholder={"+966"}
                    placeholderTextColor="#7D7B7D"
                  />
                </View>

                <View style={styles.numbersectionWrapper}>
                  <TextInput
                    //editable={false}
                    style={styles.numberEnters}
                    placeholder={appTexts.OTP.Enter}
                    placeholder={appTexts.EDIT_PROFILE.PHONE}
                    placeholderTextColor="#242424"
                  />
                </View>
              </View>

              <View style={styles.acceptSection}>
                <CheckBox />
                <Text style={styles.setasText}>
                  {appTexts.ADDRESSDETAILS.setas}
                </Text>
              </View>
            </View>
            <View style={styles.buttonWrapper}>
              <View style={styles.cancelButton}>
                <Text style={styles.cancelText}>
                  {appTexts.ADDRESSDETAILS.Cancel}
                </Text>
              </View>
              <View style={styles.submitButton}>
                <Text style={styles.saveText}>
                  {appTexts.ADDRESSDETAILS.Save}
                </Text>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </Modal>
        <View style={styles.buttonWrapper}>
        <TouchableOpacity onPress={() => toggleModal()}>
            <CustomGreenLargeButton
              buttonText={appTexts.BILLINGDELIVERY.AddNewAddress}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

AddressView.propTypes = {
  logoutButtonPress: PropTypes.func,
};

export default AddressView;
