import React from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity, TextInput,ScrollView } from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";

const ChatView = (props) => {
  const { logoutButtonPress } = props;

  return (
    <View style={styles.screenMain}>
      <Header
        navigation={props.navigation}
        isLogoRequired={false}
        headerTitle={appTexts.CHAT.Type}
        isBackButtonRequired={true}
        // isRightButtonRequired={true}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
	  <ScrollView>
      <View style={{ flex: 1 }}>
        <View style={styles.toWrapper}>
          <View style={styles.toChat}>
            <Text style={styles.tochatText}>
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout.{" "}
            </Text>
          </View>
        </View>
        <View style={styles.fromWrapper}>
          <View style={styles.fromChat}>
            <Text style={styles.fromchatText}>
              It is a long established fact that a reader will be distracted by
              the readable content of a page when.
            </Text>
          </View>
        </View>
		<View style={styles.toWrapper}>
          <View style={styles.toChat}>
            <Text style={styles.tochatText}>
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout.{" "}
            </Text>
          </View>
        </View>
		{/* <View style={styles.fromWrapper}>
          <View style={styles.fromChat}>
            <Text style={styles.fromchatText}>
              It is a long established fact that a reader will be distracted by
              the readable content of a page when.
            </Text>
          </View>
        </View> */}
      </View>
      <View style={{ flex:1 }}>
        <View
          style={
           styles.typeMessage
          }
        >
          <TextInput
            style={styles.inputmessage}
            underlineColorAndroid="transparent"
            placeholder={appTexts.CHAT.placeholder}
            placeholderTextColor="#C9C9C9"
            autoCapitalize="none"
          />
        </View>
        <View style={styles.buttonWrappers}>
          <TouchableOpacity onPress={() => console.log("click")}>
            <View style={styles.submitButton}>
              <Text style={styles.sendbuttonText}>{appTexts.CHAT.Send}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
	  </ScrollView>
    </View>
  );
};

ChatView.propTypes = {
  logoutButtonPress: PropTypes.func,
};

export default ChatView;
