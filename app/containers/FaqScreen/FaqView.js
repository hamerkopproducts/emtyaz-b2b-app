import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";
import {
	Accordion,
} from "native-base";
import Icon from 'react-native-vector-icons/AntDesign';
import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import {
	heightPercentageToDP as hp,
	widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const dataArray = [
	{
		title: appTexts.FAQ_CONTENT.Title,
		content: appTexts.FAQ_CONTENT.Content,
	},
	{
		title: appTexts.FAQ_CONTENT.Title,
		content: appTexts.FAQ_CONTENT.Content,
	},
	{
		title: appTexts.FAQ_CONTENT.Title,
		content: appTexts.FAQ_CONTENT.Content,
	},
	{
		title: appTexts.FAQ_CONTENT.Title,
		content: appTexts.FAQ_CONTENT.Content,
	},
];



const ProfileView = (props) => {
	const {
		onBackButtonPress,

	} = props;
	const _renderHeader = (item, expanded) => {
		return (
			<View style={styles.renderHeadings}>
				<View style={{flexDirection: 'row'}}>
				<Text style={styles.rendercontentText}> {item.title}</Text>
				</View>
				{expanded ? (
					<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
					<Image
						source={
							require("../../assets/images/Faq/up-arrow.png")
						}
						style={styles.arrowIcon}
					/>
					</View>
				) : (
					<View style={{flexDirection: 'row',}}>
						<Image
							source={
								require("../../assets/images/Faq/down-arrow.png")
							}
							style={styles.arrowIcon}
						/>
						</View>
					)}
			</View>
		);
	}



	const _renderContent = (item) => {
		return (
			<View style={{flexDirection: 'row',justifyContent: 'space-between'}}>
		<Text style={styles.renderContents}>{item.content}</Text>
		</View>
		);
		
	}


	return (

		<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isLogoRequired={false}
				headerTitle={appTexts.PROFILEITEM.FAQs}
				isBackButtonRequired={true}
				onBackButtonPress={onBackButtonPress}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
			<View style={styles.formWrapper}>
				<View style={{flexDirection: 'row'}}>
					<Text style={styles.contentDescription}>{appTexts.FAQ_CONTENT.Head}</Text>
					</View>
					<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
					<Text style={styles.bodyDescritpion}>{appTexts.FAQ_CONTENT.Descritpion}
					</Text>
					
				</View>
				<View style={{ height: hp(".5%"), backgroundColor: "#f8f8f8" }}></View>
				<View style={styles.accordinWrapper}>
				<Accordion
					dataArray={dataArray}
					animation={true}
					expanded={true}
					renderHeader={_renderHeader}
					renderContent={_renderContent}
					style={{ borderWidth: 0 }}
				/>
				</View>
			</View>
		</View>


	);
};

ProfileView.propTypes = {
	logoutButtonPress: PropTypes.func,
};

export default ProfileView;
