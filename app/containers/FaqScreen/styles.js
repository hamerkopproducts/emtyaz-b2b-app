import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainer:{
    flex:1,
    marginTop:  globals.INTEGER.heightTen,
    flexDirection: 'column',
    backgroundColor: globals.COLOR.white,
    alignItems: 'center',
    justifyContent:'center'
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  formWrapper:{
    flex:1,
   paddingTop:hp('2%')
  },
  contentDescription:{
    textAlign: 'left',
    paddingLeft:'5%',
    paddingRight:'5%',
    color:'#1c1c1c',
    fontSize:13,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS. poppinsRegular,
  },
  bodyDescritpion:{
    textAlign: 'left',
    paddingLeft:'5%',
    paddingRight:'5%',
    paddingTop:hp('1%'),
    color:'#707070',
    fontSize:11,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS. poppinsRegular,
  },
  accordinWrapper:{
    backgroundColor:'white',
    alignItems:'center',
    flexDirection: 'row',
    justifyContent: 'space-between'
      },
  renderHeadings:{
    flexDirection: "row",
    
    alignItems: "center",
    borderBottomColor:'lightgray',
     borderBottomWidth:.5,
    paddingTop:'2%',
    paddingBottom:'2%',
    marginLeft:'3.3%',
    marginRight:'3.3%'
  },
  rendercontentText:{
    //fontFamily: globals.FONTS.avenirMedium,
    color:globals.COLOR.lightBlack,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS. poppinsRegular,
    width: '95%',
     fontSize:14,
     paddingTop:hp('1%'),
     paddingBottom:hp('1.1%'),
     textAlign: 'left'

  
  },
  renderContents:{
    //backgroundColor: "#e3f1f1",
            //padding: 10,
            // borderBottomColor:'red',
            // borderBottomWidth:1
            //fontStyle: "italic",
            //fontFamily: globals.FONTS.avenirMedium,
            //fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirMedium,
            paddingTop:'4%',
            paddingRight:'4%',
            paddingBottom:'4%',
            paddingLeft:'4%',
            color:globals.COLOR.lightBlack,
            fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS. poppinsRegular,
            fontSize:12,
            lineHeight:22
  },
  arrowIcon:{
    flexDirection: 'row',justifyContent: 'space-between', 
  fontSize: 18,
  transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]

  }
});

export { images, styles };