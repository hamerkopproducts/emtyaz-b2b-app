import React, { useState, useRef } from 'react';
import { View, I18nManager} from 'react-native';
import SearchModal from '../../components/SearchModal'
import PropTypes from 'prop-types';
import SearchContent from '../../components/SearchModal/SearchContent'
import globals from "../../lib/globals";
import { styles } from "./styles";

const SearchAreaView = (props) => {
	const {
        isSearchModalVisible,
        closeModal
        
	} = props;
	
	return (

		<View style={{flex:1}}>
			<SearchContent isSearchModalVisible={isSearchModalVisible} closeModal={closeModal} />
		</View>
	);
};

SearchAreaView.propTypes = {
	isSearchModalVisible: PropTypes.bool,
	closeModal: PropTypes.func,
};

export default SearchAreaView;
