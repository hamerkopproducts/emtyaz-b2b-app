import PropTypes from 'prop-types';
import React, {useState} from 'react';
import SearchAreaView from './SearchAreaView'
const SearchArea = (props) => {
    const [isSearchModalVisible, setIsSortModalVisible] = useState(true); 
    
      const closeModal = () => {
        setIsSortModalVisible(false)
      };
  return (
      <SearchAreaView 
      isSearchModalVisible={isSearchModalVisible}
      closeModal={closeModal}/>
  );
};

SearchArea.propTypes = {
 
};

export default SearchArea;
